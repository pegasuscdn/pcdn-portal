FROM php:7-apache

RUN rm -f /etc/localtime \
 && cp /usr/share/zoneinfo/Asia/Ho_Chi_Minh /etc/localtime

RUN apt-get update \
 && apt-get install -y python3 python3-pip \
 && docker-php-ext-install mysqli \
 && docker-php-ext-install pdo_mysql \
 && pecl install redis \
 && docker-php-ext-enable redis \
 && pip3 install siphash

ADD php.ini /usr/local/etc/php/php.ini
ADD source /var/www
#ADD my.cnf /etc/mysql/my.cnf
#RUN apt-get update
#RUN apt-get install -y wget unzip
#RUN wget https://codeload.github.com/jedisct1/siphash-php/zip/master -O /root/siphash-php-master.zip
#RUN cd /root && unzip siphash-php-master.zip
#RUN cd /root/siphash-php-master && phpize && ./configure --enable-siphash && make install
#RUN echo "extension=siphash.so" > /usr/local/etc/php/conf.d/siphash.ini
