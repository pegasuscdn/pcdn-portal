#!/bin/bash

git checkout dev
git pull
git checkout master
git merge dev
git push

./build_docker.sh
docker push registry.pegasus-cdn.com/pcdn-portal:latest

./run_docker.sh
