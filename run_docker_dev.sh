#!/bin/sh

container="pcdn-portal-dev"
docker pull registry.pegasus-cdn.com/pcdn-portal:latest
docker rm -f $container
docker run --name $container --hostname $container --network netcdn --ip=172.18.0.16 --add-host="hostdb:172.18.0.17" --add-host="hostredis:172.18.0.18" -e "PORTAL=dev" -v /home/dungvv/pcdn/pcdn-portal/source:/var/www --restart always -d registry.pegasus-cdn.com/pcdn-portal:latest
docker logs -f $container
