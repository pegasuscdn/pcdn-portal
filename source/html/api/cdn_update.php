<?php

require_once '../include/config.php';
require_once '../include/global.php';
require_once '../include/cdn.php';
require_once '../include/ssl.php';
require_once '../include/config_country.php';

require_once '../include/user.php';
api_check_session();
/*
if($_SESSION['portal']['expired_date']){
     $cdate=date("Y-m-d");
	if($cdate>=$_SESSION['portal']['expired_date'])
	 response_api(API_ERROR, array('msg'=>'ERROR account expired'));
}
*/
$arrUserInfo=getaccountinfo('',get_user_id());
if(empty($arrUserInfo))  response_api(API_ERROR, array('msg'=>'ERROR invalid access.'));  
if(in_array( $arrUserInfo['recordstatus'],[ RECORD_STATUS_DISABLE, RECORD_STATUS_TRIAL_EXPIRED])){
    response_api(API_ERROR, array('msg'=>'ERROR account expired or disabled'));	
}
if(  $arrUserInfo['recordstatus'] == RECORD_STATUS_TRIAL){
  if($arrUserInfo['trial_bandwidth']<=0) response_api(API_ERROR, array('msg'=>'ERROR account expired'));

  if($cdate>= $arrUserInfo['expired_date'])  response_api(API_ERROR, array('msg'=>'ERROR account expired'));

}
$hostid = trim($_POST['hostid']);
if (empty($hostid)) {
	response_api(API_ERROR, array('msg'=>'ERROR hostid not found'));
}

$resource = get_cdn_resource(get_user_id(), $hostid);
if (empty($resource)) {
	response_api(API_ERROR, array('msg'=>'ERROR hostid is invalid'));
}

if($_POST['update'] == 'purgecache'){
	$_SESSION['ss_cdn_activetab']='tab_PurgeCache';
	//$doamin= get_cdn_resource_domain(get_user_id(), $hostid);
	$_SESSION['ss_cdn_activetabData']=$_POST['path'];
	$tmp= explode("\n", $_POST['path']);
	$arrPath=[];$error=1;
	
	/*
    $regex = "((https?|ftp)://)?"; // SCHEME
    $regex .= "([a-z0-9+!*(),;?&=$_.-]+(:[a-z0-9+!*(),;?&=$_.-]+)?@)?"; // User and Pass
    $regex .= "([a-z0-9\-\.]*)\.(([a-z]{2,4})|([0-9]{1,3}\.([0-9]{1,3})\.([0-9]{1,3})))"; // Host or IP
    $regex .= "(:[0-9]{2,5})?"; // Port
   */ 
    $regex = '(/([A-Za-z0-9+$_%-*\,]\.?)+)*/?'; // Path
   // $regex .= '(\?[a-z+&\$_.-][a-z0-9;:@&%=+*/$_.-]*)?'; // GET Query
    //$regex .= '(#[a-z_.-][a-z0-9+$%_.-]*)?'; // Anchor
	$num=1;
	foreach($tmp as $x){
		$xx= trim($x);
		if($xx==="") continue;
		if(preg_match("~^$regex$~i", $xx, $m)){
			$arrPath[]=str_replace(',','%2C',$xx);$error=0; }
		else { $error=1; break;}		
		$num++;
		
	}
	if($num>20) response_api(API_ERROR, array('msg'=>'only accept 20 paths'));
	if($error)   response_api(API_ERROR, array('msg'=>'path is invalid.'));

	$userId=get_user_id();
	$api_key = get_api_key( $userId);
	if($api_key==="") response_api(API_ERROR, array('msg'=>'API KEY is not defined.'));	
	$resource = get_cdn_resource(get_user_id(), $hostid);
        $cname = select_cdn_cname_resources($resource['id']); 
	$arrDomain=[];
	$arrDomain[]=$resource['cdn_domain'];
	if (is_array($cname)) {
			foreach ($cname as $key => $value) $arrDomain[]=$value['hostname'];
	}

	$path= implode( "," , $arrPath );
	$error=0; $msg="";
	foreach( $arrDomain as $domainx){
		//$userId= 1035 ;$api_key='28146b5edae459710bd84db8429b61f5';
		$tmp = callAPIPurgeCache($userId ,$api_key,$domainx,$path);
		$retData=[];	
		$retData = json_decode( $tmp ,1);
		//$msg .=  $tmp ."|" ;
		
		if($retData['code'] != 200) { 
			$error=1; 
			$msg .= "<br>$domainx - ".  $retData['message'];// . $retData['data']. "<br>";
		}
	}
	if($error)
 		response_api(API_ERROR, array('msg'=> 'Purge Cache fail.' . $msg ));
	else
		response_api(API_SUCCESS,  array('msg' => 'Purge Cache successful'));

}
if($_POST['update'] == 'tabmobile'){
	$_SESSION['ss_cdn_activetab']='tab_MOBILE';
	$mobileURL = trim($_POST['mobile_url']);
	$cdn_platform = intval($_POST['cdn_platform']);
	if( ($cdn_platform <0) || ($cdn_platform>2)) response_api(API_ERROR, array('msg'=>'Invalid data, Data is not updated'));
	if($mobileURL==""){
		if (update_cdntab_mobileurl(get_user_id(),$hostid,$cmobileURL,$cdn_platform)) {
                        $data = array('msg' => 'Data is update successful');
                        response_api(API_SUCCESS, $data);
                } else {
                        response_api(API_ERROR, array('msg'=>'ERROR Data is not updated'));
                }
	}
	else{
		if (!filter_var($mobileURL, FILTER_VALIDATE_URL)) {
                        response_api(API_ERROR, array('msg'=>'url is invalid'));
                }
		if (update_cdntab_mobileurl(get_user_id(),$hostid,$mobileURL,$cdn_platform)) {
                        $data = array('msg' => 'Data is update successful');
                        response_api(API_SUCCESS, $data);
                } else {
                        response_api(API_ERROR, array('msg'=>'ERROR Data is not updated'));
                }
	}
}
if($_POST['update'] == 'tab3ssl'){
	$_SESSION['ss_cdn_activetab']='tab_SSL';
	/*
	custom_ssl_https_domain	doamin
custom_ssl_sslid	7
default_ssl	false
hostid	1370339731982370974
update	tab3ssl
	*/
	$default_ssl = ($_POST['default_ssl']==="true");
	$custom_ssl_https_domain = trim($_POST['custom_ssl_https_domain']);
	$custom_ssl_sslid = trim($_POST['custom_ssl_sslid']);
	if($default_ssl){
		// set ssl default

		if (update_cdntab3_ssl(get_user_id(),$hostid,'',0)) {
			$data = array('msg' => 'Data is update successful');
			response_api(API_SUCCESS, $data);
		} else {
			response_api(API_ERROR, array('msg'=>'ERROR Data is not updated'));
		}

	}
	else {
		//// set custom ssl.a
		/*
		if (empty($custom_ssl_https_domain) OR  empty($custom_ssl_sslid)) {
			response_api(API_ERROR, array('msg'=>'Fields (*) are required'));
		}*/
		if ( empty($custom_ssl_sslid)) {
                        response_api(API_ERROR, array('msg'=>'Fields (*) are required'));
                }
		/*
		if (!filter_var($custom_ssl_https_domain, FILTER_VALIDATE_DOMAIN, FILTER_FLAG_HOSTNAME) OR !preg_match('/\.[a-z0-9]+$/i', $custom_ssl_https_domain)) {
			response_api(API_ERROR, array('msg'=>'Domain is invalid'));
		}*/
		$ssl = select_ssl_resources(get_user_id(),$custom_ssl_sslid);
		if (empty($ssl)) {
			response_api(API_ERROR, array('msg'=>'ERROR SSL Add-on is invalid'));
		}
		/*
		if (checkExistedCnameSslDomain($custom_ssl_https_domain)){
			response_api(API_ERROR, array('msg'=> $custom_ssl_https_domain .' is existed.'));
		}*/
		if (update_cdntab3_ssl(get_user_id(),$hostid,$custom_ssl_https_domain,$custom_ssl_sslid)) {
			$data = array('msg' => 'Data is update successful');
			response_api(API_SUCCESS, $data);
		} else {
			response_api(API_ERROR, array('msg'=>'ERROR Data is not updated'));
		}

	}

}
elseif($_POST['update'] == 'removeResource'){
	if (update_cdn_resource_remove(get_user_id(),$hostid)) {
		$data = array('msg' => 'Resource is remove successful');
		response_api(API_SUCCESS, $data);
	} else {
		response_api(API_ERROR, array('msg'=>'ERROR Resource is not remove.'));
	}
}
elseif ($_POST['update'] == 'settings') {
	/*
	hostid	1370339731982370974
idtab1_cdn_name	test-011
idtab1_cdn_type	1

origin_normal	true
idtab1_my_origin_domain	google.com.vn
idtab1_my_origin_type	http

update	settings
	*/
		$cdn_name = trim($_POST['idtab1_cdn_name']);
		$cdn_type = intval($_POST['idtab1_cdn_type']);
		$cdn_origin = trim($_POST['origin_normal']); //Origin
		$my_origin_type = trim($_POST['idtab1_my_origin_type']);
		$my_origin_domain = trim($_POST['idtab1_my_origin_domain']);
		//$my_chacheValue = [1=>'14d',2=>'15m',3=>'0'];
		if (empty($cdn_name) OR empty($cdn_type) OR empty($cdn_origin)) {
			response_api(API_ERROR, array('msg'=>'Fields (*) are required'));
		}
		if ($cdn_type != 1 AND $cdn_type != 2 AND $cdn_type != 3) {
			response_api(API_ERROR, array('msg'=>'CDN type is invalid'));
		}

		if (1) { //$cdn_origin==="true") {

			if ($my_origin_type != 'http' AND $my_origin_type != 'https') {
				response_api(API_ERROR, array('msg'=>'Origin protocol is invalid'));
			}
			if (empty($my_origin_domain)) {
				response_api(API_ERROR, array('msg'=>'Origin domain is invalid'));

			} else {
				$array = explode(':', $my_origin_domain);
				$domain = $array[0];
				if (isset($array[1])) {
					$port = intval($array[1]);
					if ($port < 1 OR $port > 60000) {
						response_api(API_ERROR, array('msg'=>'Origin port is invalid'));
					}
				}

				if (!filter_var($domain, FILTER_VALIDATE_DOMAIN, FILTER_FLAG_HOSTNAME) OR !preg_match('/\.[a-z0-9]+$/i', $domain)) {
					response_api(API_ERROR, array('msg'=>'Origin domain is invalid'));

				}
				$cdn_https_domain= $resource['ssl_domain'];
				if ($cdn_https_domain && $cdn_https_domain == $domain){
					response_api(API_ERROR, array('msg'=>'CDN private domain must be not same origin domain'));
				}
			}
			$data = array(
				'id' => $hostid,
				'name' => $cdn_name,
				'type' => $cdn_type,
				//'cache'=> $my_chacheValue[$cdn_type],
				'origin' => $my_origin_type.'://'.$domain,
				'userid' => get_user_id(),
			);
			$idtab1_cdn_storage_bucket      = trim($_POST['idtab1_cdn_storage_bucket']);
                        $idtab1_cdn_storage_access      = strtoupper(trim($_POST['idtab1_cdn_storage_access']));
                        $idtab1_cdn_storage_secret      = trim($_POST['idtab1_cdn_storage_secret']);
		
			if ($port) {
				$data['origin'] = $data['origin'].':'.$port;
			}
			$storage=[];
                        $str_storage = '';
                        if($cdn_origin !=="true" ){
				if( (!$idtab1_cdn_storage_bucket) || (!$idtab1_cdn_storage_bucket) || (!$idtab1_cdn_storage_secret) )
					response_api(API_ERROR, array('msg'=>'Fields (*) are required'));
				
				 if(!preg_match('/^[\w-]+$/', $idtab1_cdn_storage_bucket))  response_api(API_ERROR, array('msg'=>"Bucket is invalid"));
                         	if(!preg_match('/^[0-9a-zA-Z\-]{20}$/', $idtab1_cdn_storage_access))  response_api(API_ERROR, array('msg'=>"Access Key is invalid"));
                         	if(!preg_match('/^[0-9a-zA-Z\-]{40}$/', $idtab1_cdn_storage_secret))  response_api(API_ERROR, array('msg'=>"Secret Key is invalid"));
		
                                $data['origin'] = 'https://'. $idtab1_cdn_storage_bucket .'.s3.pegasus-storage.com';
				$data['type'] =1;
	
                                $storage['bucket']=$idtab1_cdn_storage_bucket;
                                $storage['access_key']= $idtab1_cdn_storage_access;
                                $storage['secret_key']=$idtab1_cdn_storage_secret ;
                                 $str_storage = json_encode( $storage, JSON_UNESCAPED_SLASHES );
                        }
                        //print_r($data);
                        //echo cdn_get_hostid($hostname); exit;
			if (update_cdntab1_resource($data, $str_storage)) {
				$data = array('msg' => 'Data is update successful');
				response_api(API_SUCCESS, $data);
			} else {
				response_api(API_ERROR, array('msg'=>'ERROR Data is not updated'));
			}
		}
		else {
				// storage
                	$idtab1_cdn_storage_bucket 	= trim($_POST['idtab1_cdn_storage_bucket']);
			$idtab1_cdn_storage_access	= trim($_POST['idtab1_cdn_storage_access']); 
                	$idtab1_cdn_storage_secret	= trim($_POST['idtab1_cdn_storage_secret']);
	
				response_api(API_ERROR, array('msg'=>'CDN Storage not support at current.'));
		}


}

elseif ($_POST['update'] === 'acl') {
	//$_SESSION['ss_cdn_activetab']='tab_AccessControlList';
	if ($_POST['action'] == 'gen') {
		$data = array('token'=>random_string(10));
		response_api(API_SUCCESS, $data);
	}
	$_SESSION['ss_cdn_activetab']='tab_AccessControlList';
	$cdn_token = trim($_POST['cdn_token']);

	$include_client_ip=0;
	if($_POST['cdn_include_client_ip']==='true')  $include_client_ip=1;
	if ($cdn_token == 'false') {
		$acl['token'] = '';
		$include_client_ip=0;	
	} else {
		$token_string = trim($_POST['token_string']);
		if (!preg_match("/^[a-z0-9]{10}$/i", $token_string)) {
			response_api(API_ERROR, array('msg'=> 'Token string is invalid'));
		}
		$acl['token'] = $token_string;
	}

	if (update_cdn_acl_token($hostid, get_user_id(), $acl['token'],$include_client_ip)) {
		$data = array(
			'token' => $acl['token'],
			'msg' => "Data is updated successful"
		);
		response_api(API_SUCCESS, $data);
	}
} 
elseif ($_POST['update'] === 'acl2') {
	
	$_SESSION['ss_cdn_activetab']='tab_AccessControlList';
	$cdn_origin = strtolower(trim($_POST['cdn_origin']));
	if (!empty($cdn_origin)) {
		if (!filter_var($cdn_origin, FILTER_VALIDATE_URL, FILTER_FLAG_SCHEME_REQUIRED) || !preg_match('/\.[a-z0-9]+$/i', $cdn_origin)) {
			response_api(API_ERROR, array('msg'=>'ERROR HTTP origin must be a valid URL and start by http://'));
		}
	} else {
		$cdn_origin = '';
	}
	$acl['origin'] = $cdn_origin;
	if (update_cdn_acl_http_origin($hostid, get_user_id(), $acl['origin'])) {
		$data = array(
			'msg' => "Data is updated successful"
		);
		response_api(API_SUCCESS, $data);
	}
}
elseif ($_POST['update'] === 'acl3') {
	$_SESSION['ss_cdn_activetab']='tab_AccessControlList';
	if ($_POST['action'] == 'add') {
		$geoip = (trim($_POST['geoip']));
		if(!isset($countryArray[$geoip])){
			response_api(API_ERROR, array('msg'=> 'Data is invalid'));
		}

		if (update_cdn_geoip($hostid, get_user_id(), $geoip)) {
			$data = array(
				'msg' => "Data is updated successful"
			);
			response_api(API_SUCCESS, $data);
		}
	}else if ($_POST['action'] == 'delete') {
		$index = intval($_POST['index']);
		if (delete_cdn_geoip($hostid, get_user_id(), $index)) {
			$data = array('msg' => 'Data is update successful');
			response_api(API_SUCCESS, $data);
		}
	}
}
else if ($_POST['update'] === 'httpheader') {
	$_SESSION['ss_cdn_activetab']='tab_HTTPHeader';
	if ($_POST['action'] === 'delete') {
		$index = (trim($_POST['index']));
		if(delete_cdn_http_header_resource($hostid,$index)){
                                updateCDNresourcePendingStatus(get_user_id(), $hostid);
                                $data = array('msg' => 'Data is update successful');
                                response_api(API_SUCCESS, $data);
                        }
                        else{
                                response_api(API_ERROR, array('msg'=> 'Data is invalid.'));
                        }

	
	}		
	if ($_POST['action'] === 'add') {
		/*
		action	add
		domain	123.google.com
		hostid	1370339731982370974
		key	kkkk
		update	httpheader
		value	eeee
			*/
			$domain = strtolower(trim($_POST['domain']));
			$key = (trim($_POST['key']));
			$value = (trim($_POST['value']));//value
			if (empty($domain) OR  empty($key) OR  empty($value)) {
				response_api(API_ERROR, array('msg'=>'All fields are required'));
			}
			$arrCheck = get_list_http_header( $key);	
			if(!count($arrCheck)) response_api(API_ERROR, array('msg'=>'Invalid Http Header key'));
			$checkDomain=0;
			if($resource['cdn_domain'] === $domain){
					$checkDomain=1;
			}elseif($resource['ssl_domain'] === $domain){
					$checkDomain=1;
			}
			else{
					$arrCname = select_cdn_cname_resources($hostid,0,$domain);
					if(count($arrCname)){
							if($arrCname[0]['hostname'] === $domain) $checkDomain=1;
						}
			}
			if (!$checkDomain) {
				response_api(API_ERROR, array('msg'=>'Domain is invalid.'));
			}
			$data['resource_id']= $hostid;
			$data['domain_name'] = $domain;
			$data['header_name'] = $key;
			$data['header_value'] = $value;
			if(insert_cdn_http_header_resource($data)){
				$_SESSION['http_header_domain']=$domain;	
				updateCDNresourcePendingStatus(get_user_id(), $hostid);
				$data = array('msg' => 'Data is update successful');
				response_api(API_SUCCESS, $data);
			}
			else{
				response_api(API_ERROR, array('msg'=> 'Data is invalid.'));
			}
	}



}
elseif ($_POST['update'] === 'cacheExpire'){
	$_SESSION['ss_cdn_activetab']='tab_PurgeCache';
	$arrayExpire=[
				'0'=>'Depend on your origin','14d'=>'14 days','7d'=>'7 days','3d'=>'3 days',
				'24h'=>'24 hours','12h'=>'12 hours','6h'=>'6 hours','3h'=>'3 hours',
				'60m'=>'60 minutes','30m'=>'30 minutes','15m'=>'15 minutes',
				];
	/*
	expireTime	24h
	hostid	1370339731982370974
	update	cacheExpire
	*/
	$expireTime = trim(strval($_POST['expireTime']));
	if(!array_key_exists($expireTime,$arrayExpire)){
			response_api(API_ERROR, array('msg'=> 'Data is invalid'));
	}
	if (update_cdn_cache_expire_time($hostid, get_user_id(), $expireTime)) {
		$data = array(
			'msg' => "Data is updated successful"
		);
		response_api(API_SUCCESS, $data);
	}

}
else if ($_POST['update'] === 'cname') {
	$_SESSION['ss_cdn_activetab']='tab_CNAME';
	if ($_POST['action'] == 'add') {
		$cdn_cname = strtolower(trim($_POST['cdn_cname']));
		if (!filter_var($cdn_cname, FILTER_VALIDATE_DOMAIN, FILTER_FLAG_HOSTNAME) || !preg_match('/\.[a-z0-9]+$/i', $cdn_cname)) {
			response_api(API_ERROR, array('msg'=>'CNAME must be a valid domain name'));
		}
		$data['resource_id']=$hostid;
		$data['hostname']= $cdn_cname;
		$cdn_https_domain= $resource['ssl_domain'];
		if ($cdn_https_domain &&  ($cdn_https_domain === $cdn_cname)){
			response_api(API_ERROR, array('msg'=>$data['hostname'] .' is existed.'));
		}
	    	if (checkExistedCnameSslDomain($cdn_cname,1)){
			response_api(API_ERROR, array('msg'=> $cdn_cname .' is existed.'));
		}
		if(insert_cdn_cname_resource($data)){
			updateCDNresourcePendingStatus(get_user_id(), $hostid,1);
			$data = array('msg' => 'Data is update successful');
			response_api(API_SUCCESS, $data);
		}
		else{
			response_api(API_ERROR, array('msg'=> $data['hostname'] .' is existed.'));
		}
	} else if ($_POST['action'] == 'del') {
		$index = intval($_POST['index']);
		//if (delete_cdn_cname($hostid, get_user_id(), $index)) {
		if (delete_cdn_cname_resource($hostid, $index)) {
			$num = min(1,count(select_cdn_cname_resources($resource['id'])));
			updateCDNresourcePendingStatus(get_user_id(), $hostid,$num);	
			$data = array('msg' => 'CName is deleted.');
			response_api(API_SUCCESS, $data);
		}
	}
} else if ($_POST['update'] === 'referer') {
	$_SESSION['ss_cdn_activetab']='tab_AccessControlList';
	if ($_POST['action'] == 'add') {
		$cdn_referer = strtolower(trim($_POST['cdn_referer']));
		if (!filter_var($cdn_referer, FILTER_VALIDATE_DOMAIN, FILTER_FLAG_HOSTNAME) || !preg_match('/\.[a-z0-9]+$/i', $cdn_referer)) {
			response_api(API_ERROR, array('msg'=>'Domain Referer must be a valid domain name'));
		}

		if (update_cdn_referer($hostid, get_user_id(), $cdn_referer)) {
			$data = array('msg' => 'Data is update successful');
			response_api(API_SUCCESS, $data);
		}
	} else if ($_POST['action'] == 'del') {
		$index = intval($_POST['index']);
		if (delete_cdn_referer($hostid, get_user_id(), $index)) {
			$data = array('msg' => 'Data is update successful');
			response_api(API_SUCCESS, $data);
		}
	}
}

response_api(API_ERROR, array('msg'=>"ERROR Data is invalid or update error"));
