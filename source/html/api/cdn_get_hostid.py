import sys, siphash

key = "0123456789ABCDEF"

if len(sys.argv) == 1:
  print("hostname is required")
  sys.exit(1)

hostname = sys.argv[1]
print(siphash.SipHash_2_4(key.encode(), hostname.encode()).hash())
sys.exit(0)