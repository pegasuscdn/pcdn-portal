<?php

require_once '../include/config.php';
require_once '../include/global.php';
require_once '../include/cdn.php';

if ($_SERVER['REQUEST_METHOD'] != 'POST') {
	response_api(API_ERROR_UNSUPPORTED, 'request is unsupported');
}

api_check_session();

$userid = intval($_SESSION['portal']['userid']);
if (empty($userid)) {
	response_api(API_ERROR_PARAMETER, 'user id is required');
}

$data = select_cdn_resources($userid);
if (empty($data)) {
	response_api(API_ERROR_RESOURCE, 'cdn resource not found');
}

if (isset($_POST['hostid'])) {
	$hostid = trim($_POST['hostid']);
	if (!isset($data[$hostid])) {
		response_api(API_ERROR, 'host id not found');
	}
}


if (!empty($hostid)) {
	$redis_keys[] = 'bandwidth_last_month_'.$hostid;
} else {
	foreach($data as $key => $value) {
		$redis_keys[] = 'bandwidth_last_month_'.$key;
	}
}

$redis = new Redis();
if (!$redis->connect($CONFIG['redis']['host'], $CONFIG['redis']['port'], 5)) {
	exit('ERROR connect redis failed');
}
$redis->select($CONFIG['redis']['db']);
$redis->setOption(Redis::OPT_SERIALIZER, Redis::SERIALIZER_NONE);

$data = array(
	'month' => array(),
	'bandwidth' => array()
);
$result = $redis->mGet($redis_keys);
foreach ($result as $value) {
	$value = json_decode($value);
	foreach ($value as $key => $val) {
		$data['month'][$key] = date('m/Y', $val->time);
		$data['bandwidth'][$key] += round($val->bandwidth/1000);
	}
}

if ($data) {
	response_api(API_SUCCESS, $data);
} else {
	response_api(API_SUCCESS, 'data is empty');
}


