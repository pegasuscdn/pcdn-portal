<?php

require_once '../include/config.php';
require_once '../include/global.php';
require_once '../include/cdn.php';

if ($_SERVER['REQUEST_METHOD'] != 'POST') {
	response_api(API_ERROR_UNSUPPORTED, 'request is unsupported');
}

api_check_session();

$userid = intval($_SESSION['portal']['userid']);
if (empty($userid)) {
	response_api(API_ERROR_PARAMETER, 'user id is required');
}

$data = select_cdn_resources($userid);
if (empty($data)) {
	response_api(API_ERROR_RESOURCE, 'cdn resource not found');
}

$strSelectResource="";
if (!empty($_POST['hostid'])) {
	$hostid = trim($_POST['hostid']);
	if (!isset($data[$hostid])) {
		response_api(API_ERROR, 'host id not found');
	}
	$tmp = select_cdn_resource_detail($userid,$hostid);
	if(count($tmp))	{
		foreach($tmp as $k=>$v){
			$strSelectResource .=" OR `hostid`=$k";
		}
		$strSelectResource = " AND ( 0 ".$strSelectResource. ")";
	}
}


/////////////
$timeTemplate= 2;
if(isset($_REQUEST['t'])){
	$timeTemplate = intval( $_REQUEST['t']);
	if($timeTemplate!=99)
	if( $timeTemplate<1 || $timeTemplate>9) $timeTemplate= 2;
}

if($timeTemplate==99){
	$ts=trim($_REQUEST['ts']); // DD-MM-YYYY time start
	$te=trim($_REQUEST['te']);
	//// check ts ts
	$ok=check_data_input_withselecttime($ts,$te);
	                if($ok){
	                        $endTime=$startTime=$duration=$inteval=$fchart='';
	                        get_chart_Time_template_withselecttime($ts,$te,$endTime,$startTime,$duration,$inteval,$fchart);

	                }
	                else{
	                         response_api(API_ERROR_PARAMETER, 'invalid input data');
	                }
}
else{
///////////////// aihx code here
$inputDate= date("Y-m-d H:i:s",time());//"2018-12-12 11:37:40";
$arrTimeTemplate = get_chart_Time_template($inputDate);


  $endTime= $arrTimeTemplate[$timeTemplate]['EndTime'];
  $duration=$arrTimeTemplate[$timeTemplate]['long'];
  $inteval =$arrTimeTemplate[$timeTemplate]['int'];
  $fchart  = $arrTimeTemplate[$timeTemplate]['ftimekey'];
  $startTime= $endTime - $duration;
}
  //$startTime+=$inteval;  $hostid
  //$endTime+=$inteval;
$sqlwhere='';


if($userid)
	$sqlwhere .=" AND `userid`=$userid ";
if($hostid)
        $sqlwhere .= $strSelectResource ;// " AND `hostid`=$hostid ";

$sql="";
 $limit=1;
//  $startTime+=$inteval;
//  $endTime+=$inteval;
  while($startTime < $endTime){

      $chartMilestone = date($fchart,$startTime);
      $sqlTimeStart = date("Y-m-d H:i:s",$startTime);
			$year=date("Y",$startTime);
      $startTime += $inteval; if($year < 2018) continue;
      $sqlTimeEnd = date("Y-m-d H:i:s",$startTime);
	
	$yearE=date("Y",$startTime);
      //$chartMilestonea
			if($year != $yearE && 0){
					$sql .= "UNION ( SELECT  '$chartMilestone' as label
, ((SELECT sum(hits)as hits  from cdn_request_$year WHERE  `logdate`>= '".$sqlTimeStart."'  AND `logdate`<'".$sqlTimeEnd."'  $sqlwhere ) +
      			(SELECT sum(hits)as hits  from cdn_request_$yearE WHERE  `logdate`>= '".$sqlTimeStart."'  AND `logdate`<'".$sqlTimeEnd."'  $sqlwhere )
					)as hits
,((SELECT sum(misses)as misses  from cdn_request_$year WHERE  `logdate`>= '".$sqlTimeStart."'  AND `logdate`<'".$sqlTimeEnd."' $sqlwhere  ) +
                        (SELECT sum(misses)as misses  from cdn_request_$yearE WHERE  `logdate`>= '".$sqlTimeStart."'  AND `logdate`<'".$sqlTimeEnd."' $sqlwhere )
                                        )as misses
	)";
			}
			else
      $sql .= "UNION SELECT  '$chartMilestone' as label, sum(hits) as hits, sum(misses)as misses   from cdn_request_$year WHERE  `logdate`>= '".$sqlTimeStart."'  AND `logdate`<'".$sqlTimeEnd."' $sqlwhere  ";
      $limit++;
  }

$sql= substr($sql, 6); // bo? di union dau` tien
//echo $sql; exit;
$oResult = get_cdn_report_data( $sql);
$bool=0;
if($oResult){
	$bool=1;
	while ($row = mysqli_fetch_array($oResult, MYSQLI_ASSOC)) {
					$dataJson['hour'][]=$row['label'];
        				$dataJson['hits'][]= round( $row['hits']/ (max(1,($row['hits']+$row['misses'])))*100 ) ;
					$dataJson['misses'][]= round( $row['misses']/ (max(1,($row['hits']+$row['misses'])))*100  ) ;
	}
}

if ($bool) {
        response_api(API_SUCCESS, $dataJson);
}
else
response_api(API_SUCCESS, 'data is empty');

exit;



if (!empty($hostid)) {
	$redis_keys[] = 'hits_last_hour_'.$hostid;
} else {
	foreach($data as $key => $value) {
		$redis_keys[] = 'hits_last_hour_'.$key;
	}
}

$redis = new Redis();
if (!$redis->connect($CONFIG['redis']['host'], $CONFIG['redis']['port'], 5)) {
	exit('ERROR connect redis failed');
}
$redis->select($CONFIG['redis']['db']);
$redis->setOption(Redis::OPT_SERIALIZER, Redis::SERIALIZER_NONE);

$data = array(
	'hour' => array(),
	'hits' => array(),
	'misses' => array(),
);
$result = $redis->mGet($redis_keys);
foreach ($result as $value) {
	$value = json_decode($value);
	foreach ($value as $key => $val) {
		$data['hour'][$key] = date('H', $val->time).':00';
		$data['hits'][$key] += $val->hits;
		$data['misses'][$key] += $val->misses;
		$data['time'][$key] = $val->time;
	}
}

if ($result) {
	response_api(API_SUCCESS, $data);
} else {
	response_api(API_SUCCESS, 'data is empty');
}


