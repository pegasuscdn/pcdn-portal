<?php
require_once '../include/config.php';
require_once '../include/global.php';
require_once '../include/cdn.php';

if ($_SERVER['REQUEST_METHOD'] != 'POST') {
	response_api(API_ERROR_UNSUPPORTED, 'request is unsupported');
}

api_check_session();

$userid = intval($_SESSION['portal']['userid']);
if (empty($userid)) {
	response_api(API_ERROR_PARAMETER, 'user id is required');
}

$data = select_cdn_resources($userid);
if (empty($data)) {
	response_api(API_ERROR_RESOURCE, 'cdn resource not found');
}
$hostid='';
$strSelectResource ;
if (!empty($_POST['hostid'])) {
	$hostid = trim($_POST['hostid']);
	if (!isset($data[$hostid])) {
		response_api(API_ERROR, 'host id not found');
	}
	$tmp = select_cdn_resource_detail($userid,$hostid);
	if(count($tmp))	{
		foreach($tmp as $k=>$v){
			$strSelectResource .=" OR `hostid`=$k";
		}
		$strSelectResource = " AND ( 0 ".$strSelectResource. ")";
	}
}

//echo $strSelectResource; exit;
///// su dung template time 1-> 9
$timeTemplate= 2;
if(isset($_REQUEST['t'])){
	$timeTemplate = intval( $_REQUEST['t']);
	if($timeTemplate!=99)
	if( $timeTemplate<1 || $timeTemplate>9) $timeTemplate= 2;
}

if($timeTemplate==99){
	$ts=trim($_REQUEST['ts']); // DD-MM-YYYY time start
	$te=trim($_REQUEST['te']);
		$ok=check_data_input_withselecttime($ts,$te);
		if($ok){		
			$endTime=$startTime=$duration=$inteval=$fchart='';
			get_chart_Time_template_withselecttime($ts,$te,$endTime,$startTime,$duration,$inteval,$fchart);
		  		
		}
		else{
			 response_api(API_ERROR_PARAMETER, 'invalid input data');
		}
}
else{
///////////////// aihx code here
$inputDate= date("Y-m-d H:i:s",time() );//"2018-12-12 11:37:40";

$arrTimeTemplate = get_chart_Time_template($inputDate);

 
 
 
 
 //$timeTemplate= 1;
 
  $endTime= $arrTimeTemplate[$timeTemplate]['EndTime'];
  $duration=$arrTimeTemplate[$timeTemplate]['long'];
  $inteval =$arrTimeTemplate[$timeTemplate]['int']; 
  $fchart  = $arrTimeTemplate[$timeTemplate]['ftimekey'];
  $startTime= $endTime - $duration;
}
  //$startTime+=$inteval;  $hostid
  //$endTime+=$inteval;
$sqlwhere='';


if($userid)
	$sqlwhere .=" AND `userid`=$userid ";
if($hostid)
        $sqlwhere .=$strSelectResource ;//" AND `hostid`=$hostid ";

$sql="";
 $limit=1;
 // $startTime+=$inteval;
 // $endTime+=$inteval;
//echo $startTime ."\n". $endTime;
$sum2Query=[];
  while($startTime < $endTime){
      
      $chartMilestone = date($fchart,$startTime);
      $sqlTimeStart = date("Y-m-d H:i:s",$startTime);
	$year=date("Y",$startTime);
      $startTime += $inteval; if($year < 2018) continue;
      $sqlTimeEnd = date("Y-m-d H:i:s",$startTime);
	$yearE=date("Y",$startTime);
      //$chartMilestoneaa
	if(!isset($sum2Query[$year]) ) $sum2Query[$year]=array('s'=>$sqlTimeStart,'e'=>$sqlTimeEnd);
			else $sum2Query[$year]['e']=$sqlTimeEnd;
	if($year != $yearE && 0){
					$sql .= "UNION ( SELECT  '$chartMilestone' as label, (
						(SELECT sum(bandwidth)as bw  from cdn_request_$year WHERE  `logdate`>= '".$sqlTimeStart."'  AND `logdate`<'".$sqlTimeEnd."' $sqlwhere ) +
      			(SELECT sum(bandwidth)as bw  from cdn_request_$yearE WHERE  `logdate`>= '".$sqlTimeStart."'  AND `logdate`<'".$sqlTimeEnd."' $sqlwhere)
					)as bw	)";
			}
			else
      $sql .= "UNION SELECT  '$chartMilestone' as label, sum(bandwidth)as bw  from cdn_request_$year WHERE  `logdate`>= '".$sqlTimeStart."'  AND `logdate`<'".$sqlTimeEnd."' $sqlwhere  ";
      $limit++;
  }

$sql= substr($sql, 6); // bo? di union dau` tien
//$sqlxx= $sql;
//echo '----' .$sql; exit;
$oResult = get_cdn_report_data( $sql);
$bool=0;
$min=$max=$sum=$count=0;
if($oResult){
	
	while ($row = mysqli_fetch_array($oResult, MYSQLI_ASSOC)) {
		$dataJson['hour'][]=$row['label'];

		$tmpValue= round($row['bw']/1000/1000/1000);
		if(!$bool) $min=$max=$tmpValue;
		if($min > $tmpValue) $min=$tmpValue;
		if($max < $tmpValue) $max=$tmpValue;
		$sum+=$tmpValue; $count++;
 
        	$dataJson['bandwidth'][]= $tmpValue;
		$bool++;
	}
}
$avg=0; if($count) $avg = round($sum/$count,2);
$dataJson['mixmaxavg']="Total: ".number_format($sum)." - Avg: ".number_format($avg,0)." - Max: ".number_format($max,0)." - Min: ".number_format($min,0);


///////////// count domestic and international
// 0 domestic; 1  international
$strSite[0]=$strSite[1]='IN (-1';
$tmp = get_cdn_site_type(0);
foreach($tmp as $k=>$v)
	$strSite[$v] .= ','.$k;

$strSite[1] .= ')';
$strSite[0] = 'NOT '. $strSite[1];


$bwSite[0]=$bwSite[1]=0;$sql2='';
for($i=0;$i<2;$i++){
	foreach($sum2Query as $year => $vals){
			$sql="SELECT sum( bandwidth)as bw
			from cdn_request_$year
			 WHERE
			 `siteid` ".  $strSite[$i] . " 
			 AND `logdate`>= '". $vals['s'] ."'  AND `logdate`<'".$vals['e']."' $sqlwhere
		";
//echo $sql2 .="<br>".$sql; exit;
		$oResult = get_cdn_report_data( $sql);
		while ($row = mysqli_fetch_array($oResult, MYSQLI_ASSOC)) {
				$bwSite[$i] += $row['bw']; // byte
		}

	}
}
$dataJson['mixmaxavg'] .="<br>Domestic: " . number_format(round($bwSite[0]/1000/1000/1000,3),3) . " - International: ". number_format(round($bwSite[1]/1000/1000/1000,3),3);


if ($bool) {
        response_api(API_SUCCESS, $dataJson);
}
else
response_api(API_SUCCESS, 'data is empty');

exit;



$redis = new Redis();
if (!$redis->connect($CONFIG['redis']['host'], $CONFIG['redis']['port'], 5)) {
	exit('ERROR connect redis failed');
}
$redis->select($CONFIG['redis']['db']);
$redis->setOption(Redis::OPT_SERIALIZER, Redis::SERIALIZER_NONE);

$data = array(
	'hour' => array(),
	'bandwidth' => array()
);
$result = $redis->mGet($redis_keys);
foreach ($result as $value) {
	$value = json_decode($value);
	foreach ($value as $key => $val) {
		$data['hour'][$key] = date('H', $val->time).':00';
		$data['bandwidth'][$key] += $val->bandwidth;
	}
}

if ($result) {
	response_api(API_SUCCESS, $data);
} else {
	response_api(API_SUCCESS, 'data is empty');
}


