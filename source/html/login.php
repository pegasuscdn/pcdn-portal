<?php

require_once 'include/config.php';
require_once 'include/global.php';
require_once 'include/user.php';
/* XU LY LOGIN */
//echo $_SERVER['HTTP_HOST'];
$error_messages = array();
$action='';
$action = trim($_GET['action']);
$SessionExpiredTime= 30*60;//30"
if($action =='autologin'){
	$ssid= trim($_GET['ssid']);
	if( $ssid ){
	$arrUserInfo = user_login_auto($ssid);
	if($arrUserInfo) {
	
		if( ($arrUserInfo['record_status'] ==RECORD_STATUS_ACTIVE) || ($arrUserInfo['record_status'] ==RECORD_STATUS_TRIAL_EXPIRED) || ( $arrUserInfo['record_status'] ==RECORD_STATUS_TRIAL ) ){
		$_SESSION['portal']['expired_date']='';
		$_SESSION['portal']['is_admin_login']=1;
		if( $_SERVER['HTTP_HOST'] === 'portal.pegasustech.io')  $_SESSION['portal']['is_admin_login']=0;	
                $_SESSION['portal']['limited_bandwidth']= -1; $_SESSION['portal']['timeExpired']=-1;
		$_SESSION['portal']['userid'] = $arrUserInfo['id'];
		$_SESSION['portal']['fullname'] = $arrUserInfo['fullname'];
		$_SESSION['portal']['email'] = $arrUserInfo["email"];
		//trial_bandwidth
		$_SESSION['portal']['registerdate'] = $arrUserInfo['registerdate'];
		if( ($arrUserInfo['record_status'] ==RECORD_STATUS_TRIAL) || ($arrUserInfo['record_status'] ==RECORD_STATUS_TRIAL_EXPIRED) ){
                                   $_SESSION['portal']['expired_date'] = $arrUserInfo['expired_date'];
                                   $_SESSION['portal']['limited_bandwidth'] = $arrUserInfo['limited_bandwidth'];
				   $_SESSION['portal']['trial_bandwidth'] = $arrUserInfo['trial_bandwidth'];
					 $_SESSION['portal']['timeExpired']= time() + $SessionExpiredTime ;	
                                }

		redirect("/report.php");
		}
		else{
                	$_SESSION['portal']['email'] = "";
       		 }
	}
	else{
		$_SESSION['portal']['email'] = "";
	}
	}
}
if($_SESSION['portal']['email'])
	redirect("/report.php");
if ($_SERVER['REQUEST_METHOD'] == 'POST') {

	$email = filter_var($_POST['portal_email'], FILTER_VALIDATE_EMAIL);
	$password = trim($_POST['portal_pass']);

	if (!$email OR $password=='') {
		$error_messages[] = "Invalid email OR password";
	}

	if (!$error_messages) {
		$arrUserInfo = user_login($email, $password);
//var_dump( $arrUserInfo);
		//print_r( $arrUserInfo ); echo $_SERVER['HTTP_HOST'];  exit;	
		if ($arrUserInfo) {
			//$_SERVER['HTTP_HOST'];
		 $allowAccess=1;
		if($arrUserInfo["domain_register"] ) 
			if($arrUserInfo["domain_register"] !== $_SERVER['HTTP_HOST'] ) $allowAccess=0;
		// khong co' registerdomain thi phai login vao portal.pegasustech.io
		if( (!$arrUserInfo["domain_register"]) && ( $_SERVER['HTTP_HOST'] !== 'portal.pegasustech.io') ) $allowAccess=0;		

		  if($allowAccess){	
			if( ($arrUserInfo['record_status'] ==RECORD_STATUS_ACTIVE)  || ($arrUserInfo['record_status'] ==RECORD_STATUS_TRIAL_EXPIRED) || ( $arrUserInfo['record_status'] ==RECORD_STATUS_TRIAL )  ){
				$_SESSION['portal']['expired_date']='';
				$_SESSION['portal']['limited_bandwidth']= -1;
				$_SESSION['portal']['timeExpired']=-1; //trial_bandwidth
			if(strlen($arrUserInfo["ggdata"])>0)
			{
				$_SESSION['portal']['tmp_email'] = $arrUserInfo["email"];
				$_SESSION['portal']['expired_date'] = $arrUserInfo['expired_date'];
				if( ($arrUserInfo['record_status'] ==RECORD_STATUS_TRIAL) || ($arrUserInfo['record_status'] ==RECORD_STATUS_TRIAL_EXPIRED) ){
                                   $_SESSION['portal']['expired_date'] = $arrUserInfo['expired_date'];
                                   $_SESSION['portal']['limited_bandwidth'] = $arrUserInfo['limited_bandwidth'];
			        $_SESSION['portal']['trial_bandwidth'] = $arrUserInfo['trial_bandwidth'];
				$_SESSION['portal']['timeExpired']= time() + $SessionExpiredTime ;	
				$_SESSION['portal']['is_admin_login']=0;
                                }
				redirect("/login_two_step.php");
			}else{
				$_SESSION['portal']['userid'] = $arrUserInfo['id'];
				$_SESSION['portal']['fullname'] = $arrUserInfo['fullname'];
				$_SESSION['portal']['email'] = $arrUserInfo["email"];	
				$_SESSION['portal']['registerdate'] = $arrUserInfo['registerdate'];
				if( ($arrUserInfo['record_status'] ==RECORD_STATUS_TRIAL) || ($arrUserInfo['record_status'] ==RECORD_STATUS_TRIAL_EXPIRED)  ){
				   $_SESSION['portal']['expired_date'] = $arrUserInfo['expired_date'];
				   $_SESSION['portal']['limited_bandwidth'] = $arrUserInfo['limited_bandwidth'];
					$_SESSION['portal']['timeExpired']= time() + $SessionExpiredTime ;
				}
				redirect("/report.php");
			}
			}
			 else {
                        	$error_messages[] = "Your account is not actived or disabled.";
              		}
		   }  else {
                                $error_messages[] = "Invalid access.";
                        }		
		} else {
			$error_messages[] = "Invalid account";
		}
	}
}

include TEMPLATE_PATH.'/login.php';
/* END */

require_once 'include/finish.php';
