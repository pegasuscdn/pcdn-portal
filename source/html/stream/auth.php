<?php

$log ='/tmp/stream_auth.log';

function dwlog($str) {
	global $log;
	file_put_contents($log, $str."\n", FILE_APPEND);
}

$log ='/tmp/stream_auth.log';

$raw_data = file_get_contents("php://input");
dwlog($raw_data);

$data = json_decode($raw_data, true);
if ($data === NULL OR $data['url'] == '') {
	dwlog("json_decode error");
	http_response_code(403);
	echo '{"return_code":403}';
	exit;
}

$query = parse_url($data['url'], PHP_URL_QUERY);
if (empty($query)) {
	dwlog("parse_url error");
	dwlog($data['url']);
        http_response_code(403);
        echo '{"return_code":403}';
        exit;
}


parse_str($query, $vars);
if (empty($vars) OR empty($vars['pauth'])) {
	dwlog("parse_str error");
	dwlog($query);
        http_response_code(403);
        echo '{"return_code":403}';
        exit;
}

$str = base64_decode($vars['pauth'], true);
if ($str === FALSE) {
        dwlog("base64_decode error");
        dwlog($vars['pauth']);
        http_response_code(403);
        echo '{"return_code":403}';
        exit;
}

$vars = array();
parse_str($str, $vars);
if (empty($vars) || count($vars) != 3) {
	dwlog("parse_str error");
        dwlog($str);
        http_response_code(403);
        echo '{"return_code":403}';
        exit;
}

// validate sign
$PRIVATE_KEY = '123456';
$USERID = 123;
$KEY = md5($USERID.$PRIVATE_KEY);

$hash = md5($data['ip'].$data['user_agent'].$vars['t'].$vars['e'].$KEY);
if ($hash != $vars['h']) {
	dwlog("hash error");
	http_response_code(403);
        echo '{"return_code":403}';
        exit;
}

http_response_code(200);
echo '{"return_code":200}';

