<div class="content-i">
  <div class="content-box">


    <div class="element-wrapper">
      <h6 class="element-header">
        Edit SSL
      </h6>
      <?php
      if (!empty($errors)) {
        echo "<div class='alert alert-warning text-center' role='alert'>";
        foreach ($errors as $error)
          echo $error."<br />";
        echo "</div>";
      }
      ?>
      <div class="element-box">
        <form action="addon_ssl.php?action=edit&id=<?php echo $id;?>" method="POST">
          <div class="form-group row">
            <label class="col-form-label col-sm-2" for="">Name *</label>
            <div class="col-sm-5">
              <input class="form-control" placeholder="Name" type="text" name="ssl_name" value="<?php echo $ssl_name; ?>">
            </div>
          </div>

          <div class="form-group row">
            <label class="col-form-label col-sm-2" for="">Key *</label>
            <div class="col-sm-5">
              <textarea class="form-control" placeholder="Data file Key" name="ssl_key" rows="5"><?php echo $ssl_key; ?></textarea>

            </div>
          </div>
          <div class="form-group row">
            <label class="col-form-label col-sm-2" for="">Cert *</label>
            <div class="col-sm-5">
                <textarea class="form-control" placeholder="Data file cert" name="ssl_cert" rows="5"><?php echo $ssl_cert; ?></textarea>
            </div>
          </div>
          <div class="form-buttons-w">
            <button class="btn btn-primary" type="submit"> Submit</button>
            <a class="btn btn-sm btn-secondary" href="addon_ssl.php">Return List</a>
          </div>
        </form>
      </div>
    </div>


  </div>
</div>

