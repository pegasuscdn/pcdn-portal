          <div class="content-i">
            <div class="content-box">
              <div class="row">
                <div class="col-sm-12 col-xxxl-9">
                  <div class="element-wrapper">
                    <div class="element-actions">
                      <form class="form-inline justify-content-sm-end">
                        <select class="form-control form-control-sm rounded" id="bw_select_resources">
                          <option value="0">All Domains</option>
                          <?php
                          foreach ($cdn_resources as $id => $name) {
                            $selected = ($hostid == $id) ? 'selected' : '';
                            echo "<option value='{$id}' {$selected}>{$name}</option>";
                          }
                          ?>
                        </select>
                      </form>
                    </div>
                    <h6 class="element-header">
                      Report Bandwidth
                    </h6>

                    <div class="element-box">
                      <div class="os-tabs-w">
                        <div class="os-tabs-controls">
                          <ul class="nav nav-tabs smaller">
                            <li class="nav-item">
                              <a class="nav-link" data-toggle="tab" href="#">&nbsp;</a>
                            </li>
                          </ul>
                          <ul class="nav nav-pills smaller d-none d-md-flex">
                            <li class="nav-item">
                              <a class="nav-link active" data-toggle="tab" href="#">Today</a>
                            </li>
                          </ul>
                        </div>
                        <div class="tab-content">
                          <div class="tab-pane active" id="tab_overview">
                            <div class="el-tablo bigger">
                              <div class="label">
                                Bandwidth on Today (MB)
                              </div>
                            </div>
                            <div class="el-chart-w">
                              <canvas height="150px" id="bandwidthTodayChart" width="600px"></canvas>
                            </div>
                          </div>
                          <div class="tab-pane" id="tab_sales"></div>
                          <div class="tab-pane" id="tab_conversion"></div>
                        </div>
                      </div>
                    </div>

                    <div class="element-box">
                      <div class="os-tabs-w">
                        <div class="os-tabs-controls">
                          <ul class="nav nav-tabs smaller">
                            <li class="nav-item">
                              <a class="nav-link" data-toggle="tab" href="#">&nbsp;</a>
                            </li>
                          </ul>
                          <ul class="nav nav-pills smaller d-none d-md-flex">
                            <li class="nav-item">
                              <a class="nav-link active" data-toggle="tab" href="#">Last week</a>
                            </li>
                          </ul>
                        </div>
                        <div class="tab-content">
                          <div class="tab-pane active" id="tab_overview">
                            <div class="el-tablo bigger">
                              <div class="label">
                                Bandwidth in Last Week (GB)
                              </div>
                            </div>
                            <div class="el-chart-w">
                              <canvas height="150px" id="bandwidthLastWeekChart" width="600px"></canvas>
                            </div>
                          </div>
                          <div class="tab-pane" id="tab_sales"></div>
                          <div class="tab-pane" id="tab_conversion"></div>
                        </div>
                      </div>
                    </div>

                    <div class="element-box">
                      <div class="os-tabs-w">
                        <div class="os-tabs-controls">
                          <ul class="nav nav-tabs smaller">
                            <li class="nav-item">
                              <a class="nav-link" data-toggle="tab" href="#">&nbsp;</a>
                            </li>
                          </ul>
                          <ul class="nav nav-pills smaller d-none d-md-flex">
                            <li class="nav-item">
                              <a class="nav-link active" data-toggle="tab" href="#">Last month</a>
                            </li>
                          </ul>
                        </div>
                        <div class="tab-content">
                          <div class="tab-pane active" id="tab_overview">
                            <div class="el-tablo bigger">
                              <div class="label">
                                Bandwidth in Last Month (GB)
                              </div>
                            </div>
                            <div class="el-chart-w">
                              <canvas height="150px" id="bandwidthLastMonthChart" width="600px"></canvas>
                            </div>
                          </div>
                          <div class="tab-pane" id="tab_sales"></div>
                          <div class="tab-pane" id="tab_conversion"></div>
                        </div>
                      </div>
                    </div>

                    <!--div class="element-box">
                      <div class="os-tabs-w">
                        <div class="os-tabs-controls">
                          <ul class="nav nav-tabs smaller">
                            <li class="nav-item">
                              <a class="nav-link" data-toggle="tab" href="#">&nbsp;</a>
                            </li>
                          </ul>
                          <ul class="nav nav-pills smaller d-none d-md-flex">
                            <li class="nav-item">
                              <a class="nav-link active" data-toggle="tab" href="#">Last Year</a>
                            </li>
                          </ul>
                        </div>
                        <div class="tab-content">
                          <div class="tab-pane active" id="tab_overview">
                            <div class="el-tablo bigger">
                              <div class="label">
                                Bandwidth in Last Year (GB)
                              </div>
                            </div>
                            <div class="el-chart-w">
                              <canvas height="150px" id="bandwidthLastYearChart" width="600px"></canvas>
                            </div>
                          </div>
                          <div class="tab-pane" id="tab_sales"></div>
                          <div class="tab-pane" id="tab_conversion"></div>
                        </div>
                      </div>
                    </div -->

                  </div>
                </div>
              </div>
            </div>
            <!--------------------
            START - Sidebar
            -------------------->
            <div class="content-panel">
              <div class="content-panel-close">
                <i class="os-icon os-icon-close"></i>
              </div>
              <div class="element-wrapper">
                <h6 class="element-header">
                  Quick Links
                </h6>
                <div class="element-box-tp">
                  <div class="el-buttons-list full-width">
                    <a class="btn btn-white btn-sm" href="cdn_resources.php?action=add"><i class="os-icon os-icon-delivery-box-2"></i><span>Add new CDN</span></a>
                    <a class="btn btn-white btn-sm" href="#"><i class="os-icon os-icon-window-content"></i><span>Add new Storage</span></a>
                    <a class="btn btn-white btn-sm" href="logout.php"><i class="os-icon os-icon-wallet-loaded"></i><span>Logout</span></a>
                  </div>
                </div>
              </div>
            </div>
            <!--------------------
            END - Sidebar
            -------------------->
          </div>