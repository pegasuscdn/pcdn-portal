
          <!--------------------
          END - Main
          -------------------->

        </div>
      </div>
      <div class="display-type"></div>
    </div>
    <script src="<?php echo CDN_URL; ?>/templates/light/bower_components/jquery/dist/jquery.min.js"></script>
    <script src="<?php echo CDN_URL; ?>/templates/light/bower_components/popper.js/dist/umd/popper.min.js"></script>
    <script src="<?php echo CDN_URL; ?>/templates/light/bower_components/moment/moment.js"></script>
<script src="<?php echo CDN_URL; ?>/templates/light/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>	
    <script src="<?php echo CDN_URL; ?>/templates/light/bower_components/chart.js/dist/Chart.min.js"></script>
    <script src="<?php echo CDN_URL; ?>/templates/light/bower_components/select2/dist/js/select2.full.min.js"></script>
    <script src="<?php echo CDN_URL; ?>/templates/light/bower_components/dropzone/dist/dropzone.js"></script>
    <script src="<?php echo CDN_URL; ?>/templates/light/bower_components/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js"></script>
    <script src="<?php echo CDN_URL; ?>/templates/light/bower_components/bootstrap/js/dist/util.js"></script>
    <script src="<?php echo CDN_URL; ?>/templates/light/bower_components/bootstrap/js/dist/alert.js"></script>
    <script src="<?php echo CDN_URL; ?>/templates/light/bower_components/bootstrap/js/dist/button.js"></script>
    <script src="<?php echo CDN_URL; ?>/templates/light/bower_components/bootstrap/js/dist/carousel.js"></script>
    <script src="<?php echo CDN_URL; ?>/templates/light/bower_components/bootstrap/js/dist/collapse.js"></script>
    <script src="<?php echo CDN_URL; ?>/templates/light/bower_components/bootstrap/js/dist/dropdown.js"></script>
    <script src="<?php echo CDN_URL; ?>/templates/light/bower_components/bootstrap/js/dist/modal.js"></script>
    <script src="<?php echo CDN_URL; ?>/templates/light/bower_components/bootstrap/js/dist/tab.js"></script>
    <script src="<?php echo CDN_URL; ?>/templates/light/bower_components/bootstrap/js/dist/tooltip.js"></script>
    <script src="<?php echo CDN_URL; ?>/templates/light/bower_components/bootstrap/js/dist/popover.js"></script>
    <script src="<?php echo CDN_URL; ?>/templates/light/js/url.min.js"></script>
    <script src="<?php echo CDN_URL; ?>/templates/light/js/main2.js"></script>

    <?php if ($active_menu == 'report') { ?>
    <script src="https://code.highcharts.com/maps/highmaps.js"></script>
    <script src="https://code.highcharts.com/mapdata/custom/world.js"></script>
    <script src="<?php echo CDN_URL; ?>/templates/light/js/report.js?t=20200109"></script>
<script src="<?php echo CDN_URL; ?>/templates/light/js/Chart.bundle.js"></script>
<script src="<?php echo CDN_URL; ?>/templates/light/js/chartjs-plugin-zoom.min.js"></script>	
    <?php } else if ($active_menu == 'cdn') { ?>
    <script src="<?php echo CDN_URL; ?>/templates/light/js/cdn.js?t=2210"></script>
    <?php } ?>

  </body>
</html>
