<div class="content-i">
  <div class="content-box">

            <div aria-hidden="true" aria-labelledby="modalLabel" class="modal fade" id="modalid" role="dialog" tabindex="-1">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <h5 class="modal-title" id="modalLabel"></h5>
                    <button aria-label="Close" class="close" id="closeDialogid" data-dismiss="modal" type="button"><span aria-hidden="true"> &times;</span></button>
                  </div>
                  <div class="modal-body">
                      <div class="form-group text-center" id="modalMessage"></div>
                  </div>
                  <div class="modal-footer">
                    <button class="btn btn-secondary" data-dismiss="modal" type="button" id="modalClose"> Close</button>
                  </div>
                </div>
              </div>
            </div>

<div aria-hidden="true" aria-labelledby="modalLabel" class="modal fade" id="modalDeleteid" role="dialog" tabindex="-1">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <h5 class="modal-title" id="modalLabel"></h5>
                    <button aria-label="Close" class="close" id="closeDialogDeleteid" data-dismiss="modal" type="button"><span aria-hidden="true"> &times;</span></button>
                  </div>
                  <div class="modal-body">
                      <div class="form-group text-center" >Are you sure Delete this resouce?</div>
                  </div>
                  <div class="modal-footer">
                    <button class="btn btn-danger" data-dismiss="modal" type="button" id="modalCloseDeleteConfirm"> Confirm</button>
                    <button class="btn btn-secondary" data-dismiss="modal" type="button" id="modalClose"> Cancel</button>
                  </div>
                </div>
              </div>
            </div>

    <div class="element-wrapper">
      <h6 class="element-header">
        Edit CDN Resource
      </h6>
      <?php
      if (!empty($errors)) {
        echo "<div class='alert alert-warning text-center' role='alert'>";
        foreach ($errors as $error)
          echo $error."<br />";
        echo "</div>";
      } else if ($message) {
        echo "<div class='alert alert-success text-center' role='alert'>";
        echo $message;
        echo "</div>";
      }
//echo $tabActive;
      ?>
                      <div class="os-tabs-w">
                      <div class="os-tabs-controls">
                        <ul class="nav nav-tabs bigger">
                          <li class="nav-item">
                            <a class="nav-link <?php if($tabActive==='tab_overview') echo 'active'; ?> displaytabclass" data-toggle="tab" href="#tab_overview">Settings</a>
                          </li>

			                    <!--li class="nav-item">
                            <a class="nav-link <?php if( ($tabActive==='tab_CNAME')) echo 'active'; ?> displaytabclass" data-toggle="tab" href="#tab_CNAME">CNAME </a>
                          </li>
 <li class="nav-item">
                            <a class="nav-link <?php if( ($tabActive==='tab_MOBILE')) echo 'active'; ?> displaytabclass" data-toggle="tab" href="#tab_MOBILE">Redirect </a>
                          </li>

			                   <li class="nav-item">
                            <a class="nav-link <?php if($tabActive==='tab_SSL') echo 'active'; ?> displaytabclass" data-toggle="tab" href="#tab_SSL">SSL</a>
                          </li>
                          <li class="nav-item">
                            <a class="nav-link <?php if($tabActive==='tab_AccessControlList') echo 'active'; ?> displaytabclass" data-toggle="tab" href="#tab_AccessControlList">ACL </a>
                          </li>

                          <li class="nav-item">
                            <a class="nav-link <?php if($tabActive==='tab_HTTPHeader') echo 'active'; ?> displaytabclass" data-toggle="tab" href="#tab_HTTPHeader">Custom HTTP Header</a>
                          </li>
                          <li class="nav-item">
                            <a class="nav-link <?php if($tabActive==='tab_PurgeCache') echo 'active'; ?> displaytabclass" data-toggle="tab" href="#tab_PurgeCache">Cache</a>
                          </li-->

                        </ul>
                      </div>
<!-- tab1-->
                      <div class="tab-content">
      <div class="tab-pane <?php if($tabActive==='tab_overview') echo 'active'; ?>" id="tab_overview">
	<form id="form_tab1_setting"> 
     <div class="element-box">
		<h5 class="form-header">
                                  Settings
                                </h5>
                                <div class="form-desc"></div>
          <div class="form-group row">
            <label class="col-form-label col-sm-2" for=""> CDN ID</label>
            <div class="col-sm-8" id="cdn_hostid"><?php echo $resource['id']; ?></div>
          </div>
          <div class="form-group row">
            <label class="col-form-label col-sm-2" for=""> CDN Domain</label>
            <div class="col-sm-8">
              <?php echo htmlentities($resource['cdn_domain']); ?>
            </div>
          </div>
          <div class="form-group row">
            <label class="col-form-label col-sm-2" for=""> Status</label>
            <div class="col-sm-8">
              <?php
              if ($resource['recordstatus']==RECORD_STATUS_PENDING_EXEC)
                echo "Pending execution";
              else if ($resource['recordstatus']==RECORD_STATUS_PENDING_DNS)
                echo "Pending DNS";
              else if ($resource['recordstatus']==RECORD_STATUS_ACTIVE)
                echo "Active";  ?>
            </div>
          </div>

          <div class="form-group row">
            <label class="col-form-label col-sm-2" for=""> CDN Name *</label>
            <div class="col-sm-5">
              <input class="form-control" placeholder="Name" type="text" name="cdn_name" id="idtab1_cdn_name" value="<?php echo htmlentities($resource['name']); ?>">
            </div>
          </div>
<div class="form-group row" id="show_my_origin2">
            <label class="col-form-label col-sm-2" for=""> CDN Type</label>
            <div class="col-sm-5">
              <select class="form-control" name="cdn_type" id="idtab1_cdn_type">
                <option value="1" >
                  Static Content / VOD
                </option>
                <option value="2">
                  Live Stream
                </option>
                <option value="3">
                  Web Accelerator
                </option>
              </select>

            </div>
          </div>

<div class="form-group row" id="show_cdn_storage2">
            <label class="col-form-label col-sm-2" for=""> CDN Type</label>
            <div class="col-sm-5">
              <select class="form-control" name="cdn_type" id="idtab1_cdn_type">
                <option value="1" >
                  Static Content / VOD
                </option>
              </select>

            </div>
          </div>


          <?php
          $cdn_origin=1;$my_origin_type='http';$my_origin_domain='';
          if($resource['origin']){
            list($my_origin_type,$my_origin_domain)=explode ( '://' , $resource['origin'],2);
            $cdn_origin=1;
          }
	  $mystorage=[];
          if($resource['storage'] ) {
                $cdn_origin=2;
		$mystorage = json_decode($resource['storage'],1);

          }
          ?>

          <div class="form-group row">
            <label class="col-sm-2 col-form-label">Origin *</label>
            <div class="col-sm-8">
              <div class="form-check">
                <label class="form-check-label"><input class="form-check-input" name="cdn_origin" type="radio" value="1" id="origin_normal" <?php if ($cdn_origin == 1) echo "checked"; ?>>My Origin</label>
              </div>
              <div class="form-check">
                <label class="form-check-label"><input class="form-check-input" name="cdn_origin" type="radio" value="2" id="origin_storage" <?php if ($cdn_origin == 2) echo "checked"; ?>>CDN Storage</label>
              </div>
            </div>
          </div>

          <fieldset class="form-group" id="show_my_origin">
            <legend><span>My Origin</span></legend>
            <div class="form-group row">
              <label class="col-sm-2 col-form-label" for="">Your domain *</label>
              <div class="col-sm-8">
                <div class="row">
                  <div class="col-sm-2">
                    <select class="form-control" name="my_origin_type" id="idtab1_my_origin_type">
                      <option value="http" <?php if ($my_origin_type == 'http') echo "selected"; ?>>http</option>
                      <option value="https" <?php if ($my_origin_type == 'https') echo "selected"; ?>>https</option>
                    </select>
                  </div>
                  <div class="col-sm-6">
                    <input class="form-control" placeholder="yourdomain.com[:port]" type="text" name="my_origin_domain" id="idtab1_my_origin_domain" value="<?php echo htmlentities($my_origin_domain); ?>">
                  </div>
                </div>
              </div>
            </div>
          </fieldset>

          <fieldset class="form-group" id="show_cdn_storage">
            <legend><span>CDN Storage</span></legend>
 <div class="form-group row">
            <label class="col-form-label col-sm-2" for="">Access Key *</label>
            <div class="col-sm-5">
              <input class="form-control" placeholder="Access Key" type="text" name="cdn_access_key" value="<?php echo $mystorage['access_key']; ?>">
            </div>
          </div>
 <div class="form-group row">
            <label class="col-form-label col-sm-2" for="">Secret Key *</label>
            <div class="col-sm-5">
              <input class="form-control" placeholder="Secret Key" type="text" name="cdn_secret_key" value="<?php echo $mystorage['secret_key']; ?>">
            </div>
          </div>
<div class="form-group row">
            <label class="col-form-label col-sm-2" for="">Bucket *</label>
            <div class="col-sm-5">
              <input class="form-control" placeholder="Bucket" type="text" name="cdn_bucket" value="<?php echo $mystorage['bucket']; ?>">
            </div>
          </div>
            <!--div class="form-group row">
            <label class="col-form-label col-sm-2" for="">Storage *</label>
            <div class="col-sm-8">
              <select class="form-control" name="cdn_storage">
                <option value="">
                  storage-1
                </option>
                <option value="">
                  storage-2
                </option>
              </select>
              <p><a href="cdn_storages.php?action=add">Add new Storage</a></p>
            </div>
          </div-->
        </fieldset>
        <div class="row">
          <div class="col-sm-6">
            <div class="form-buttons-w">
              <button class="btn btn-primary" type="button" id="form_edit_tabsetting_submit"> Submit</button>

		 <button class="btn btn-danger" type="button" id="form_edit_tabsetting_delete"> Delete </button>
		
              <a href="cdn_resources.php" class="btn btn-sm btn-secondary"> Return List</a>
            </div>
          </div>
        </div>
      </div>
  </form>
</div>
<!-- tab2 CNAME -->
                  <div class="tab-pane <?php if($tabActive==='tab_CNAME') echo 'active'; ?>" id="tab_CNAME">

                        	<div class="element-box">
                                <h5 class="form-header">
                                  CNAME
                                </h5>
                                <div class="form-desc">
                                  To make all of this work, you'll need to create a CNAME with your DNS provider
                                </div>
                                <?php if ($resource['https'] == 0) { ?>
                                <div class="form-group">
                                  <form id="form_cname">
                                    <label> Add CNAME</label>
                                    <div class="row">
                                      <div class="col-sm-3">
                                        <input type="text" class="form-control" placeholder="cname.yourdomain.com" name="cdn_cname" id="form_cname_domain">
                                      </div>
                                      <div class="col-sm-3">
                                        <button class="btn btn-primary" type="button" id="form_cname_add"> Add</button>
					<a href="cdn_resources.php" class="btn btn-sm btn-secondary"> Return List</a>
                                      </div>
                                    </div>
                                  </form>
                                </div>
                                <?php } ?>
                                <table class="table table-striped">
                                  <thead>
                                    <tr>
                                      <th> CNAME</th>
                                      <th class="text-right">Action</th>
                                    </tr>
                                  </thead>
                                  <tbody>
                                    <?php
                                    if (is_array($cname)) {
                                      foreach ($cname as $key => $value) {
                                        echo "<tr>";
                                        echo "<td>{$value['hostname']}</td>";
                                        echo "<td class='text-right'><a class='danger cnamedelete' href='javascript:;' rev='{$value['id']}'><i class='os-icon os-icon-ui-15'></i></a></td>";
                                        echo "</tr>";
                                      }
                                    }
                                    ?>
                                  </tbody>
                                </table>
                              </div>
                  </div>
<!-- tab3 MOBILE -->
                  <div class="tab-pane <?php if($tabActive==='tab_MOBILE') echo 'active'; ?>" id="tab_MOBILE">

                                <div class="element-box">
                                <h5 class="form-header">
                                  CDN Redirect
                                </h5>
                                <div class="form-desc">
                                  Redirect visitors that are using right devices to a optimized website.
                                </div>
                                <?php if ($resource['https'] == 0) { ?>
                                <div class="form-group">
                                  <form id="form_mobile">
                                    <label>Redirect URL</label>
                                    <div class="row">
                                      <div class="col-sm-3">
                                        <select class="form-control" name="platform_redirect" id="form_platform_redirect">
                                          <option value="0">Select platform</option>
                                          <option value="1" <?php if($cdn_platform==1) echo 'selected'; ?>>Desktop</option>
                                          <option value="2" <?php if($cdn_platform==2) echo 'selected'; ?>>Mobile</option>
                                        </select>
                                      </div>
                                      <div class="col-sm-3">
                                        <input type="text" class="form-control" placeholder="url with mobile access" value="<?php echo htmlentities($mobileURL); ?>" name="mobile_redirect" id="form_mobile_redirect_url"> 
                                      </div>
                                      <div class="col-sm-3">
                                        <button class="btn btn-primary" type="button" id="form_mobile_update"> submit</button>
                                        <a href="cdn_resources.php" class="btn btn-sm btn-secondary"> Return List</a>
                                      </div>
                                    </div>
                                  </form>
                                </div>
                                <?php } ?>
                                  </tbody>
                                </table>
                              </div>
                  </div>

<!--tab3 ssl-->
                  <div class="tab-pane <?php if($tabActive==='tab_SSL') echo 'active'; ?>" id="tab_SSL">
<form id="form_tab3_ssl">
<div class="element-box">
<h5 class="form-header">
                                  SSL
                                </h5>
                                <div class="form-desc">
                                </div>

			                            <div class="form-group row">
                              <label class="col-form-label col-sm-2" for=""></label>
                              <div class="col-sm-5">
                                <div class="form-check">
                                  <label class="form-check-label"><input class="form-check-input" type="radio" name="cdn_https" value="1" id="iddefault_ssl" <?php if (empty($resource['ssl_id'])) echo "checked"; ?>>Default SSL (*.pegasus-cdn.com)</label>
                                </div>
                                <div class="form-check">
                                  <label class="form-check-label"><input class="form-check-input" type="radio" name="cdn_https" value="2" id="idcustom_ssl" <?php if ( !empty($resource['ssl_id'])) echo "checked"; ?>>User custom SSL</label>
                                </div>
                              </div>
                            </div>


				<div class="form-group row"  id="idtabssl_ssl_domain">
                              <label class="col-sm-2 col-form-label" for=""><!--SSL Domain *--></label>
                              <div class="col-sm-8">
                                <div class="row">
                                  <!--div class="col-sm-5">
                                    <input class="form-control" placeholder="Name" type="text" name="cdn_https_domain" id="idcustom_ssl_https_domain" value="<?php echo $resource['ssl_domain']; ?>">
                                  </div-->
                                  <div class="col-sm-5">
                                    <select class="form-control" name="cdn_type" id="idcustom_ssl_sslid">
					<?php
						echo '<option value="0" > select ssl addon (*)</option>';
                                          foreach( $arrAddonSSL as $tmp){
                                              if($resource['ssl_id']==$tmp['id'])
                                                echo '<option value="'.$tmp['id'].'" selected>'.htmlentities($tmp['name']).'</option>';
                                              else
                                                echo '<option value="'.$tmp['id'].'">'.htmlentities($tmp['name']).'</option>';
                                          }
                                          ?>
                                    </select> 
				<p><a href="addon_ssl.php">Add new key/cert</a></p>	
                                  </div>
                                </div>
                              </div>
                            </div>  

                            <div class="form-buttons-w">
                  		          <button class="btn btn-primary" type="button" id="form_edit_tabssl_submit"> Submit</button>
                                <a href="cdn_resources.php" class="btn btn-sm btn-secondary"> Return List</a>
                            </div>			
                  </div>
</form>
	</div>
<!-- tab4 tab_AccessControlList ------------------->
                        <div class="tab-pane <?php if($tabActive==='tab_AccessControlList') echo 'active'; ?>" id="tab_AccessControlList">



                          <div class="element-box">
                            <h5 class="form-header">
                              Token Key
                            </h5>
				 <div class="form-desc">
                        <!--    This section allows you to choose and activate various security features -->
                          </div>
                          <form id="form_acl1">
                            <div class="form-check">
                              <label class="form-check-label"><input class="form-check-input" type="checkbox" id="form_acl_token" <?php if (!empty($resource['token'])) echo "checked"; ?>>Enable TOKEN</label>
                            </div>
                            <div class="form-group">
                              <div>Token Key: <span id="token_string" style="font-weight: bold;"><?php if (!empty($resource['token'])) echo $resource['token']; else echo htmlentities("<empty>"); ?></span> <a href="javascript:;" id="token_regenerate">re-generate</a></div>
                            </div>
				
			<div class="form-group row"  id="idcustom_include_client_ip">
				<label class="col-form-label col-sm-1" for="">&nbsp;&nbsp;&nbsp;&nbsp;</label>
                              <label class="col-form-label col-sm-2" for="">Include Client IP</label>
                              <div class="col-sm-4">
                                <div class="form-check">
                                  <label class="form-check-label"><input class="form-check-input" type="radio" name="include_client_ip" value="0" id="iddefault_include_client_ip" <?php if (empty($resource['include_client_ip'])) echo "checked"; ?>>No &nbsp;&nbsp;&nbsp;&nbsp;</label>&nbsp;&nbsp;&nbsp;&nbsp;
                                  <label class="form-check-label"><input class="form-check-input" type="radio" name="include_client_ip" value="1" id="idcustom_include_client_ipyes" <?php if (!empty($resource['include_client_ip'])) echo "checked"; ?>>Yes</label>
                                </div>
                              </div>
                            </div>

                            <div class="form-buttons">
                              <button class="btn btn-primary" type="button" id="form_acl1_update"> Submit</button> <a href="cdn_resources.php" class="btn btn-sm btn-secondary"> Return List</a>
                            </div>
                          </form>
                        </div>


                        <div class="element-box">
                          <h5 class="form-header">
                            HTTP Origin
                          </h5>
				 <div class="form-desc">
                        <!--    This section allows you to choose and activate various security features -->
                          </div>
                        <form id="form_acl2">
                          <div class="form-group">
                            <label> Update HTTP Origin</label>
                            <div class="row">
                              <div class="col-sm-3">
                                <input type="text" class="form-control" placeholder="http://www.yourdomain.com" name="cdn_origin" value="<?php if (!empty($resource['http_origin'])) echo $resource['http_origin']; ?>" id="form_acl_origin">
                              </div>
                            </div>
                          </div>
                          <div class="form-buttons">
                            <button class="btn btn-primary" type="button" id="form_acl2_update"> Submit</button> <a href="cdn_resources.php" class="btn btn-sm btn-secondary"> Return List</a>
                          </div>
                        </form>
                      </div>

                      <!--div class="element-box">
                        <h5 class="form-header">
                          GEO IP
                        </h5>
                        <div class="form-group">
                          <form id="form_acl3">
                            <label> Allow Access From Country </label>
                            <div class="row">
                              <div class="col-sm-3">
                                <select class="form-control" name="cdn_geoip" id="geoip_country">
                                  <?php
                                      foreach( $countryArray as $k=>$v){
                                          echo '<option value="'.$k.'" >'.$v['name'].'</option>';
                                      }
                                      ?>
                                </select>
                              </div>
                              <div class="col-sm-2">
                                <button class="btn btn-primary" type="button" id="form_acl3_update"> Add</button>
                                <a href="cdn_resources.php" class="btn btn-sm btn-secondary"> Return List</a>
                              </div>
                            </div>
                          </form>
                        </div>
                      <table class="table table-striped">
                        <thead>
                          <tr>
                            <th>
                              Country
                            </th>
                            <th class="text-right">
                              Action
                            </th>
                          </tr>
                        </thead>
                        <tbody>
                          <?php
                          $arrgeoip = json_decode($resource['geoip']);
                          if (is_array($arrgeoip) && count($arrgeoip) ) {
                            foreach ($arrgeoip as $key => $value) {
                              echo "<tr>";
                              echo "<td>".$countryArray[$value]['name']."</td>";
                              echo "<td class='text-right'><a class='danger geoipdelete' href='javascript:;' rev='{$key}'><i class='os-icon os-icon-ui-15'></i></a></td>";
                              echo "</tr>";
                            }
                          }
  else{
                              echo '<tr><td>Allow Access From All</td></tr>';
                            
                          }
                          ?>
                        </tbody>
                      </table>
                    </div-->

                        <div class="element-box">
                          <h5 class="form-header">
                            HTTP Referer
                          </h5>
                          <div class="form-desc">
                        <!--    This section allows you to choose and activate various security features -->
                          </div>
                          <div class="form-group">
                            <form id="form_referer">
                              <label> Allow Domain</label>
                              <div class="row">
                                <div class="col-sm-3">
                                  <input type="text" class="form-control" placeholder="domain.com" name="cdn_referer" id="form_referer_domain">
                                </div>
                                <div class="col-sm-2">
                                  <button class="btn btn-primary" type="button" id="form_referer_add"> Add</button> <a href="cdn_resources.php" class="btn btn-sm btn-secondary"> Return List</a>
                                </div>
                              </div>
                            </form>
                          </div>
                          <table class="table table-striped">
                            <thead>
                              <tr>
                                <th>
                                  Domain
                                </th>
                                <th class="text-right">
                                  Action
                                </th>
                              </tr>
                            </thead>
                            <tbody>
                              <?php
                              if (is_array($referer)) {
                                foreach ($referer as $key => $value) {
                                  echo "<tr>";
                                  echo "<td>{$value}</td>";
                                  echo "<td class='text-right'><a class='danger refererdelete' href='javascript:;' rev='{$key}'><i class='os-icon os-icon-ui-15'></i></a></td>";
                                  echo "</tr>";
                                }
                              }
                              ?>
                            </tbody>
                          </table>
                        </div>


                        </div>

<!-- tab5  ------------------->
             <div class="tab-pane <?php if($tabActive==='tab_HTTPHeader') echo 'active'; ?>" id="tab_HTTPHeader">
                  <div class="element-box">
			<h5 class="form-header">
                              HTTP Header
                            </h5>
                                 <div class="form-desc">
                        <!--    This section allows you to choose and activate various security features -->
                          </div>

	                    <div class="form-group">
                      <form id="form_tab5">
                        <label> Select Domain </label>
                        <div class="row">
                          <div class="col-sm-3">
                            <select class="form-control" name="cdn_type" id="idtab5_domain">
				<option value=""> Select Domain </option>
                              <?php
				if($resource['cdn_domain']){
                                    $_selected = ($_SESSION['http_header_domain']=== $resource['cdn_domain']) ? 'selected' : '';
                                    echo '<option value="'.$resource['cdn_domain'].'" '.$_selected.'>'.$resource['cdn_domain'].'</option>';
                                  } 
				if($resource['ssl_domain']){
					$_selected = ($_SESSION['http_header_domain']=== $resource['ssl_domain']) ? 'selected' : '';
                                    echo '<option value="'.$resource['ssl_domain'].'" '.$_selected.'>'.$resource['ssl_domain'].'</option>';
                                  }
                                  foreach( $cname as $row){
					$_selected = ($_SESSION['http_header_domain']=== $row['hostname']) ? 'selected' : '';
                                      echo '<option value="'.$row['hostname'].'" '.$_selected.'>'.$row['hostname'].'</option>';
                                  }
                                  ?>
                            </select>
                          </div>
                          <div class="col-sm-3">
                            <!--input type="text" class="form-control" placeholder="http header name" name="tab5_key" id="idtab5_key"-->
				 <select class="form-control" name="cdn_type" id="idtab5_key">
					 <option value=""> Http Header Key </option>	
				<?php foreach($arrListHttpHeader as $headKey) {
					 echo '<option value="'.$headKey['name'].'">'.$headKey['name'].'</option>';
					
				}	
				?>
				</select>
                          </div>
                          <div class="col-sm-3">
                            <input type="text" class="form-control" placeholder="http header value" name="tab5_value" id="idtab5_value">
                          </div>
                          <div class="col-sm-2">
                            <button class="btn btn-primary" type="button" id="idtab5_add"> Add</button> <a href="cdn_resources.php" class="btn btn-sm btn-secondary"> Return List</a>
                          </div>
                        </div>
                      </form>
                    </div>
                    <table class="table table-striped">
                      <thead>
                        <tr>
                          <th>
                            Domain
                          </th>
                          <th>
                            Header Key
                          </th>
                          <th>
                            Header Value
                          </th>
                          <th class="text-right">
                            Action
                          </th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php
                        if (is_array($http_header)) {
                          foreach ($http_header as $key => $value) {
                            echo "<tr>";
                            echo "<td>{$value['domain_name']}</td>";
			     echo "<td>{$value['header_name']}</td>";
			 echo "<td>{$value['header_value']}</td>";	
                            echo "<td class='text-right'><a class='danger httpheaderdelete' href='javascript:;' rev='{$value['id']}'><i class='os-icon os-icon-ui-15'></i></a></td>";
                            echo "</tr>";
                          }
                        }
                        ?>
                      </tbody>
                    </table>					
                  </div>
            </div>
<!-- tab6  ------------------->
                        <div class="tab-pane <?php if($tabActive==='tab_PurgeCache') echo 'active'; ?>" id="tab_PurgeCache">

      <div class="element-box">
            <h5 class="form-header">
              Edge cache
            </h5>
            <div class="form-desc">

            </div>
            <form id="form_cache_expire">
              <div class="form-group">
                <label> Time Expire </label>
                <select class="form-control" name="cdn_type" id="idtab6_cache_expire">
                  <?php
                      //14 day, 7 day, 3 day, 24h, 12h, 6h, 3h, 60m, 30m, 15m   --- 14d, 7d,...24h...15m...
                      $arrayExpire=[
                           '0'=>'Depend on your origin', '14d'=>'14 days','7d'=>'7 days','3d'=>'3 days',
                            '24h'=>'24 hours','12h'=>'12 hours','6h'=>'6 hours','3h'=>'3 hours',
                            '60m'=>'60 minutes','30m'=>'30 minutes','15m'=>'15 minutes',
                            ];
                      foreach( $arrayExpire as $k=>$v){
                          $_selected = ($k=== $resource['cache']) ? 'selected' : '';
                          echo '<option value="'.$k.'" '.$_selected.'>'.$v.'</option>';
                      }
                      ?>
                </select>
              </div>
              <div class="form-buttons">
                <button class="btn btn-primary" type="button" id="form_cache_expire_submit"> Submit</button>
		<a href="cdn_resources.php" class="btn btn-sm btn-secondary"> Return List</a>
              </div>
            </form>
          </div>


        <div class="element-box">
        <h5 class="form-header">
          Purge Cache
        </h5>
        <div class="form-desc">
          Purge tool allows instant removal of cached content from all datacenters
        </div>
        <form id="form_purge_cache">
          <div class="form-group">
            <label> Specify paths to your individual files (not directories), one per line - max 20 lines</label>
            <textarea class="form-control" rows="5" id="purge_cache_path" placeholder="/myimages/subfolder/image.png"><?php if(isset($_SESSION['ss_cdn_activetabData'])) {echo $_SESSION['ss_cdn_activetabData']; $_SESSION['ss_cdn_activetabData']='';} ?></textarea>
          </div>
          <div class="form-buttons">
            <button class="btn btn-primary" type="button" id="form_purge_cache_submit"> Submit</button>
		<a href="cdn_resources.php" class="btn btn-sm btn-secondary"> Return List</a>
          </div>
        </form>
      </div>


      <!-- div class="element-box">
        <h5 class="form-header">
          Pre-cache
        </h5>
        <div class="form-desc">
          Pre-cache tool allows new HTTP Pull content to be pre-populated at CDN datacenters. Recommended if files are large
        </div>
        <form id="form_prefetch">
          <div class="form-group">
            <label> Specify paths to your individual files (not directories), one per line</label>
            <textarea class="form-control" rows="5" placeholder="/myimages/subfolder/image.png"></textarea>
          </div>
          <div class="form-buttons">
            <button class="btn btn-primary" type="button" id="form_prefetch_submit"> Submit</button>
		<a href="cdn_resources.php" class="btn btn-sm btn-secondary"> Return List</a>
          </div>
        </form>
      </div -->

                        </div>

                      </div>
                    </div>

  </div>
</div>

