<div class="content-i">
  <div class="content-box">


  	<div class="element-wrapper">
  <div class="element-box">
    <h5 class="form-header">
      CDN Storages
    </h5>

    <div class="controls-above-table">
      <div class="row">
        <div class="col-sm-6">
          <a class="btn btn-sm btn-primary" href="#">Add New Storage</a>
        </div>
    </div>

    <div class="table-responsive">
      <!--------------------
      START - Basic Table
      -------------------->
      <table class="table table-striped">
        <thead>
          <tr>
            <th>
              Name
            </th>
            <th>
              Usage
            </th>
            <th>
              Quota
            </th>
            <th >
              Location
            </th>
            <th class="text-center">
              Status
            </th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>
              ehost
            </td>
            <td>
              98 GB
            </td>
            <td>
              100 GB
            </td>
            <td>
              HCM - VN
            </td>
            <td class="text-center">
              <div class="status-pill green" data-title="Complete" data-toggle="tooltip"></div>
            </td>
          </tr>
          
        </tbody>
      </table>
      <!--------------------
      END - Basic Table
      -------------------->
    </div>
  </div>
</div>


  </div>
</div>