<!DOCTYPE html>
<html>
  <head>
    <title><?php echo SITE_NAME; ?></title>
    <meta charset="utf-8">
    <meta content="ie=edge" http-equiv="x-ua-compatible">
    <meta content="width=device-width, initial-scale=1" name="viewport">
    <link href="favicon.png" rel="shortcut icon">
    <link href="apple-touch-icon.png" rel="apple-touch-icon">
    <link href="https://fonts.googleapis.com/css?family=Rubik:300,400,500" rel="stylesheet" type="text/css">
    <link href="<?php echo CDN_URL; ?>/templates/light/bower_components/select2/dist/css/select2.min.css" rel="stylesheet">
    <link href="<?php echo CDN_URL; ?>/templates/light/bower_components/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
    <link href="<?php echo CDN_URL; ?>/templates/light/bower_components/dropzone/dist/dropzone.css" rel="stylesheet">
    <link href="<?php echo CDN_URL; ?>/templates/light/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo CDN_URL; ?>/templates/light/bower_components/fullcalendar/dist/fullcalendar.min.css" rel="stylesheet">
    <link href="<?php echo CDN_URL; ?>/templates/light/bower_components/perfect-scrollbar/css/perfect-scrollbar.min.css" rel="stylesheet">
    <link href="<?php echo CDN_URL; ?>/templates/light/bower_components/slick-carousel/slick/slick.css" rel="stylesheet">
    <link href="<?php echo CDN_URL; ?>/templates/light/css/main.css?version=4.4.0" rel="stylesheet">
  </head>
  <body class="auth-wrapper">
    <div class="all-wrapper menu-side with-pattern">
      <div class="auth-box-w">
        <div class="logo-w">

		<?php
//echo '<pre>';print_r($_SERVER);
//echo $_SERVER['HTTP_HOST'];
          $fileLogo='logo-viettel.jpg';
          if ( strrpos($_SERVER['HTTP_HOST'] , 'pegasustech'))
            $fileLogo='logo-250x102.png';
	  if ( strrpos($_SERVER['HTTP_HOST'] , 'cdnservice') !== false) // cdnservice.pegasus or portal.cdnservice.vn
	   $fileLogo='logo-cdnservice.jpeg';
	//$fileLogo='logo-viettel.jpg';
          ?>
          <a href="#"><img alt="" width="250px" src="<?php echo CDN_URL; ?>/templates/light/img/<?php echo $fileLogo; ?>"></a>
        </div>
        <h4 class="auth-header">
          Login Form
        </h4>
        <?php
        if (count($error_messages) > 0) {
          echo "<div class='alert alert-warning text-center' role='alert'>";
          foreach ($error_messages as $message) {
            echo $message."\n";
          }
          echo "</div>";
        }
        ?>
        <form action="login.php" method="post">

          <div class="form-group">
            <label for="">Email</label><input class="form-control" placeholder="Enter your email" type="text" name="portal_email">
            <div class="pre-icon os-icon os-icon-user-male-circle"></div>
          </div>
          <div class="form-group">
            <label for="">Password</label><input class="form-control" placeholder="Enter your password" type="password" name="portal_pass">
            <div class="pre-icon os-icon os-icon-fingerprint"></div>
          </div>
          <div class="buttons-w">
            <button class="btn btn-primary">Log me in</button>
            <div class="form-check-inline">
              <label class="form-check-label"><input class="form-check-input" type="checkbox">Remember Me</label>
            </div>
          </div>
	 <div class="form-group">
            <center><br><br> 

<a href="<?php echo SITE_URL;?>/register.php">Create new account</a>

&nbsp;|&nbsp;

<label for=""><a href="<?php echo SITE_URL;?>/forgotpassword.php">Forgot Password?</a></label>
</center>
          </div>
		      <!--div class="form-group">
            <br />
		
            <label for=""><a href="<?php echo SITE_URL;?>/forgotpassword.php">Forgot Password?</a></label>
          </div-->
        </form>
      </div>
    </div>
  </body>
</html>
