$(function () {
	var submit_error = 0;

	// Edit CDN
	var token_regenerate = $("#token_regenerate");
	if (token_regenerate.length) {
		$("#token_regenerate").click(function(){
			$.ajax({
				url: './api/cdn_update.php',
			    method: 'post',
			    data: {
			    	hostid: $("#cdn_hostid").text(),
			    	update: 'acl',
			    	action: 'gen'
			    },
			    success: function(response){
			      if (response.code == 11) {
			      	$("#token_string").html(response.data.token);
			      }
			    }
			});
		});
	}

	if ($("#token_string").text() == "<empty>") {
		$("#token_regenerate").click();
	}


var form_mobile = $("#form_mobile");
if(form_mobile.length){
		 $("#form_mobile_update").click(function(){
                        $.ajax({
                                url: './api/cdn_update.php',
                                        method: 'post',
                                        data: {
                                                cdn_platform : $("#form_platform_redirect").val(),
                                                mobile_url: $("#form_mobile_redirect_url").val(),
                                                //default_ssl: $("#iddefault_ssl:checked").is(":checked"),
                                                hostid: $("#cdn_hostid").text(),
                                                update: 'tabmobile'
                                        },
                                        success: function(response){
                                                if (response.code != 11) {
                                                        submit_error = 1;
                                                }
                                                else submit_error = 0;
                                                $("#modalLabel").html("MOBILE");
                                                $("#modalMessage").html(response.data.msg);
                                                $('#modalid').modal('show');
                                                //$(".close").focus();

                                        }
                        });
                });

	
}

var form_tab3_ssl = $("#form_tab3_ssl");

if(form_tab3_ssl.length){
		$("#form_edit_tabssl_submit").click(function(){
			$.ajax({
				url: './api/cdn_update.php',
					method: 'post',
					data: {
						//custom_ssl_https_domain: $("#idcustom_ssl_https_domain").val(),
						custom_ssl_sslid: $("#idcustom_ssl_sslid").val(),
						default_ssl: $("#iddefault_ssl:checked").is(":checked"),

						hostid: $("#cdn_hostid").text(),
						update: 'tab3ssl'
					},
					success: function(response){
						if (response.code != 11) {
							submit_error = 1;
						}
						else submit_error = 0;
						$("#modalLabel").html("SSL");
						$("#modalMessage").html(response.data.msg);
						$('#modalid').modal('show');
						//$(".close").focus();
						
					}
			});
		});
		$("#iddefault_ssl").click(function(){
			$("#idtabssl_ssl_domain").hide();
		});
		$("#idcustom_ssl").click(function(){
			$("#idtabssl_ssl_domain").show();
		});
		if ($("#idcustom_ssl").is(":checked")) {
			$("#idtabssl_ssl_domain").show();
		} else {
			$("#idtabssl_ssl_domain").hide();
		}	

}
var form_tab1_setting = $("#form_tab1_setting");
if(form_tab1_setting.length){
		$("#form_edit_tabsetting_delete").click(function(){
			 $('#modalDeleteid').modal('show');
		});
		$("#modalCloseDeleteConfirm").click(function(){
		$.ajax({
			url: './api/cdn_update.php',
				method: 'post',
				data: {
					hostid: $("#cdn_hostid").text(),
					update: 'removeResource'
				},
				success: function(response){
					if (response.code != 11) {
						submit_error = 1;
					}
					else submit_error = 9;
					$("#modalLabel").html("Settings");
					$("#modalMessage").html(response.data.msg);
					$('#modalid').modal('show');
				}
		});
	});
		$("#form_edit_tabsetting_submit").click(function(){
			$.ajax({
				url: './api/cdn_update.php',
					method: 'post',
					data: {
						idtab1_cdn_name: $("#idtab1_cdn_name").val(),
						idtab1_cdn_type: $("#idtab1_cdn_type").val(),
						origin_normal: $("#origin_normal:checked").is(":checked"),
						idtab1_my_origin_type: $("#idtab1_my_origin_type").val(),
						idtab1_my_origin_domain: $("#idtab1_my_origin_domain").val(),
						idtab1_cdn_storage: $("#idtab1_cdn_storage").val(),
				idtab1_cdn_storage_bucket: $("#idtab1_cdn_bucket").val(),
				idtab1_cdn_storage_access: $("#idtab1_cdn_access_key").val(),
				idtab1_cdn_storage_secret: $("#idtab1_cdn_secret_key").val(),
						hostid: $("#cdn_hostid").text(),
						
						update: 'settings'
					},
					success: function(response){
						if (response.code != 11) {
							submit_error = 1;
						}
						else submit_error = 0;
						$("#modalLabel").html("Settings");
						$("#modalMessage").html(response.data.msg);
						$('#modalid').modal('show');
						//$('#closeDialogid').blur();
						//console.log( $('#closeDialogid'));
						//$('#closeDialogid').focus();
						//console.log($("#closeDialogid").is(":focus"));
					}
			});
		});
}

$('#modalid').on('shown.bs.modal', function() {
  $('#modalClose').focus();
})
	var form_acl1 = $("#form_acl1");
	if (form_acl1.length) {
		$("#form_acl_token").click(function() {
				if($("#form_acl_token").is(":checked")) $("#idcustom_include_client_ip").show();
				else
				$("#idcustom_include_client_ip").hide();

			});
			if($("#form_acl_token").is(":checked")) $("#idcustom_include_client_ip").show();
			else
			$("#idcustom_include_client_ip").hide();
		$("#form_acl1_update").click(function(){
			$.ajax({
				url: './api/cdn_update.php',
			    method: 'post',
			    data: {
			    	hostid: $("#cdn_hostid").text(),
			    	update: 'acl',
			    	cdn_token: $("#form_acl_token").is(":checked"),
				cdn_include_client_ip: $("#idcustom_include_client_ipyes").is(":checked"),
			    	token_string: $("#token_string").text()
			    },
			    success: function(response){
			      if (response.code != 11) {
			      	submit_error = 1;
			      }
			      $("#modalLabel").html("Access Control List");
			      $("#modalMessage").html(response.data.msg);
			      $('#modalid').modal('show');
			    }
			});
		});
	}	
	var form_acl2 = $("#form_acl2");
	if (form_acl2.length) {
		$("#form_acl2_update").click(function(){
			$.ajax({
				url: './api/cdn_update.php',
					method: 'post',
					data: {
						hostid: $("#cdn_hostid").text(),
						update: 'acl2',
						cdn_origin: $("#form_acl_origin").val(),		
					},
					success: function(response){
						if (response.code != 11) {
							submit_error = 1;
						}
						$("#modalLabel").html("Access Control List");
						$("#modalMessage").html(response.data.msg);
						$('#modalid').modal('show');
					}
			});
		});
	}
	var form_acl3 = $("#form_acl3");
	if (form_acl3.length) {
		$("#form_acl3_update").click(function(){
			$.ajax({
				url: './api/cdn_update.php',
					method: 'post',
					data: {
						hostid: $("#cdn_hostid").text(),
						update: 'acl3',
						geoip: $("#geoip_country").val(),
						action: 'add',
					},
					success: function(response){
						if (response.code != 11) {
							submit_error = 1;
						}
						$("#modalLabel").html("Access Control List");
						$("#modalMessage").html(response.data.msg);
						$('#modalid').modal('show');
					}
			});
		});
		$(".geoipdelete").click(function(){
			$.ajax({
				url: './api/cdn_update.php',
					method: 'post',
					data: {
						update: 'acl3',
						action:'delete',
						index: $(this).prop('rev'),
						hostid: $("#cdn_hostid").text(),
					},
					success: function(response){
						if (response.code != 11) {
							submit_error = 1;
						}
						else submit_error = 0;
						$("#modalLabel").html("Access Control List");
						$("#modalMessage").html(response.data.msg);
						$('#modalid').modal('show');
					}
			});
		});
		

	}

	var form_tab5 = $("#form_tab5");
	if (form_tab5.length) {
		$("#idtab5_add").click(function(){
			$.ajax({
				url: './api/cdn_update.php',
					method: 'post',
					data: {
						update: 'httpheader',
						action:'add',
						domain: $("#idtab5_domain").val(),
						key: $("#idtab5_key").val(),
						value: $("#idtab5_value").val(),
						hostid: $("#cdn_hostid").text(),
					},
					success: function(response){
						if (response.code != 11) {
							submit_error = 1;
						}
						else submit_error = 0;
						$("#modalLabel").html("Custom HTTP Header");
						$("#modalMessage").html(response.data.msg);
						$('#modalid').modal('show');
					}
			});
		});
		$(".httpheaderdelete").click(function(){
			$.ajax({
				url: './api/cdn_update.php',
					method: 'post',
					data: {
						update: 'httpheader',
						action:'delete',
						index: $(this).prop('rev'),
						hostid: $("#cdn_hostid").text(),
					},
					success: function(response){
						if (response.code != 11) {
							submit_error = 1;
						}
						else submit_error = 0;
						$("#modalLabel").html("Custom HTTP Header");
						$("#modalMessage").html(response.data.msg);
						$('#modalid').modal('show');
					}
			});
		});
	}
	var form_cache_expire = $("#form_cache_expire");
	if (form_cache_expire) {
		$("#form_cache_expire_submit").click(function(){
			$.ajax({
				url: './api/cdn_update.php',
				method: 'post',
				data: {
					hostid: $("#cdn_hostid").text(),
					update: 'cacheExpire',
					expireTime: $("#idtab6_cache_expire").val()
				},
				success: function(response) {
				  if (response.code != 11) {
				  	submit_error = 1;
				  }
					else submit_error = 0;
				  $("#modalLabel").html("Cache");
			      $("#modalMessage").html(response.data.msg);
			      $('#modalid').modal('show');
				}
			});
		});
	}
/*
	var form_acl = $("#form_acl");
	if (form_acl.length) {
		$("#form_acl_update").click(function(){
			$.ajax({
				url: './api/cdn_update.php',
			    method: 'post',
			    data: {
			    	hostid: $("#cdn_hostid").text(),
			    	update: 'acl',
			    	cdn_token: $("#form_acl_token").is(":checked"),
			    	cdn_origin: $("#form_acl_origin").val(),
			    	token_string: $("#token_string").text()
			    },
			    success: function(response){
			      if (response.code != 11) {
			      	submit_error = 1;
			      }
			      else submit_error = 0;	
			      $("#modalLabel").html("Access Control List");
			      $("#modalMessage").html(response.data.msg);
			      $('#modalid').modal('show');
			    }
			});
		});
	}
*/
	var form_cname = $("#form_cname");
	if (form_cname.length) {
		$("#form_cname_add").click(function(){
			$.ajax({
				url: './api/cdn_update.php',
				method: 'post',
				data: {
					hostid: $("#cdn_hostid").text(),
					update: 'cname',
					action: 'add',
					cdn_cname: $("#form_cname_domain").val()
				},
				success: function(response) {
				  if (response.code != 11) {
				  	submit_error = 1;
				  }
				  else submit_error = 0;	
				  $("#modalLabel").html("CNAME");
			      $("#modalMessage").html(response.data.msg);
			      $('#modalid').modal('show');
				}
			});
		});
	}

	$(".cnamedelete").click(function(){
		$.ajax({
			url: './api/cdn_update.php',
			method: 'post',
			data: {
				hostid: $("#cdn_hostid").text(),
				update: 'cname',
				action: 'del',
				index: $(this).prop('rev')
			},
			success: function(response) {
			  if (response.code != 11) {
			    submit_error = 1;
			  }
			else submit_error = 0;	
			  $("#modalLabel").html("Delete CNAME");
		      $("#modalMessage").html(response.data.msg);
		      $('#modalid').modal('show');
			}
		});
	});

	var form_referer = $("#form_referer");
	if (form_referer.length) {
		$("#form_referer_add").click(function(){
			$.ajax({
				url: './api/cdn_update.php',
				method: 'post',
				data: {
					hostid: $("#cdn_hostid").text(),
					update: 'referer',
					action: 'add',
					cdn_referer: $("#form_referer_domain").val()
				},
				success: function(response) {
				  if (response.code != 11) {
				  	submit_error = 1;
				  }
				else submit_error = 0;	
				  $("#modalLabel").html("Access Control List");
			      $("#modalMessage").html(response.data.msg);
			      $('#modalid').modal('show');
				}
			});
		});
	}

	$(".refererdelete").click(function(){
		$.ajax({
			url: './api/cdn_update.php',
			method: 'post',
			data: {
				hostid: $("#cdn_hostid").text(),
				update: 'referer',
				action: 'del',
				index: $(this).prop('rev')
			},
			success: function(response) {
			  if (response.code != 11) {
			    submit_error = 1;
			  }else submit_error = 0;
			  $("#modalLabel").html("Access Control List");
		      $("#modalMessage").html(response.data.msg);
		      $('#modalid').modal('show');
			}
		});
	});

	var form_purge_cache = $("#form_purge_cache");
	if (form_purge_cache.length) {
		$("#form_purge_cache_submit").click(function(){
		   
			$.ajax({
                        url: './api/cdn_update.php',
                        method: 'post',
                        data: { 
                                hostid: $("#cdn_hostid").text(),
			        path: $("#purge_cache_path").val(),
                                update: 'purgecache',
                                index: $(this).prop('rev')
                        },
                        success: function(response) {
                          if (response.code != 11) {
                            submit_error = 1;
                          }else submit_error = 0;
                          $("#modalLabel").html("Purge Cache");
                      $("#modalMessage").html(response.data.msg);
                      $('#modalid').modal('show');
                        }
                });
	 
                    //submit_error = 1;
		    //$("#modalLabel").html("Purge Cache");
		    //$("#modalMessage").html("Service is closed");
		    //$('#modalid').modal('show');
		});
	}

	var form_prefetch = $("#form_prefetch");
	if (form_prefetch.length) {
		$("#form_prefetch_submit").click(function(){
			submit_error = 1;
			$("#modalLabel").html("Prefetch");
		    $("#modalMessage").html("Service is closed");
		    $('#modalid').modal('show');
		});
	}
	$("#closeDialogid").click(function(){
		if (submit_error == 0) {
			location.reload();
		} else if(submit_error == 9){
			window.location.replace("cdn_resources.php");
		}else {
			submit_error = 0;
		}
	});
	$("#modalClose").click(function(){
		if (submit_error == 0) {
			location.reload();
		} else if(submit_error == 9){
			window.location.replace("cdn_resources.php");
		}else {
			submit_error = 0;
		}
	});
	document.onkeydown = function (e) {
		  e = e || window.event;
		  switch (e.which || e.keyCode) {
		        case 13 :
									if($("#closeDialogid").is(":focus")) $('#closeDialogid').click();
		            break;
		  }
		}
	// Add new CDN

	$("#show_my_origin").show();
	 $("#show_my_origin2").show();
	$("#show_cdn_storage").hide();
       $("#show_cdn_storage2").hide();
	$("#origin_normal").click(function() {
		$("#show_my_origin").show();
		$("#show_my_origin2").show();
		$("#show_cdn_storage").hide();
		$("#show_cdn_storage2").hide();	
	});

	$("#origin_storage").click(function() {
		$("#show_my_origin").hide();
		$("#show_my_origin2").hide();
		$("#show_cdn_storage").show();
		$("#show_cdn_storage2").show();
	});
 var origin_storage  = $("#origin_storage");
	if (origin_storage.length){
        if (origin_storage.is(':checked')) {
		 $("#show_my_origin").hide();
                $("#show_my_origin2").hide();
                $("#show_cdn_storage").show();
                $("#show_cdn_storage2").show();	
	
}
}

	var cdn_https_domain = $("#cdn_https_domain");
	if (cdn_https_domain.length) {
		if (cdn_https_domain.val() != '' || $("#cdn_https_private").is(":checked")) {
			$("#show_https_domain").show();	
		} else {
			$("#show_https_domain").hide();
		}
	} else {
		$("#show_https_domain").hide();
	}
	$("#cdn_https_no").click(function(){
		$("#show_https_domain").hide();
	});
	$("#cdn_https_default").click(function(){
		$("#show_https_domain").hide();
	});
	$("#cdn_https_private").click(function(){
		$("#show_https_domain").show();
	});


});
