var realTimeCtx = $("#realTimeChart");
if (realTimeCtx.length) {
  var realTimeChartData = {
    labels: [],
    datasets: [{
      label: "Requests",
      fill: false,
      lineTension: 0.3,
      backgroundColor: "#fff",
      borderColor: "#047bf8",
      borderCapStyle: 'butt',
      borderDash: [],
      borderDashOffset: 0.0,
      borderJoinStyle: 'miter',
      pointBorderColor: "#fff",
      pointBackgroundColor: "#141E41",
      pointBorderWidth: 3,
      pointHoverRadius: 10,
      pointHoverBackgroundColor: "#FC2055",
      pointHoverBorderColor: "#fff",
      pointHoverBorderWidth: 3,
      pointRadius: 5,
      pointHitRadius: 10,
      data: [],
      spanGaps: false
    }]
  };

  var realTimeChart = new Chart(realTimeCtx, {
    type: 'line',
    data: realTimeChartData,
    options: {
      legend: {
        display: false
      },
      scales: {
        xAxes: [{
          display: true,
          ticks: {
            fontSize: '11',
            fontColor: '#969da5'
          },
          gridLines: {
            color: 'rgba(0,0,0,0.05)',
            zeroLineColor: 'rgba(0,0,0,0.05)'
          }
        }],
        yAxes: [{
          display: true,
          ticks: {
            beginAtZero: true,
            callback: function(value) {
              var ranges = [
                { divider: 1e9, suffix: 'B' },
                { divider: 1e6, suffix: 'M' },
                { divider: 1e3, suffix: 'K' }
              ];
              function formatNumber(n) {
                for (var i = 0; i < ranges.length; i++) {
                  if (n >= ranges[i].divider) {
                    return (n / ranges[i].divider).toString() + ranges[i].suffix;
                  }
                }
                if (n < 1) {
                  n = Math.round(n * 100) / 100;
                }
                return n;
              }
              return formatNumber(value);
            }
          }
        }]
      }
    }
  });
}

function updateRealTimeChart(){
  var hostid = $.url('?hostid');
  $.ajax({
    url: './api/requests_last_minute.php',
    method: 'post',
    data: {hostid: hostid},
    success: function(response){
      if (response.code == 11) {
        $("#realtime_value").text(response.data.requests[14]);
        realTimeChartData.labels = response.data.minutes;
        realTimeChartData.datasets[0].data = response.data.requests;
        realTimeChart.update();
      }
    }
  });
}


var bwTodayChartCtx = $("#bandwidthTodayChart");
if (bwTodayChartCtx.length) {
  var bwTodayChartData = {
    labels: [],
    datasets: [{
      label: "MB",
      backgroundColor: "#5797FC",
      borderColor: 'rgba(255,99,132,0)',
      borderWidth: 1,
      data: []
    }]
  };

  var bwTodayChart = new Chart(bwTodayChartCtx, {
    type: 'bar',
    data: bwTodayChartData,
    options: {
      scales: {
        xAxes: [{
          display: true,
          ticks: {
            fontSize: '11',
            fontColor: '#969da5'
          },
          gridLines: {
            color: 'rgba(0,0,0,0.05)',
            zeroLineColor: 'rgba(0,0,0,0.05)'
          }
        }],
        yAxes: [{
          ticks: {
            beginAtZero: true,
            callback: function(value) {
              var ranges = [
                { divider: 1e9, suffix: 'B' },
                { divider: 1e6, suffix: 'M' },
                { divider: 1e3, suffix: 'K' }
              ];
              function formatNumber(n) {
                for (var i = 0; i < ranges.length; i++) {
                  if (n >= ranges[i].divider) {
                    return (n / ranges[i].divider).toString() + ranges[i].suffix;
                  }
                }
                if (n < 1) {
                  n = Math.round(n * 100) / 100;
                }
                return n;
              }
              return formatNumber(value);
            }
          },
          gridLines: {
            color: 'rgba(0,0,0,0.05)',
            zeroLineColor: '#6896f9'
          }
        }]
      },
      legend: {
        display: false
      },
      animation: {
        animateScale: true
      }
    }
  });
}

function updateBWTodayChart(){
  var hostid = $.url('?hostid');
  $.ajax({
    url: './api/bandwidth_on_today.php',
    method: 'post',
    data: {hostid: hostid},
    success: function(response){
      if (response.code == 11) {
        bwTodayChartData.labels = response.data.hour;
        bwTodayChartData.datasets[0].data = response.data.bandwidth;
        bwTodayChart.update();
      }
    }
  });
}


var bwLastWeekChartCtx = $("#bandwidthLastWeekChart");
if (bwLastWeekChartCtx.length) {
  var bwLastWeekChartData = {
    labels: [],
    datasets: [{
      label: "GB",
      backgroundColor: "#5797FC",
      borderColor: 'rgba(255,99,132,0)',
      borderWidth: 1,
      data: []
    }]
  };

  var bwLastWeekChart = new Chart(bwLastWeekChartCtx, {
    type: 'bar',
    data: bwLastWeekChartData,
    options: {
      scales: {
        xAxes: [{
          display: true,
          ticks: {
            fontSize: '11',
            fontColor: '#969da5'
          },
          gridLines: {
            color: 'rgba(0,0,0,0.05)',
            zeroLineColor: 'rgba(0,0,0,0.05)'
          }
        }],
        yAxes: [{
          ticks: {
            beginAtZero: true,
            callback: function(value) {
              var ranges = [
                { divider: 1e9, suffix: 'B' },
                { divider: 1e6, suffix: 'M' },
                { divider: 1e3, suffix: 'K' }
              ];
              function formatNumber(n) {
                for (var i = 0; i < ranges.length; i++) {
                  if (n >= ranges[i].divider) {
                    return (n / ranges[i].divider).toString() + ranges[i].suffix;
                  }
                }
                if (n < 1) {
                  n = Math.round(n * 100) / 100;
                }
                return n;
              }
              return formatNumber(value);
            }
          },
          gridLines: {
            color: 'rgba(0,0,0,0.05)',
            zeroLineColor: '#6896f9'
          }
        }]
      },
      legend: {
        display: false
      },
      animation: {
        animateScale: true
      }
    }
  });
}

function updateBWLastWeekChart(){
  var hostid = $.url('?hostid');
  $.ajax({
    url: './api/bandwidth_in_week.php',
    method: 'post',
    data: {hostid: hostid},
    success: function(response){
      if (response.code == 11) {
        bwLastWeekChartData.labels = response.data.day;
        bwLastWeekChartData.datasets[0].data = response.data.bandwidth;
        bwLastWeekChart.update();
      }
    }
  });
}


var bwLastMonthChartCtx = $("#bandwidthLastMonthChart");
if (bwLastMonthChartCtx.length) {
  var bwLastMonthChartData = {
    labels: [],
    datasets: [{
      label: "GB",
      backgroundColor: "#5797FC",
      borderColor: 'rgba(255,99,132,0)',
      borderWidth: 1,
      data: []
    }]
  };

  var bwLastMonthChart = new Chart(bwLastMonthChartCtx, {
    type: 'bar',
    data: bwLastMonthChartData,
    options: {
      scales: {
        xAxes: [{
          display: true,
          ticks: {
            fontSize: '11',
            fontColor: '#969da5'
          },
          gridLines: {
            color: 'rgba(0,0,0,0.05)',
            zeroLineColor: 'rgba(0,0,0,0.05)'
          }
        }],
        yAxes: [{
          ticks: {
            beginAtZero: true,
            callback: function(value) {
              var ranges = [
                { divider: 1e9, suffix: 'B' },
                { divider: 1e6, suffix: 'M' },
                { divider: 1e3, suffix: 'K' }
              ];
              function formatNumber(n) {
                for (var i = 0; i < ranges.length; i++) {
                  if (n >= ranges[i].divider) {
                    return (n / ranges[i].divider).toString() + ranges[i].suffix;
                  }
                }
                if (n < 1) {
                  n = Math.round(n * 100) / 100;
                }
                return n;
              }
              return formatNumber(value);
            }
          },
          gridLines: {
            color: 'rgba(0,0,0,0.05)',
            zeroLineColor: '#6896f9'
          }
        }]
      },
      legend: {
        display: false
      },
      animation: {
        animateScale: true
      }
    }
  });
}

function updateBWLastMonthChart(){
  var hostid = $.url('?hostid');
  $.ajax({
    url: './api/bandwidth_in_month.php',
    method: 'post',
    data: {hostid: hostid},
    success: function(response){
      if (response.code == 11) {
        bwLastMonthChartData.labels = response.data.month;
        bwLastMonthChartData.datasets[0].data = response.data.bandwidth;
        bwLastMonthChart.update();
      }
    }
  });
}


/*
var bwLastYearChartCtx = $("#bandwidthLastYearChart");
if (bwLastYearChartCtx.length) {
  var bwLastYearChartData = {
    labels: [],
    datasets: [{
      label: "GB",
      backgroundColor: "#5797FC",
      borderColor: 'rgba(255,99,132,0)',
      borderWidth: 1,
      data: []
    }]
  };

  var bwLastYearChart = new Chart(bwLastYearChartCtx, {
    type: 'bar',
    data: bwLastYearChartData,
    options: {
      scales: {
        xAxes: [{
          display: true,
          ticks: {
            fontSize: '11',
            fontColor: '#969da5'
          },
          gridLines: {
            color: 'rgba(0,0,0,0.05)',
            zeroLineColor: 'rgba(0,0,0,0.05)'
          }
        }],
        yAxes: [{
          ticks: {
            beginAtZero: true
          },
          gridLines: {
            color: 'rgba(0,0,0,0.05)',
            zeroLineColor: '#6896f9'
          }
        }]
      },
      legend: {
        display: false
      },
      animation: {
        animateScale: true
      }
    }
  });
}

function updateBWLastYearChart(){
  var hostid = $.url('?hostid');
  $.ajax({
    url: './api/bandwidth_in_year.php',
    method: 'post',
    data: {hostid: hostid},
    success: function(response){
      if (response.code == 11) {
        bwLastYearChartData.labels = response.data.year;
        bwLastYearChartData.datasets[0].data = response.data.bandwidth;
        bwLastYearChart.update();
      }
    }
  });
}
*/


var costsByDayChartCtx = $("#costsByDayChart");
if (costsByDayChartCtx.length) {
  var costsByDayChartData = {
    labels: [],
    datasets: [{
      label: "VND",
      backgroundColor: "rgb(255, 159, 64)",
      borderColor: 'rgb(255, 159, 64)',
      borderWidth: 1,
      data: []
    }]
  };

  var costsByDayChart = new Chart(costsByDayChartCtx, {
    type: 'bar',
    data: costsByDayChartData,
    options: {
      scales: {
        xAxes: [{
          display: true,
          ticks: {
            fontSize: '11',
            fontColor: '#969da5'
          },
          gridLines: {
            color: 'rgba(0,0,0,0.05)',
            zeroLineColor: 'rgba(0,0,0,0.05)'
          }
        }],
        yAxes: [{
          ticks: {
            beginAtZero: true,
            callback: function(value) {
              var ranges = [
                { divider: 1e9, suffix: 'B' },
                { divider: 1e6, suffix: 'M' },
                { divider: 1e3, suffix: 'K' }
              ];
              function formatNumber(n) {
                for (var i = 0; i < ranges.length; i++) {
                  if (n >= ranges[i].divider) {
                    return (n / ranges[i].divider).toString() + ranges[i].suffix;
                  }
                }
                if (n < 1) {
                  n = Math.round(n * 100) / 100;
                }
                return n;
              }
              return formatNumber(value);
            }
          },
          gridLines: {
            color: 'rgba(0,0,0,0.05)',
            zeroLineColor: '#6896f9'
          }
        }]
      },
      legend: {
        display: false
      },
      animation: {
        animateScale: true
      }
    }
  });
}

function updateCostsByDayChart(){
  var hostid = $.url('?hostid');
  $.ajax({
    url: './api/costs_by_day.php',
    method: 'post',
    data: {hostid: hostid},
    success: function(response){
      if (response.code == 11) {
        costsByDayChartData.labels = response.data.day;
        costsByDayChartData.datasets[0].data = response.data.costs;
        costsByDayChart.update();
      }
    }
  });
}


var costsByMonthChartCtx = $("#costsByMonthChart");
if (costsByMonthChartCtx.length) {
  var costsByMonthChartData = {
    labels: [],
    datasets: [{
      label: "VND",
      backgroundColor: "rgb(255, 159, 64)",
      borderColor: 'rgb(255, 159, 64)',
      borderWidth: 1,
      data: []
    }]
  };

  var costsByMonthChart = new Chart(costsByMonthChartCtx, {
    type: 'bar',
    data: costsByMonthChartData,
    options: {
      scales: {
        xAxes: [{
          display: true,
          ticks: {
            fontSize: '11',
            fontColor: '#969da5'
          },
          gridLines: {
            color: 'rgba(0,0,0,0.05)',
            zeroLineColor: 'rgba(0,0,0,0.05)'
          }
        }],
        yAxes: [{
          ticks: {
            beginAtZero: true,
            callback: function(value) {
              var ranges = [
                { divider: 1e9, suffix: 'B' },
                { divider: 1e6, suffix: 'M' },
                { divider: 1e3, suffix: 'K' }
              ];
              function formatNumber(n) {
                for (var i = 0; i < ranges.length; i++) {
                  if (n >= ranges[i].divider) {
                    return (n / ranges[i].divider).toString() + ranges[i].suffix;
                  }
                }
                if (n < 1) {
                  n = Math.round(n * 100) / 100;
                }
                return n;
              }
              return formatNumber(value);
            }
          },
          gridLines: {
            color: 'rgba(0,0,0,0.05)',
            zeroLineColor: '#6896f9'
          }
        }]
      },
      legend: {
        display: false
      },
      animation: {
        animateScale: true
      }
    }
  });
}

function updateCostsByMonthChart(){
  var hostid = $.url('?hostid');
  $.ajax({
    url: './api/costs_by_month.php',
    method: 'post',
    data: {hostid: hostid},
    success: function(response){
      if (response.code == 11) {
        costsByMonthChartData.labels = response.data.month;
        costsByMonthChartData.datasets[0].data = response.data.costs;
        costsByMonthChart.update();
      }
    }
  });
}


var hitTodayChartCtx = $("#hitTodayChart");
if (hitTodayChartCtx.length) {
  var hitTodayChartData = {
    labels: [],
    datasets: [{
      label: "Hits",
      backgroundColor: "#5797FC",
      borderColor: 'rgba(255,99,132,0)',
      borderWidth: 1,
      data: []
    },
    {
      label: "Misses",
      backgroundColor: "#FC2055",
      borderColor: '#FC2055',
      borderWidth: 1,
      data: []
    }]
  };

  var hitTodayChart = new Chart(hitTodayChartCtx, {
    type: 'bar',
    data: hitTodayChartData,
    options: {
      scales: {
        xAxes: [{
          display: true,
          ticks: {
            fontSize: '11',
            fontColor: '#969da5'
          },
          gridLines: {
            color: 'rgba(0,0,0,0.05)',
            zeroLineColor: 'rgba(0,0,0,0.05)'
          }
        }],
        yAxes: [{
          ticks: {
            beginAtZero: true,
            callback: function(value) {
              var ranges = [
                { divider: 1e9, suffix: 'B' },
                { divider: 1e6, suffix: 'M' },
                { divider: 1e3, suffix: 'K' }
              ];
              function formatNumber(n) {
                for (var i = 0; i < ranges.length; i++) {
                  if (n >= ranges[i].divider) {
                    return (n / ranges[i].divider).toString() + ranges[i].suffix;
                  }
                }
                if (n < 1) {
                  n = Math.round(n * 100) / 100;
                }
                return n;
              }
              return formatNumber(value);
            }
          },
          gridLines: {
            color: 'rgba(0,0,0,0.05)',
            zeroLineColor: '#6896f9'
          }
        }]
      },
      tooltips: {
        mode: 'index',
        intersect: true
      },
      legend: {
        display: true
      },
      animation: {
        animateScale: true
      }
    }
  });
}

function updateHitTodayChart(){
  var hostid = $.url('?hostid');
  $.ajax({
    url: './api/hit_on_today.php',
    method: 'post',
    data: {hostid: hostid},
    success: function(response){
      if (response.code == 11) {
        hitTodayChartData.labels = response.data.hour;
        hitTodayChartData.datasets[0].data = response.data.hits;
        hitTodayChartData.datasets[1].data = response.data.misses;
        hitTodayChart.update();
      }
    }
  });
}



var codeTodayChartCtx = $("#codeTodayChart");
if (codeTodayChartCtx.length) {
  var codeTodayChartData = {
    labels: [],
    datasets: [{
      label: "200",
      backgroundColor: "#5797FC",
      borderColor: 'rgba(255,99,132,0)',
      borderWidth: 1,
      data: []
    },
    {
      label: "4xx",
      backgroundColor: "rgb(255, 159, 64)",
      borderColor: 'rgb(255, 159, 64)',
      borderWidth: 1,
      data: []
    },
    {
      label: "5xx",
      backgroundColor: "#FC2055",
      borderColor: '#FC2055',
      borderWidth: 1,
      data: []
    }]
  };

  var codeTodayChart = new Chart(codeTodayChartCtx, {
    type: 'bar',
    data: codeTodayChartData,
    options: {
      scales: {
        xAxes: [{
          display: true,
          stacked: true,
          ticks: {
            fontSize: '11',
            fontColor: '#969da5'
          },
          gridLines: {
            color: 'rgba(0,0,0,0.05)',
            zeroLineColor: 'rgba(0,0,0,0.05)'
          }
        }],
        yAxes: [{
          stacked: true,
          ticks: {
            beginAtZero: true,
            callback: function(value) {
              var ranges = [
                { divider: 1e9, suffix: 'B' },
                { divider: 1e6, suffix: 'M' },
                { divider: 1e3, suffix: 'K' }
              ];
              function formatNumber(n) {
                for (var i = 0; i < ranges.length; i++) {
                  if (n >= ranges[i].divider) {
                    return (n / ranges[i].divider).toString() + ranges[i].suffix;
                  }
                }
                if (n < 1) {
                  n = Math.round(n * 100) / 100;
                }
                return n;
              }
              return formatNumber(value);
            }
          },
          gridLines: {
            color: 'rgba(0,0,0,0.05)',
            zeroLineColor: '#6896f9'
          }
        }]
      },
      tooltips: {
        mode: 'index',
        intersect: false
      },
      legend: {
        display: true
      },
      animation: {
        animateScale: true
      },
      responsive: true
    }
  });
}

function updateCodeTodayChart(){
  var hostid = $.url('?hostid');
  $.ajax({
    url: './api/code_on_today.php',
    method: 'post',
    data: {hostid: hostid},
    success: function(response){
      if (response.code == 11) {
        codeTodayChartData.labels = response.data.hour;
        codeTodayChartData.datasets[0].data = response.data.code_200;
        codeTodayChartData.datasets[1].data = response.data.code_4xx;
        codeTodayChartData.datasets[2].data = response.data.code_5xx;
        codeTodayChart.update();
      }
    }
  });
}


var codeLastWeekChartCtx = $("#codeLastWeekChart");
if (codeLastWeekChartCtx.length) {
  var codeLastWeekChartData = {
    labels: [],
    datasets: [{
      label: "200",
      backgroundColor: "#5797FC",
      borderColor: 'rgba(255,99,132,0)',
      borderWidth: 1,
      data: []
    },
    {
      label: "4xx",
      backgroundColor: "rgb(255, 159, 64)",
      borderColor: 'rgb(255, 159, 64)',
      borderWidth: 1,
      data: []
    },
    {
      label: "5xx",
      backgroundColor: "#FC2055",
      borderColor: '#FC2055',
      borderWidth: 1,
      data: []
    }]
  };

  var codeLastWeekChart = new Chart(codeLastWeekChartCtx, {
    type: 'bar',
    data: codeLastWeekChartData,
    options: {
      scales: {
        xAxes: [{
          display: true,
          stacked: true,
          ticks: {
            fontSize: '11',
            fontColor: '#969da5'
          },
          gridLines: {
            color: 'rgba(0,0,0,0.05)',
            zeroLineColor: 'rgba(0,0,0,0.05)'
          }
        }],
        yAxes: [{
          stacked: true,
          ticks: {
            beginAtZero: true,
            callback: function(value) {
              var ranges = [
                { divider: 1e9, suffix: 'B' },
                { divider: 1e6, suffix: 'M' },
                { divider: 1e3, suffix: 'K' }
              ];
              function formatNumber(n) {
                for (var i = 0; i < ranges.length; i++) {
                  if (n >= ranges[i].divider) {
                    return (n / ranges[i].divider).toString() + ranges[i].suffix;
                  }
                }
                if (n < 1) {
                  n = Math.round(n * 100) / 100;
                }
                return n;
              }
              return formatNumber(value);
            }
          },
          gridLines: {
            color: 'rgba(0,0,0,0.05)',
            zeroLineColor: '#6896f9'
          }
        }]
      },
      tooltips: {
        mode: 'index',
        intersect: false
      },
      legend: {
        display: true
      },
      animation: {
        animateScale: true
      },
      responsive: true
    }
  });
}

function updateCodeLastWeekChart(){
  var hostid = $.url('?hostid');
  $.ajax({
    url: './api/code_in_week.php',
    method: 'post',
    data: {hostid: hostid},
    success: function(response){
      if (response.code == 11) {
        codeLastWeekChartData.labels = response.data.day;
        codeLastWeekChartData.datasets[0].data = response.data.code_200;
        codeLastWeekChartData.datasets[1].data = response.data.code_4xx;
        codeLastWeekChartData.datasets[2].data = response.data.code_5xx;
        codeLastWeekChart.update();
      }
    }
  });
}


var codeLastMonthChartCtx = $("#codeLastMonthChart");
if (codeLastMonthChartCtx.length) {
  var codeLastMonthChartData = {
    labels: [],
    datasets: [{
      label: "200",
      backgroundColor: "#5797FC",
      borderColor: 'rgba(255,99,132,0)',
      borderWidth: 1,
      data: []
    },
    {
      label: "4xx",
      backgroundColor: "rgb(255, 159, 64)",
      borderColor: 'rgb(255, 159, 64)',
      borderWidth: 1,
      data: []
    },
    {
      label: "5xx",
      backgroundColor: "#FC2055",
      borderColor: '#FC2055',
      borderWidth: 1,
      data: []
    }]
  };

  var codeLastMonthChart = new Chart(codeLastMonthChartCtx, {
    type: 'bar',
    data: codeLastMonthChartData,
    options: {
      scales: {
        xAxes: [{
          display: true,
          stacked: true,
          ticks: {
            fontSize: '11',
            fontColor: '#969da5'
          },
          gridLines: {
            color: 'rgba(0,0,0,0.05)',
            zeroLineColor: 'rgba(0,0,0,0.05)'
          }
        }],
        yAxes: [{
          stacked: true,
          ticks: {
            beginAtZero: true,
            callback: function(value) {
              var ranges = [
                { divider: 1e9, suffix: 'B' },
                { divider: 1e6, suffix: 'M' },
                { divider: 1e3, suffix: 'K' }
              ];
              function formatNumber(n) {
                for (var i = 0; i < ranges.length; i++) {
                  if (n >= ranges[i].divider) {
                    return (n / ranges[i].divider).toString() + ranges[i].suffix;
                  }
                }
                if (n < 1) {
                  n = Math.round(n * 100) / 100;
                }
                return n;
              }
              return formatNumber(value);
            }
          },
          gridLines: {
            color: 'rgba(0,0,0,0.05)',
            zeroLineColor: '#6896f9'
          }
        }]
      },
      tooltips: {
        mode: 'index',
        intersect: false
      },
      legend: {
        display: true
      },
      animation: {
        animateScale: true
      },
      responsive: true
    }
  });
}

function updateCodeLastMonthChart(){
  var hostid = $.url('?hostid');
  $.ajax({
    url: './api/code_in_month.php',
    method: 'post',
    data: {hostid: hostid},
    success: function(response){
      if (response.code == 11) {
        codeLastMonthChartData.labels = response.data.month;
        codeLastMonthChartData.datasets[0].data = response.data.code_200;
        codeLastMonthChartData.datasets[1].data = response.data.code_4xx;
        codeLastMonthChartData.datasets[2].data = response.data.code_5xx;
        codeLastMonthChart.update();
      }
    }
  });
}



var hitWeekChartCtx = $("#hitWeekChart");
if (hitWeekChartCtx.length) {
  var hitWeekChartData = {
    labels: [],
    datasets: [{
      label: "Hits",
      backgroundColor: "#5797FC",
      borderColor: 'rgba(255,99,132,0)',
      borderWidth: 1,
      data: []
    },
    {
      label: "Misses",
      backgroundColor: "#FC2055",
      borderColor: '#FC2055',
      borderWidth: 1,
      data: []
    }]
  };

  var hitWeekChart = new Chart(hitWeekChartCtx, {
    type: 'bar',
    data: hitWeekChartData,
    options: {
      scales: {
        xAxes: [{
          display: true,
          ticks: {
            fontSize: '11',
            fontColor: '#969da5'
          },
          gridLines: {
            color: 'rgba(0,0,0,0.05)',
            zeroLineColor: 'rgba(0,0,0,0.05)'
          }
        }],
        yAxes: [{
          ticks: {
            beginAtZero: true,
            callback: function(value) {
              var ranges = [
                { divider: 1e9, suffix: 'B' },
                { divider: 1e6, suffix: 'M' },
                { divider: 1e3, suffix: 'K' }
              ];
              function formatNumber(n) {
                for (var i = 0; i < ranges.length; i++) {
                  if (n >= ranges[i].divider) {
                    return (n / ranges[i].divider).toString() + ranges[i].suffix;
                  }
                }
                if (n < 1) {
                  n = Math.round(n * 100) / 100;
                }
                return n;
              }
              return formatNumber(value);
            }
          },
          gridLines: {
            color: 'rgba(0,0,0,0.05)',
            zeroLineColor: '#6896f9'
          }
        }]
      },
      tooltips: {
        mode: 'index',
        intersect: true
      },
      legend: {
        display: true
      },
      animation: {
        animateScale: true
      }
    }
  });
}

function updateHitWeekChart(){
  var hostid = $.url('?hostid');
  $.ajax({
    url: './api/hit_in_week.php',
    method: 'post',
    data: {hostid: hostid},
    success: function(response){
      if (response.code == 11) {
        hitWeekChartData.labels = response.data.day;
        hitWeekChartData.datasets[0].data = response.data.hits;
        hitWeekChartData.datasets[1].data = response.data.misses;
        hitWeekChart.update();
      }
    }
  });
}


var hitMonthChartCtx = $("#hitMonthChart");
if (hitMonthChartCtx.length) {
  var hitMonthChartData = {
    labels: [],
    datasets: [{
      label: "Hits",
      backgroundColor: "#5797FC",
      borderColor: 'rgba(255,99,132,0)',
      borderWidth: 1,
      data: []
    },
    {
      label: "Misses",
      backgroundColor: "#FC2055",
      borderColor: '#FC2055',
      borderWidth: 1,
      data: []
    }]
  };

  var hitMonthChart = new Chart(hitMonthChartCtx, {
    type: 'bar',
    data: hitMonthChartData,
    options: {
      scales: {
        xAxes: [{
          display: true,
          ticks: {
            fontSize: '11',
            fontColor: '#969da5'
          },
          gridLines: {
            color: 'rgba(0,0,0,0.05)',
            zeroLineColor: 'rgba(0,0,0,0.05)'
          }
        }],
        yAxes: [{
          ticks: {
            beginAtZero: true,
            callback: function(value) {
              var ranges = [
                { divider: 1e9, suffix: 'B' },
                { divider: 1e6, suffix: 'M' },
                { divider: 1e3, suffix: 'K' }
              ];
              function formatNumber(n) {
                for (var i = 0; i < ranges.length; i++) {
                  if (n >= ranges[i].divider) {
                    return (n / ranges[i].divider).toString() + ranges[i].suffix;
                  }
                }
                if (n < 1) {
                  n = Math.round(n * 100) / 100;
                }
                return n;
              }
              return formatNumber(value);
            }
          },
          gridLines: {
            color: 'rgba(0,0,0,0.05)',
            zeroLineColor: '#6896f9'
          }
        }]
      },
      tooltips: {
        mode: 'index',
        intersect: true
      },
      legend: {
        display: true
      },
      animation: {
        animateScale: true
      }
    }
  });
}

function updateHitMonthChart(){
  var hostid = $.url('?hostid');
  $.ajax({
    url: './api/hit_in_month.php',
    method: 'post',
    data: {hostid: hostid},
    success: function(response){
      if (response.code == 11) {
        hitMonthChartData.labels = response.data.month;
        hitMonthChartData.datasets[0].data = response.data.hits;
        hitMonthChartData.datasets[1].data = response.data.misses;
        hitMonthChart.update();
      }
    }
  });
}

var worldmapChart;

function loadWorldmap() {
  var hostid = $.url('?hostid');
  $.ajax({
    url: './api/worldmap.php',
    method: 'post',
    data: {hostid: hostid},
    success: function(response){
      if (response.code == 11) {
        worldmapChart.series[0].update({data: response.data});
      }
    }
  });
}

// GEOIP world map
var worldmap = $("#worldmap");
if (worldmap.length) {
  worldmapChart = Highcharts.mapChart('worldmap', {
    chart: {
        map: 'custom/world',
        borderWidth: 1,
        events: {
          load: loadWorldmap
        }
    },
    title: {
        text: 'Visitors Country'
    },

    subtitle: {
        text: 'Vistors are from highlight Country'
    },

    legend: {
        enabled: false
    },

    credits: {
      enabled: false
    },

    series: [{
        name: 'Country',
        data: [],
        dataLabels: {
            enabled: true,
            color: '#FFFFFF',
            formatter: function () {
                if (this.point.value) {
                    return this.point.name;
                }
            }
        },
        tooltip: {
            headerFormat: '',
            pointFormat: '{point.name}'
        }
    }]
  });
}

$(function () {

  if (realTimeCtx.length) {
    $("#overview_select_resources").change(function(){
      if (this.value == '0') {
        window.location = "/report.php";
      } else {
        window.location = "/report.php?hostid="+ this.value;
      }
    });
    updateRealTimeChart();
    setInterval(updateRealTimeChart, 60000);
  }

  if (bwTodayChartCtx.length) {
    $("#bw_select_resources").change(function(){
      if (this.value == '0') {
        window.location = "/report.php?type=bw";
      } else {
        window.location = "/report.php?type=bw&hostid="+ this.value;
      }
    });
    updateBWTodayChart();
    setInterval(updateBWTodayChart, 3600000);
  }
  if (bwLastWeekChartCtx.length) {
    updateBWLastWeekChart();
  }
  if (bwLastMonthChartCtx.length) {
    updateBWLastMonthChart();
  }

  /*if (bwLastYearChartCtx.length) {
    $("#bw_select_resources").change(function(){
      if (this.value == '0') {
        window.location = "/report.php?type=bw";
      } else {
        window.location = "/report.php?type=bw&hostid="+ this.value;
      }
    });
    updateBWLastYearChart();
  }*/

  if (costsByDayChartCtx.length) {
    $("#costs_select_resources").change(function(){
      if (this.value == '0') {
        window.location = "/report.php?type=costs";
      } else {
        window.location = "/report.php?type=costs&hostid="+ this.value;
      }
    });
    updateCostsByDayChart();
  }
  if (costsByMonthChartCtx.length) {
    updateCostsByMonthChart();
  }

  if (hitTodayChartCtx.length) {
    $("#hit_select_resources").change(function(){
      if (this.value == '0') {
        window.location = "/report.php?type=hit";
      } else {
        window.location = "/report.php?type=hit&hostid="+ this.value;
      }
    });
    updateHitTodayChart();
  }
  if (hitWeekChartCtx.length) {
    updateHitWeekChart();
  }
  if (hitMonthChartCtx.length) {
    updateHitMonthChart();
  }


  if (codeTodayChartCtx.length) {
    $("#code_select_resources").change(function(){
      if (this.value == '0') {
        window.location = "/report.php?type=code";
      } else {
        window.location = "/report.php?type=code&hostid="+ this.value;
      }
    });
    updateCodeTodayChart();
  }
  if (codeLastWeekChartCtx.length) {
    updateCodeLastWeekChart();
  }
  if (codeLastMonthChartCtx.length) {
    updateCodeLastMonthChart();
  }

  // download csv
  $("#report_costs_download").click(function(){
    var date = $("#report_costs_month").val();
    if (date.length) {
      window.open("/download_bw_data.php?date="+date, "_blank");
    }
  });

});

