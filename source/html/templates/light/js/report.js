var t=2;
var hostid ='';
$('#startDate').daterangepicker({
          singleDatePicker: true,
          startDate: moment().subtract(7, 'days'),
	maxDate: moment(),
        minDate: "01/01/2018",
	showDropdowns: true,
	locale: {
            format: 'DD-MM-YYYY'
        }
        });

        $('#endDate').daterangepicker({
          singleDatePicker: true,
          startDate: moment(),
	  maxDate: moment(),
	minDate: "01/01/2018",
	showDropdowns: true,
	locale: {
            format: 'DD-MM-YYYY'
        }
        });
 //var ts = moment();
 //var te = moment().subtract(7, 'days');
var ts = ( $("#startDate").val() );
var te = ( $("#endDate").val() );
var realTimeCtx = $("#realTimeChart");
if (realTimeCtx.length) {
  var realTimeChartData = {
    labels: [],
    datasets: [{
      label: "Requests",
      fill: false,
      lineTension: 0.3,
      backgroundColor: "#fff",
      borderColor: "#047bf8",
      borderCapStyle: 'butt',
      borderDash: [],
      borderDashOffset: 0.0,
      borderJoinStyle: 'miter',
      pointBorderColor: "#fff",
      pointBackgroundColor: "#141E41",
      pointBorderWidth: 3,
      pointHoverRadius: 10,
      pointHoverBackgroundColor: "#047bf8",
      pointHoverBorderColor: "#fff",
      pointHoverBorderWidth: 3,
      pointRadius: 5,
      pointHitRadius: 10,
      data: [],
      spanGaps: false
    }]
  };

  var realTimeChart = new Chart(realTimeCtx, {
    type: 'line',
    data: realTimeChartData,
    options: {
      legend: {
        display: false
      },
      scales: {
        xAxes: [{
          display: true,
          ticks: {
            fontSize: '11',
            fontColor: '#969da5'
          },
          gridLines: {
            color: 'rgba(0,0,0,0.05)',
            zeroLineColor: 'rgba(0,0,0,0.05)'
          }
        }],
        yAxes: [{
          display: true,
          ticks: {
            beginAtZero: true,
            callback: function(value) {
              var ranges = [
                { divider: 1e9, suffix: 'B' },
                { divider: 1e6, suffix: 'M' },
                { divider: 1e3, suffix: 'K' }
              ];
              function formatNumber(n) {
                for (var i = 0; i < ranges.length; i++) {
                  if (n >= ranges[i].divider) {
                    return (n / ranges[i].divider).toString() + ranges[i].suffix;
                  }
                }
                if (n < 1) {
                  n = Math.round(n * 100) / 100;
                }
                return n;
              }
              return formatNumber(value);
            }
          }
        }]
      },
	responsive: true,
	zoom: {
					enabled: true,
					drag: true,
					mode: 'x',
					limits: {
						max: 10,
						min: 0.5
					}
				}
    }
  });
}
$(document).on('click', '.rt_changeTimeTemplate', function (event) {
  t = $(this).attr('rel');
  if(t==99){
        ts = ( $("#startDate").val() );
        te = ( $("#endDate").val() );
        updateRealTimeChart();
}
else
  updateRealTimeChart();
});

function updateRealTimeChart(){
  //var hostid = $.url('?hostid');
  $.ajax({
    url: './api/requests_last_minute.php',
    method: 'post',
    data: {hostid: hostid ,t:t,ts:ts,te:te},
    success: function(response){
      if (response.code == 11) {
        $("#realtime_value").text(response.data.requests[14]);
        realTimeChartData.labels = response.data.minutes;
        realTimeChartData.datasets[0].data = response.data.requests;
	$("#idminmaxagv").html( response.data.mixmaxavg);
        realTimeChart.update();
      }
    }
  });
}


var bwTodayChartCtx = $("#bandwidthTodayChart");
if (bwTodayChartCtx.length) {
  var bwTodayChartData = {
    labels: [],
    datasets: [{
      label: "GB",
      backgroundColor: "#5797FC",
      borderColor: 'rgba(255,99,132,0)',
      borderWidth: 1,
      data: []
    }]
  };

  var bwTodayChart = new Chart(bwTodayChartCtx, {
    type: 'bar',
    data: bwTodayChartData,
    options: {
      scales: {
        xAxes: [{
          display: true,
          ticks: {
            fontSize: '11',
            fontColor: '#969da5'
          },
          gridLines: {
            color: 'rgba(0,0,0,0.05)',
            zeroLineColor: 'rgba(0,0,0,0.05)'
          }
        }],
        yAxes: [{
          ticks: {
            beginAtZero: true,
            callback: function(value) {
              var ranges = [
                { divider: 1e9, suffix: 'B' },
                { divider: 1e6, suffix: 'M' },
                { divider: 1e3, suffix: 'K' }
              ];
              function formatNumber(n) {
                for (var i = 0; i < ranges.length; i++) {
                  if (n >= ranges[i].divider) {
                    return (n / ranges[i].divider).toString() + ranges[i].suffix;
                  }
                }
                if (n < 1) {
                  n = Math.round(n * 100) / 100;
                }
                return n;
              }
              return formatNumber(value);
            }
          },
          gridLines: {
            color: 'rgba(0,0,0,0.05)',
            zeroLineColor: '#6896f9'
          }
        }]
      },
      legend: {
        display: false
      },
      animation: {
        animateScale: true
      },
	responsive: true,
	zoom: {
					enabled: true,
					drag: true,
					mode: 'x',
					limits: {
						max: 10,
						min: 0.5
					}
				}
    }
  });
}
$(document).on('click', '.changeTimeTemplate', function (event) {
  t = $(this).attr('rel');
if(t==99){
	ts = ( $("#startDate").val() );
	te = ( $("#endDate").val() );
	updateBWTodayChart();
}
else updateBWTodayChart();
});
function updateBWTodayChart( x=0){
	if(x) t=x;
  //var hostid = $.url('?hostid');
  $.ajax({
    url: './api/bandwidth_on_today.php',
    method: 'post',
    data: {hostid: hostid,t:t,ts:ts,te:te},
    success: function(response){
      if (response.code == 11) {
        bwTodayChartData.labels = response.data.hour;
        bwTodayChartData.datasets[0].data = response.data.bandwidth;
	$("#idminmaxagv").html( response.data.mixmaxavg);
        bwTodayChart.update();
      }
    }
  });
}
////////////////////////////////////////////////////////////////

var bwLastWeekChartCtxDownload = $("#bandwidthTodayChartspeeddownloaad");
if (bwLastWeekChartCtxDownload.length) {
  var bwLastWeekChartDataDownload = {
    labels: [],
    datasets: [{
      label: "Mbps",
      backgroundColor: "#5797FC",
      borderColor: 'rgba(255,99,132,0)',
      borderWidth: 1,
      data: []
    }]
  };

  var bwLastWeekChartDownload = new Chart(bwLastWeekChartCtxDownload, {
    type: 'line',
    data: bwLastWeekChartDataDownload,
    options: {
      scales: {
        xAxes: [{
          display: true,
          ticks: {
            fontSize: '11',
            fontColor: '#969da5'
          },
          gridLines: {
            color: 'rgba(0,0,0,0.05)',
            zeroLineColor: 'rgba(0,0,0,0.05)'
          }
        }],
        yAxes: [{
          ticks: {
            beginAtZero: true,
            callback: function(value) {
		var ranges = [
                { divider: 1e9, suffix: 'B' },
                { divider: 1e6, suffix: 'M' },
                { divider: 1e3, suffix: 'K' }
              ];
              function formatNumber(n) {
                for (var i = 0; i < ranges.length; i++) {
                  if (n >= ranges[i].divider) {
                    return (n / ranges[i].divider).toString() + ranges[i].suffix;
                  }
                }
                if (n < 1) {
                  n = Math.round(n * 100) / 100;
                }
                return n;
              }
              return formatNumber(value);
            }
          },
          gridLines: {
            color: 'rgba(0,0,0,0.05)',
            zeroLineColor: '#6896f9'
          }
        }]
      },
      legend: {
        display: false
      },
      animation: {
        animateScale: true
      }
    }
  });
}

///////////////////////////////////////////////////////////////////////////
var bwLastWeekChartCtx = $("#bandwidthTodayChartspeed");
if (bwLastWeekChartCtx.length) {
  var bwLastWeekChartData = {
    labels: [],
    datasets: [{
      label: "Gbps",
      backgroundColor: "#5797FC",
      borderColor: 'rgba(255,99,132,0)',
      borderWidth: 1,
      data: []
    }]
  };

  var bwLastWeekChart = new Chart(bwLastWeekChartCtx, {
    type: 'line',
    data: bwLastWeekChartData,
    options: {
      scales: {
        xAxes: [{
          display: true,
          ticks: {
            fontSize: '11',
            fontColor: '#969da5'
          },
          gridLines: {
            color: 'rgba(0,0,0,0.05)',
            zeroLineColor: 'rgba(0,0,0,0.05)'
          }
        }],
        yAxes: [{
          ticks: {
            beginAtZero: true,
            callback: function(value) {
              var ranges = [
                { divider: 1e9, suffix: 'B' },
                { divider: 1e6, suffix: 'M' },
                { divider: 1e3, suffix: 'K' }
              ];
              function formatNumber(n) {
                for (var i = 0; i < ranges.length; i++) {
                  if (n >= ranges[i].divider) {
                    return (n / ranges[i].divider).toString() + ranges[i].suffix;
                  }
                }
                //if (n < 1) {
                  n = Math.round(n * 100) / 100;
                //}
                return n;
              }
              return formatNumber(value);
            }
          },
          gridLines: {
            color: 'rgba(0,0,0,0.05)',
            zeroLineColor: '#6896f9'
          }
        }]
      },
      legend: {
        display: false
      },
      animation: {
        animateScale: true
      }
    }
  });
}
$(document).on('click', '.speed_changeTimeTemplate', function (event) {
  t = $(this).attr('rel');
  if(t==99){
        ts = ( $("#startDate").val() );
        te = ( $("#endDate").val() );
        updateBWLastWeekChart();
	updateBWLastWeekChartdownload();
}
else{
  updateBWLastWeekChart();
  updateBWLastWeekChartdownload();
}
});
function updateBWLastWeekChart(){
  //var hostid = $.url('?hostid');
  $.ajax({
    url: './api/bandwidth_on_todayspeed.php',
    method: 'post',
    data: {hostid: hostid,t:t,ts:ts,te:te},
    success: function(response){
      if (response.code == 11) {
        bwLastWeekChartData.labels = response.data.hour;
        bwLastWeekChartData.datasets[0].data = response.data.bandwidth;
	$("#idminmaxagv").html( response.data.mixmaxavg);
        bwLastWeekChart.update();
      }
    }
  });
}
function updateBWLastWeekChartdownload( ){
  //var hostid = $.url('?hostid');
  $.ajax({
    url: './api/bandwidth_on_todayspeed_download.php',
    method: 'post',
    data: {hostid: hostid,t:t,ts:ts,te:te},
    success: function(response){
      if (response.code == 11) {
        bwLastWeekChartDataDownload.labels = response.data.hour;
        bwLastWeekChartDataDownload.datasets[0].data = response.data.bandwidth;
	$("#idminmaxagv2").html( response.data.mixmaxavg);
        bwLastWeekChartDownload.update();
      }
    }
  });
}

/////////////////////////////////////////////////////////////////////////////////////////

var bwLastMonthChartCtx = $("#bandwidthLastMonthChart");
if (bwLastMonthChartCtx.length) {
  var bwLastMonthChartData = {
    labels: [],
    datasets: [{
      label: "GB",
      backgroundColor: "#5797FC",
      borderColor: 'rgba(255,99,132,0)',
      borderWidth: 1,
      data: []
    }]
  };

  var bwLastMonthChart = new Chart(bwLastMonthChartCtx, {
    type: 'bar',
    data: bwLastMonthChartData,
    options: {
      scales: {
        xAxes: [{
          display: true,
          ticks: {
            fontSize: '11',
            fontColor: '#969da5'
          },
          gridLines: {
            color: 'rgba(0,0,0,0.05)',
            zeroLineColor: 'rgba(0,0,0,0.05)'
          }
        }],
        yAxes: [{
          ticks: {
            beginAtZero: true,
            callback: function(value) {
              var ranges = [
                { divider: 1e9, suffix: 'B' },
                { divider: 1e6, suffix: 'M' },
                { divider: 1e3, suffix: 'K' }
              ];
              function formatNumber(n) {
                for (var i = 0; i < ranges.length; i++) {
                  if (n >= ranges[i].divider) {
                    return (n / ranges[i].divider).toString() + ranges[i].suffix;
                  }
                }
                if (n < 1) {
                  n = Math.round(n * 100) / 100;
                }
                return n;
              }
              return formatNumber(value);
            }
          },
          gridLines: {
            color: 'rgba(0,0,0,0.05)',
            zeroLineColor: '#6896f9'
          }
        }]
      },
      legend: {
        display: false
      },
      animation: {
        animateScale: true
      }
    }
  });
}

function updateBWLastMonthChart(){
  //var hostid = $.url('?hostid');
  $.ajax({
    url: './api/bandwidth_in_month.php',
    method: 'post',
    data: {hostid: hostid},
    success: function(response){
      if (response.code == 11) {
        bwLastMonthChartData.labels = response.data.month;
        bwLastMonthChartData.datasets[0].data = response.data.bandwidth;
        bwLastMonthChart.update();
      }
    }
  });
}


/*
var bwLastYearChartCtx = $("#bandwidthLastYearChart");
if (bwLastYearChartCtx.length) {
  var bwLastYearChartData = {
    labels: [],
    datasets: [{
      label: "GB",
      backgroundColor: "#5797FC",
      borderColor: 'rgba(255,99,132,0)',
      borderWidth: 1,
      data: []
    }]
  };

  var bwLastYearChart = new Chart(bwLastYearChartCtx, {
    type: 'bar',
    data: bwLastYearChartData,
    options: {
      scales: {
        xAxes: [{
          display: true,
          ticks: {
            fontSize: '11',
            fontColor: '#969da5'
          },
          gridLines: {
            color: 'rgba(0,0,0,0.05)',
            zeroLineColor: 'rgba(0,0,0,0.05)'
          }
        }],
        yAxes: [{
          ticks: {
            beginAtZero: true
          },
          gridLines: {
            color: 'rgba(0,0,0,0.05)',
            zeroLineColor: '#6896f9'
          }
        }]
      },
      legend: {
        display: false
      },
      animation: {
        animateScale: true
      }
    }
  });
}

function updateBWLastYearChart(){
  var hostid = $.url('?hostid');
  $.ajax({
    url: './api/bandwidth_in_year.php',
    method: 'post',
    data: {hostid: hostid},
    success: function(response){
      if (response.code == 11) {
        bwLastYearChartData.labels = response.data.year;
        bwLastYearChartData.datasets[0].data = response.data.bandwidth;
        bwLastYearChart.update();
      }
    }
  });
}
*/


var costsByDayChartCtx = $("#costsByDayChart");
if (costsByDayChartCtx.length) {
  var costsByDayChartData = {
    labels: [],
    datasets: [{
      label: "VND",
      backgroundColor: "rgb(255, 159, 64)",
      borderColor: 'rgb(255, 159, 64)',
      borderWidth: 1,
      data: []
    }]
  };

  var costsByDayChart = new Chart(costsByDayChartCtx, {
    type: 'bar',
    data: costsByDayChartData,
    options: {
      scales: {
        xAxes: [{
          display: true,
          ticks: {
            fontSize: '11',
            fontColor: '#969da5'
          },
          gridLines: {
            color: 'rgba(0,0,0,0.05)',
            zeroLineColor: 'rgba(0,0,0,0.05)'
          }
        }],
        yAxes: [{
          ticks: {
            beginAtZero: true,
            callback: function(value) {
              var ranges = [
                { divider: 1e9, suffix: 'B' },
                { divider: 1e6, suffix: 'M' },
                { divider: 1e3, suffix: 'K' }
              ];
              function formatNumber(n) {
                for (var i = 0; i < ranges.length; i++) {
                  if (n >= ranges[i].divider) {
                    return (n / ranges[i].divider).toString() + ranges[i].suffix;
                  }
                }
                if (n < 1) {
                  n = Math.round(n * 100) / 100;
                }
                return n;
              }
              return formatNumber(value);
            }
          },
          gridLines: {
            color: 'rgba(0,0,0,0.05)',
            zeroLineColor: '#6896f9'
          }
        }]
      },
      legend: {
        display: false
      },
      animation: {
        animateScale: true
      }
    }
  });
}

function updateCostsByDayChart(){
  //var hostid = $.url('?hostid');
  $.ajax({
    url: './api/costs_by_day.php',
    method: 'post',
    data: {hostid: hostid},
    success: function(response){
      if (response.code == 11) {
        costsByDayChartData.labels = response.data.day;
        costsByDayChartData.datasets[0].data = response.data.costs;
        costsByDayChart.update();
      }
    }
  });
}


var costsByMonthChartCtx = $("#costsByMonthChart");
if (costsByMonthChartCtx.length) {
  var costsByMonthChartData = {
    labels: [],
    datasets: [{
      label: "VND",
      backgroundColor: "rgb(255, 159, 64)",
      borderColor: 'rgb(255, 159, 64)',
      borderWidth: 1,
      data: []
    }]
  };

  var costsByMonthChart = new Chart(costsByMonthChartCtx, {
    type: 'bar',
    data: costsByMonthChartData,
    options: {
      scales: {
        xAxes: [{
          display: true,
          ticks: {
            fontSize: '11',
            fontColor: '#969da5'
          },
          gridLines: {
            color: 'rgba(0,0,0,0.05)',
            zeroLineColor: 'rgba(0,0,0,0.05)'
          }
        }],
        yAxes: [{
          ticks: {
            beginAtZero: true,
            callback: function(value) {
              var ranges = [
                { divider: 1e9, suffix: 'B' },
                { divider: 1e6, suffix: 'M' },
                { divider: 1e3, suffix: 'K' }
              ];
              function formatNumber(n) {
                for (var i = 0; i < ranges.length; i++) {
                  if (n >= ranges[i].divider) {
                    return (n / ranges[i].divider).toString() + ranges[i].suffix;
                  }
                }
                if (n < 1) {
                  n = Math.round(n * 100) / 100;
                }
                return n;
              }
              return formatNumber(value);
            }
          },
          gridLines: {
            color: 'rgba(0,0,0,0.05)',
            zeroLineColor: '#6896f9'
          }
        }]
      },
      legend: {
        display: false
      },
      animation: {
        animateScale: true
      }
    }
  });
}

function updateCostsByMonthChart(){
  //var hostid = $.url('?hostid');
  $.ajax({
    url: './api/costs_by_month.php',
    method: 'post',
    data: {hostid: hostid},
    success: function(response){
      if (response.code == 11) {
        costsByMonthChartData.labels = response.data.month;
        costsByMonthChartData.datasets[0].data = response.data.costs;
        costsByMonthChart.update();
      }
    }
  });
}

var visTodayChartCtx = $("#visTodayChart");
if (visTodayChartCtx.length) {
  var visTodayChartData = {
    labels: [],
    datasets: [{
      label: "Visitors",
      fill: false,
      lineTension: 0.3,
      backgroundColor: "#fff",
      borderColor: "#047bf8",
      borderCapStyle: 'butt',
      borderDash: [],
      borderDashOffset: 0.0,
      borderJoinStyle: 'miter',
      pointBorderColor: "#fff",
      pointBackgroundColor: "#141E41",
      pointBorderWidth: 3,
      pointHoverRadius: 10,
      pointHoverBackgroundColor: "#047bf8",
      pointHoverBorderColor: "#fff",
      pointHoverBorderWidth: 3,
      pointRadius: 5,
      pointHitRadius: 10,
      data: [],
      spanGaps: false
    }]
  };

  var visTodayChart = new Chart(visTodayChartCtx, {
    type: 'line',
    data: visTodayChartData,
    options: {
      legend: {
        display: false
      },
      scales: {
        xAxes: [{
          display: true,
          ticks: {
            fontSize: '11',
            fontColor: '#969da5'
          },
          gridLines: {
            color: 'rgba(0,0,0,0.05)',
            zeroLineColor: 'rgba(0,0,0,0.05)'
          }
        }],
        yAxes: [{
          display: true,
          ticks: {
            beginAtZero: true,
            callback: function(value) {
              var ranges = [
                { divider: 1e9, suffix: 'B' },
                { divider: 1e6, suffix: 'M' },
                { divider: 1e3, suffix: 'K' }
              ];
              function formatNumber(n) {
                for (var i = 0; i < ranges.length; i++) {
                  if (n >= ranges[i].divider) {
                    return (n / ranges[i].divider).toString() + ranges[i].suffix;
                  }
                }
                if (n < 1) {
                  n = Math.round(n * 100) / 100;
                }
                return n;
              }
              return formatNumber(value);
            }
          }
        }]
      },
	responsive: true,
	zoom: {
					enabled: true,
					drag: true,
					mode: 'x',
					limits: {
						max: 10,
						min: 0.5
					}
				}
    }
  });
  

}
$(document).on('click', '.hm_changeTimeTemplatevis', function (event) {
  t = $(this).attr('rel');
 if(t==99){
        ts = ( $("#startDate").val() );
        te = ( $("#endDate").val() );
        updateVisTodayChart();
}
else
  updateVisTodayChart(t);
});
function updateVisTodayChart(){
  //var hostid = $.url('?hostid');
  $.ajax({
    url: './api/visitor_on_today.php',
    method: 'post',
    data: {hostid: hostid,t:t,ts:ts,te:te},
    success: function(response){
      if (response.code == 11) {
        visTodayChartData.labels = response.data.hour;
        visTodayChartData.datasets[0].data = response.data.ips;
        $("#idminmaxagv").html( response.data.mixmaxavg);
        visTodayChart.update();
      }
    }
  });
}


var hitTodayChartCtx = $("#hitTodayChart");
if (hitTodayChartCtx.length) {
  var hitTodayChartData = {
    labels: [],
    datasets: [{
      label: "Hits",
	fill: false,
      lineTension: 0.3,
      backgroundColor: "#fff",
      borderColor: "#047bf8",
      borderCapStyle: 'butt',
      borderDash: [],
      borderDashOffset: 0.0,
      borderJoinStyle: 'miter',
      pointBorderColor: "#fff",
      pointBackgroundColor: "#141E41",
      pointBorderWidth: 3,
      pointHoverRadius: 10,
      pointHoverBackgroundColor: "#047bf8",
      pointHoverBorderColor: "#fff",
      pointHoverBorderWidth: 3,
      pointRadius: 5,
      pointHitRadius: 10,
      data: []
    },
    {
      label: "Misses",
      backgroundColor: "#fff",
      borderColor: '#FC2055',
      borderWidth: 1,
      data: []
    }]
  };

  var hitTodayChart = new Chart(hitTodayChartCtx, {
    type: 'line',
    data: hitTodayChartData,
    options: {
      scales: {
        xAxes: [{
          display: true,
          ticks: {
            fontSize: '11',
            fontColor: '#969da5'
          },
          gridLines: {
            color: 'rgba(0,0,0,0.05)',
            zeroLineColor: 'rgba(0,0,0,0.05)'
          }
        }],
        yAxes: [{
          ticks: {
            beginAtZero: true,
            callback: function(value) {
              var ranges = [
                { divider: 1e9, suffix: 'B' },
                { divider: 1e6, suffix: 'M' },
                { divider: 1e3, suffix: 'K' }
              ];
              function formatNumber(n) {
                for (var i = 0; i < ranges.length; i++) {
                  if (n >= ranges[i].divider) {
                    return (n / ranges[i].divider).toString() + ranges[i].suffix;
                  }
                }
                if (n < 1) {
                  n = Math.round(n * 100) / 100;
                }
		if(!n) return n;
                return n + '%';
              }
              return formatNumber(value);
            }
          },
          gridLines: {
            color: 'rgba(0,0,0,0.05)',
            zeroLineColor: '#6896f9'
          }
        }]
      },
      tooltips: {
        mode: 'index',
        intersect: true
      },
      legend: {
        display: true
      },
      animation: {
        animateScale: true
      }
    }
  });
}
$(document).on('click', '.hm_changeTimeTemplate', function (event) {
  t = $(this).attr('rel');
 if(t==99){
        ts = ( $("#startDate").val() );
        te = ( $("#endDate").val() );
        updateHitTodayChart();
}
else
  updateHitTodayChart(t);
});
function updateHitTodayChart(){
  //var hostid = $.url('?hostid');
  $.ajax({
    url: './api/hit_on_today.php',
    method: 'post',
    data: {hostid: hostid,t:t,ts:ts,te:te},
    success: function(response){
      if (response.code == 11) {
        hitTodayChartData.labels = response.data.hour;
        hitTodayChartData.datasets[0].data = response.data.hits;
        hitTodayChartData.datasets[1].data = response.data.misses;
        hitTodayChart.update();
      }
    }
  });
}



var codeTodayChartCtx = $("#codeTodayChart");
if (codeTodayChartCtx.length) {
  var codeTodayChartData = {
    labels: [],
    datasets: [{
      label: "200",
	fill: false,
      backgroundColor: "#fff",
      borderColor: '#047bf8',
      borderWidth: 1,
      data: []
    },
    {
      label: "4xx",fill: false,
	backgroundColor: "#fff",
     borderColor: '#ffd24d',
     borderWidth: 1,
      data: []
    },
    {
      label: "5xx",fill: false,
      backgroundColor: "#fff",
      borderColor: '#FC2055',
      borderWidth: 1,
      data: []
    }]
  };

  var codeTodayChart = new Chart(codeTodayChartCtx, {
    type: 'line',
    data: codeTodayChartData,
    options: {
      scales: {
        xAxes: [{
          display: true,
          //stacked: true,
          ticks: {
            fontSize: '11',
            fontColor: '#969da5'
          },
          gridLines: {
            color: 'rgba(0,0,0,0.05)',
            //zeroLineColor: 'rgba(0,0,0,0.05)'
          }
        }],
        yAxes: [{
          //stacked: true,
	ticks: {
            //beginAtZero: true,
            callback: function(value) {
              var ranges = [
                { divider: 1e9, suffix: 'B' },
                { divider: 1e6, suffix: 'M' },
                { divider: 1e3, suffix: 'K' }
              ];
              function formatNumber(n) {
                for (var i = 0; i < ranges.length; i++) {
                  if (n >= ranges[i].divider) {
                    return (n / ranges[i].divider).toString() + ranges[i].suffix;
                  }
                }
                if (n < 1) {
                  n = Math.round(n * 100) / 100;
                }
                return n;
              }
              return formatNumber(value);
            }
          },
          gridLines: {
            color: 'rgba(0,0,0,0.05)',
            zeroLineColor:'#6896f9'
          }
        }]
      },
      tooltips: {
        mode: 'index',
        intersect: true
      },
      legend: {
        display: true
      },
      animation: {
        animateScale: true
      },
      //responsive: true
    }
  });
}

$(document).on('click', '.code_changeTimeTemplate', function (event) {
  t = $(this).attr('rel');
if(t==99){
        ts = ( $("#startDate").val() );
        te = ( $("#endDate").val() );
        updateCodeTodayChart();
}
else
  updateCodeTodayChart();
});
function updateCodeTodayChart(){
  //var hostid = $.url('?hostid');
  $.ajax({
    url: './api/code_on_today.php',
    method: 'post',
    data: {hostid: hostid,t:t,ts:ts,te:te},
    success: function(response){
      if (response.code == 11) {
        codeTodayChartData.labels = response.data.hour;
        codeTodayChartData.datasets[0].data = response.data.code_200;
        codeTodayChartData.datasets[1].data = response.data.code_4xx;
        codeTodayChartData.datasets[2].data = response.data.code_5xx;
        codeTodayChart.update();
      }
    }
  });
}


var codeLastWeekChartCtx = $("#codeLastWeekChart");
if (codeLastWeekChartCtx.length) {
  var codeLastWeekChartData = {
    labels: [],
    datasets: [{
      label: "200",
      backgroundColor: "#5797FC",
      borderColor: 'rgba(255,99,132,0)',
      borderWidth: 1,
      data: []
    },
    {
      label: "4xx",
      backgroundColor: "rgb(255, 159, 64)",
      borderColor: 'rgb(255, 159, 64)',
      borderWidth: 1,
      data: []
    },
    {
      label: "5xx",
      backgroundColor: "#FC2055",
      borderColor: '#FC2055',
      borderWidth: 1,
      data: []
    }]
  };

  var codeLastWeekChart = new Chart(codeLastWeekChartCtx, {
    type: 'bar',
    data: codeLastWeekChartData,
    options: {
      scales: {
        xAxes: [{
          display: true,
          stacked: true,
          ticks: {
            fontSize: '11',
            fontColor: '#969da5'
          },
          gridLines: {
            color: 'rgba(0,0,0,0.05)',
            zeroLineColor: 'rgba(0,0,0,0.05)'
          }
        }],
        yAxes: [{
          stacked: true,
          ticks: {
            beginAtZero: true,
            callback: function(value) {
              var ranges = [
                { divider: 1e9, suffix: 'B' },
                { divider: 1e6, suffix: 'M' },
                { divider: 1e3, suffix: 'K' }
              ];
              function formatNumber(n) {
                for (var i = 0; i < ranges.length; i++) {
                  if (n >= ranges[i].divider) {
                    return (n / ranges[i].divider).toString() + ranges[i].suffix;
                  }
                }
                if (n < 1) {
                  n = Math.round(n * 100) / 100;
                }
                return n;
              }
              return formatNumber(value);
            }
          },
          gridLines: {
            color: 'rgba(0,0,0,0.05)',
            zeroLineColor: '#6896f9'
          }
        }]
      },
      tooltips: {
        mode: 'index',
        intersect: false
      },
      legend: {
        display: true
      },
      animation: {
        animateScale: true
      },
      responsive: true
    }
  });
}

function updateCodeLastWeekChart(){
  //var hostid = $.url('?hostid');
  $.ajax({
    url: './api/code_in_week.php',
    method: 'post',
    data: {hostid: hostid},
    success: function(response){
      if (response.code == 11) {
        codeLastWeekChartData.labels = response.data.day;
        codeLastWeekChartData.datasets[0].data = response.data.code_200;
        codeLastWeekChartData.datasets[1].data = response.data.code_4xx;
        codeLastWeekChartData.datasets[2].data = response.data.code_5xx;
        codeLastWeekChart.update();
      }
    }
  });
}


var codeLastMonthChartCtx = $("#codeLastMonthChart");
if (codeLastMonthChartCtx.length) {
  var codeLastMonthChartData = {
    labels: [],
    datasets: [{
      label: "200",
      backgroundColor: "#5797FC",
      borderColor: 'rgba(255,99,132,0)',
      borderWidth: 1,
      data: []
    },
    {
      label: "4xx",
      backgroundColor: "rgb(255, 159, 64)",
      borderColor: 'rgb(255, 159, 64)',
      borderWidth: 1,
      data: []
    },
    {
      label: "5xx",
      backgroundColor: "#FC2055",
      borderColor: '#FC2055',
      borderWidth: 1,
      data: []
    }]
  };

  var codeLastMonthChart = new Chart(codeLastMonthChartCtx, {
    type: 'bar',
    data: codeLastMonthChartData,
    options: {
      scales: {
        xAxes: [{
          display: true,
          stacked: true,
          ticks: {
            fontSize: '11',
            fontColor: '#969da5'
          },
          gridLines: {
            color: 'rgba(0,0,0,0.05)',
            zeroLineColor: 'rgba(0,0,0,0.05)'
          }
        }],
        yAxes: [{
          stacked: true,
          ticks: {
            beginAtZero: true,
            callback: function(value) {
              var ranges = [
                { divider: 1e9, suffix: 'B' },
                { divider: 1e6, suffix: 'M' },
                { divider: 1e3, suffix: 'K' }
              ];
              function formatNumber(n) {
                for (var i = 0; i < ranges.length; i++) {
                  if (n >= ranges[i].divider) {
                    return (n / ranges[i].divider).toString() + ranges[i].suffix;
                  }
                }
                if (n < 1) {
                  n = Math.round(n * 100) / 100;
                }
                return n;
              }
              return formatNumber(value);
            }
          },
          gridLines: {
            color: 'rgba(0,0,0,0.05)',
            zeroLineColor: '#6896f9'
          }
        }]
      },
      tooltips: {
        mode: 'index',
        intersect: false
      },
      legend: {
        display: true
      },
      animation: {
        animateScale: true
      },
      responsive: true
    }
  });
}

function updateCodeLastMonthChart(){
  //var hostid = $.url('?hostid');
  $.ajax({
    url: './api/code_in_month.php',
    method: 'post',
    data: {hostid: hostid},
    success: function(response){
      if (response.code == 11) {
        codeLastMonthChartData.labels = response.data.month;
        codeLastMonthChartData.datasets[0].data = response.data.code_200;
        codeLastMonthChartData.datasets[1].data = response.data.code_4xx;
        codeLastMonthChartData.datasets[2].data = response.data.code_5xx;
        codeLastMonthChart.update();
      }
    }
  });
}



var hitWeekChartCtx = $("#hitWeekChart");
if (hitWeekChartCtx.length) {
  var hitWeekChartData = {
    labels: [],
    datasets: [{
      label: "Hits",
      backgroundColor: "#5797FC",
      borderColor: 'rgba(255,99,132,0)',
      borderWidth: 1,
      data: []
    },
    {
      label: "Misses",
      backgroundColor: "#FC2055",
      borderColor: '#FC2055',
      borderWidth: 1,
      data: []
    }]
  };

  var hitWeekChart = new Chart(hitWeekChartCtx, {
    type: 'bar',
    data: hitWeekChartData,
    options: {
      scales: {
        xAxes: [{
          display: true,
          ticks: {
            fontSize: '11',
            fontColor: '#969da5'
          },
          gridLines: {
            color: 'rgba(0,0,0,0.05)',
            zeroLineColor: 'rgba(0,0,0,0.05)'
          }
        }],
        yAxes: [{
          ticks: {
            beginAtZero: true,
            callback: function(value) {
              var ranges = [
                { divider: 1e9, suffix: 'B' },
                { divider: 1e6, suffix: 'M' },
                { divider: 1e3, suffix: 'K' }
              ];
              function formatNumber(n) {
                for (var i = 0; i < ranges.length; i++) {
                  if (n >= ranges[i].divider) {
                    return (n / ranges[i].divider).toString() + ranges[i].suffix;
                  }
                }
                if (n < 1) {
                  n = Math.round(n * 100) / 100;
                }
                return n;
              }
              return formatNumber(value);
            }
          },
          gridLines: {
            color: 'rgba(0,0,0,0.05)',
            zeroLineColor: '#6896f9'
          }
        }]
      },
      tooltips: {
        mode: 'index',
        intersect: true
      },
      legend: {
        display: true
      },
      animation: {
        animateScale: true
      }
    }
  });
}

function updateHitWeekChart(){
  //var hostid = $.url('?hostid');
  $.ajax({
    url: './api/hit_in_week.php',
    method: 'post',
    data: {hostid: hostid},
    success: function(response){
      if (response.code == 11) {
        hitWeekChartData.labels = response.data.day;
        hitWeekChartData.datasets[0].data = response.data.hits;
        hitWeekChartData.datasets[1].data = response.data.misses;
        hitWeekChart.update();
      }
    }
  });
}


var hitMonthChartCtx = $("#hitMonthChart");
if (hitMonthChartCtx.length) {
  var hitMonthChartData = {
    labels: [],
    datasets: [{
      label: "Hits",
      backgroundColor: "#5797FC",
      borderColor: 'rgba(255,99,132,0)',
      borderWidth: 1,
      data: []
    },
    {
      label: "Misses",
      backgroundColor: "#FC2055",
      borderColor: '#FC2055',
      borderWidth: 1,
      data: []
    }]
  };

  var hitMonthChart = new Chart(hitMonthChartCtx, {
    type: 'bar',
    data: hitMonthChartData,
    options: {
      scales: {
        xAxes: [{
          display: true,
          ticks: {
            fontSize: '11',
            fontColor: '#969da5'
          },
          gridLines: {
            color: 'rgba(0,0,0,0.05)',
            zeroLineColor: 'rgba(0,0,0,0.05)'
          }
        }],
        yAxes: [{
          ticks: {
            beginAtZero: true,
            callback: function(value) {
              var ranges = [
                { divider: 1e9, suffix: 'B' },
                { divider: 1e6, suffix: 'M' },
                { divider: 1e3, suffix: 'K' }
              ];
              function formatNumber(n) {
                for (var i = 0; i < ranges.length; i++) {
                  if (n >= ranges[i].divider) {
                    return (n / ranges[i].divider).toString() + ranges[i].suffix;
                  }
                }
                if (n < 1) {
                  n = Math.round(n * 100) / 100;
                }
                return n;
              }
              return formatNumber(value);
            }
          },
          gridLines: {
            color: 'rgba(0,0,0,0.05)',
            zeroLineColor: '#6896f9'
          }
        }]
      },
      tooltips: {
        mode: 'index',
        intersect: true
      },
      legend: {
        display: true
      },
      animation: {
        animateScale: true
      }
    }
  });
}

function updateHitMonthChart(){
  //var hostid = $.url('?hostid');
  $.ajax({
    url: './api/hit_in_month.php',
    method: 'post',
    data: {hostid: hostid},
    success: function(response){
      if (response.code == 11) {
        hitMonthChartData.labels = response.data.month;
        hitMonthChartData.datasets[0].data = response.data.hits;
        hitMonthChartData.datasets[1].data = response.data.misses;
        hitMonthChart.update();
      }
    }
  });
}

var worldmapChart;

function loadWorldmap() {
  //var hostid = $.url('?hostid');
  $.ajax({
    url: './api/worldmap.php',
    method: 'post',
    data: {hostid: hostid},
    success: function(response){
      if (response.code == 11) {
        worldmapChart.series[0].update({data: response.data});
      }
    }
  });
}

// GEOIP world map
var worldmap = $("#worldmap");
if (worldmap.length) {
  worldmapChart = Highcharts.mapChart('worldmap', {
    chart: {
        map: 'custom/world',
        borderWidth: 1,
        events: {
          load: loadWorldmap
        }
    },
    title: {
        text: 'Visitors Country'
    },

    subtitle: {
        text: 'Vistors are from highlight Country'
    },

    legend: {
        enabled: false
    },

    credits: {
      enabled: false
    },

    series: [{
        name: 'Country',
        data: [],
        dataLabels: {
            enabled: true,
            color: '#FFFFFF',
            formatter: function () {
                if (this.point.value) {
                    return this.point.name;
                }
            }
        },
        tooltip: {
            headerFormat: '',
            pointFormat: '{point.name}'
        }
    }]
  });
}

$(function () {

  if (realTimeCtx.length) {
    $("#overview_select_resources").change(function(){
      hostid =this.value;
      updateRealTimeChart();
      /*
      if (this.value == '0') {
        window.location = "/report.php";
      } else {
        window.location = "/report.php?hostid="+ this.value;
      }*/
    });
    updateRealTimeChart();
    setInterval(updateRealTimeChart, 60000);
  }

  if (bwTodayChartCtx.length) {
    $("#bw_select_resources").change(function(){
      hostid =this.value;
      updateBWTodayChart();
      /*
      if (this.value == '0') {
        window.location = "/report.php?type=bw";
      } else {
        window.location = "/report.php?type=bw&hostid="+ this.value;
      }
      */
    });
    updateBWTodayChart(5);
    setInterval(updateBWTodayChart, 60000);
  }
  if (bwLastWeekChartCtx.length) {

  	$("#speed_select_resources").change(function(){
      hostid =this.value;
      updateBWLastWeekChart();
      updateBWLastWeekChartdownload();
      /*
      if (this.value == '0') {
        window.location = "/report.php?type=speed";
      } else {
        window.location = "/report.php?type=speed&hostid="+ this.value;
      }
      */
    });

    updateBWLastWeekChart();
    updateBWLastWeekChartdownload();
    setInterval(updateBWLastWeekChart, 60000);
    setInterval(updateBWLastWeekChartdownload, 60000);
  }
  if (bwLastMonthChartCtx.length) {
    updateBWLastMonthChart();
  }


    if (visTodayChartCtx.length) {

    $("#vis_select_resources").change(function(){
      hostid =this.value;
      updateVisTodayChart();
      /*
      if (this.value == '0') {
        window.location = "/report.php?type=hit";
      } else {
        window.location = "/report.php?type=hit&hostid="+ this.value;
      }*/
    });
    updateVisTodayChart();
    setInterval(updateVisTodayChart, 60000);
  }
  if (hitWeekChartCtx.length) {
    updateHitWeekChart();
  }
  if (hitMonthChartCtx.length) {
    updateHitMonthChart();
  }
  


  if (hitTodayChartCtx.length) {

    $("#hit_select_resources").change(function(){
      hostid =this.value;
      updateHitTodayChart();
      /*
      if (this.value == '0') {
        window.location = "/report.php?type=hit";
      } else {
        window.location = "/report.php?type=hit&hostid="+ this.value;
      }*/
    });
    updateHitTodayChart();
    setInterval(updateHitTodayChart, 60000);
  }
  if (hitWeekChartCtx.length) {
    updateHitWeekChart();
  }
  if (hitMonthChartCtx.length) {
    updateHitMonthChart();
  }


  if (codeTodayChartCtx.length) {

    $("#code_select_resources").change(function(){
      hostid =this.value;
      updateCodeTodayChart();
      /*
      if (this.value == '0') {
        window.location = "/report.php?type=code";
      } else {
        window.location = "/report.php?type=code&hostid="+ this.value;
      }*/
    });
    updateCodeTodayChart();
   setInterval(updateCodeTodayChart, 60000);
  }
  if (codeLastWeekChartCtx.length) {
    updateCodeLastWeekChart();
    updateBWLastWeekChartdownload();
    setInterval(updateCodeLastWeekChart, 60000);
    setInterval(updateBWLastWeekChartdownload, 60000);
  }
  if (codeLastMonthChartCtx.length) {
    updateCodeLastMonthChart();
  }

  // download csv
  $("#report_costs_download").click(function(){
    var date = $("#report_costs_month").val();
    if (date.length) {
      window.open("/download_bw_data.php?date="+date, "_blank");
    }
  });

});





