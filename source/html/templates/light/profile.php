          <div class="content-i">
            <div class="content-box">
              <div class="row">
  
                <div class="col-sm">
                  <div class="element-wrapper">
                    <div class="element-box">
                      <form id="formValidate" novalidate="true" action="profile.php" method="post">
                        <div class="element-info">
                          <div class="element-info-with-icon">
                            <div class="element-info-icon">
                              <div class="os-icon os-icon-wallet-loaded"></div>
                            </div>
                            <div class="element-info-text">
                              <h5 class="element-inner-header">
                                Profile Settings
                              </h5>
                              <div class="element-inner-desc">
                                Info Account
                              </div>
                            </div>
                          </div>
                        </div>
						 <?php
						if (count($error_messages) > 0) {
						  echo "<div class='alert alert-warning text-center' role='alert'>";
						  foreach ($error_messages as $message) {
							echo $message."<br />";
						  }
						  echo "</div>";
						}
						?>
                        <div class="form-group">
                          <label for=""> Email address</label><input class="form-control" data-error="Your email address is invalid" placeholder="Enter email" required="required" type="email" value="<?php echo $arrUserInfo["email"];?>" readonly>
                          <div class="help-block form-text with-errors form-control-feedback"></div>
                        </div>
						<div class="form-group">
                          <label for="">FullName</label><input name="fullname" class="form-control" type="text" value="<?php echo $arrUserInfo["fullname"];?>">
                          <div class="help-block form-text with-errors form-control-feedback"></div>
                        </div>
						<div class="form-group">
                          <label for="">Address</label><input name="address" class="form-control" type="text" value="<?php echo $arrUserInfo["address"];?>">
                          <div class="help-block form-text with-errors form-control-feedback"></div>
                        </div>
                        <div class="form-group">
                          <label for="">Phone</label><input name="phone" class="form-control" type="text" value="<?php echo $arrUserInfo["phone"];?>">
                          <div class="help-block form-text with-errors form-control-feedback"></div>
                        </div>
                       <div class="form-group">
                          <label for="">Register Date</label><input class="form-control" type="text" value="<?php echo $arrUserInfo["registerdate"];?>" readonly>
                          <div class="help-block form-text with-errors form-control-feedback"></div>
                        </div>
                        <div class="form-buttons-w">
                          <button class="btn btn-primary" type="submit"> Update</button>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
              </div>


            </div>
            
          </div>
