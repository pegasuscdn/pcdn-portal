

<div class="content-i">
  <div class="content-box">


  	<div class="element-wrapper">
	
	<div class="element-box">
		<div class="element-actions">
			
			<select class="form-control form-control-sm rounded" id="bw_select_resources" onchange="getStatus(this.value)">
			  <option value="">All Status</option>
			  <option value="11">Active</option>
			  <option value="1">Inactive</option>
			  <option value="0">Disable</option>
			</select>
		  
		</div>
		<div class="element-actions">
			
			<input onkeypress="return searchKeyPress(event);" type="text" name="username" id="username" value="<?php echo $username;?>" class="form-control" placeholder="Name" />
		  
		</div>
    <h5 class="form-header">
      Administrator List
    </h5>

    <div class="table-responsive">
      <!--------------------
      START - Basic Table
      -------------------->
      <table class="table table-striped">
        <thead>
          <tr>
            <th>
              Username
            </th>
            <th>
              Hash
            </th>
            <th>
              GGToken
            </th>
			<th >
              Date Register
            </th>
            <th class="text-center">
              Status
            </th>
			<th class="text-center">
              
            </th>
          </tr>
        </thead>
        <tbody>
			<?php
			if($arrData)
			{	
				while($row = $arrData->fetch_assoc()) {
					echo '<tr>
							<td>'.$row["username"].'</td>
							<td>'.$row["hash"].'</td>
							<td>'.$row["ggtoken"].'</td>
							<td>'.date("m/d/Y",strtotime($row["createdate"])).'</td>
							<td class="text-center">'.$row["recordstatus"].'</td>
							<td class="text-center"><a href="'.SITE_URL.'/admin_edit.php?username='.$row["username"].'"><button class="btn btn-primary">Edit</button></a></td>
						  </tr>';
				}
			}	
			?> 
        </tbody>
      </table>
      <!--------------------
      END - Basic Table
      -------------------->
    </div>
	
</div>

<div class="controls-below-table">
		  <div class="table-records-info">
			Showing records 1 - <?php echo $arrPaging['TOTAL_RESULTS'];?>
		  </div>
		  <div class="table-records-pages">
			<ul>
				<?php 
					if ($arrPaging['CURRENT_PAGE'] > 1) {
						
						echo '<li>
								<a href="'.SITE_URL.'/admin_list.php?page='.$arrPaging['PREV_PAGE'].'&status='.$intStatus.'&username='.$username.'">Previous</a>
							</li>';
						
						
					};
					
					foreach ($arrPaging['PAGE_NUMBERS'] as $key=>$value)
					{
							if ($value == $arrPaging['CURRENT_PAGE']) {
								echo '<li><a class="current" href="javascript:void(0);">'.$value.'</a></li>';
							}else
							{
								
									echo '<li>
											<a href="'.SITE_URL.'/admin_list.php?page='.$value.'&status='.$intStatus.'&username='.$username.'">'.$value.'</a>
										</li>';
								
							}
					}
					
					if ($arrPaging['CURRENT_PAGE'] < $arrPaging['TOTAL_PAGES']) {
						
						echo '<li>
								<a href="'.SITE_URL.'/admin_list.php?page='.$arrPaging['NEXT_PAGE'].'&status='.$intStatus.'&username='.$username.'">Next</a>
							</li>';
						
						
					};
				?>		
				

			</ul>
		  </div>
		</div>
  </div>
  </div>
</div>
<script>
var username="<?php echo $username;?>";
var status="<?php echo $intStatus;?>";
function searchKeyPress(e)
{
    // look for window.event in case event isn't passed in
    e = e || window.event;
    if (e.keyCode == 13)
    {
        var txtSearch = $('#username').val();
		if(txtSearch)
			window.location.href= "<?php echo SITE_URL;?>/admin_list.php?page=1&status="+status+"&username="+txtSearch;
		else
			alert("Enter Username...");
        return false;
    }
    return true;
}
function getStatus(value)
{
	
	window.location.href = "<?php echo SITE_URL;?>/admin_list.php?page=1&status="+value+"&username="+username;
}
</script>