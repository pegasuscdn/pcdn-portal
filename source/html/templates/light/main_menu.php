
        <div class="menu-w color-scheme-light color-style-default menu-position-top menu-layout-compact sub-menu-style-over sub-menu-color-bright selected-menu-color-bright menu-activated-on-hover menu-has-selected-link">
          <div class="logo-w">
            <a class="logo" href="<?php echo SITE_URL; ?>">
              <div class="logo-label">
		
		<?php
                $fileLogo='Viettel CDN Portal';
                if ( strrpos($_SERVER['HTTP_HOST'] , 'pegasustech'))
                  $fileLogo='PEGASUSTECH CDN Portal';
		if ( strrpos($_SERVER['HTTP_HOST'] , 'cdnservice'))
                  $fileLogo='CDN SERVICE PORTAL';
              //  $fileLogo='Viettel CDN Portal';

                echo  $fileLogo; 
                ?>

              </div>
            </a>
          </div>
          <h1 class="menu-page-header">
            Page Header
          </h1>
          <ul class="main-menu">
            <li class="sub-header">
              <span>Layouts</span>
            </li>
            <li class="<?php if ($active_menu == 'report') echo 'selected'; ?> has-sub-menu">
              <a href="#">
                <div class="icon-w">
                  <div class="os-icon os-icon-layout"></div>
                </div>
                <span>Report</span></a>
              <div class="sub-menu-w">
                <div class="sub-menu-header">
                  Report
                </div>
                <div class="sub-menu-icon">
                  <i class="os-icon os-icon-layout"></i>
                </div>
                <div class="sub-menu-i">
                  <ul class="sub-menu">
                  	<li>
                      <a href="report.php">Overview</a>
                    </li>
		<li>
                    <a href="report.php?type=visitor">Unique Visitors</a>
                  </li>
                    <li>
                      <a href="report.php?type=bw">Bandwidth</a>
                    </li>
                    <!--li>
                      <a href="report.php?type=costs">Costs</a>
                    </li-->
                    <li>
                      <a href="report.php?type=hit">Hit/Miss Ratio</a>
                    </li>
                    <li>
                      <a href="report.php?type=code">Response Code</a>
                    </li>
		 <!--li>
                      <a href="report.php?type=speed">Network Speed</a>
                    </li-->	
                  </ul>
                </div>
              </div>
            </li>
            <li class="<?php if ($active_menu == 'cdn') echo 'selected'; ?> has-sub-menu">
              <a href="cdn_resources.php">
                <div class="icon-w">
                  <div class="os-icon os-icon-package"></div>
                </div>
                <span>CDN</span></a>
              <div class="sub-menu-w">
                <div class="sub-menu-header">
                  CDN
                </div>
                <div class="sub-menu-icon">
                  <i class="os-icon os-icon-package"></i>
                </div>
                <div class="sub-menu-i">
                  <ul class="sub-menu">
                    <li>
                      <a href="cdn_resources.php">CDN Resources</a>
                    </li>
                    <!--li>
                      <a href="cdn_storages.php">CDN Storages</a>
                    </li-->
                  </ul>
                </div>
              </div>
            </li>
            <li class="<?php if ($active_menu == 'add-ons') echo 'selected'; ?> has-sub-menu">
              <a href="#">
                <div class="icon-w">
                  <div class="os-icon os-icon-package"></div>
                </div>
                <span>Add-ons</span></a>
              <div class="sub-menu-w">
                <div class="sub-menu-header">
                  Add-ons
                </div>
                <div class="sub-menu-icon">
                  <i class="os-icon os-icon-package"></i>
                </div>
                <div class="sub-menu-i">
                  <ul class="sub-menu">
                    <li>
                      <a href="addon_ssl.php">SSL</a>
                    </li>
                  </ul>
                </div>
              </div>
            </li>
<?php   if ( strrpos($_SERVER['HTTP_HOST'] , 'pegasustech')) { ?>
            <li class="<?php if ($active_menu == 'billing') echo 'selected'; ?> has-sub-menu">
              <a href="#">
                <div class="icon-w">
                  <div class="os-icon os-icon-edit-32"></div>
                </div>
                <span>Billing</span></a>
              <div class="sub-menu-w">
                <div class="sub-menu-header">
                  Billing
                </div>
                <div class="sub-menu-icon">
                  <i class="os-icon os-icon-edit-32"></i>
                </div>
                <div class="sub-menu-i">
                  <ul class="sub-menu">
                    <li>
                      <a href="invoice.php">Invoice</a>
                    </li>
                    <!-- li>
                      <a href="upgrade.php">Plan</a>
                    </li -->
                  </ul>
                </div>
              </div>
            </li>
<?php } ?>
            <li class="<?php if ($active_menu == 'account') echo 'selected'; ?> has-sub-menu">
              <a href="#">
                <div class="icon-w">
                  <div class="os-icon os-icon-users"></div>
                </div>
                <span>Account</span></a>
              <div class="sub-menu-w">
                <div class="sub-menu-header">
                  Account
                </div>
                <div class="sub-menu-icon">
                  <i class="os-icon os-icon-users"></i>
                </div>
                <div class="sub-menu-i">
                  <ul class="sub-menu">
		
		<!--li>
                      <a href="#">  <?php echo $_SESSION['portal']['email'];
                        //if($_SESSION['portal']['expired_date'])
                        ;//echo "<br><font color='red'>Expired Date: ".date("m/d/Y",strtotime($_SESSION['portal']["expired_date"]))."</font>";
                      ?> </a>
                    </li-->	
                    <li>
                      <a href="profile.php">Profile</a>
                    </li>
                    <li>
                      <a href="change_password.php">Change Password</a>
                    </li>
                    <li>
                      <a href="twostep.php">Two-Step Authentication</a>
                    </li>
                  </ul>
                </div>
              </div>
            </li>
            <li  style="border-right: 1px solid rgba(0, 0, 0, 0);" class="<?php if ($active_menu == 'logout') echo 'selected'; ?>">
              <a href="logout.php">
                <div class="icon-w">
                  <div class="os-icon os-icon-signs-11"></div>
                </div>
                <span>Logout  &nbsp;
                        <?php echo $_SESSION['portal']['email'];
                        //if($_SESSION['portal']['expired_date'])
                        //echo "&nbsp;&nbsp;<font color='red'>Expired Date: ".date("m/d/Y",strtotime($_SESSION['portal']["expired_date"]))."</font>";
                      ?></span>
              </a>
            </li>
		
		<!--li style="border-right: 1px solid rgba(0, 0, 0, 0);float:right;"><div class="icon-w"></div><span>&nbsp;
			<?php echo $_SESSION['portal']['email'];
                        if($_SESSION['portal']['expired_date'])
                        echo "&nbsp;&nbsp;<font color='red'>Expired Date: ".date("m/d/Y",strtotime($_SESSION['portal']["expired_date"]))."</font>";
                      ?></span>
		</li-->
          </ul>
        </div>
