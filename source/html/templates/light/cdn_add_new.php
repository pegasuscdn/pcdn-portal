<div class="content-i">
  <div class="content-box">

    
    <div class="element-wrapper">
      <h6 class="element-header">
        Add new CDN
      </h6>
      <?php 
      if (!empty($errors)) {
        echo "<div class='alert alert-warning text-center' role='alert'>";
        foreach ($errors as $error)
          echo $error."<br />";
        echo "</div>";
      }
      ?>
      <div class="element-box">
        <form action="cdn_resources.php?action=add" method="POST">
          <div class="form-group row">
            <label class="col-form-label col-sm-2" for="">Name *</label>
            <div class="col-sm-5">
              <input class="form-control" placeholder="Name" type="text" name="cdn_name" value="<?php echo $cdn_name; ?>">
            </div>
          </div>
          <div class="form-group row" id="show_https_domain">
            <label class="col-form-label col-sm-2" for="">Private Domain *</label>
            <div class="col-sm-5">
              <input class="form-control" placeholder="Name" type="text" name="cdn_https_domain" id="cdn_https_domain" value="<?php echo $cdn_https_domain; ?>">
            </div>
          </div>

          <div class="form-group row">
            <label class="col-sm-2 col-form-label">Origin *</label>
            <div class="col-sm-8">
              <div class="form-check">
                <label class="form-check-label"><input class="form-check-input" name="cdn_origin" type="radio" value="1" id="origin_normal" <?php if ($cdn_origin == 1) echo "checked"; ?>>My Origin</label>
              </div>
              <div class="form-check">
                <label class="form-check-label"><input class="form-check-input" name="cdn_origin" type="radio" value="2" id="origin_storage" <?php if ($cdn_origin == 2) echo "checked"; ?>>CDN Storage</label>
              </div>
            </div>
          </div>

  <div class="form-group row" id="show_my_origin2">
            <label class="col-form-label col-sm-2" for=""> CDN Type</label>
            <div class="col-sm-5">
              <select class="form-control" name="cdn_type" id="idtab1_cdn_type">
                <option value="1" >
                  Static Content / VOD
                </option>
                <option value="2">
                  Live Stream
                </option>
                <option value="3">
                  Web Accelerator
                </option>
              </select>

            </div>
          </div>

<div class="form-group row" id="show_cdn_storage2">
            <label class="col-form-label col-sm-2" for=""> CDN Type</label>
            <div class="col-sm-5">
              <select class="form-control" name="cdn_type11111" id="idtab1111_cdn_type">
                <option value="1" >
                  Static Content / VOD
                </option>
              </select>

            </div>
          </div>


          <fieldset class="form-group" id="show_my_origin">
            <legend><span>My Origin</span></legend>
            <div class="form-group row">
              <label class="col-sm-2 col-form-label" for="">Your domain *</label>
              <div class="col-sm-8">
                <div class="row">
                  <div class="col-sm-2">
                    <select class="form-control" name="my_origin_type">
                      <option value="http" <?php if ($my_origin_type == 'http') echo "selected"; ?>>http</option>
                      <option value="https" <?php if ($my_origin_type == 'https') echo "selected"; ?>>https</option>
                    </select>
                  </div>
                  <div class="col-sm-10">
                    <input class="form-control" placeholder="yourdomain.com[:port]" type="text" name="my_origin_domain" value="<?php echo $my_origin_domain; ?>">
                  </div>
                </div>
              </div>
            </div>
          </fieldset>

          <fieldset class="form-group" id="show_cdn_storage">
            <legend><span>CDN Storage</span></legend>
 <div class="form-group row">
            <label class="col-form-label col-sm-2" for="">Access Key *</label>
            <div class="col-sm-5">
              <input class="form-control" placeholder="Access Key" type="text" name="cdn_access_key" value="<?php echo $cdn_access_key; ?>">
            </div>
          </div>
 <div class="form-group row">
            <label class="col-form-label col-sm-2" for="">Secret Key *</label>
            <div class="col-sm-5">
              <input class="form-control" placeholder="Secret Key" type="text" name="cdn_secret_key" value="<?php echo $cdn_secret_key; ?>">
            </div>
          </div>
 <div class="form-group row">
            <label class="col-form-label col-sm-2" for="">Bucket *</label>
            <div class="col-sm-5">
              <input class="form-control" placeholder="Bucket" type="text" name="cdn_bucket" value="<?php echo $cdn_bucket; ?>">
            </div>
          </div>
            <!--div class="form-group row">
            <label class="col-form-label col-sm-2" for="">Storage *</label>
            <div class="col-sm-8">
              <select class="form-control" name="cdn_storage">
                <option value="">
                  storage-1
                </option>
                <option value="">
                  storage-2
                </option>
              </select>
              <p><a href="cdn_storages.php?action=add">Add new Storage</a></p>
            </div>
          </div-->

          </fieldset>
          
          <div class="form-buttons-w">
            <button class="btn btn-primary" type="submit"> Submit</button>
            <a class="btn btn-sm btn-secondary" href="cdn_resources.php">Return List</a>
          </div>
        </form>
      </div>
    </div>


  </div>
</div>

c
