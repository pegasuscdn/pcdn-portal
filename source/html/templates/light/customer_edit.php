          <div class="content-i">
            <div class="content-box">
              <div class="row">
  
                <div class="col-sm">
                  <div class="element-wrapper">
                    <div class="element-box">
                      <form id="formValidate" novalidate="true" action="customer_edit.php?email=<?php echo $arrUserInfo["email"];?>" method="post">
                        <div class="element-info">
                          <div class="element-info-with-icon">
                            
                            <div class="element-info-text">
                              <h5 class="element-inner-header">
                                Customer Edit
                              </h5>
                              
                            </div>
                          </div>
                        </div>
						 <?php
						if (count($error_messages) > 0) {
						  echo "<div class='alert alert-warning text-center' role='alert'>";
						  foreach ($error_messages as $message) {
							echo $message."<br />";
						  }
						  echo "</div>";
						}
						?>
                        <div class="form-group">
                          <label for=""> Email address</label><input class="form-control" required="required" name="email" type="email" value="<?php echo $arrUserInfo["email"];?>" />
                          <div class="help-block form-text with-errors form-control-feedback"></div>
                        </div>
						 <div class="form-group">
                          <label for=""> Password</label><input name="password" class="form-control" required="required" type="password" value="<?php echo $arrUserInfo["password"];?>">
                          <div class="help-block form-text with-errors form-control-feedback"></div>
                        </div>
						<div class="form-group">
                          <label for="">FullName</label><input name="fullname" class="form-control" type="text" value="<?php echo $arrUserInfo["fullname"];?>">
                          <div class="help-block form-text with-errors form-control-feedback"></div>
                        </div>
						<div class="form-group">
                          <label for="">Address</label><input name="address" class="form-control" type="text" value="<?php echo $arrUserInfo["address"];?>">
                          <div class="help-block form-text with-errors form-control-feedback"></div>
                        </div>
                        <div class="form-group">
                          <label for="">Phone</label><input name="phone" class="form-control" type="text" value="<?php echo $arrUserInfo["phone"];?>">
                          <div class="help-block form-text with-errors form-control-feedback"></div>
                        </div>
                       <div class="form-group">
                          <label for="">Status</label>
						  <select class="form-control form-control-sm rounded" id="bw_select_resources" name="status">
							  <option <?php if($arrUserInfo["recordstatus"]==11) echo 'selected="selected"'; else echo "";?> value="11">Active</option>
							  <option <?php if($arrUserInfo["recordstatus"]==1) echo 'selected="selected"'; else echo "";?>value="1">Inactive</option>
							  <option <?php if($arrUserInfo["recordstatus"]==0) echo 'selected="selected"'; else echo "";?>value="0">Disable</option>
							</select>
                          <div class="help-block form-text with-errors form-control-feedback"></div>
                        </div>
                        <div class="form-buttons-w">
                          <button class="btn btn-primary" type="submit">Edit</button>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
              </div>


            </div>
            
          </div>
