          <div class="content-i">
            <div class="content-box">
              <div class="row">
  
                <div class="col-sm">
                  <div class="element-wrapper">
                    <div class="element-box">
                      <form id="formValidate" novalidate="true" action="twostep.php" method="post">
                        <div class="element-info">
                          <div class="element-info-with-icon">
                            <div class="element-info-icon">
                              <div class="os-icon os-icon-wallet-loaded"></div>
                            </div>
                            <div class="element-info-text">
                              <h5 class="element-inner-header">
                                Two-Step Authentication
                              </h5>
                              <div class="element-inner-desc">
                                Info Account
                              </div>
                            </div>
                          </div>
                        </div>
						<?php
						if (count($error_messages) > 0) {
						  echo "<div class='alert alert-warning text-center' role='alert'>";
						  foreach ($error_messages as $message) {
							echo $message."<br />";
						  }
						  echo "</div>";
						}
						?>
						<div class="form-group">
                          <label for="">Do you want enable/disable two-step authentication?</label>
						  <select name="status">
							<option value="1" <?php if(strlen($arrUserInfo["ggdata"])>0) echo "selected='selected'";else echo "";?> >Enable</option>
							<option value="0" <?php if(strlen($arrUserInfo["ggdata"])==0) echo "selected='selected'";else echo "";?>>Disable</option>
						  </select>
                          <div class="help-block form-text with-errors form-control-feedback"></div>
                        </div>
						<div class="form-group">
							<?php
							if(strlen($arrUserInfo["ggtoken"])>0)
							{
								$ggtoken=$arrUserInfo["ggtoken"];
							}else{
								$arrToken=array();
								for($i=0;$i<5;$i++)
								{
									$arrToken[$i] = substr(md5(mt_rand()), 0, 5);
								}
								$ggtoken=implode(",",$arrToken);
							}
							?>
                          <label for="">Do you want general token emergency?</label><input name="ggtoken" class="form-control" type="text" value="<?php echo $ggtoken;?>" readonly>
                          <div class="help-block form-text with-errors form-control-feedback"></div>
                        </div>
						
                        <div class="form-group">
                          <label for="">Step 1: Install authentication app on smart phone</label>
						  <div class="element-inner-desc">We recommend using Google Authentication, which you can download from the following links</div>
						  <a href="https://play.google.com/store/apps/details?id=com.google.android.apps.authenticator2" target="_blank"><img src="<?php echo CDN_URL; ?>/templates/light/img/chplay.png" width="150px" /></a>
						  <a href="https://itunes.apple.com/vn/app/google-authenticator/id388497605?mt=8" target="_blank"><img src="<?php echo CDN_URL; ?>/templates/light/img/iosstore.png" width="150px" /></a>
                          <div class="help-block form-text with-errors form-control-feedback"></div>
                        </div>
						
						<div class="form-group">
                          <label for="">Step 2: Send the configuration QR code</label>
						  <div class="element-inner-desc">Open your application and scan the configuration QR code with it</div>
						  <img src="<?php echo $qrCodeUrl; ?>" />
                          <div class="help-block form-text with-errors form-control-feedback"></div>
                        </div>
						                        
                        <div class="form-group">
							<label for="">Step 3: Verify the app configuration</label>
							<div class="element-inner-desc">Fill in the 6 digit code from yout authentication app</div>
							<input class="form-control" required="required" type="text" name="oneCode" />
                        </div>
                       
                        <div class="form-buttons-w">
                          <button class="btn btn-primary" type="submit"> Active Two-Step</button>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
              </div>


            </div>
            
          </div>
