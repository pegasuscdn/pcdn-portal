<div class="content-i">
  <div class="content-box">


    <div class="element-wrapper">
      <div class="element-box">
        <h5 class="form-header">
          SSL
        </h5>
	<?php
        if (!empty($errors)) {
          echo "<div class='alert alert-warning text-center' role='alert'>";
          foreach ($errors as $error)
            echo $error."<br />";
          echo "</div>";
        } else if ($message) {
          echo "<div class='alert alert-success text-center' role='alert'>";
          echo $message;
          echo "</div>";
        }
        ?>  
        <div class="controls-above-table">
          <div class="row">
            <div class="col-sm-6">
              <a class="btn btn-sm btn-primary" href="addon_ssl.php?action=add">Add New SSL</a>
            </div>
          </div>

          <div class="table-responsive">
            <table class="table table-striped">
              <thead>
                <tr>
			<th>Name</th>
                  <th class="text-right">Status</th>
                  <th class="text-right">Action</th>
                </tr>
              </thead>
              <tbody>
                <?php
                foreach ($resources as $resource) {
                  $color = ($resource['recordstatus'] == RECORD_STATUS_ACTIVE) ? 'green' : 'yellow';
                  //$https = ($resource['https'] == 1) ? 'os-icon os-icon-arrow-down3' : '';
                  echo "<tr>";
                  echo "<td><a href='addon_ssl.php?action=edit&id={$resource['id']}'>{$resource['name']}</a></td>";

                  echo "<td class='text-right'><div class='status-pill {$color}' data-toggle='tooltip'></div></td>";
		  echo "<td class='text-right'><a class='danger refererdelete' href='addon_ssl.php?action=del&id={$resource['id']}'><i class='os-icon os-icon-ui-15'></i></a></td>";

                  echo "</tr>";
                }
                ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>


  </div>
</div>

