<!DOCTYPE html>
<html>
  <head>
    <title>Admin Dashboard HTML Template</title>
    <meta charset="utf-8">
    <meta content="ie=edge" http-equiv="x-ua-compatible">
    <meta content="template language" name="keywords">
    <meta content="Tamerlan Soziev" name="author">
    <meta content="Admin dashboard html template" name="description">
    <meta content="width=device-width, initial-scale=1" name="viewport">
    <link href="favicon.png" rel="shortcut icon">
    <link href="apple-touch-icon.png" rel="apple-touch-icon">
    <link href="https://fonts.googleapis.com/css?family=Rubik:300,400,500" rel="stylesheet" type="text/css">
    <link href="<?php echo CDN_URL; ?>/templates/light/bower_components/select2/dist/css/select2.min.css" rel="stylesheet">
    <link href="<?php echo CDN_URL; ?>/templates/light/bower_components/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
    <link href="<?php echo CDN_URL; ?>/templates/light/bower_components/dropzone/dist/dropzone.css" rel="stylesheet">
    <link href="<?php echo CDN_URL; ?>/templates/light/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo CDN_URL; ?>/templates/light/bower_components/fullcalendar/dist/fullcalendar.min.css" rel="stylesheet">
    <link href="<?php echo CDN_URL; ?>/templates/light/bower_components/perfect-scrollbar/css/perfect-scrollbar.min.css" rel="stylesheet">
    <link href="<?php echo CDN_URL; ?>/templates/light/bower_components/slick-carousel/slick/slick.css" rel="stylesheet">
    <link href="<?php echo CDN_URL; ?>/templates/light/css/main.css?version=4.4.0" rel="stylesheet">
  </head>
  <body>
    <div class="all-wrapper menu-side with-pattern">
      <div class="auth-box-w wider">
        <div class="logo-w">
          <a href="#"><img alt=""  width="250px"  src="<?php echo CDN_URL; 
$fileLogo='logo-viettel.jpg';
          if ( strrpos($_SERVER['HTTP_HOST'] , 'pegasustech'))
            $fileLogo='logo-250x102.png';
          if ( strrpos($_SERVER['HTTP_HOST'] , 'cdnservice') !== false) // cdnservice.pegasus or portal.cdnservice.vn
           $fileLogo='logo-cdnservice.jpeg';
?>/templates/light/img/<?php echo $fileLogo;?>"></a>
        </div>
        <h4 class="auth-header">
          Create new account
        </h4>
		 <?php
        if (count($error_messages) > 0) {
          echo "<div class='alert alert-warning text-center' role='alert'>";
          foreach ($error_messages as $message) {
            echo $message."<br />";
          }
          echo "</div>";
        }
        ?>
        <form action="register.php" method="post">
          <div class="form-group">
            <label for="">Fullname</label><input class="form-control" placeholder="Enter Fullname" type="text" name="fullname">
            <div class="pre-icon os-icon os-icon-users"></div>
          </div>
		  <div class="form-group">
            <label for=""> Email address</label><input class="form-control" placeholder="Enter email" type="email" name="portal_email">
            <div class="pre-icon os-icon os-icon-email-2-at2"></div>
          </div>
          <div class="row">
            <div class="col-sm-6">
              <div class="form-group">
                <label for=""> Password</label><input class="form-control" placeholder="Password" type="password" name="portal_pass">
                <div class="pre-icon os-icon os-icon-fingerprint"></div>
              </div>
            </div>
            <div class="col-sm-6">
              <div class="form-group">
                <label for="">Confirm Password</label><input class="form-control" placeholder="Password" type="password" name="portal_repass">
              </div>
            </div>
          </div>
          <div class="buttons-w">
            <button class="btn btn-primary">Register Now</button>
          </div><br />
		  <div class="form-group">
            <label for=""><a href="<?php echo SITE_URL;?>/login.php">Have you account? Login Now</a></label>
          </div>
        </form>
      </div>
    </div>
  </body>
</html>
