<?php
$cdate=date("Y-m-d");
if( ($cdate>=$_SESSION['portal']['expired_date']) || ($_SESSION['portal']['trial_bandwidth']==0) ){
?>

<div class="content-i">
  <div class="content-box">


    <h5 class="form-header">
      Your Acount is expired. Please contact to administrator.	
    </h5>

    </div>
	
</div>
<?php } else { ?>
<div class="content-i">
  <div class="content-box">
    <h5 class="form-header">
      Your Acount will  expire at <?php echo date("M/d/Y", strtotime($_SESSION['portal']['expired_date']. '00:00:00')) ; ?> , Remain  <?php  echo round($_SESSION['portal']['trial_bandwidth']/1000/1000/1000,2).'/'. $_SESSION['portal']['limited_bandwidth'];?>G bandwidth.   
    </h5>
    </div>
</div>

<?php }?>
