

<div class="content-i">
  <div class="content-box">


  	<div class="element-wrapper">
	
	<div class="element-box">
		<div class="element-actions">
			
			<select class="form-control form-control-sm rounded bright" onchange="getStatus(this.value)">
					<option <?php if($intStatus==10) echo 'selected="selected"';else echo "";?> value="10">Select Status</option>
					<?php
						foreach($arrStatus as $key=>$value)
						{
							if($key==$intStatus) $sel='selected="selected"';else $sel='';
							echo '<option '.$sel.' value="'.$key.'">'.$value.'</option>';
						}							
					?>
				</select>
		  
		</div>
		
    <h5 class="form-header">
      Invoice List
    </h5>

    <div class="table-responsive">
      <!--------------------
      START - Basic Table
      -------------------->
      <table class="table table-striped">
        <thead>
          <tr>
				<th>
				  Invoice
				</th>
				<th>
				  Invoice Date
				</th>
				<th>
				  Due Date
				</th>
				<th>
				  Total
				</th>
				<th>
				  Status
				</th>
				<!--th>
				  
				</th-->
			  </tr>
        </thead>
        <tbody>
			<?php
			if($arrData)
			{	
				while($row = $arrData->fetch_assoc()) {
					echo '<tr>
							<td>'.$row["id"].'</td>
							<td>'.date("m/d/Y",strtotime($row["created_date"])).'</td>
							<td>'.date("m/d/Y",strtotime($row["expired_date"])).'</td>
							<td>'.number_format($row["total"]).'</td>
							<td>'.$arrStatus[$row["status"]].'</td>
							<!--td><a href="'.SITE_URL.'/invoice_detail.php?id='.$row["id"].'"><i class="os-icon os-icon-ui-49"></i></a></td-->
							
						  </tr>';
				}
			}	
			?> 
        </tbody>
      </table>
      <!--------------------
      END - Basic Table
      -------------------->
    </div>
	
</div>
	<div class="controls-below-table">
		  <div class="table-records-info">
			Showing records 1 - <?php echo $arrPaging['TOTAL_RESULTS'];?>
		  </div>
		  <div class="table-records-pages">
			<ul>
				<?php 
					if ($arrPaging['CURRENT_PAGE'] > 1) {
						
						echo '<li>
								<a href="'.SITE_URL.'/invoice.php?page='.$arrPaging['PREV_PAGE'].'&status='.$intStatus.'">Previous</a>
							</li>';
						
						
					};
					
					foreach ($arrPaging['PAGE_NUMBERS'] as $key=>$value)
					{
							if ($value == $arrPaging['CURRENT_PAGE']) {
								echo '<li><a class="current" href="javascript:void(0);">'.$value.'</a></li>';
							}else
							{
								
									echo '<li>
											<a href="'.SITE_URL.'/invoice.php?page='.$value.'&status='.$intStatus.'">'.$value.'</a>
										</li>';
								
							}
					}
					
					if ($arrPaging['CURRENT_PAGE'] < $arrPaging['TOTAL_PAGES']) {
						
						echo '<li>
								<a href="'.SITE_URL.'/invoice.php?page='.$arrPaging['NEXT_PAGE'].'&status='.$intStatus.'">Next</a>
							</li>';
						
						
					};
				?>		
				

			</ul>
		  </div>
		</div>
	
  </div>
  </div>
</div>
<script>
function getStatus(value)
{
	window.location.href = "<?php echo SITE_URL;?>/invoice.php?page=1&status="+value;
}
</script>
