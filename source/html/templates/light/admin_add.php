          <div class="content-i">
            <div class="content-box">
              <div class="row">
  
                <div class="col-sm">
                  <div class="element-wrapper">
                    <div class="element-box">
                      <form id="formValidate" novalidate="true" action="admin_add.php" method="post">
                        <div class="element-info">
                          <div class="element-info-with-icon">
                            
                            <div class="element-info-text">
                              <h5 class="element-inner-header">
                                Admistrators Add
                              </h5>
                              
                            </div>
                          </div>
                        </div>
						 <?php
						if (count($error_messages) > 0) {
						  echo "<div class='alert alert-warning text-center' role='alert'>";
						  foreach ($error_messages as $message) {
							echo $message."<br />";
						  }
						  echo "</div>";
						}
						?>
                        <div class="form-group">
                          <label for=""> Username</label><input class="form-control" required="required" name="username" type="text" value="<?php echo $arrUserInfo["username"];?>" />
                          <div class="help-block form-text with-errors form-control-feedback"></div>
                        </div>
						 <div class="form-group">
                          <label for=""> Password</label><input name="password" class="form-control" required="required" type="password" value="<?php echo $arrUserInfo["password"];?>">
                          <div class="help-block form-text with-errors form-control-feedback"></div>
                        </div>
						
                       <div class="form-group">
                          <label for="">Status</label>
						  <select class="form-control form-control-sm rounded" id="bw_select_resources" name="status">
							  <option value="11">Active</option>
							  <option value="1">Inactive</option>
							  <option value="0">Disable</option>
							</select>
                          <div class="help-block form-text with-errors form-control-feedback"></div>
                        </div>
                        <div class="form-buttons-w">
                          <button class="btn btn-primary" type="submit"> Add</button>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
              </div>


            </div>
            
          </div>
