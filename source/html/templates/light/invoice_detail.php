          <div class="content-i">
            <div class="content-box">
              <div class="element-wrapper">
                <div class="invoice-w">
                  
				  <div class="infos">
                    <div class="info-1">
                      <div class="invoice-logo-w">
                        <img alt="" src="img/logo2.png">
                      </div>
                      <div class="company-name">
                        Paper Inc
                      </div>
                      <div class="company-address">
                        47 Countrylane Ave, <br/>Los Angeles, CA 97446 <br/>United States
                      </div>
                      <div class="company-extra">
                        tel. 858.745.5577
                      </div>
                    </div>
                    <div class="info-2">
                      <div class="company-name">
                        <?php echo $arrData["fullname"];?>
                      </div>
                      <div class="company-address">
                        <?php echo $arrData["address"];?>
                      </div>
					  <div class="company-extra">
                         <?php echo $arrData["phone"];?>
                      </div>
                    </div>
                  </div>
				  
                  <div class="invoice-heading">
                    <h3>
                      Invoice
                    </h3>
                    <div class="invoice-date">
                      <?php echo date("d M Y",strtotime($arrData["expired_date"]));?>
                    </div>
                  </div>
                  <div class="invoice-body">
                    <div class="invoice-desc">
                      <div class="desc-label">
                        Invoice #
                      </div>
                      <div class="desc-value">
                        <?php echo $arrData["invoiceid"];?>
                      </div>
                    </div>
                    <div class="invoice-table">
                      <table class="table">
                        <thead>
                          <tr>
                            <th>
                              Description
                            </th>
                            <th class="text-right">
                              Price
                            </th>
                          </tr>
                        </thead>
                        <tbody>
						<?php
						$total=0;
							while($row = $arrDataInvoiceDetail->fetch_assoc()) {
								$total = $total+$row["amount"];
								echo '<tr>
										<td>'.$row["description"].'</td>
										<td class="text-right">'.number_format($row["amount"]).'</td>
									  </tr>';
							}
						?>
                         
                        </tbody>
                        <tfoot>
                          <tr>
                            <td>
                              Total
                            </td>
                            <td class="text-right" colspan="2">
                              <?php echo number_format($total);?>
                            </td>
                          </tr>
                        </tfoot>
                      </table>
                      <div class="terms">
                        <div class="terms-header">
                          Terms and Conditions
                        </div>
                        <div class="terms-content">
                          Should be paid as soon as received, otherwise a 5% penalty fee is applied
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="invoice-footer">
                    <div class="invoice-logo">
                      <img alt="" src="img/logo.png"><span>Paper Inc</span>
                    </div>
                    <div class="invoice-info">
                      <span>hello@paper.inc</span><span>858.757.4455</span>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
