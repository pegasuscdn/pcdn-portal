<div class="content-i">
  <div class="content-box">


    <div class="element-wrapper">
      <div class="element-box">
        <h5 class="form-header">
          CDN Resources
        </h5>

        <div class="controls-above-table">
          <div class="row">
            <div class="col-sm-6">
              <a class="btn btn-sm btn-primary" href="cdn_resources.php?action=add">Add New CDN</a>
            </div>
          </div>

          <div class="table-responsive">
            <table class="table table-striped">
              <thead>
                <tr>
                  <th>
                    Name
                  </th>
                  <th>
                    CDN Domain
                  </th>
                  <th>
                    Type
                  </th>
                  <th class="text-center">
                    Status
                  </th>
                </tr>
              </thead>
              <tbody>
                <?php
/*
define('RECORD_STATUS_DISABLE', 0);
define('RECORD_STATUS_ACTIVE', 11);
define('RECORD_STATUS_INACTIVE', 1);
define('RECORD_STATUS_PENDING_EXEC', 2);
define('RECORD_STATUS_PENDING_DNS', 3);
define('RECORD_LIMIT', 15);

*/
                foreach ($resources as $resource) {
                  $color = ($resource['recordstatus'] == RECORD_STATUS_ACTIVE) ? 'green' : 'yellow';
		  if($resource['deleted']) $color ='red';
                  $https = ($resource['https'] == 1) ? 'os-icon os-icon-arrow-down3' : '';
                  echo "<tr>";
		  if($resource['deleted'])
                   echo "<td>{$resource['name']}</td>";
		  else	
                  echo "<td><a href='cdn_resources.php?action=edit&id={$resource['id']}'>{$resource['name']}</a></td>";
                  //echo "<td><div class='{$https}'></div></td>";
                  echo "<td>{$resource['hostname']}</td>";
		echo "<td>";
		if(($resource['type']==1) ) echo 'Static Content / VOD';
		 elseif(($resource['type']==2) ) echo 'Live Stream';
		else echo "Web Accelerator";	
		echo "</td>";
                  //echo "<td>".( ($resource['type']==1) ? "Static Content / VOD" :  ($resource['type']==3) ? "Web Accelerator" : "Live Stream")."</td>";
                  echo "<td class='text-center'><div class='status-pill {$color}' data-toggle='tooltip'></div></td>";
                  echo "</tr>";
                }
                ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>


  </div>
</div>
