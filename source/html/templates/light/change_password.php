          <div class="content-i">
            <div class="content-box">
              <div class="row">
  
                <div class="col-sm">
                  <div class="element-wrapper">
                    <div class="element-box">
                      <form id="formValidate" novalidate="true" action="change_password.php" method="post">
                        <div class="element-info">
                          <div class="element-info-with-icon">
                            <div class="element-info-icon">
                              <div class="os-icon os-icon-wallet-loaded"></div>
                            </div>
                            <div class="element-info-text">
                              <h5 class="element-inner-header">
                                Change Password
                              </h5>
                              <div class="element-inner-desc">
                                Info Account
                              </div>
                            </div>
                          </div>
                        </div>
						 <?php
						if (count($error_messages) > 0) {
						  echo "<div class='alert alert-warning text-center' role='alert'>";
						  foreach ($error_messages as $message) {
							echo $message."<br />";
						  }
						  echo "</div>";
						}
						?>
                        <div class="form-group">
                          <label for="">Current Password</label><input class="form-control" required="required" type="password" name="currentpass"  />
                          <div class="help-block form-text with-errors form-control-feedback"></div>
                        </div>
						<div class="form-group">
                          <label for="">New Password</label><input required="required" name="newpass" class="form-control" type="password">
                          <div class="help-block form-text with-errors form-control-feedback"></div>
                        </div>
                        <div class="form-group">
                          <label for="">Re-New Password</label><input required="required" name="renewpass" class="form-control" type="password">
                          <div class="help-block form-text with-errors form-control-feedback"></div>
                        </div>
                        <div class="form-buttons-w">
                          <button class="btn btn-primary" type="submit"> Change Password</button>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
              </div>


            </div>
            
          </div>
