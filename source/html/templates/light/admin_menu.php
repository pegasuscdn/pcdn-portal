        <div class="menu-w color-scheme-light color-style-default menu-position-top menu-layout-compact sub-menu-style-over sub-menu-color-bright selected-menu-color-bright menu-activated-on-hover menu-has-selected-link">
          <div class="logo-w">
            <a class="logo" href="#">
              <div class="logo-label">
                PEGASUSTECH CDN Admin
              </div>
            </a>
          </div>
          <h1 class="menu-page-header">
            Page Header
          </h1>
          <ul class="main-menu">
            <li class="sub-header">
              <span>Layouts</span>
            </li>
            <li class="<?php if ($active_menu == 'dashboard') echo 'selected'; ?>">
              <a href="admin_dashboard.php">
                <div class="icon-w">
                  <div class="os-icon os-icon-grid-squares-22"></div>
                </div>
                <span>Dashboard</span>
              </a>
            </li>
            <li class="<?php if ($active_menu == 'customers') echo 'selected'; ?> has-sub-menu">
              <a href="#">
                <div class="icon-w">
                  <div class="os-icon os-icon-user-male-circle2"></div>
                </div>
                <span>Customers</span></a>
              <div class="sub-menu-w">
                <div class="sub-menu-header">
                  Customers
                </div>
                <div class="sub-menu-icon">
                  <i class="os-icon os-icon-user-male-circle2"></i>
                </div>
                <div class="sub-menu-i">
                  <ul class="sub-menu">
                  	<li>
                      <a href="customer_list.php">List</a>
                    </li>
		<?php if (  $_permistAdd == 2){ ?>	
                    <li>
                      <a href="customer_add.php">Add New</a>
                    </li>
		<?php } ?>
                  </ul>
                </div>
              </div>
            </li>
            <li class="<?php if ($active_menu == 'admistrators') echo 'selected'; ?> has-sub-menu">
              <a href="cdn_resources.php">
                <div class="icon-w">
                  <div class="os-icon os-icon-crown"></div>
                </div>
                <span>Administrators</span></a>
              <div class="sub-menu-w">
                <div class="sub-menu-header">
                  Administrators
                </div>
                <div class="sub-menu-icon">
                  <i class="os-icon os-icon-crown"></i>
                </div>
                <div class="sub-menu-i">
                  <ul class="sub-menu">
                    <li>
                      <a href="admin_two_step.php">Two Step Auth</a>
                    </li>
					<li>
                      <a href="admin_list.php">List</a>
                    </li>
                    <li>
                      <a href="admin_add.php">Add New</a>
                    </li>
                  </ul>
                </div>
              </div>
            </li>
            <li class="<?php if ($active_menu == 'cdn_resources') echo 'selected'; ?> has-sub-menu">
              <a href="cdn_resources.php">
                <div class="icon-w">
                  <div class="os-icon os-icon-hierarchy-structure-2"></div>
                </div>
                <span>CDN Resources</span></a>
              <div class="sub-menu-w">
                <div class="sub-menu-header">
                  CDN Resources
                </div>
                <div class="sub-menu-icon">
                  <i class="os-icon os-icon-hierarchy-structure-2"></i>
                </div>
                <div class="sub-menu-i">
                  <ul class="sub-menu">
                    <li>
                      <a href="cdn_resources_list.php">List</a>
                    </li>
                    <li>
                      <a href="cdn_resources_add.php">Add New</a>
                    </li>
                  </ul>
                </div>
              </div>
            </li>
            <li class="<?php if ($active_menu == 'cdn_storages') echo 'selected'; ?> has-sub-menu">
              <a href="cdn_resources.php">
                <div class="icon-w">
                  <div class="os-icon os-icon-coins-4"></div>
                </div>
                <span>CDN Storages</span></a>
              <div class="sub-menu-w">
                <div class="sub-menu-header">
                  CDN Storages
                </div>
                <div class="sub-menu-icon">
                  <i class="os-icon os-icon-coins-4"></i>
                </div>
                <div class="sub-menu-i">
                  <ul class="sub-menu">
                    <li>
                      <a href="cdn_storages_list.php">List</a>
                    </li>
                    <li>
                      <a href="cdn_storages_add.php">Add New</a>
                    </li>
                  </ul>
                </div>
              </div>
            </li>
            <li class="<?php if ($active_menu == 'billing') echo 'selected'; ?> has-sub-menu">
              <a href="cdn_resources.php">
                <div class="icon-w">
                  <div class="os-icon os-icon-wallet-loaded"></div>
                </div>
                <span>Billing</span></a>
              <div class="sub-menu-w">
                <div class="sub-menu-header">
                  Billing
                </div>
                <div class="sub-menu-icon">
                  <i class="os-icon os-icon-wallet-loaded"></i>
                </div>
                <div class="sub-menu-i">
                  <ul class="sub-menu">
                    <li>
                      <a href="billing_list.php">List Invoices</a>
                    </li>
                    <li>
                      <a href="billing_list.php">Details Invoice</a>
                    </li>
                  </ul>
                </div>
              </div>
            </li>
            <li class="<?php if ($active_menu == 'settings') echo 'selected'; ?> has-sub-menu">
              <a href="cdn_resources.php">
                <div class="icon-w">
                  <div class="os-icon os-icon-robot-2"></div>
                </div>
                <span>Settings</span></a>
              <div class="sub-menu-w">
                <div class="sub-menu-header">
                  Settings
                </div>
                <div class="sub-menu-icon">
                  <i class="os-icon os-icon-robot-2"></i>
                </div>
                <div class="sub-menu-i">
                  <ul class="sub-menu">
                    <li>
                      <a href="#">Edge Servers</a>
                    </li>
                    <li>
                      <a href="#">Traffic Controll</a>
                    </li>
                  </ul>
                </div>
              </div>
            </li>
            
            <li class="<?php if ($active_menu == 'logout') echo 'selected'; ?>">
              <a href="admin_logout.php">
                <div class="icon-w">
                  <div class="os-icon os-icon-signs-11"></div>
                </div>
                <span>Logout</span>
              </a>
            </li>
          </ul>
        </div>
