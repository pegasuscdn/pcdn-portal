          <div class="content-i">
            <div class="content-box">
              <div class="row">
                <div class="col-sm-12 col-xxxl-9">
                  <div class="element-wrapper">
                    <div class="element-actions">
                    </div>
                    <h6 class="element-header">
                      Report Bandwidth
                    </h6>

                    <div class="element-box">
                      <div class="os-tabs-w">
                        <div class="os-tabs-controls">
                          <ul class="nav nav-tabs smaller">
                            <li class="nav-item">
					 <form class="form-inline justify-content-sm-end">
                        <select class="form-control form-control-sm rounded" id="bw_select_resources">
                          <option value="0">All Resources</option>
                          <?php
                          foreach ($cdn_resources as $id => $name) {
                            $selected = ($hostid == $id) ? 'selected' : '';
                            echo "<option value='{$id}' {$selected}>{$name}</option>";
                          }
                          ?>
                        </select>
                      </form>

                            </li>
                          </ul>
                          <ul class="nav nav-pills smaller">
					  <li class="nav-item">
                              <a class="nav-link changeTimeTemplate" data-toggle="tab" href="#" rel="1" >Last 15"</a>
                            </li>
                            <li class="nav-item">
                              <a class="nav-link  changeTimeTemplate" data-toggle="tab" href="#"  rel="2" >Last 30"</a>
                            </li>
                            <li class="nav-item">
                              <a class="nav-link  changeTimeTemplate" data-toggle="tab" href="#"  rel="3" >Last 1h</a>
                            </li>
                            <li class="nav-item">
                              <a class="nav-link  changeTimeTemplate" data-toggle="tab" href="#"  rel="4" >Last 12h</a>
                            </li>
                            <li class="nav-item">
                              <a class="nav-link active  changeTimeTemplate" data-toggle="tab" href="#"  rel="5" >Last 24h</a>
                            </li>
                            <li class="nav-item">
                              <a class="nav-link  changeTimeTemplate" data-toggle="tab" href="#"  rel="6" >Last 7 days</a>
                            </li>
                            <li class="nav-item">
                              <a class="nav-link  changeTimeTemplate" data-toggle="tab" href="#"  rel="7" >Last 30 days</a>
                            </li>
                            <li class="nav-item">
                              <a class="nav-link  changeTimeTemplate" data-toggle="tab" href="#"  rel="9" >Last 365 days</a>
                            </li>
                          </ul>
                        </div>
			<div>
			<div class="os-tabs-controls">
                          <ul class="nav nav-tabs smaller">
                            <li class="nav-item">
                              <a class="nav-link" data-toggle="tab" href="#">&nbsp;</a>
                            </li>
                          </ul>
                          <ul class="nav nav-pills smaller">
                            <li class="nav-item">   <div class="form-group"><label for="startDate">Start Date</label>
                              <input type="text" class="form-control" id="startDate" value="01/01/2018">
				</div>
                            </li>
				<li> &nbsp; </li>
                            <li class="nav-item">
				<div class="form-group"> 
						<label for="endDate">End Date</label>
                              <input type="text" class="form-control" id="endDate" value="01/01/2018">
				 </div>
                            </li>
			<li> &nbsp; </li>	
                            <li class="nav-item ">
				<div class="form-group"><label>  	&nbsp;</label>		 
                              <a class="form-control  active changeTimeTemplate" style=" background-color:#047bf8;color:white;"  data-toggle="tab" href="#"  rel="99" > View</a>
                	</div>            
			</li>
                          </ul>
                        </div>
			</div>
                        <div class="tab-content">
                          <div class="tab-pane active" id="tab_overview">
                            <div class="el-tablo bigger">
                              <div class="label">
                                Bandwidth (GB)
                              </div>
                            </div>
                            <div class="el-chart-w">
                              <canvas height="170px" id="bandwidthTodayChart" width="600px"></canvas>
                            </div>
			<div class="el-tablo bigger">
				<div class="label" id="idminmaxagv">
				Total: 0000 - Avg: 0000 - Max: 0000 - Min: 0000 
				<br>Domestic: 0.00 - International: 0.00
				</div>
		
			</div>			
                          </div>
                          <div class="tab-pane" id="tab_sales"></div>
                          <div class="tab-pane" id="tab_conversion"></div>
                        </div>
                      </div>
                    </div>


                    <!--div class="element-box">
                      <div class="os-tabs-w">
                        <div class="os-tabs-controls">
                          <ul class="nav nav-tabs smaller">
                            <li class="nav-item">
                              <a class="nav-link" data-toggle="tab" href="#">&nbsp;</a>
                            </li>
                          </ul>
                          <ul class="nav nav-pills smaller d-none d-md-flex">
                            <li class="nav-item">
                              <a class="nav-link active" data-toggle="tab" href="#">Last Year</a>
                            </li>
                          </ul>
                        </div>
                        <div class="tab-content">
                          <div class="tab-pane active" id="tab_overview">
                            <div class="el-tablo bigger">
                              <div class="label">
                                Bandwidth in Last Year (GB)
                              </div>
                            </div>
                            <div class="el-chart-w">
                              <canvas height="150px" id="bandwidthLastYearChart" width="600px"></canvas>
                            </div>
                          </div>
                          <div class="tab-pane" id="tab_sales"></div>
                          <div class="tab-pane" id="tab_conversion"></div>
                        </div>
                      </div>
                    </div -->

                  </div>
                </div>
              </div>
            </div>
            <!--------------------
            START - Sidebar
            -------------------->
 <?php   if (  0 && strrpos($_SERVER['HTTP_HOST'] , 'pegasustech') ) { ?>
            <div class="content-panel">
              <div class="content-panel-close">
                <i class="os-icon os-icon-close"></i>
              </div>
              <div class="element-wrapper">
                <h6 class="element-header">
                  Quick Links
                </h6>
                <div class="element-box-tp">
                  <div class="el-buttons-list full-width">
                    <a class="btn btn-white btn-sm" href="cdn_resources.php?action=add"><i class="os-icon os-icon-delivery-box-2"></i><span>Add new CDN</span></a>
                    <a class="btn btn-white btn-sm" href="#"><i class="os-icon os-icon-window-content"></i><span>Add new Storage</span></a>
                    <a class="btn btn-white btn-sm" href="logout.php"><i class="os-icon os-icon-wallet-loaded"></i><span>Logout</span></a>
                  </div>
                </div>
              </div>
            </div>
<?php } ?>
            <!--------------------
            END - Sidebar
            -------------------->
          </div>
