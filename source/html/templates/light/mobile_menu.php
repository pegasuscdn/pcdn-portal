        <div class="menu-mobile menu-activated-on-click color-scheme-dark">
          <div class="mm-logo-buttons-w">
            <a class="mm-logo" href="#"><span>

<?php
                $fileLogo='Viettel CDN Portal';
                if ( strrpos($_SERVER['HTTP_HOST'] , 'pegasustech'))
                   $fileLogo='PEGASUSTECH CDN Portal';
		if ( strrpos($_SERVER['HTTP_HOST'] , 'cdnservice'))
                    $fileLogo='CDN SERVICE PORTAL';
                //$fileLogo='Viettel CDN Portal';

                echo  $fileLogo; 
                ?>
</span></a>
            <div class="mm-buttons">
              <div class="content-panel-open">
                <div class="os-icon os-icon-grid-circles"></div>
              </div>
              <div class="mobile-menu-trigger">
                <div class="os-icon os-icon-hamburger-menu-1"></div>
              </div>
            </div>
          </div>
          <div class="menu-and-user">
            <!--------------------
            START - Mobile Menu List
            -------------------->
            <ul class="main-menu">
              <li class="has-sub-menu">
                <a href="report.php">
                  <div class="icon-w">
                    <div class="os-icon os-icon-layout"></div>
                  </div>
                  <span>Report</span></a>
                <ul class="sub-menu">
                  <li>
                    <a href="report.php">Overview</a>
                  </li>
                  <li>
                    <a href="report.php?type=bw">Bandwidth</a>
                  </li>
                  <!--li>
                    <a href="report.php?type=costs">Costs</a>
                  </li-->
                  <li>
                    <a href="report.php?type=hit">Hits/Misses</a>
                  </li>
                  <li>
                    <a href="report.php?type=code">Response Code</a>
                  </li>
				  <li>
                    <a href="report.php?type=visitor">Unique Visitors</a>
                  </li>
		 <!--li>
                      <a href="report.php?type=speed">Network Speed</a>
                    </li-->    	
                </ul>
              </li>
              
              <li class="has-sub-menu">
                <a href="cdn_resources.php">
                  <div class="icon-w">
                    <div class="os-icon os-icon-package"></div>
                  </div>
                  <span>CDN</span></a>
                <ul class="sub-menu">
                  <li>
                    <a href="cdn_resources.php">CDN Resources</a>
                  </li>
                  <li>
                    <a href="cdn_storages.php">CDN Storages</a>
                  </li>
                </ul>
              </li>
	<?php   if ( strrpos($_SERVER['HTTP_HOST'] , 'pegasustech')) { ?>
              <li class="has-sub-menu">
                <a href="invoice.php">
                  <div class="icon-w">
                    <div class="os-icon os-icon-edit-32"></div>
                  </div>
                  <span>Billing</span></a>
                <ul class="sub-menu">
                  <li>
                    <a href="invoice.php">Invoice</a>
                  </li>
                </ul>
              </li>
<?php } ?>
              <li class="has-sub-menu">
                <a href="profile.php">
                  <div class="icon-w">
                    <div class="os-icon os-icon-users"></div>
                  </div>
                  <span>Account</span></a>
                <ul class="sub-menu">
                  <li>
                    <a href="profile.php">Profile</a>
                  </li>
                  <li>
                    <a href="change_password.php">Change Password</a>
                  </li>
                  <li>
                    <a href="twostep.php">Two-Step Authentication</a>
                  </li>
                </ul>
              </li>
              <li class="has-sub-menu">
                <a href="logout.php">
                  <div class="icon-w">
                    <div class="os-icon os-icon-signs-11"></div>
                  </div>
                  <span>Logout</span>
                </a>
                <ul class="sub-menu">
                  <li><a href="logout.php">Logout</a></li>
                </ul>
              </li>
            </ul>
          </div>
        </div>
