
<?php

// load the itrAcmeClient library
require 'src/itr-acme-client.php';

// load a simple logger
require 'simplelogger.php';

try {

  // Create the itrAcmeClient object
  $iac = new itrAcmeClient();

  // Activate debug mode, we automatically use staging endpoint in testing mode
  $iac->testing = true;

  // The root directory of the certificate store
  $iac->certDir = '/usr/local/nginx/html/cnit.codedemo.com';
  // The root directory of the account store
  $iac->certAccountDir = 'accounts';
  // This token will be attached to the $certAccountDir
  $iac->certAccountToken = 'itronic';

  // The certificate contact information
  $iac->certAccountContact = [
    'mailto:quangluan0909vn@gmail.com',
    'tel:+84904595773'
  ];

  $iac->certDistinguishedName = [
    /** @var string The certificate ISO 3166 country code */
    'countryName'            => 'VN',
    'stateOrProvinceName'    => 'municipality',
    'localityName'           => 'municipality',
    'organizationName'       => 'Cnit',
    'organizationalUnitName' => 'Cnit',
    'street'                 => '123 truong dinh'
  ];

  $iac->webRootDir          = '/usr/local/nginx/html/cnit.codedemo.com';
  $iac->appendDomain        = false;
  $iac->appendWellKnownPath = true;

  // A \Psr\Log\LoggerInterface or null The logger to use
  // At the end of this file we have as simplePsrLogger implemntation
  $iac->logger = new simplePsrLogger;

  // Initialise the object
  $iac->init();

  // Create an account if it doesn't exists
  $iac->createAccount();

  // The Domains we want to sign
  $domains = [
    'cnit1.dulichtantai.com'
  ];

  // Sign the Domains and get the certificates
  $pem = $iac->signDomains($domains);

  // Output the certificate informatione
 echo"<pre>"; print_r($pem);echo"</pre>";

} catch (\Throwable $e) {
  echo"<pre>";print_r($e->getMessage());echo"</pre>";
  echo"<pre>";print_r($e->getTraceAsString());echo"</pre>";
}

