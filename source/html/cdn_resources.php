<?php
require_once 'include/config.php';
require_once 'include/global.php';
require_once 'include/cdn.php';
require_once 'include/ssl.php';
require_once 'include/config_country.php';
check_session();

$active_menu = 'cdn';
include TEMPLATE_PATH.'/main_header.php';

if($_SESSION['portal']['expired_date'] || ($_SESSION['portal']['limited_bandwidth']>-1)){
$cdate=date("Y-m-d");
//if($cdate>=$_SESSION['portal']['expired_date'])
	include TEMPLATE_PATH.'/acount_expired.php';
	//include TEMPLATE_PATH.'/main_footer.php';
	//exit;
}
$action = trim($_GET['action']);

if ($action == 'add') {
	if($_SESSION['portal']['expired_date'] || ($_SESSION['portal']['limited_bandwidth']>-1)){
		if(($cdate>=$_SESSION['portal']['expired_date']) || ($_SESSION['portal']['limited_bandwidth']==0))
        	//include TEMPLATE_PATH.'/acount_expired.php';
        	{include TEMPLATE_PATH.'/main_footer.php';
        	exit;}
	}

	if ($_SERVER['REQUEST_METHOD'] == 'POST') {

		$errors = array();
		$cdn_name = trim($_POST['cdn_name']);
		$cdn_https = 1;//intval($_POST['cdn_https']);
		$cdn_type = intval($_POST['cdn_type']);
		$cdn_origin = intval($_POST['cdn_origin']);
		$my_origin_type = trim($_POST['my_origin_type']);
		$my_origin_domain = trim($_POST['my_origin_domain']);

		$cdn_bucket = trim($_POST['cdn_bucket']);
		$cdn_access_key = strtoupper(trim($_POST['cdn_access_key']));
		$cdn_secret_key = trim($_POST['cdn_secret_key']);

		if( $cdn_origin ==2 ) $cdn_type = 1;

		if (empty($cdn_name) OR empty($cdn_type) OR empty($cdn_origin) ) {
			$errors[] = "Fields (*) are required";
		}
		if ($cdn_type != 1 AND $cdn_type != 2 AND $cdn_type != 3) {
			$errors[] = "CDN type is invalid";
		}
		if ($cdn_origin != 1 AND $cdn_origin != 2) {
			$errors[] = "CDN origin is invalid";
		}
		
		if ($cdn_origin == 1) {
			if ($my_origin_type != 'http' AND $my_origin_type != 'https') {
				$errors[] = "Origin protocol is invalid";
			}
			if (empty($my_origin_domain)) {
				$errors[] = "Origin domain is invalid";
			} else {
				$array = explode(':', $my_origin_domain);
				$domain = $array[0];
				if (isset($array[1])) {
					$port = intval($array[1]);
					if ($port < 1 OR $port > 60000) {
						$errors[] = "Origin port is invalid";	
					}
				}

				if (!filter_var($domain, FILTER_VALIDATE_DOMAIN, FILTER_FLAG_HOSTNAME) OR !preg_match('/\.[a-z0-9]+$/i', $domain)) {
					$errors[] = "Origin domain is invalid";
				}
				if ($cdn_https_domain && $cdn_https_domain == $domain) {
					$errors[] = "CDN private domain must be not same origin domain";
				}
			}
		} else if ($cdn_origin == 2) {
			// storage
			if ($cdn_type != 1 ) {
                        	$errors[] = "CDN type is invalid";
                	}
			if (empty($cdn_bucket) OR empty($cdn_access_key) OR empty($cdn_secret_key)) {
                        	$errors[] = "Fields (*) are required";
                        }
			if(!preg_match('/^[\w-]+$/', $cdn_bucket)) $errors[] ="Bucket is invalid";
			 if(!preg_match('/^[0-9a-zA-Z\-]{20}$/', $cdn_access_key)) $errors[] ="Access Key is invalid";
			 if(!preg_match('/^[0-9a-zA-Z\-]{40}$/', $cdn_secret_key)) $errors[] ="Secret Key is invalid";	

		}

		// insert db
		
		if (count($errors) == 0) {

			if ($cdn_https == 3) {
				$hostname = strtolower($cdn_https_domain);
				$cdn_cname = get_user_id().rand(100000,999999);
				$cdn_cname = $cdn_cname.'.'.CDN_DOMAIN;
			} else {
				// userid-randnum
				$hostname = get_user_id().rand(100000,999999);
				$hostname = $hostname.'.'.CDN_DOMAIN;
				while(checkExistedCnameSslDomain($hostname)){
					$hostname = get_user_id().rand(100000,999999);	
					$hostname = $hostname.'.'.CDN_DOMAIN;
					
				}
				$cdn_cname = $hostname;
			}
			
			$cdn_https = ($cdn_https > 1) ? 1 : 0;

			if ($cdn_https == 1) {
				$ssl = array(
					'cert' => 'pegasuscdncom.crt',
					'key' => 'pegasuscdncom.key'
				);
			} else {
				$ssl = array();
			}
			$hostname_uid = cdn_get_hostid($hostname);
			$data = array(
				'id' => $hostname_uid,//cdn_get_hostid($hostname),
				'name' => $cdn_name,
				'cdn_domain' => $hostname,
				'type' => $cdn_type,
				'origin' => $my_origin_type.'://'.$domain,
				'userid' => get_user_id(),
			);
			
		
			if ($port) {
				$data['origin'] = $data['origin'].':'.$port;
			}
			// storage
			$storage=[];
			$str_storage = '';	
			if($cdn_origin == 2){
				$data['origin'] = 'https://'.$cdn_bucket.'.s3.pegasus-storage.com';
				//$data['origin'] = '';
				$storage['bucket']=$cdn_bucket;
				$storage['access_key']=$cdn_access_key; 
				$storage['secret_key']=$cdn_secret_key;
				 $str_storage = json_encode( $storage, JSON_UNESCAPED_SLASHES );	
			}
			//print_r($data);
			//echo cdn_get_hostid($hostname); exit;
			if (insert_cdn_resource($data,$str_storage)) {
				$_SESSION['cdn_resources_new'] = 1;
				$_SESSION['ss_cdn_activetab']='tab_overview';
				redirect('cdn_resources.php?action=edit&id='.$hostname_uid);
			} else {
				$errors[] = "CDN is existed, try again.";
			}
		}
	}

	if (empty($cdn_origin)) $cdn_origin = 1;

	include TEMPLATE_PATH.'/cdn_add_new.php';

} else if ($action == 'edit') {
     if($_SESSION['portal']['expired_date'] || ($_SESSION['portal']['limited_bandwidth']>-1)){
                if(($cdate>=$_SESSION['portal']['expired_date']) || ($_SESSION['portal']['limited_bandwidth']==0))
                //include TEMPLATE_PATH.'/acount_expired.php';
                {include TEMPLATE_PATH.'/main_footer.php';
                exit;}
        }

	$id = trim($_GET['id']);
	if (empty($id)) {
		redirect('cdn_resources.php');
	}

	$resource = get_cdn_resource(get_user_id(), $id);
	$cnames = json_decode($resource['cname']);
	$mobileURL = $resource['cdn_redirect'];
	$cdn_platform = $resource['cdn_platform'];
	if (empty($resource)) {
		redirect('cdn_resources.php');
	}

	if (isset($_SESSION['cdn_resources_new'])) {
		$message = "Your CDN resource has been added success and will be avaliable on next minute. You can customize the CDN in bellow form";
		unset($_SESSION['cdn_resources_new']);
	}

	$acl = json_decode($resource['acl']);
	$cname = select_cdn_cname_resources($resource['id']); 		//json_decode($resource['user_cname']);
	$http_header = select_cdn_http_header_resources($resource['id']);
	$arrListHttpHeader= get_list_http_header();
	$referer = json_decode($resource['user_referer']);
	$arrAddonSSL = select_ssl_resources(get_user_id());
	$tabActive='tab_overview';
	if (isset($_SESSION['ss_cdn_activetab'])) {
		if($_SESSION['ss_cdn_activetab'])
			$tabActive=$_SESSION['ss_cdn_activetab'];
		$_SESSION['ss_cdn_activetab']='tab_overview';
	}
	//echo $tabActive.$_SESSION['ss_cdn_activetab'];
	$storageResource=0;
	if($resource['storage'])
		$storageResource=1;
	//include TEMPLATE_PATH.'/cdn_edit_storage.php';	
	//else	
	include TEMPLATE_PATH.'/cdn_edit.php';

} else {
	$resources = select_cdn_resources2(get_user_id());

	include TEMPLATE_PATH.'/cdn_resources.php';
}

include TEMPLATE_PATH.'/main_footer.php';
