<?php

define('APP_PATH', '/var/www/html');
require_once APP_PATH.'/include/config.php';
require_once APP_PATH.'/include/global.php';
require_once APP_PATH.'/include/cdn.php';


$time = strtotime("-1 month");
$year = date('Y', $time);
$month = date('n', $time);

$sql = "select hostid, sum(bandwidth) as bandwidth_in_mb from cdn.cdn_bw_hourly where year={$year} and month={$month} group by hostid";
$data = execute_datalog($sql);

$costs = array();
foreach ($data as $value) {
	$costs['costs_by_month_'.$value[0]] = round($value[1]/1000)*$CONFIG['cost_by_day'];
}

$cdn_resources = select_cdn_resources(0);
foreach ($cdn_resources as $key => $value) {
	$costs['costs_by_month_'.$key] = array(
		'id' => $key,
		'hostname' => $value,
		'time' => $time,
		'costs' => isset($costs['costs_by_month_'.$key]) ? intval($costs['costs_by_month_'.$key]) : 0
	);
}

reset($costs);
foreach ($costs as $key => $value) {
	if (!is_array($value)) {
		unset($costs[$key]);
	}
}


$redis = new Redis();
if (!$redis->connect($CONFIG['redis']['host'], $CONFIG['redis']['port'], 5)) {
	exit('ERROR connect redis failed');
}
$redis->select($CONFIG['redis']['db']);
$redis->setOption(Redis::OPT_SERIALIZER, Redis::SERIALIZER_NONE);

$redis_keys = $redis->mGet(array_keys($costs));

foreach ($redis_keys as $value) {
	$value = json_decode($value);
	$redis_keys['costs_by_month_'.$value[0]->id] = $value;
}

$costs_by_month = array();

foreach ($costs as $key => $value) {
	if (!isset($redis_keys[$key])) {
		$redis_keys[$key] = array();
	}
	$costs_by_month[$key] = json_costs($redis_keys[$key], $value);
}

$redis->mSet($costs_by_month);
$redis->close();
