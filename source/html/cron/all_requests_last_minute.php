<?php

define('APP_PATH', '/var/www/html');
require_once APP_PATH.'/include/config.php';
require_once APP_PATH.'/include/global.php';
require_once APP_PATH.'/include/cdn.php';

//$current_requests = get_requests_last_minute();

$time = time()-60;
$year = date('Y', $time);
$month = date('n', $time);
$day = date('j', $time);
$hour = date('G', $time);
$minute = intval(date('i', $time));

$sql = "select hostid, count(*) as requests from cdn.cdn_access_log_all where year={$year} and month={$month} and day={$day} and hour={$hour} and minute={$minute} group by hostid";
$data = execute_datalog($sql);
$current_requests = array();
foreach ($data as $value) {
	$current_requests['requests_last_minute_'.$value[0]] = $value[1];
}

$cdn_resources = select_cdn_resources(0);

foreach ($cdn_resources as $key => $value) {
	$current_requests['requests_last_minute_'.$key] = array(
		'id' => $key,
		'hostname' => $value,
		'time' => $time,
		'requests' => isset($current_requests['requests_last_minute_'.$key]) ? intval($current_requests['requests_last_minute_'.$key]) : 0
	);
}

reset($current_requests);
foreach ($current_requests as $key => $value) {
	if (!is_array($value)) {
		unset($current_requests[$key]);
	}
}


$redis = new Redis();
if (!$redis->connect($CONFIG['redis']['host'], $CONFIG['redis']['port'], 5)) {
	exit('ERROR connect redis failed');
}
$redis->select($CONFIG['redis']['db']);
$redis->setOption(Redis::OPT_SERIALIZER, Redis::SERIALIZER_NONE);

$redis_keys = $redis->mGet(array_keys($current_requests));
$redis_keys = redis_requests_last_minute($redis_keys);
$requests_last_minute = array();

foreach ($current_requests as $key => $value) {
	if (!isset($redis_keys[$key])) {
		$redis_keys[$key] = array();
	}
	$requests_last_minute[$key] = update_requests_last_minute($redis_keys[$key], $value);
}


$redis->mSet($requests_last_minute);

$redis->close();