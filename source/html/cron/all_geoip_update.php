<?php

define('APP_PATH', '/var/www/html');
require_once APP_PATH.'/include/config.php';
require_once APP_PATH.'/include/global.php';
require_once APP_PATH.'/include/cdn.php';


$time = strtotime("-1 month");
$date = date('Y-m-d', $time);

$sql = "select hostid,country,sum(visits) as visits from cdn.cdn_geoip_daily where logdate >= toDateTime('{$date} 00:00:00') group by hostid,country";
$result = execute_datalog($sql);

$data = array();
foreach ($result as $value) {
	if (isset($data[$value[0]][$value[1]])) {
		$data['geoip_'.$value[0]][$value[1]] += $value[2];
	}
	$data['geoip_'.$value[0]][$value[1]] = $value[2];
}

$data2 = array();
$cdn_resources = select_cdn_resources(0);
foreach ($cdn_resources as $key => $value) {
	if (isset($data['geoip_'.$key])) {
		foreach ($data['geoip_'.$key] as $k2 => $v2) {
			$data2['geoip_'.$key][] = array(strtolower($k2),$v2);
		}
	}
}

reset($data2);
foreach ($data2 as $key => $value) {
	$data2[$key] = json_encode($value);
}

$redis = new Redis();
if (!$redis->connect($CONFIG['redis']['host'], $CONFIG['redis']['port'], 5)) {
	exit('ERROR connect redis failed');
}
$redis->select($CONFIG['redis']['db']);
$redis->setOption(Redis::OPT_SERIALIZER, Redis::SERIALIZER_NONE);

$redis->mSet($data2);

$redis->close();
