<?php

define('APP_PATH', '/var/www/html');
require_once APP_PATH.'/include/config.php';
require_once APP_PATH.'/include/global.php';
require_once APP_PATH.'/include/cdn.php';

$time = time()-3600;
$year = date('Y', $time);
$month = date('n', $time);
$day = date('j', $time);
$hour = date('G', $time);

$query = "insert into cdn.cdn_code_hourly select now()-3600,hostid,year,month,day,hour,code_200,code_4xx,code_5xx from (select hostid,year,month,day,hour,code_200,code_4xx from (select hostid,year,month,day,hour,count(code) as code_200 from cdn.cdn_access_log_all where code=200 and year={$year} and month={$month} and day={$day} and hour={$hour} group by hostid,year,month,day,hour) any left join (select hostid,year,month,day,hour,count(code) as code_4xx from cdn.cdn_access_log_all where code>=400 and code<=499 and year={$year} and month={$month} and day={$day} and hour={$hour} group by hostid,year,month,day,hour) using hostid) any left join (select hostid,year,month,day,hour,count(code) as code_5xx from cdn.cdn_access_log_all where code>=500 and code<=599 and year={$year} and month={$month} and day={$day} and hour={$hour} group by hostid,year,month,day,hour) using hostid";

$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, $CONFIG['datalog_url']."/?user={$CONFIG['datalog']['user']}&password={$CONFIG['datalog']['password']}&query=".urlencode($query));
//curl_setopt($ch, CURLOPT_ENCODING, "");
curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-type: text/plain', 'Content-length: 0'));
curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
curl_setopt($ch, CURLOPT_TIMEOUT, 20);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, false);
curl_setopt($ch, CURLOPT_POST, true);
curl_exec($ch);
$code1 = curl_getinfo($ch, CURLINFO_HTTP_CODE);

// replicate
curl_setopt($ch, CURLOPT_URL, $CONFIG['datalog_url2']."/?user={$CONFIG['datalog']['user']}&password={$CONFIG['datalog']['password']}&query=".urlencode($query));
curl_exec($ch);
$code2 = curl_getinfo($ch, CURLINFO_HTTP_CODE);
