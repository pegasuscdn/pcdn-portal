<?php

define('APP_PATH', '/var/www/html');
require_once APP_PATH.'/include/config.php';
require_once APP_PATH.'/include/global.php';
require_once APP_PATH.'/include/cdn.php';

/*
$redis = new Redis();
if (!$redis->connect($CONFIG['redis']['host'], $CONFIG['redis']['port'], 5)) {
	exit('ERROR connect redis failed');
}
$redis->select($CONFIG['redis']['db']);
$redis->setOption(Redis::OPT_SERIALIZER, Redis::SERIALIZER_NONE);

debug($redis->delete($redis->keys('bandwidth_last_month_*')));
*/


$data = get_bandwidth_last_month();
$cdn_resources = select_cdn_resources(0);

// depend on running time
$now = time()-300;

foreach ($cdn_resources as $key => $value) {
	$data['bandwidth_last_month_'.$key] = array(
		'id' => $key,
		'hostname' => $value,
		'time' => $now,
		'bandwidth' => isset($data['bandwidth_last_month_'.$key]) ? intval($data['bandwidth_last_month_'.$key]) : 0
	);
}

reset($data);
foreach ($data as $key => $value) {
	if (!is_array($value)) {
		unset($data[$key]);
	}
}

$redis = new Redis();
if (!$redis->connect($CONFIG['redis']['host'], $CONFIG['redis']['port'], 5)) {
	exit('ERROR connect redis failed');
}
$redis->select($CONFIG['redis']['db']);
$redis->setOption(Redis::OPT_SERIALIZER, Redis::SERIALIZER_NONE);

$redis_keys = $redis->mGet(array_keys($data));
$redis_keys = redis_bandwidth_last_month($redis_keys);

$bandwidth_last_month = array();
foreach ($data as $key => $value) {
	if (!isset($redis_keys[$key])) {
		$redis_keys[$key] = array();
	}
	$bandwidth_last_month[$key] = update_bandwidth_last_month($redis_keys[$key], $value);
}

$redis->mSet($bandwidth_last_month);
$redis->close();
