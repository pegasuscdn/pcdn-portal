<?php

define('APP_PATH', '/var/www/html');
require_once APP_PATH.'/include/config.php';
require_once APP_PATH.'/include/global.php';
require_once APP_PATH.'/include/cdn.php';

$time = time()-3600;
$year = date('Y', $time);
$month = date('n', $time);
$day = date('j', $time);
$hour = date('G', $time);

$sql = "select hostid,sum(bandwidth) as bandwidth_in_mb from cdn.cdn_bw_hourly where year={$year} and month={$month} and day={$day} and hour={$hour} group by hostid";
$result = execute_datalog($sql);

$data = array();
foreach ($result as $value) {
	$data['bandwidth_last_hour_'.$value[0]] = $value[1];
}


$cdn_resources = select_cdn_resources(0);

foreach ($cdn_resources as $key => $value) {
	$data['bandwidth_last_hour_'.$key] = array(
		'id' => $key,
		'hostname' => $value,
		'time' => $time,
		'bandwidth' => isset($data['bandwidth_last_hour_'.$key]) ? intval($data['bandwidth_last_hour_'.$key]) : 0
	);
}

reset($data);
foreach ($data as $key => $value) {
	if (!is_array($value)) {
		unset($data[$key]);
	}
}

$redis = new Redis();
if (!$redis->connect($CONFIG['redis']['host'], $CONFIG['redis']['port'], 5)) {
	exit('ERROR connect redis failed');
}
$redis->select($CONFIG['redis']['db']);
$redis->setOption(Redis::OPT_SERIALIZER, Redis::SERIALIZER_NONE);

$redis_keys = $redis->mGet(array_keys($data));
$redis_keys = redis_bandwidth_last_hour($redis_keys);
$bandwidth_last_hour = array();

//debug($redis_keys);

foreach ($data as $key => $value) {
	if (!isset($redis_keys[$key])) {
		$redis_keys[$key] = array();
	}
	$bandwidth_last_hour[$key] = update_bandwidth_last_hour($redis_keys[$key], $value);
}

$redis->mSet($bandwidth_last_hour);
$redis->close();
