<?php

define('APP_PATH', '/var/www/html');
require_once APP_PATH.'/include/config.php';
require_once APP_PATH.'/include/global.php';
require_once APP_PATH.'/include/cdn.php';

// insert new data
$result = calculate_code_by_hour(1);


$data = get_code_last_hour();
$cdn_resources = select_cdn_resources(0);

// depend on running time
$now = time()-180;

foreach ($cdn_resources as $key => $value) {
	if (isset($data['code_last_hour_'.$key])) {
		list($code_200,$code_4xx,$code_5xx) = explode(",", $data['code_last_hour_'.$key]);
	} else {
		$code_200 = 0;
		$code_4xx = 0;
		$code_5xx = 0;
	}
	$data['code_last_hour_'.$key] = array(
		'id' => $key,
		'hostname' => $value,
		'time' => $now,
		'code_200' => $code_200,
		'code_4xx' => $code_4xx,
		'code_5xx' => $code_5xx,
	);
}

reset($data);
foreach ($data as $key => $value) {
	if (!is_array($value)) {
		unset($data[$key]);
	}
}

$redis = new Redis();
if (!$redis->connect($CONFIG['redis']['host'], $CONFIG['redis']['port'], 5)) {
	exit('ERROR connect redis failed');
}
$redis->select($CONFIG['redis']['db']);
$redis->setOption(Redis::OPT_SERIALIZER, Redis::SERIALIZER_NONE);

$redis_keys = $redis->mGet(array_keys($data));
$redis_keys = redis_code_last_hour($redis_keys);
$code_last_hour = array();

foreach ($data as $key => $value) {
	if (!isset($redis_keys[$key])) {
		$redis_keys[$key] = array();
	}
	$code_last_hour[$key] = update_code_last_hour($redis_keys[$key], $value);
}

$redis->mSet($code_last_hour);
$redis->close();
