<?php

define('APP_PATH', '/var/www/html');
require_once APP_PATH.'/include/config.php';
require_once APP_PATH.'/include/global.php';
require_once APP_PATH.'/include/cdn.php';

//$data = get_hits_last_hour();

$time = strtotime("-1 month");
$year = date('Y', $time);
$month = date('n', $time);

$sql = "select hostid,sum(hits) as hits, sum(misses) as misses from cdn.cdn_hits_hourly where year={$year} and month={$month} group by hostid";
$result = execute_datalog($sql);

$data = array();
foreach ($result as $value) {
	$data['hits_last_month_'.$value[0]] = $value[1].','.$value[2];
}

$cdn_resources = select_cdn_resources(0);

foreach ($cdn_resources as $key => $value) {
	if (isset($data['hits_last_month_'.$key])) {
		list($hits,$misses) = explode(",", $data['hits_last_month_'.$key]);
	} else {
		$hits = 0;
		$misses = 0;
	}
	$data['hits_last_month_'.$key] = array(
		'id' => $key,
		'hostname' => $value,
		'time' => $time,
		'hits' => $hits,
		'misses' => $misses
	);
}

reset($data);
foreach ($data as $key => $value) {
	if (!is_array($value)) {
		unset($data[$key]);
	}
}

$redis = new Redis();
if (!$redis->connect($CONFIG['redis']['host'], $CONFIG['redis']['port'], 5)) {
	exit('ERROR connect redis failed');
}
$redis->select($CONFIG['redis']['db']);
$redis->setOption(Redis::OPT_SERIALIZER, Redis::SERIALIZER_NONE);

$redis_keys = $redis->mGet(array_keys($data));
$redis_keys = redis_hits_last_month($redis_keys);
$hits_last_month = array();

foreach ($data as $key => $value) {
	if (!isset($redis_keys[$key])) {
		$redis_keys[$key] = array();
	}
	$hits_last_month[$key] = update_hits_last_month($redis_keys[$key], $value);
}

$redis->mSet($hits_last_month);
$redis->close();
