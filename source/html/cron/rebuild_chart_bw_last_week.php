<?php

define('APP_PATH', '/var/www/html');
require_once APP_PATH.'/include/config.php';
require_once APP_PATH.'/include/global.php';
require_once APP_PATH.'/include/cdn.php';


$redis = new Redis();
if (!$redis->connect($CONFIG['redis']['host'], $CONFIG['redis']['port'], 5)) {
	exit('ERROR connect redis failed');
}
$redis->select($CONFIG['redis']['db']);
$redis->setOption(Redis::OPT_SERIALIZER, Redis::SERIALIZER_NONE);
// delete all keys
$redis_keys = $redis->keys("bandwidth_last_day_*");
$redis->delete($redis_keys);


for ($count = 14; $count > 0; $count--) {
	$time = time()-86400*$count;
	$year = date('Y', $time);
	$month = date('n', $time);
	$day = date('j', $time);
	$hour = date('G', $time);

	$sql = "select hostid, sum(bandwidth) as bandwidth_in_mb from cdn.cdn_bw_hourly where year={$year} and month={$month} and day={$day} group by hostid";
	$data = execute_datalog($sql);

	$bandwidth = array();
	foreach ($data as $value) {
		$bandwidth['bandwidth_last_day_'.$value[0]] = $value[1];
	}

	$cdn_resources = select_cdn_resources(0);
	foreach ($cdn_resources as $key => $value) {
		$bandwidth['bandwidth_last_day_'.$key] = array(
			'id' => $key,
			'hostname' => $value,
			'time' => $time,
			'bandwidth' => isset($bandwidth['bandwidth_last_day_'.$key]) ? intval($bandwidth['bandwidth_last_day_'.$key]) : 0
		);
	}

	reset($bandwidth);
	foreach ($bandwidth as $key => $value) {
		if (!is_array($value)) {
			unset($bandwidth[$key]);
		}
	}

	$redis_keys = $redis->mGet(array_keys($bandwidth));
	$redis_keys = redis_bandwidth_last_day($redis_keys);
	$bandwidth_last_day = array();

	foreach ($bandwidth as $key => $value) {
		if (!isset($redis_keys[$key])) {
			$redis_keys[$key] = array();
		}
		$bandwidth_last_day[$key] = update_bandwidth_last_day($redis_keys[$key], $value);
	}

	$redis->mSet($bandwidth_last_day);

	usleep(500000);
}


$redis->close();
