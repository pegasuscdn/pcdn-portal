<?php
define('APP_PATH', '/var/www/html');
require_once APP_PATH.'/include/config.php';
require_once APP_PATH.'/include/global.php';
require_once APP_PATH.'/include/cdn.php';


$param = intval($_GET['day']);
//$param = 16;

$start = mktime(0,0,0,11,$param,2018);
$end = mktime(23,59,59,11,$param,2018);

for ($time=$start; $time<=$end; $time+=60) {



//$time = time()-60;
$year = date('Y', $time);
$month = date('n', $time);
$day = date('j', $time);
$hour = date('G', $time);
$minute = intval(date('i', $time));

$sql = "select * from cdn.cdn_access_log_all where year={$year} and month={$month} and day={$day} and hour={$hour} and minute={$minute}";
$result = execute_datalog($sql);

//var_dump($sql);
//var_dump(count($result));
//exit;

$data = array();
foreach ($result as $row) {
	if (!isset($data[$row[1]]['hits'])) {
		$data[$row[1]]['hits'] = 0;
	}
	if (!isset($data[$row[1]]['misses'])) {
		$data[$row[1]]['misses'] = 0;
	}
	if (!isset($data[$row[1]]['code_2xx'])) {
		$data[$row[1]]['code_2xx'] = 0;
	}
	if (!isset($data[$row[1]]['code_4xx'])) {
		$data[$row[1]]['code_4xx'] = 0;
	}
	if (!isset($data[$row[1]]['code_5xx'])) {
		$data[$row[1]]['code_5xx'] = 0;
	}

	$data[$row[1]]['hostid'] = $row[1];
	$data[$row[1]]['hostname'] = $row[10];
	$data[$row[1]]['siteid'] = $row[23];
	$data[$row[1]]['logdate'] = $row[0];
	$data[$row[1]]['bandwidth'] += intval($row[14]);
	$data[$row[1]]['requests'] += 1;
	if ($row[18] == 'hit') {
		$data[$row[1]]['hits'] += 1;
	}
	if ($row[18] == 'miss') {
		$data[$row[1]]['misses'] += 1;
	}
	if ($row[13] >= 200 && $row[13] <= 299) {
		$data[$row[1]]['code_2xx'] += 1;
	}
	if ($row[13] >= 400 && $row[13] <= 499) {
		$data[$row[1]]['code_4xx'] += 1;
	}
	if ($row[13] >= 500 && $row[13] <= 599) {
		$data[$row[1]]['code_5xx'] += 1;
	}
}


$mysqli = new mysqli($CONFIG['db_report']['host'], $CONFIG['db_report']['user'], $CONFIG['db_report']['pass'], $CONFIG['db_report']['dbname']);
if (mysqli_connect_errno()) {
	error(mysqli_connect_errno(), mysqli_connect_error());
}

// execute
$count = 0;
$insert = array();
foreach ($data as $row) {
	$insert[] = "('{$row['hostid']}',{$row['siteid']},'{$row['logdate']}','{$row['hostname']}',{$row['bandwidth']},{$row['requests']},{$row['hits']},{$row['misses']},{$row['code_2xx']},{$row['code_4xx']},{$row['code_5xx']})";
	exit('1');
	if ($count >= 20) {
		$sql = "INSERT INTO cdn_request_{$year} (`hostid`,`siteid`,`logdate`,`hostname`,`bandwidth`,`requests`,`hits`,`misses`,`code_2xx`,`code_4xx`,`code_5xx`) VALUES ". implode(',', $insert);
		var_dump($sql);
		exit;
		$result = $mysqli->query($sql);
		if (!$result) {
			var_dump($mysqli->error);
		}
		$count = 0;
		$insert = array();
	} else {
		$count++;
	}
}


}

$mysqli->close();
