<?php

define('APP_PATH', '/var/www/html');
require_once APP_PATH.'/include/config.php';
require_once APP_PATH.'/include/global.php';
require_once APP_PATH.'/include/cdn.php';


$redis = new Redis();
if (!$redis->connect($CONFIG['redis']['host'], $CONFIG['redis']['port'], 5)) {
	exit('ERROR connect redis failed');
}
$redis->select($CONFIG['redis']['db']);
$redis->setOption(Redis::OPT_SERIALIZER, Redis::SERIALIZER_NONE);
// delete all keys
$redis_keys = $redis->keys("code_last_hour_*");
$redis->delete($redis_keys);

for ($count = 24; $count > 0; $count--) {
	$time = time()-3600*$count;
	$year = date('Y', $time);
	$month = date('n', $time);
	$day = date('j', $time);
	$hour = date('G', $time);

	$sql = "select hostid,sum(code_200) as code_200, sum(code_4xx) as code_4xx, sum(code_5xx) as code_5xx from cdn.cdn_code_hourly where year={$year} and month={$month} and day={$day} and hour={$hour} group by hostid";
	$result = execute_datalog($sql);

	$data = array();
	foreach ($result as $value) {
		$data['code_last_hour_'.$value[0]] = $value[1].','.$value[2].','.$value[3];
	}

	$cdn_resources = select_cdn_resources(0);
	foreach ($cdn_resources as $key => $value) {
		if (isset($data['code_last_hour_'.$key])) {
			list($code_200,$code_4xx,$code_5xx) = explode(",", $data['code_last_hour_'.$key]);
		} else {
			$code_200 = 0;
			$code_4xx = 0;
			$code_5xx = 0;
		}
		$data['code_last_hour_'.$key] = array(
			'id' => $key,
			'hostname' => $value,
			'time' => $time,
			'code_200' => $code_200,
			'code_4xx' => $code_4xx,
			'code_5xx' => $code_5xx,
		);
	}

	reset($data);
	foreach ($data as $key => $value) {
		if (!is_array($value)) {
			unset($data[$key]);
		}
	}

	$redis_keys = $redis->mGet(array_keys($data));
	$redis_keys = redis_code_last_hour($redis_keys);
	$code_last_hour = array();

	foreach ($data as $key => $value) {
		if (!isset($redis_keys[$key])) {
			$redis_keys[$key] = array();
		}
		$code_last_hour[$key] = update_code_last_hour($redis_keys[$key], $value);
	}

	$redis->mSet($code_last_hour);


	usleep(500000);
}

$redis->close();