<?php

require_once 'include/config.php';
require_once 'include/global.php';
require_once 'include/user.php';
check_session();
$active_menu = 'account';

include TEMPLATE_PATH.'/main_header.php';

/* XU LY CHINH */
$error_messages = array();
$arrUserInfo=getaccountinfo($_SESSION['portal']['email']);
if(!$arrUserInfo)
	redirect("/logout.php");
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
	
	$currentpass = htmlspecialchars ($_POST['currentpass']);
	$newpass = htmlspecialchars ($_POST['newpass']);
	$renewpass = htmlspecialchars ($_POST['renewpass']);

	if ($currentpass=='' OR $newpass=='') {
		$error_messages[] = "Password is not blank";
	}
	
	$password2 = md5($currentpass.$arrUserInfo['hash']);
	if ($password2 != $arrUserInfo['password']) {
		$error_messages[] = "Current Password is incorrect";
	}
	
	if ($newpass != $renewpass) {
		$error_messages[] = "New Password is not match";
	}			
		
	if (!$error_messages) {
		if (resetpassword($_SESSION['portal']['email'],$newpass)) {
			$error_messages[] = "Change Password Success";
			//redirect("/profile.php");
		} else {
			$error_messages[] = "System error";
		}
	}
}
include TEMPLATE_PATH.'/change_password.php';

/* END */

include TEMPLATE_PATH.'/main_footer.php';