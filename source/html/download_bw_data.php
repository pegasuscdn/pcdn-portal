<?php

define('APP_PATH', '/var/www/html');
require_once APP_PATH.'/include/config.php';
require_once APP_PATH.'/include/global.php';
require_once APP_PATH.'/include/cdn.php';

check_session();

$date = trim($_GET['date']);
if (!preg_match('/^\d{4}\-\d{2}$/i', $date)) {
	exit;
}
list($year, $month) = explode('-', $date);

$hostids = array();
db_connect();
$sql = "select id, hostname from cdn_resource where userid=".get_user_id();
$stmt = $mysqli->prepare($sql);
$stmt->execute();
$result = $stmt->get_result();
if ($result) {
	while ($row = $result->fetch_array(MYSQLI_ASSOC)) {
		$hostids[$row['id']] = $row['hostname'];
	}
}

if (empty($hostids)) {
	exit;
}
$stmt->close();

$str_hostid = '';
foreach($hostids as $key=>$value) {
	$str_hostid .= ($str_hostid=='') ? $key : ','.$key;
}

$sql = "select hostid, year, month, day, sum(bandwidth) as bandwidth_in_mb from cdn.cdn_bw_hourly where hostid in ({$str_hostid}) and year={$year} and month={$month} group by hostid, year, month, day order by day";
$result = execute_datalog($sql);

$filename = "cdn_bandwidth_{$year}_{$month}.csv";
header('Content-Type: application/csv');
header('Content-Disposition: attachment; filename="'.$filename.'";');
echo "hostid,hostname,year,month,day,bandwidth_in_MB\n";
foreach ($result as $value) {
	echo "\"{$value[0]}\",\"{$hostids[$value[0]]}\",{$value[1]},{$value[2]},{$value[3]},{$value[4]}\n";
}