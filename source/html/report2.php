<?php

require_once 'include/config.php';
require_once 'include/global.php';
require_once 'include/cdn.php';

check_session();

$active_menu = 'report';
include TEMPLATE_PATH.'/main_header.php';

$cdn_resources = select_cdn_resources($_SESSION['portal']['userid']);
$hostid = trim($_GET['hostid']);
if (!preg_match('/[^0]\d+/i', $hostid)) {
	$hostid = 0;
}




if ($_GET['type'] == 'bw') {

	include TEMPLATE_PATH.'/report_bw2.php';
} else if ($_GET['type'] == 'hit') {

	include TEMPLATE_PATH.'/report_hit.php';
} else if ($_GET['type'] == 'code') {

	include TEMPLATE_PATH.'/report_code.php';
} else if ($_GET['type'] == 'costs') {

	$costs_month = array();
	if (preg_match('/^(\d{4})\-(\d{2})\-(\d{2}) (\d{2}):(\d{2}):(\d{2})$/i', $_SESSION['portal']['registerdate'], $match)) {
		$start = mktime($match[4],$match[5],$match[6],$match[2],$match[3],$match[1]);
		$time = strtotime("last day of this month");
		do {
			$costs_month[] = date('Y-m', $time);
			$time = strtotime("-1 month", $time);
		} while ($time >= $start);
	}

	include TEMPLATE_PATH.'/report_costs.php';
} else {

	include TEMPLATE_PATH.'/report_overview.php';
}

include TEMPLATE_PATH.'/main_footer.php';
