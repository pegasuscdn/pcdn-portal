<?php

require_once 'include/config.php';
require_once 'include/global.php';
require_once 'include/user.php';
require_once 'include/GoogleAuthenticator.php';
if($_SESSION['portal']['email'])
	redirect("/report.php");
if(!$_SESSION['portal']['tmp_email'])
	redirect("/login.php");
/* XU LY LOGIN */
$error_messages = array();

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
	$arrUserInfo=getaccountinfo($_SESSION['portal']['tmp_email']);
	$otp = htmlspecialchars($_POST['otp']);
	$typelogin = htmlspecialchars($_POST['typelogin']);
	if($typelogin==1){
		$ga = new PHPGangsta_GoogleAuthenticator();
		$checkResult = $ga->verifyCode($arrUserInfo['ggdata'], $otp, 2);    // 2 = 2*30sec clock tolerance
		if ($checkResult) {
			$_SESSION['portal']['userid'] = $arrUserInfo['id'];
			$_SESSION['portal']['fullname'] = $arrUserInfo['fullname'];
			$_SESSION['portal']['email'] = $arrUserInfo["email"];	
			redirect("/report.php");		
		} else {
			$error_messages[] = "Verify google authen is failed";
		}
	}else{
		$arrToken = explode(",",$arrUserInfo["ggtoken"]);
		if(in_array($otp,$arrToken))
		{
			$arrNewToken = array_diff($arrToken, array($otp));
			updateggtoken($arrUserInfo["email"],implode(",",$arrNewToken),$arrUserInfo["ggdata"]);
			$_SESSION['portal']['userid'] = $arrUserInfo['id'];
			$_SESSION['portal']['fullname'] = $arrUserInfo['fullname'];
			$_SESSION['portal']['email'] = $arrUserInfo["email"];	
			redirect("/report.php");		
		}else{
			$error_messages[] = "Verify token is failed";
		}
	}		
}

include TEMPLATE_PATH.'/login_two_step.php';
/* END */

require_once 'include/finish.php';