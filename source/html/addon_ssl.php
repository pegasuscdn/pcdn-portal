<?php

require_once 'include/config.php';
require_once 'include/global.php';
require_once 'include/ssl.php';

check_session();

$active_menu = 'add-ons';
include TEMPLATE_PATH.'/main_header.php';

if($_SESSION['portal']['expired_date'] || ($_SESSION['portal']['limited_bandwidth']>-1) ){
$cdate=date("Y-m-d");
//if($cdate>=$_SESSION['portal']['expired_date'])
        include TEMPLATE_PATH.'/acount_expired.php';
        //include TEMPLATE_PATH.'/main_footer.php';
        //exit;
}

$action = trim($_GET['action']);

if($_SESSION['portal']['is_admin_login']==1){
	 include TEMPLATE_PATH.'/acount_limitaccess.php';
	include TEMPLATE_PATH.'/main_footer.php';exit;
}
if ($action == 'add') {
	if($_SESSION['portal']['expired_date'] || ($_SESSION['portal']['limited_bandwidth']>-1)){
               if(($cdate>=$_SESSION['portal']['expired_date']) || ($_SESSION['portal']['limited_bandwidth']==0))
                //include TEMPLATE_PATH.'/acount_expired.php';
               { include TEMPLATE_PATH.'/main_footer.php';
                exit;}
        }

	if ($_SERVER['REQUEST_METHOD'] == 'POST') {
		$errors = array();
		$ssl_name = trim($_POST['ssl_name']);
		$ssl_key = trim($_POST['ssl_key']);
		$ssl_cert = trim($_POST['ssl_cert']);

		if (empty($ssl_name) OR empty($ssl_key) OR empty($ssl_cert)) {
			$errors[] = "Fields (*) are required";
		}
		if (!checkKeyCert($ssl_key,$ssl_cert)) {
			$errors[] = "Key, Cert invalid";
		}


		// insert db
		if (count($errors) == 0) {
			/*
				table cdn_ssl
			id	int(10) unsigned Auto Increment
			userid	int(10) unsigned
			name	varchar(100)
			key	text
			cert	text
			status	tinyint(3) unsigned [0]	0: can xu ly
			deleted	tinyint(3) unsigned [0]	1: da xoa
			updateddate	timestamp NULL
			*/
			$data = array(
				'name' => $ssl_name,
				'key' => $ssl_key,
				'cert' => $ssl_cert,
				'deleted' => 0,
				'userid' => get_user_id(),
			);


			if (insert_ssl_resource($data)) {
				$_SESSION['ssl_resources_new'] = 1;
				redirect('addon_ssl.php');
			} else {
				$errors[] = "error.";
			}
		}
	}

	if (empty($cdn_origin)) $cdn_origin = 1;

	include TEMPLATE_PATH.'/ssl_add_new.php';

}
else if ($action == 'edit') {
	 if($_SESSION['portal']['expired_date'] || ($_SESSION['portal']['limited_bandwidth']>-1)){
                if(($cdate>=$_SESSION['portal']['expired_date']) || ($_SESSION['portal']['limited_bandwidth']==0))
                //include TEMPLATE_PATH.'/acount_expired.php';
                {include TEMPLATE_PATH.'/main_footer.php';
                exit;}
        }

	$id = trim($_GET['id']);
	if (empty($id)) {
		redirect('cdn_resources.php');
	}

	$resource = select_ssl_resources(get_user_id(), $id);
	if (empty($resource)) {
		redirect('addon_ssl.php');
	}


	if ($_SERVER['REQUEST_METHOD'] == 'POST') {
		$errors = array();
		$ssl_name = trim($_POST['ssl_name']);
		$ssl_key = trim($_POST['ssl_key']);
		$ssl_cert = trim($_POST['ssl_cert']);

		if (empty($ssl_name) OR empty($ssl_key) OR empty($ssl_cert)) {
			$errors[] = "Fields (*) are required";
		}
		if (!checkKeyCert($ssl_key,$ssl_cert)) {
			$errors[] = "Key, Cert invalid";
		}
		// insert db
		if (count($errors) == 0) {
			$data = array(
				'name' => $ssl_name,
				'key' => $ssl_key,
				'cert' => $ssl_cert,
				'deleted' => 0,
				'userid' => get_user_id(),
			);


			if (update_ssl_resource($data,$id)) {
				$_SESSION['ssl_resources_new'] = 2;
				redirect('addon_ssl.php');
			} else {
				$errors[] = "error.";
			}
		}
	}
	 	$ssl_name = ($resource[0]['name']);
                $ssl_key =  ($resource[0]['key']);
                $ssl_cert = ($resource[0]['cert']);

	include TEMPLATE_PATH.'/ssl_edit.php';

}
else {
	
	if ($action == 'del') {
		 if($_SESSION['portal']['expired_date'] || ($_SESSION['portal']['limited_bandwidth']>-1)){
                if(($cdate>=$_SESSION['portal']['expired_date']) || ($_SESSION['portal']['limited_bandwidth']==0))
                //include TEMPLATE_PATH.'/acount_expired.php';
                {include TEMPLATE_PATH.'/main_footer.php';
                exit;}
        }

 	$id = trim($_GET['id']);
 	if (empty($id)) {
 		redirect('cdn_resources.php');
 	}
 	$get_user_id = get_user_id();
 	$resource = select_ssl_resources($get_user_id, $id);
 	if (empty($resource)) {
 		redirect('addon_ssl.php');
 	}

	if(checkUsedSSL($id)) { $errors[] = " Remove ssl error. SSL is used.";}
	else {
 			if (delete_ssl_resource($get_user_id,$id)) {
 				$_SESSION['ssl_resources_new'] = 3;
 				redirect('addon_ssl.php');
 			} else {
 				$errors[] = "Remove ssl error.";
 			}
	     }	
	}
	//////////////// end delete ssl
	if (isset($_SESSION['ssl_resources_new'])) {
		 if($_SESSION['ssl_resources_new']==3)
                        $message = "Remove SSL successfull.";	
		if($_SESSION['ssl_resources_new']==2)
			$message = "Update SSL successfull.";
		if($_SESSION['ssl_resources_new']==1)
                        $message = "Add new SSL successfull.";
		unset($_SESSION['ssl_resources_new']);
	}
	
	$resources = select_ssl_resources(get_user_id());

	include TEMPLATE_PATH.'/ssl_resources.php';
}

include TEMPLATE_PATH.'/main_footer.php';

