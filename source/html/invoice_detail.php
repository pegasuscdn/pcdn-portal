<?php

require_once 'include/config.php';
require_once 'include/global.php';
require_once 'include/invoice.php';

check_session();

$active_menu = 'billing';
include TEMPLATE_PATH.'/main_header.php';
$id =  isset($_GET['id']) ? (int)htmlspecialchars($_GET['id']) : 0;
$arrData = getinfoinvoice($id);
if(!$arrData) redirect("/invoice.php");
$arrDataInvoiceDetail = getinvoicedetail($id);

include TEMPLATE_PATH.'/invoice_detail.php';

include TEMPLATE_PATH.'/main_footer.php';