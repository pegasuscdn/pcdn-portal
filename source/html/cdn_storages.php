<?php

require_once 'include/config.php';
require_once 'include/global.php';

check_session();

$active_menu = 'cdn';
include TEMPLATE_PATH.'/main_header.php';

include TEMPLATE_PATH.'/cdn_storages.php';

include TEMPLATE_PATH.'/main_footer.php';