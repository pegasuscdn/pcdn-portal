<?php
require_once 'include/config.php';
require_once 'include/global.php';
require_once 'include/user.php';
require_once 'include/function.php';
if($_SESSION['portal']['email'])
	redirect("/report.php");
/* XU LY LOGIN */
$error_messages = array();
	
$email = htmlspecialchars($_GET['email']);
$time = htmlspecialchars($_GET['time']);
$token = htmlspecialchars($_GET['token']);
if($token!=md5(KEY_SECRET.$email.$time))
{
	$error_code=1;
	$error_messages[] = "Token is failed";
}else{
	$timeexpired = strtotime('+30 minutes',$time);
	if(time()>$timeexpired)
	{
		$error_code=1;
		$error_messages[] = "Time Expired";
	}	
}

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
	
	$password = htmlspecialchars ($_POST['portal_pass']);
	$repassword = htmlspecialchars ($_POST['portal_repass']);

	if ($password=="" OR $password != $repassword) {
		$error_code=0;
		$error_messages[] = "Password is not match";
	}

	if (!$error_messages) {
		if (resetpassword($email, $password)) {
			redirect("/login.php");
		} else {
			$error_code=1;
			$error_messages[] = "System error";
		}
	}
}

include TEMPLATE_PATH.'/resetpassword.php';
/* END */

require_once 'include/finish.php';