<?php

require_once 'include/config.php';
require_once 'include/global.php';

logout();
redirect('/login.php');