<?php
require_once 'include/config.php';
require_once 'include/global.php';
require_once 'include/user.php';
require_once 'include/function.php';
if($_SESSION['portal']['email'])
	redirect("/report.php");
/* XU LY LOGIN */
$error_messages = array();
	
$email = htmlspecialchars($_GET['email']);
$time = htmlspecialchars($_GET['time']);
$token = htmlspecialchars($_GET['token']);
if($token!=md5(KEY_SECRET.$email.$time))
{
	$error_messages[] = "Token is failed";
}else{
	$timeexpired = strtotime('+30 minutes',$time);
	if(time()>$timeexpired)
	{
		$error_messages[] = "Time Expired";
	}else{
		
		if (activeaccount($email)) {
			$error_messages[] = "Account has been actived";
		} else {
			$error_messages[] = "System error";
		}
	}		
}

include TEMPLATE_PATH.'/activeaccount.php';
/* END */

require_once 'include/finish.php';