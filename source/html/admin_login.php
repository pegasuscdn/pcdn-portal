<?php
require_once 'include/config.php';
require_once 'include/global.php';
require_once 'include/admin.php';
require_once 'include/GoogleAuthenticator.php';
if($_SESSION['admin']['username'])
	redirect("/admin_dashboard.php");
/* XU LY LOGIN */
$error_messages = array();

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
	
	$username = htmlspecialchars($_POST['portal_email']);
	$password = htmlspecialchars($_POST['portal_pass']);
	$token = htmlspecialchars($_POST['portal_token']);

	if (!$username OR $password=='') {
		$error_messages[] = "Invalid username OR password";
	}

	if (!$error_messages) {
		$arrUserInfo = admin_user_login($username, $password);
		if ($arrUserInfo) {
			if(strlen($arrUserInfo["ggtoken"])>1)
			{
				$ga = new PHPGangsta_GoogleAuthenticator();
				$checkResult = $ga->verifyCode($arrUserInfo["ggtoken"], $token, 2);    // 2 = 2*30sec clock tolerance
				if ($checkResult) {
					$_SESSION['admin']['userid'] = $arrUserInfo['id'];
					$_SESSION['admin']['username'] = $arrUserInfo["username"];		
					redirect("/admin_dashboard.php");
				} else {
					$error_messages[] = "Verify google authen is failed";
				}
			}else{
				$_SESSION['admin']['userid'] = $arrUserInfo['id'];
				$_SESSION['admin']['username'] = $arrUserInfo["username"];		
				redirect("/admin_dashboard.php");
			}
		} else {
			$error_messages[] = "Invalid account";
		}
	}
}

include TEMPLATE_PATH.'/admin_login.php';
require_once 'include/finish.php';