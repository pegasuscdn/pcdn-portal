<?php
//exit;
require_once 'include/config.php';
//require 'vendor/autoload.php';
//use Mailgun\Mailgun;
require_once 'include/global.php';
require_once 'include/user.php';
require_once 'include/function.php';
if($_SESSION['portal']['email'])
	redirect("/report.php");
/* XU LY LOGIN */
$error_messages = array();

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
	
	$email = filter_var(htmlspecialchars($_POST['portal_email']), FILTER_VALIDATE_EMAIL);
	$password = ($_POST['portal_pass']);
	$repassword = ($_POST['portal_repass']);
	$fullname = htmlspecialchars ($_POST['fullname']);
	$error_messages=[];
	if ($fullname=='') {
		$error_messages[] = "Fullname is not blank";
	}
	if (!$email) {
		$error_messages[] = "Invalid email OR password";
	}
	if(!$password)  $error_messages[] = "Password required";
	if ($password !== $repassword) {
		$error_messages[] = "Password is not match";
	}
	$arrUserInfo=getaccountinfo($email);
	//print_r(  $arrUserInfo ); exit;
	if($arrUserInfo){
		$error_messages[] = "Email is existed";
	}
	$domain_register=$_SERVER['HTTP_HOST'];
	if( $domain_register === 'cdnservice.pegasustech.io')  $domain_register ='portal.cdnservice.vn';
	if (! count($error_messages)) {
		if (register($email,$password,$fullname,$domain_register)) {
			$time=time();
			$token=md5(KEY_SECRET.$email.$time);
			$link=SITE_URL.'/activeaccount.php?email='.$email.'&token='.$token.'&time='.$time;


			//sent email register
mailGunSendmail($email,'Register Account',content_email_register($fullname,$email,$link) );

/*
$mgClient = new Mailgun( MAILGUN_APIKEY  );
$domain = 'mail.pegasustech.io';// $domain_register;//"YOUR_DOMAIN_NAME";
$result = $mgClient->sendMessage($domain, array(
 'from' => 'noreply<noreply@'.$domain.'>',
 'to' => $fullname .' <'.$email.'>',
 'subject' => 'Register Account',
 'text' => content_email_register($fullname,$email,$link),
));
			
	
			require BASE_PATH.'/PHPMailer-master/PHPMailerAutoload.php';
			$mail = new PHPMailer;
			//$mail->SMTPDebug = 3;                             // Enable verbose debug output
			$mail->isSMTP();                                    // Set mailer to use SMTP
			$mail->Host = 'smtp.gmail.com';  					// Specify main and backup SMTP servers
			$mail->SMTPAuth = true;                             // Enable SMTP authentication
			$mail->Username = EMAIL_SENT;         // SMTP username
			$mail->Password = PASSWORD_EMAIL;                       // SMTP password
			$mail->SMTPSecure = 'tls';                          // Enable TLS encryption, `ssl` also accepted
			$mail->Port = 587;                                  // TCP port to connect to
			$mail->From = EMAIL_SENT;
			$mail->FromName = 'Register Account';
			$mail->addAddress($email);        // Name is optional
			$mail->isHTML(true);                                // Set email format to HTML
			$mail->Subject = "Register Account";
			$mail->Body    = content_email_register($fullname,$email,$link);
			$mail->AltBody = $email;
			$mail->send();
*/
			redirect("/login.php");
		} else {
			$error_messages[] = "System error";
		}
	}
}

include TEMPLATE_PATH.'/register.php';
/* END */

require_once 'include/finish.php';
