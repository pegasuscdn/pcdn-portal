<?php

require_once 'include/config.php';
require_once 'include/global.php';
require_once 'include/user.php';
if(!$_SESSION['portal']['email'])
	redirect("/login.php");
$active_menu = 'account';

include TEMPLATE_PATH.'/main_header.php';

/* XU LY CHINH */
$error_messages = array();
$arrUserInfo=getaccountinfo($_SESSION['portal']['email']);
if(!$arrUserInfo)
	redirect("/logout.php");
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
	
	$phone = htmlspecialchars ($_POST['phone']);
	$fullname = htmlspecialchars ($_POST['fullname']);
	$address = htmlspecialchars ($_POST['address']);

	if ($fullname=='') {
		$error_messages[] = "Fullname is not blank";
	}
	if ($phone=='') {
		$error_messages[] = "Phone is not blank";
	}
	if (!$error_messages) {
		if (updateaccountinfo($_SESSION['portal']['email'],$fullname,$phone,$address)) {
			//$error_messages[] = "Update Account Info Success";
			redirect("/profile.php");
		} else {
			$error_messages[] = "System error";
		}
	}
}

include TEMPLATE_PATH.'/profile.php';

/* END */

include TEMPLATE_PATH.'/main_footer.php';