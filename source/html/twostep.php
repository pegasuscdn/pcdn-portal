<?php

require_once 'include/config.php';
require_once 'include/global.php';
require_once 'include/user.php';
require_once 'include/GoogleAuthenticator.php';
check_session();
$active_menu = 'account';

include TEMPLATE_PATH.'/main_header.php';

/* XU LY CHINH */
$error_messages = array();
$arrUserInfo=getaccountinfo($_SESSION['portal']['email']);
$ga = new PHPGangsta_GoogleAuthenticator();
if(!$_SESSION['portal']['ggdata'])
	$_SESSION['portal']['ggdata'] = $ga->createSecret();

$qrCodeUrl = $ga->getQRCodeGoogleUrl("CDN", $_SESSION['portal']['ggdata']);

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
	
	$ggtoken = htmlspecialchars ($_POST['ggtoken']);
	$oneCode = htmlspecialchars ($_POST['oneCode']);
	$status = htmlspecialchars ($_POST['status']);
	if($status==0){
		if(updateggtoken($_SESSION['portal']['email'],"","")){
			$error_messages[] = "Update success";
			redirect("/twostep.php");
		}else{
			$error_messages[] = "System error";
		}
	}else{
		$checkResult = $ga->verifyCode($_SESSION['portal']['ggdata'], $oneCode, 2);    // 2 = 2*30sec clock tolerance
		if ($checkResult) {
			if(updateggtoken($_SESSION['portal']['email'],$ggtoken,$_SESSION['portal']['ggdata'])){
				redirect("/twostep.php");
			}else{
				$error_messages[] = "System error";
			}
		} else {
			$error_messages[] = "Verify google authen is failed";
		}
	}
	
	
	
}
include TEMPLATE_PATH.'/two_step_auth.php';

/* END */

include TEMPLATE_PATH.'/main_footer.php';