<?php

require_once 'include/config.php';
require_once 'include/global.php';
require_once 'include/user.php';
require_once 'include/function.php';
if($_SESSION['portal']['email'])
	redirect("/report.php");
/* XU LY LOGIN */
$error_messages = array();
$error_code=1;
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
	
	$email = filter_var(htmlspecialchars($_POST['portal_email']), FILTER_VALIDATE_EMAIL);
	if (!$email) {
		$error_messages[] = "Invalid email";
	}

	$arrUserInfo=getaccountinfo($email);
	if(! count( $arrUserInfo ) )	 $error_messages[] = "Email is not existed";
	if (!$error_messages) {
		
		$time=time();
		$token=md5(KEY_SECRET.$email.$time);
		$link=SITE_URL.'/resetpassword.php?email='.$email.'&token='.$token.'&time='.$time;
	
/*	
		require BASE_PATH.'/PHPMailer-master/PHPMailerAutoload.php';
		$mail = new PHPMailer;
		//$mail->SMTPDebug = 3;                             // Enable verbose debug output
		$mail->isSMTP();                                    // Set mailer to use SMTP
		$mail->Host = 'smtp.gmail.com';  					// Specify main and backup SMTP servers
		$mail->SMTPAuth = true;                             // Enable SMTP authentication
		$mail->Username = EMAIL_SENT;         // SMTP username
		$mail->Password = PASSWORD_EMAIL;                       // SMTP password
		$mail->SMTPSecure = 'tls';                          // Enable TLS encryption, `ssl` also accepted
		$mail->Port = 587;                                  // TCP port to connect to
		$mail->From = EMAIL_SENT;
		$mail->FromName = 'Reset Password';
		$mail->addAddress($email);        // Name is optional
		$mail->isHTML(true);                                // Set email format to HTML
		$mail->Subject = "Reset Password";
		$mail->Body    = content_email_forgotpass($email,$link);
		$mail->AltBody = $email;
		$mail->send();
*/
mailGunSendmail($email,'Reset Password',content_email_forgotpass($email,$link) );
		$error_code=0;
		$error_messages[] = "Please check email for reset password";
	}
}

include TEMPLATE_PATH.'/forgotpassword.php';
/* END */

require_once 'include/finish.php';
