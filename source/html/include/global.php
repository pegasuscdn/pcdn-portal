<?php

$mysqli = NULL;
function mailGunSendmail($toEmail,$subject,$body){
  $API_URL="https://api.mailgun.net/v3/mail.pegasustech.io/messages";
  $oHTTPClient = curl_init($API_URL);
  $post = [
    'from' => 'Pegasus Technology <noreply@mail.pegasustech.io>',
    'to' => $toEmail,
    'subject'   => $subject,
    'html'=>$body
];

  //curl_setopt($oHTTPClient, CURLOPT_VERBOSE, 1);
  curl_setopt($oHTTPClient, CURLOPT_SSL_VERIFYPEER, FALSE);
  curl_setopt($oHTTPClient, CURLOPT_SSL_VERIFYHOST, FALSE);
  curl_setopt($oHTTPClient, CURLOPT_RETURNTRANSFER, TRUE);
  curl_setopt($oHTTPClient, CURLOPT_USERPWD, 'api' . ':' . '79e23882b310303900c4ac6f73e6bbf9-816b23ef-aa99f276');
  curl_setopt($oHTTPClient, CURLOPT_CUSTOMREQUEST, 'POST');
  curl_setopt($oHTTPClient, CURLOPT_POSTFIELDS, $post);
return   $strResult = curl_exec($oHTTPClient);

}


function redirect($url) {
	ob_end_clean();
	header('Location: '.$url);
	exit;
}

function error($code, $message) {
	global $CONFIG;
	ob_end_clean();
	if ($CONFIG['debug']) {
		var_dump($code);
		var_dump($message);
	} else {
		echo "There're some error, please contact site administrator!";
	}
	exit;
}

function mysqli_DoFetchName($oResult){

		$i =0;

			$arrResult = array();
			if($oResult){
					$row = '';

					while ($row = mysqli_fetch_array($oResult, MYSQLI_ASSOC)) {

						$arrResult[$i] = $row;

			    		$i++;

					}

			}
		return $arrResult;

	}
function db_connectReport() {
	global $report_mysqli, $CONFIG;
	if (!$report_mysqli) {
		$report_mysqli = new mysqli($CONFIG['db_report']['host'], $CONFIG['db_report']['user'], $CONFIG['db_report']['pass'], $CONFIG['db_report']['dbname']);
		if (mysqli_connect_errno()) {
			error(mysqli_connect_errno(), mysqli_connect_error());
		}
	}
}

function db_connect() {
	global $mysqli, $CONFIG;
	if (!$mysqli) {
		$mysqli = new mysqli($CONFIG['db']['host'], $CONFIG['db']['user'], $CONFIG['db']['pass'], $CONFIG['db']['dbname']);
		if (mysqli_connect_errno()) {
			error(mysqli_connect_errno(), mysqli_connect_error());
		}
	}
}

function db_close() {
	global $mysqli;
	if ($mysqli) {
		$mysqli->close();
	}
}

function login($email, $password) {
	global $mysqli;
	$record_status = RECORD_STATUS_ACTIVE;
	db_connect();
	$stmt = $mysqli->prepare("select id, fullname, password, hash from portal_user where email=? and recordstatus=?");
	$stmt->bind_param('si', $email, $record_status);
	$stmt->execute();
	$result = $stmt->get_result();
	if ($result->num_rows != 1) {
		return false;
	}
	$row = $result->fetch_array(MYSQLI_ASSOC);
	$password2 = md5($password.$row['hash']);
	if ($password2 == $row['password']) {
		$_SESSION['portal']['userid'] = $row['id'];
		$_SESSION['portal']['fullname'] = $row['fullname'];
		$_SESSION['portal']['email'] = $email;
		$_SESSION['portal']['registerdate'] = $row['registerdate'];
		return true;
	} else {
		return false;
	}
}

function logout() {
	session_regenerate_id(true);
	session_destroy();
}

function check_session() {
	if (!isset($_SESSION['portal']['userid']) OR !isset($_SESSION['portal']['email'])) {
		logout();
		redirect('/login.php');
	}
	if($_SESSION['portal']['timeExpired']>0) if($_SESSION['portal']['timeExpired']< time())  {  logout(); redirect('/login.php');}
}

function api_check_session() {
	if (!isset($_SESSION['portal']['userid']) OR !isset($_SESSION['portal']['email'])) {
		response_api(API_ERROR_AUTH, 'auth is required');
	}
	 if($_SESSION['portal']['timeExpired']>0) if($_SESSION['portal']['timeExpired']< time())  response_api(API_ERROR_AUTH, 'auth is required');
}

function get_user_id() {
	check_session();
	return intval($_SESSION['portal']['userid']);
}

function debug($var) {
	
	echo "<pre>";
	var_dump($var);
	exit;
}

function response_api($code, $data) {
	ob_end_clean();
	header('Content-Type: application/json');
	$obj = new StdClass();
	$obj->code = $code;
	$obj->data = $data;
	echo json_encode($obj);
	exit;
}

function random_string($length) {
	$str = '';
	$chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
	$index = strlen($chars) - 1;
	for ($i = 0; $i < $length; $i++) {
		$str .= $chars[rand(0, $index)];
	}
	return $str;
}
