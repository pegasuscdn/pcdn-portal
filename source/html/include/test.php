<?php
function callAPIPurgeCache($id,$key,$domain,$url){
  $API_URL="https://api.pegasustech.io/cache/purge";
  $oHTTPClient = curl_init($API_URL);
  $post = [
    'id' => $id,
    'key' => $key,
    'domain'   => $domain,
    'url'=>$url
];

  curl_setopt($oHTTPClient, CURLOPT_VERBOSE, 1);
  curl_setopt($oHTTPClient, CURLOPT_SSL_VERIFYPEER, FALSE);
  curl_setopt($oHTTPClient, CURLOPT_SSL_VERIFYHOST, FALSE);
  curl_setopt($oHTTPClient, CURLOPT_RETURNTRANSFER, TRUE);

  curl_setopt($oHTTPClient, CURLOPT_CUSTOMREQUEST, 'POST');
  curl_setopt($oHTTPClient, CURLOPT_POSTFIELDS, $post);
  $strResult = curl_exec($oHTTPClient);
 if ($strResult === FALSE) {
    printf("cUrl error (#%d): %s<br>\n", curl_errno($handle),
           htmlspecialchars(curl_error($handle)));
	}
return $strResult;
}

$id=1;
$key=2;
$domain='abc.com';
$url='/image/*';

$xx= callAPIPurgeCache($id,$key,$domain,$url);
var_dump($xx);
?>
