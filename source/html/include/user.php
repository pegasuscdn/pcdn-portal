<?php
function user_login_auto($ssid){
	global $mysqli;
	$record_status = RECORD_STATUS_ACTIVE;
	db_connect();

	$stmt = $mysqli->prepare("select u.id,u.domain_register, u.email, u.fullname,  u.registerdate, u.expired_date,u.`limited_bandwidth`, u.`trial_bandwidth`,  u.recordstatus as record_status
	from portal_user u ,admin_autologin a
	where a.isUsed=0 AND a.ssid=?  AND a.login_user_id=u.id");
if($stmt){
	$stmt->bind_param('s', $ssid);
	$stmt->execute();
	$result = $stmt->get_result();
	if ($result->num_rows != 1) {
		return false;
	}
	$row = $result->fetch_array(MYSQLI_ASSOC);
	
	$stmt = $mysqli->prepare("update admin_autologin set isUsed=1 where ssid=?");
	$stmt->bind_param('s', $ssid);
	$stmt->execute();
	return $row;
}
else{
    return [];	
    $error = $mysqli->errno . ' ' . $mysqli->error;
    echo $error; // 1054 Unknown column 'foo' in 'field list'
}
}
function user_login($email, $password) {
	global $mysqli;
	$record_status = RECORD_STATUS_ACTIVE;
	db_connect();
	$stmt = $mysqli->prepare("select id , domain_register ,email, recordstatus as record_status, fullname, password, hash, ggdata, ggtoken, registerdate, expired_date, limited_bandwidth,trial_bandwidth  from portal_user where email=?");// and recordstatus=?");
	//$stmt->bind_param('si', $email, $record_status);
	$stmt->bind_param('s', $email);
	$stmt->execute();
	$result = $stmt->get_result();
//var_dump( $result); exit;
	if ($result->num_rows != 1) {
		return false;
	}
	$row = $result->fetch_array(MYSQLI_ASSOC);
	//print_r( $row );
	$password2 = md5($password.$row['hash']);
	//echo $password2 ."\n". $row['password'];
	if ( ($password2 == $row['password']) ) {
		return $row;
	} else {
		return null;
	}
}

function register($email, $password,$fullname,$domain_register) {
	global $mysqli;
	$record_status = RECORD_STATUS_INACTIVE;
	db_connect();
	$hash = substr(md5(mt_rand()), 0, 32);
	$pass = md5($password.$hash);
	$sql = "INSERT INTO portal_user (email, fullname, password, hash, registerdate, recordstatus,domain_register)VALUES ('".$email."','".$fullname."','".$pass."','".$hash."',NOW(),".$record_status.",'".$domain_register."')";
	if ($mysqli->query($sql) === TRUE) {
		return true;
	} else {
		//echo "Error: " . $sql . "<br>" . $mysqli->error;
		return false;
	}
}
function resetpassword($email, $password) {
	global $mysqli;
	db_connect();
	$hash = substr(md5(mt_rand()), 0, 32);
	$pass = md5($password.$hash);
	$sql = "update portal_user set password='".$pass."',hash='".$hash."' where email='".$email."'";
	if ($mysqli->query($sql) === TRUE) {
		return true;
	} else {
		//echo "Error: " . $sql . "<br>" . $mysqli->error;
		return false;
	}
}
function getaccountinfo($email,$id=0) {
	global $mysqli;
	db_connect();
	$sql = "select * from portal_user where email='".$email."'";
	if($id)  $sql = "select * from portal_user where id=".intval($id);
	$result = $mysqli->query($sql);
	return $result->fetch_assoc();
}
function activeaccount($email) {
	global $mysqli;
	db_connect();
	$record_status = RECORD_STATUS_TRIAL;
	// 11 is active 4 is trial	
	$ndate= date("Y-m-d", (time() + (24*3600*8))); // 7 ngay
	$sql = "update portal_user set recordstatus = $record_status,
	limited_bandwidth=10, trial_bandwidth=10*1000*1000*1000, 
	expired_date='$ndate'
	  where email='".$email."' AND recordstatus!=".RECORD_STATUS_DISABLE;
	$result = $mysqli->query($sql);
	if ($mysqli->query($sql) === TRUE) {
		return true;
	} else {
		//echo "Error: " . $sql . "<br>" . $mysqli->error;
		return false;
	}
}
function updateaccountinfo($email,$fullname,$phone,$address) {
	global $mysqli;
	db_connect();
	$sql = "update portal_user set address='".$address."',fullname='".$fullname."',phone='".$phone."',lastupdate=NOW() where email='".$email."'";
	$result = $mysqli->query($sql);
	if ($mysqli->query($sql) === TRUE) {
		return true;
	} else {
		//echo "Error: " . $sql . "<br>" . $mysqli->error;
		return false;
	}
}
function updateggtoken($email,$ggtoken,$ggdata) {
	global $mysqli;
	db_connect();
	$sql = "update portal_user set ggdata='".$ggdata."',ggtoken='".$ggtoken."',lastupdate=NOW() where email='".$email."'";
	$result = $mysqli->query($sql);
	if ($mysqli->query($sql) === TRUE) {
		return true;
	} else {
		//echo "Error: " . $sql . "<br>" . $mysqli->error;
		return false;
	}
}
