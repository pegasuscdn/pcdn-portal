<?php
class Page
{
   /* These are defaults */
   public $TotalResults;
   public $CurrentPage = 1;
   public $ResultsPerPage;
   public $LinksPerPage;
   public $LinkPattern='';
   public $TotalPages=1;
   public $ImagePre="&#9668;";
   public $ImageNext="&#9658;";
   public $More = '...';
   public function InfoArray()
   {
      $this->TotalPages = $this->getTotalPages();
      $this->CurrentPage = $this->getCurrentPage();
      $this->ResultArray = array(
                           "PREV_PAGE" => $this->getPrevPage(),
                           "NEXT_PAGE" => $this->getNextPage(),
                           "CURRENT_PAGE" => $this->CurrentPage,
                           "TOTAL_PAGES" => $this->TotalPages,
                           "TOTAL_RESULTS" => $this->TotalResults,
                           "PAGE_NUMBERS" => $this->getNumbers(),
						   "END_PAGE" => $this->getEndNumber(),
						   "LINK_PER_PAGE" => $this->LinksPerPage,
                           "START_OFFSET" => $this->getStartOffset(),
                           "END_OFFSET" => $this->getEndOffset(),
                           "RESULTS_PER_PAGE" => $this->ResultsPerPage,
						   "LINKS" => $this->getLinks()
                           );
      return $this->ResultArray;
   }
   /* Start information functions */
   public function getTotalPages()
   {
		/* Make sure we don't devide by zero */
		if($this->TotalResults != 0 && $this->ResultsPerPage != 0)
		{
			$result = ceil($this->TotalResults / $this->ResultsPerPage);
		}
		if(empty($result))
		{
			return 1;
		}
		else
		{
			return $result;
		}
   }
   public function getStartOffset()
   {
      $offset = $this->ResultsPerPage * ($this->CurrentPage - 1)+1;
      return $offset;
   }

   public function getEndOffset()
   {
	  if($this->getStartOffset() > ($this->TotalResults - $this->ResultsPerPage))
	  {
         $offset = $this->TotalResults;
      } else
	  {
         $offset = $this->getStartOffset() + $this->ResultsPerPage - 1;
      }
      return $offset;
   }

   public function getCurrentPage()
   {

         if ($this->CurrentPage > $this->TotalPages)
		 {
		 	$this->CurrentPage = $this->TotalPages;
		 }
		 if ($this->CurrentPage < 1)
		 {
		 	$this->CurrentPage = 1;
		 }
		 return $this->CurrentPage;

   }
   public function getPrevPage()
   {
      if($this->CurrentPage > 1) {
         return $this->CurrentPage - 1;
      } else {
         return 1;
      }
   }
   public function getNextPage()
   {
      if($this->CurrentPage < $this->TotalPages) {
         return $this->CurrentPage + 1;
      } else {
         return $this->TotalPages;
      }
   }
   public function getStartNumber()
   {
      $links_per_page_half = intval($this->LinksPerPage / 2);
      /* See if curpage is less than half links per page */
      if($this->CurrentPage <= $links_per_page_half || $this->TotalPages <= $this->LinksPerPage) {
         return 1;
      } elseif($this->CurrentPage >= ($this->TotalPages - $links_per_page_half)) {
         return $this->TotalPages - $this->LinksPerPage + 1;
      } else {
         return $this->CurrentPage - $links_per_page_half;
      }
   }
   public function getEndNumber()
   {
      if($this->TotalPages < $this->LinksPerPage) {
         return $this->TotalPages;
      } else {
         return $this->getStartNumber() + $this->LinksPerPage - 1;
      }
   }
	//lay so trang
   public function getNumbers()
   {
      for($i=$this->getStartNumber(); $i<=$this->getEndNumber(); $i++)
	  {
	  	 if($this->LinksPerPage<$this->TotalPages)
		 {
			 if($this->CurrentPage>1)
			{
				//$numbers[0]=$this->ImagePre;
				if($this->CurrentPage>=$this->LinksPerPage-1)
				{
						$numbers[1]=1;
				}
				if($this->CurrentPage>=$this->LinksPerPage)
				{

						$numbers[2]=$this->More;
				}
			}
		}
/*	   	else
		{
			if($this->CurrentPage>1)
			{
				$numbers[0]=$this->ImagePre;
			}
		}
*/			$numbers[]=$i;
      }
	   if($this->LinksPerPage<$this->TotalPages)
	   {
		   if($this->CurrentPage<$this->TotalPages)
		   {

				if($this->CurrentPage<=($this->TotalPages-$this->LinksPerPage)+2)
				{
					array_push($numbers,$this->More,$this->TotalPages/*,$this->ImageNext*/);
				}
				/*else
				{
					//if($this->CurrentPage<=$this->TotalPages-2)
					array_push($numbers,$this->ImageNext);
				}*/
		   }
		 }
      return $numbers;
   }
   public function getLinks()
   {
      for($i=$this->getStartNumber(); $i<=$this->getEndNumber(); $i++) {
         $links[$i] = sprintf($this->LinkPattern,$i);
      }
	  $prev=$this->getPrevPage();
	  $next=$this->getNextPage();
	  $first=1;
	  $last=$this->TotalPages;

	  $links[$prev] = sprintf($this->LinkPattern,$prev);
	  $links[$next] = sprintf($this->LinkPattern,$next);
	  $links[$first] = sprintf($this->LinkPattern,$first);
  	  $links[$last] = sprintf($this->LinkPattern,$last);
      return $links;
   }

   // son add them nxbadmin
    public function getNumbers2()
	{
      for($i=$this->getStartNumber(); $i<=$this->getEndNumber(); $i++)
	  {
	  	 if($this->LinksPerPage<$this->TotalPages)
		 {
			 if($this->CurrentPage>1)
			{
				if($this->CurrentPage>=$this->LinksPerPage-1)
				{
					$numbers[1]=1;
				}
				if($this->CurrentPage>=$this->LinksPerPage)
				{
					$numbers[2]=$this->More;
				}
			}
		}
		$numbers[]=$i;
      }
      return $numbers;
   }

   public function InfoArray2()
   {
      $this->TotalPages = $this->getTotalPages();
      $this->CurrentPage = $this->getCurrentPage();
      $this->ResultArray = array(
                           "PREV_PAGE" => $this->getPrevPage(),
                           "NEXT_PAGE" => $this->getNextPage(),
                           "CURRENT_PAGE" => $this->CurrentPage,
                           "TOTAL_PAGES" => $this->TotalPages,
                           "TOTAL_RESULTS" => $this->TotalResults,
                           "PAGE_NUMBERS" => $this->getNumbers2(),
						   "END_PAGE" => $this->getEndNumber(),
						   "LINK_PER_PAGE" => $this->LinksPerPage,
                           "START_OFFSET" => $this->getStartOffset(),
                           "END_OFFSET" => $this->getEndOffset(),
                           "RESULTS_PER_PAGE" => $this->ResultsPerPage,
						   "LINKS" => $this->getLinks(),
                           );
      return $this->ResultArray;
   }
}
?>
