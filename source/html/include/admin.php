<?php
function admin_check_session() {
	if (!isset($_SESSION['admin']['userid']) OR !isset($_SESSION['admin']['username'])) {
		//logout();
		redirect('/admin_login.php');
	}
}
function admin_user_login($username, $password) {
	global $mysqli;
	$record_status = RECORD_STATUS_ACTIVE;
	db_connect();
	$stmt = $mysqli->prepare("select id,username,password, hash, ggtoken from portal_admin where username=? and recordstatus=?");
	$stmt->bind_param('si', $username, $record_status);
	$stmt->execute();
	$result = $stmt->get_result();
	if ($result->num_rows != 1) {
		return false;
	}
	$row = $result->fetch_array(MYSQLI_ASSOC);
	$password2 = md5($password.$row['hash']);
	if ($password2 == $row['password']) {
		return $row;
	} else {
		return null;
	}
}

//CUSTOMER
function getcustomer($fullname,$intStatus,$page,$limit) {
	global $mysqli;
	db_connect();
	$start=$page*$limit-$limit;
	if($intStatus!=100)
		$where1=" AND recordstatus=".$intStatus;
	if($fullname!="")
		$where2=" AND (fullname like '%".$fullname."%' OR email like '%".$fullname."%')";
	$sql = "select * from portal_user where 1=1 ".$where1.$where2." order by id DESC limit ".$start.",".$limit;
	$result = $mysqli->query($sql);
	return $result;
}
function gettotalcustomer($fullname,$intStatus) {
	global $mysqli;
	db_connect();
	if($intStatus!=100)
		$where1=" AND recordstatus=".$intStatus;
	if($fullname!="")
		$where2=" AND (fullname like '%".$fullname."%' OR email like '%".$fullname."%')";
	$sql = "select count(id) as total from portal_user where 1=1 ".$where1.$where2."";
	$result = $mysqli->query($sql);
	return $result->fetch_assoc()["total"];
}
function customer_add($email, $password,$fullname,$address,$phone,$record_status) {
	global $mysqli;
	db_connect();
	$hash = substr(md5(mt_rand()), 0, 32);
	$pass = md5($password.$hash);
	$sql = "INSERT INTO portal_user (email, fullname, phone, address, password, hash, registerdate, recordstatus)VALUES ('".$email."','".$fullname."','".$phone."','".$address."','".$pass."','".$hash."',NOW(),".$record_status.")";
	if ($mysqli->query($sql) === TRUE) {
		return true;
	} else {
		//echo "Error: " . $sql . "<br>" . $mysqli->error;
		return false;
	}
}
function customer_info($email) {
	global $mysqli;
	db_connect();
	$sql = "select * from portal_user where email='".$email."'";
	$result = $mysqli->query($sql);
	return $result->fetch_assoc();
}
function customer_update($id,$email,$password,$fullname,$address,$phone,$record_status,$type) {
	global $mysqli;
	db_connect();
	if($type==1)
	{
		$hash = substr(md5(mt_rand()), 0, 32);
		$pass = md5($password.$hash);	
		$sql = "update portal_user set email='".$email."',password='".$pass."',hash='".$hash."',recordstatus='".$record_status."', address='".$address."',fullname='".$fullname."',phone='".$phone."',lastupdate=NOW() where id='".$id."'";
	}else{
		$sql = "update portal_user set email='".$email."',address='".$address."',fullname='".$fullname."',recordstatus='".$record_status."',phone='".$phone."',lastupdate=NOW() where id='".$id."'";
	}
	
	$result = $mysqli->query($sql);
	if ($mysqli->query($sql) === TRUE) {
		return true;
	} else {
		//echo "Error: " . $sql . "<br>" . $mysqli->error;
		return false;
	}
}




//ADMINISTRATOR
function getlistadmin($username,$intStatus,$page,$limit) {
	global $mysqli;
	db_connect();
	$start=$page*$limit-$limit;
	if($intStatus!=100)
		$where1=" AND recordstatus=".$intStatus;
	if($username!="")
		$where2=" AND username like '%".$username."%'";
	$sql = "select * from portal_admin where 1=1 ".$where1.$where2." order by id DESC limit ".$start.",".$limit;
	$result = $mysqli->query($sql);
	return $result;
}
function gettotaladmin($username,$intStatus) {
	global $mysqli;
	db_connect();
	if($intStatus!=100)
		$where1=" AND recordstatus=".$intStatus;
	if($username!="")
		$where2=" AND username like '%".$username."%'";
	$sql = "select count(id) as total from portal_admin where 1=1 ".$where1.$where2."";
	$result = $mysqli->query($sql);
	return $result->fetch_assoc()["total"];
}
function admin_info($username) {
	global $mysqli;
	db_connect();
	$sql = "select * from portal_admin where username='".$username."' OR id='".$username."'";
	$result = $mysqli->query($sql);
	return $result->fetch_assoc();
}
function admin_add($username,$password,$status) {
	global $mysqli;
	db_connect();
	$hash = substr(md5(mt_rand()), 0, 32);
	$pass = md5($password.$hash);
	$sql = "INSERT INTO portal_admin (username, password, hash, createdate, recordstatus)VALUES ('".$username."','".$pass."','".$hash."',NOW(),".$status.")";
	if ($mysqli->query($sql) === TRUE) {
		return true;
	} else {
		//echo "Error: " . $sql . "<br>" . $mysqli->error;
		return false;
	}
}
function admin_update($id,$username,$password,$record_status,$type) {
	global $mysqli;
	db_connect();
	if($type==1)
	{
		$hash = substr(md5(mt_rand()), 0, 32);
		$pass = md5($password.$hash);	
		$sql = "update portal_admin set username='".$username."',password='".$pass."',hash='".$hash."',lastupdate=NOW(),recordstatus='".$record_status."' where id='".$id."'";
	}else{
		$sql = "update portal_admin set username='".$username."',recordstatus='".$record_status."',lastupdate=NOW() where id='".$id."'";
	}
	//echo $sql;exit;
	$result = $mysqli->query($sql);
	if ($mysqli->query($sql) === TRUE) {
		return true;
	} else {
		//echo "Error: " . $sql . "<br>" . $mysqli->error;
		return false;
	}
}
function admin_updateggtoken($username,$ggtoken) {
	global $mysqli;
	db_connect();
	$sql = "update portal_admin set ggtoken='".$ggtoken."',lastupdate=NOW() where username='".$username."'";
	$result = $mysqli->query($sql);
	if ($mysqli->query($sql) === TRUE) {
		return true;
	} else {
		//echo "Error: " . $sql . "<br>" . $mysqli->error;
		return false;
	}
}

//CDN RESOURCE
function getlistcdnresource($keyword,$intStatus,$page,$limit) {
	global $mysqli;
	db_connect();
	$start=$page*$limit-$limit;
	if($intStatus!=100)
		$where1=" AND recordstatus=".$intStatus;
	if($keyword!="")
		$where2=" AND (name like '%".$keyword."%' OR hostname like '%".$keyword."%')";
	$sql = "select * from cdn_resource where 1=1 ".$where1.$where2." order by id DESC limit ".$start.",".$limit;
	$result = $mysqli->query($sql);
	return $result;
}
function gettotalcdnresource($keyword,$intStatus) {
	global $mysqli;
	db_connect();
	if($intStatus!=100)
		$where1=" AND recordstatus=".$intStatus;
	if($keyword!="")
		$where2=" AND (name like '%".$keyword."%' OR hostname like '%".$keyword."%')";
	$sql = "select count(id) as total from cdn_resource where 1=1 ".$where1.$where2."";
	$result = $mysqli->query($sql);
	return $result->fetch_assoc()["total"];
}

//BILLING
function getlistbilling($userid,$startdate,$enddate,$intStatus,$page,$limit) {
	global $mysqli;
	db_connect();
	$start=$page*$limit-$limit;
	if($intStatus!=100)
		$where1=" AND status=".$intStatus;
	if($userid>0)
		$where2=" AND userid=".$userid;
	if($startdate!="")
		$where3=" AND created_date=".$startdate;
	if($enddate!="")
		$where4=" AND expired_date=".$enddate;
	
	$sql = "select * from invoice where 1=1 ".$where1.$where2.$where3.$where4." order by id DESC limit ".$start.",".$limit;
	$result = $mysqli->query($sql);
	return $result;
}
function gettotalbilling($userid,$startdate,$enddate,$intStatus) {
	global $mysqli;
	db_connect();
	if($intStatus!=100)
		$where1=" AND status=".$intStatus;
	if($userid>0)
		$where2=" AND userid=".$userid;
	if($startdate!="")
		$where3=" AND created_date=".$startdate;
	if($enddate!="")
		$where4=" AND expired_date=".$enddate;
	$sql = "select count(id) as total from invoice where 1=1 ".$where1.$where2.$where3.$where4."";
	$result = $mysqli->query($sql);
	return $result->fetch_assoc()["total"];
}

