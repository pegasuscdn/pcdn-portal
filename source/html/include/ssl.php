<?php
function checkUsedSSL($id){
	global $mysqli;
	$data = array();

	db_connect();
	$stmt = $mysqli->prepare("select * from cdn_resource WHERE ssl_id=? limit 1");
	$stmt->bind_param('i', $id);
	$stmt->execute();
	$data=[];
	$result = $stmt->get_result();
	if ($result) {
		while ($row = $result->fetch_array(MYSQLI_ASSOC)) {
			$data[] = $row;
		}
	}
	return count($data);
}
function checkKeyCert($ssl_key,$ssl_cert){

		$arrCert= explode('-----END CERTIFICATE-----', $ssl_cert);
		if(count($arrCert)<=2)  return openssl_x509_check_private_key( $ssl_cert , $ssl_key );
		else{
			if(!openssl_pkey_get_private($ssl_key)) return false;
			$data=openssl_x509_parse($ssl_cert);
			if(count($data)) return true;
			return false;
		}
		foreach($arrCert as $cert){
		    if(trim($cert)){
				$_ssl_cert = $cert.'-----END CERTIFICATE-----';
				if(!openssl_x509_check_private_key( $_ssl_cert , $ssl_key ) ) {
		                return false;
		        }
		     }		
		}
	return true;
}
function checkKeyCert1($ssl_key,$ssl_cert){
	if(openssl_x509_check_private_key( $ssl_cert , $ssl_key ) ) {
      		return true;
  	} else {
     		return false;
  	}
	return 1;
}
function select_ssl_resources($userid,$id=0) {
	global $mysqli;
	$data = array();

	db_connect();
	if($id){
		$stmt = $mysqli->prepare("select * from cdn_ssl  where deleted=0 and userid=? AND `id`=?");;
	 	 $stmt->bind_param('ii', $userid,$id);
	}
	else{	
		$stmt = $mysqli->prepare("select * from cdn_ssl  where deleted=0 and userid=? ");
		$stmt->bind_param('i', $userid);
	}
	$stmt->execute();
	$result = $stmt->get_result();
	if ($result) {
		while ($row = $result->fetch_array(MYSQLI_ASSOC)) {
			$data[] = $row;
		}
	}
	$stmt->close();
	return $data;
}
function insert_ssl_resource($data) {
	global $mysqli;
	$record_status = RECORD_STATUS_PENDING_EXEC;
	db_connect();
	$sql = "insert into cdn_ssl (`name`,`key`,`cert`,`recordstatus`,`deleted`,`updateddate`,`userid`) values (?,?,?,?,?,NOW(),?)";
	$stmt = $mysqli->prepare($sql);
	$stmt->bind_param("sssiii",$data['name'], $data['key'], $data['cert'], $record_status  , $data['deleted'],  $data['userid']);
	return $stmt->execute();
}
function update_ssl_resource($data,$id) {
        global $mysqli;
        $record_status = RECORD_STATUS_PENDING_EXEC;
        db_connect();
        $sql = "update cdn_ssl SET `name`=?,`key`=?,`cert`=?,`recordstatus`=?,`updateddate`=NOW() WHERE userid=? AND `id`=?";
        $stmt = $mysqli->prepare($sql);
        $stmt->bind_param("sssiii",$data['name'], $data['key'], $data['cert'], $record_status , $data['userid'],$id);
        return $stmt->execute();
}
function delete_ssl_resource($get_user_id,$id){
	global $mysqli;
	$record_status = RECORD_STATUS_PENDING_EXEC;
        db_connect();
        $sql = "update cdn_ssl SET `deleted`=1 ,`recordstatus`=?,`updateddate`=NOW() WHERE userid=? AND `id`=?";
        $stmt = $mysqli->prepare($sql);
        $stmt->bind_param("iii",$record_status , $get_user_id ,$id);
        return $stmt->execute();
	
}
