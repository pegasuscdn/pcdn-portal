<?php
function get_cdn_site_type( $status=0){
                global $mysqli;
                db_connect();
                $sql = "select * FROM  `site` WHERE 1";
                if($status) $sql = "select * FROM  `site` WHERE status=1";
                $result = $mysqli->query($sql);
                $data=array();
                if ($result) {
                        while ($row = $result->fetch_array(MYSQLI_ASSOC)) {
                                $id=$row['id'];
                                $data[$id] = $row['is_international'];
                        }
                }
                return $data;
}

function get_list_http_header($value=""){
	global $mysqli;
	$data = array();
	db_connect();
	if($value==""){
		$stmt = $mysqli->prepare("select * from list_http_header  where  1 ORDER BY `name` ");
	 	 
	}
	else{
		$stmt = $mysqli->prepare("select * from list_http_header  where  name=?  ORDER BY `name`");
		$stmt->bind_param('s', $value);
	}
	$stmt->execute();
	$result = $stmt->get_result();
	if ($result) {
		while ($row = $result->fetch_array(MYSQLI_ASSOC)) {
			$data[] = $row;
		}
	}
	$stmt->close();
	return $data;
	
}
function get_api_key( $userid){
	 global $mysqli;db_connect();
        $stmt = $mysqli->prepare("select `api_key` FROM `cdn_api` WHERE `user_id`=? LIMIT 1 ");
        $stmt->bind_param('i', $userid);
        $stmt->execute();
        $result = $stmt->get_result();
	$data=[];	
        if ($result) {
                while ($row = $result->fetch_array(MYSQLI_ASSOC)) {
                        $data[] = $row;
                }
        }
        $stmt->close();
	if(count($data)) return $data[0]['api_key'];
	return "";
}
function callAPIPurgeCache($id,$key,$domain,$url){
  $API_URL="https://api.pegasustech.io/cache/purge";
  $oHTTPClient = curl_init($API_URL);
  $post = [
    'id' => $id,
    'key' => $key,
    'domain'   => $domain,
    'url'=>$url
];

  curl_setopt($oHTTPClient, CURLOPT_VERBOSE, 0);
  curl_setopt($oHTTPClient, CURLOPT_SSL_VERIFYPEER, FALSE);
  curl_setopt($oHTTPClient, CURLOPT_SSL_VERIFYHOST, FALSE);
  curl_setopt($oHTTPClient, CURLOPT_RETURNTRANSFER, TRUE);

  curl_setopt($oHTTPClient, CURLOPT_CUSTOMREQUEST, 'POST');
  curl_setopt($oHTTPClient, CURLOPT_POSTFIELDS, $post);
  return $strResult = curl_exec($oHTTPClient);
  
}
function update_cdn_resource_remove($userid,$hostid){
	global $mysqli;
	db_connect();
	$stmt = $mysqli->prepare("update cdn_resource set `deleted`=1, recordstatus=".RECORD_STATUS_PENDING_EXEC."
	where userid=? and id=? and recordstatus in (".RECORD_STATUS_ACTIVE.",".RECORD_STATUS_PENDING_EXEC.")");
	$stmt->bind_param('ii', $userid, $hostid);
	return $stmt->execute();
}
function select_cdn_resource_detail($userid,$resourceId){
	global $mysqli;db_connect();	
	$stmt = $mysqli->prepare("select  re.cdn_domain , re.ssl_domain,c.hostname  from cdn_resource  re
	left join `cdn_cname` c ON (c.`resource_id`=re.id )
	where re.userid=? and re.id=? and re.recordstatus in (".RECORD_STATUS_ACTIVE.",".RECORD_STATUS_PENDING_EXEC.")");
	$stmt->bind_param('ii', $userid,$resourceId);
	$stmt->execute();
	$result = $stmt->get_result();
	if ($result) {
		while ($row = $result->fetch_array(MYSQLI_ASSOC)) {
			$data[] = $row;
		}
	}
	$stmt->close();
	$return=[];
	$arrCheck=[];
	$otherDomain=['pegasus-edge.com','pegasus-pop.com'];
	foreach($data as $k=>$v){

				$tmp=trim($v['cdn_domain']);
				if($tmp){
					if(!isset($arrCheck[$tmp])){
						if(!isset( $_SESSION['genhostid'][$tmp] )){
							$id=cdn_get_hostid($tmp);
						 	$_SESSION['genhostid'][$tmp]=$id;
						}
						else $id=$_SESSION['genhostid'][$tmp];
						$return[$id]=$tmp;
						foreach($otherDomain as $oDomain){
                                                        $tmp1 = str_replace( CDN_DOMAIN , $oDomain , $tmp);
                                                        if(!isset( $_SESSION['genhostid'][$tmp1] )){
                                                                $id=cdn_get_hostid($tmp1);
                                                                $_SESSION['genhostid'][$tmp1]=$id;
                                                        }
                                                        else $id=$_SESSION['genhostid'][$tmp1];
                                                        $return[$id]=$tmp1;
                                                }
					}

				}
				$tmp=trim($v['ssl_domain']);
				if($tmp){
					if(!isset($arrCheck[$tmp])){
						//$id=cdn_get_hostid($tmp);
						if(!isset( $_SESSION['genhostid'][$tmp] )){
                                                	$id=cdn_get_hostid($tmp);
                                                 	$_SESSION['genhostid'][$tmp]=$id;
                                                }
                                                else $id=$_SESSION['genhostid'][$tmp];
						$return[$id]=$tmp;
						foreach($otherDomain as $oDomain){
                                                        $tmp1 = str_replace( CDN_DOMAIN , $oDomain , $tmp);
                                                        if(!isset( $_SESSION['genhostid'][$tmp1] )){
                                                                $id=cdn_get_hostid($tmp1);
                                                                $_SESSION['genhostid'][$tmp1]=$id;
                                                        }
                                                        else $id=$_SESSION['genhostid'][$tmp1];
                                                        $return[$id]=$tmp1;
                                                }
					}

				}
				$tmp=trim($v['hostname']);
				if($tmp){
					if(!isset($arrCheck[$tmp])){
						//$id=cdn_get_hostid($tmp);
						if(!isset( $_SESSION['genhostid'][$tmp] )){
                                                	$id=cdn_get_hostid($tmp);
                                                 	$_SESSION['genhostid'][$tmp]=$id;
                                                }
                                                else $id=$_SESSION['genhostid'][$tmp];
						$return[$id]=$tmp;
						foreach($otherDomain as $oDomain){
                                                        $tmp1 = str_replace( CDN_DOMAIN , $oDomain , $tmp);
                                                        if(!isset( $_SESSION['genhostid'][$tmp1] )){
                                                                $id=cdn_get_hostid($tmp1);
                                                                $_SESSION['genhostid'][$tmp1]=$id;
                                                        }
                                                        else $id=$_SESSION['genhostid'][$tmp1];
                                                        $return[$id]=$tmp1;
                                                }

					}

				}


	}

	return $return;
}
function delete_cdn_http_header_resource($resource_id,$id){
	global $mysqli;
	$record_status = RECORD_STATUS_PENDING_EXEC;
        db_connect();
        $sql = "DELETE FROM `cdn_http_header` WHERE `resource_id`=? AND `id`=?";
        $stmt = $mysqli->prepare($sql);
        $stmt->bind_param("ii",$resource_id , $id);
        return $stmt->execute();

}
function select_cdn_http_header_resources($resource_id,$id=0) {
	global $mysqli;
	$data = array();

	db_connect();
	if($id){
		$stmt = $mysqli->prepare("select * from cdn_http_header  where  resource_id=? AND `id`=?");;
	 	 $stmt->bind_param('ii', $resource_id,$id);
	}
	else{
		$stmt = $mysqli->prepare("select * from cdn_http_header  where  resource_id=? ");
		$stmt->bind_param('i', $resource_id);
	}
	$stmt->execute();
	$result = $stmt->get_result();
	if ($result) {
		while ($row = $result->fetch_array(MYSQLI_ASSOC)) {
			$data[] = $row;
		}
	}
	$stmt->close();
	return $data;
}
function insert_cdn_http_header_resource($data) {
	global $mysqli;
	$record_status = RECORD_STATUS_PENDING_EXEC;
	db_connect();
	$sql = "insert into cdn_http_header (`resource_id`,`domain_name`,`header_name`,`header_value`) values (?,?,?,?)";
	$stmt = $mysqli->prepare($sql);
	$stmt->bind_param("isss",$data['resource_id'],$data['domain_name'], $data['header_name'], $data['header_value']);
	return $stmt->execute();
}
function updateCDNresourcePendingStatus($userid,$id,$cname_id=-1){
	global $mysqli;
	db_connect();
	$acl = json_encode($acl);
	if($cname_id ==-1){
		$stmt = $mysqli->prepare("update cdn_resource set recordstatus=".RECORD_STATUS_PENDING_EXEC."
		where userid=? and id=? and recordstatus in (".RECORD_STATUS_ACTIVE.",".RECORD_STATUS_PENDING_EXEC.")");
		$stmt->bind_param('ii', $userid , $id);
	}
	else{

			$stmt = $mysqli->prepare("update cdn_resource set recordstatus=".RECORD_STATUS_PENDING_EXEC.",cname_id=?
			where userid=? and id=? and recordstatus in (".RECORD_STATUS_ACTIVE.",".RECORD_STATUS_PENDING_EXEC.")");
			$stmt->bind_param('iii', $cname_id,$userid , $id);

	}
	return $stmt->execute();

}
function update_cdntab_mobileurl($userid,$hostid,$cmobileURL,$cdn_platform){
	global $mysqli;
        db_connect();
	if($cmobileURL){
		 $stmt = $mysqli->prepare("update cdn_resource set cdn_redirect=? , cdn_platform= ?, recordstatus=".RECORD_STATUS_PENDING_EXEC."
                where userid=? and id=? and recordstatus in (".RECORD_STATUS_ACTIVE.",".RECORD_STATUS_PENDING_EXEC.")");
                $stmt->bind_param('siii', $cmobileURL,$cdn_platform , $userid, $hostid);
	}
	else{
		$stmt = $mysqli->prepare("update cdn_resource set cdn_redirect =NULL,cdn_platform=? , recordstatus=".RECORD_STATUS_PENDING_EXEC."
                where userid=? and id=? and recordstatus in (".RECORD_STATUS_ACTIVE.",".RECORD_STATUS_PENDING_EXEC.")");
                $stmt->bind_param('iii',$cdn_platform , $userid, $hostid);
	}
 	return $stmt->execute();
}
function update_cdntab3_ssl($userid,$hostid,$custom_ssl_https_domain,$custom_ssl_sslid){
	global $mysqli;
	db_connect();
	if($custom_ssl_sslid){
                $stmt = $mysqli->prepare("update cdn_resource set ssl_domain=NULL,ssl_id=?, recordstatus=".RECORD_STATUS_PENDING_EXEC."
                where userid=? and id=? and recordstatus in (".RECORD_STATUS_ACTIVE.",".RECORD_STATUS_PENDING_EXEC.")");
                $stmt->bind_param('iii', $custom_ssl_sslid, $userid, $hostid);
        }
	elseif($custom_ssl_https_domain && $custom_ssl_sslid){
		$stmt = $mysqli->prepare("update cdn_resource set ssl_domain=?, ssl_id=?, recordstatus=".RECORD_STATUS_PENDING_EXEC."
		where userid=? and id=? and recordstatus in (".RECORD_STATUS_ACTIVE.",".RECORD_STATUS_PENDING_EXEC.")");
		$stmt->bind_param('siii', $custom_ssl_https_domain,$custom_ssl_sslid, $userid, $hostid);
	}
	else {
		$stmt = $mysqli->prepare("update cdn_resource set ssl_domain=NULL, ssl_id=0, recordstatus=".RECORD_STATUS_PENDING_EXEC."
		where userid=? and id=? and recordstatus in (".RECORD_STATUS_ACTIVE.",".RECORD_STATUS_PENDING_EXEC.")");
		$stmt->bind_param('ii', $userid, $hostid);

	}
	return $stmt->execute();
}
function update_cdntab1_resource($data,$storage='') {
	/*
		$data = array(
		'id' => $hostid,
		'name' => $cdn_name,
		'type' => $cdn_type,
		//'cache'=>'khgsdfgkfgkjdhgfahfkj'
		'origin' => $my_origin_type.'://'.$domain,
		'userid' => get_user_id(),
	);
	*/
	global $mysqli;
	db_connect();
	$acl = json_encode($acl);
	$stmt = $mysqli->prepare("update cdn_resource set storage=?, name=?, type=?, origin=?, recordstatus=".RECORD_STATUS_PENDING_EXEC."
	where userid=? and id=? and recordstatus in (".RECORD_STATUS_ACTIVE.",".RECORD_STATUS_PENDING_EXEC.")");
	$stmt->bind_param('ssisii',$storage,  $data['name'],$data['type'],$data['origin'], $data['userid'], $data['id']);
	return $stmt->execute();
}
///////////////////cdn_cname
/*
cdn_cname
id	int(11) Auto Increment
resource_id	bigint(20)
hostname	varchar(100)
*/
function checkExistedCnameSslDomain($custom_ssl_https_domain,$sslDomain=0){
	global $mysqli;
	db_connect();
	///$sslDomain=1 : check trong table resource; action trong table cname
	// count in all col; if existed return num <> 0
	if(0){//$sslDomain){
		$stmt = $mysqli->prepare("select count(1) as num from `cdn_resource`  where  `ssl_domain`=? OR `cdn_domain`=?");
		$stmt->bind_param('ss', $custom_ssl_https_domain,$custom_ssl_https_domain);
	}
	else {
		$stmt = $mysqli->prepare("select count(1) as num from `cdn_cname`  where  `hostname`=?
			union select count(1) as num from `cdn_resource`  where  `ssl_domain`=? OR `cdn_domain`=?
		");
		$stmt->bind_param('sss', $custom_ssl_https_domain,$custom_ssl_https_domain,$custom_ssl_https_domain);
	}
	$stmt->execute();
	$result = $stmt->get_result();
	$num=0;
	if ($result) {
		while ($row = $result->fetch_array(MYSQLI_ASSOC))
			$num += $row['num'];
	}
	$stmt->close();
	return $num;
	$stmt = $mysqli->prepare("select count(1) as num from `cdn_cname`  where  `hostname`=? ");
	if($sslDomain)
		$stmt = $mysqli->prepare("select count(1) as num from `cdn_resource`  where  `ssl_domain`=? ");
	$stmt->bind_param('s', $custom_ssl_https_domain);
	$stmt->execute();
	$result = $stmt->get_result();
	if ($result) {
		$row = $result->fetch_array(MYSQLI_ASSOC);

	}
	$stmt->close();
	return $row['num'];
}
function select_cdn_cname_resources($resource_id,$id=0,$hostname='') {
	global $mysqli;
	$data = array();

	db_connect();
	if($id){
		$stmt = $mysqli->prepare("select * from cdn_cname  where  resource_id=? AND `id`=?");;
	 	 $stmt->bind_param('ii', $resource_id,$id);
	}
	elseif($hostname){
		$stmt = $mysqli->prepare("select * from cdn_cname  where  resource_id=? AND `hostname`=?");
		$stmt->bind_param('is', $resource_id,$hostname);
	}
	else{
		$stmt = $mysqli->prepare("select * from cdn_cname  where  resource_id=? ");
		$stmt->bind_param('i', $resource_id);
	}
	$stmt->execute();
	$result = $stmt->get_result();
	if ($result) {
		while ($row = $result->fetch_array(MYSQLI_ASSOC)) {
			$data[] = $row;
		}
	}
	$stmt->close();
	return $data;
}
function insert_cdn_cname_resource($data) {
	global $mysqli;
	db_connect();
	$sql = "insert into cdn_cname(`resource_id`,`hostname`) values (?,?)";
	$stmt = $mysqli->prepare($sql);
	//print_r( $data);
	$stmt->bind_param("is",$data['resource_id'], $data['hostname']);
	// print_r( $stmt); exit;	
	return $stmt->execute();
}
function update_cdn_acl_token($hostid, $userid, $token,$include_client_ip=0) {
	global $mysqli;
	db_connect();
	$acl = json_encode($acl);
	$stmt = $mysqli->prepare("update cdn_resource set token=?,include_client_ip=?, recordstatus=".RECORD_STATUS_PENDING_EXEC." where userid=? and id=? and recordstatus in (".RECORD_STATUS_ACTIVE.",".RECORD_STATUS_PENDING_EXEC.")");
	$stmt->bind_param('siii', $token,$include_client_ip, $userid, $hostid);
	return $stmt->execute();
}
function update_cdn_cache_expire_time($hostid, $userid, $expireTime) {
	global $mysqli;
	db_connect();
	$acl = json_encode($acl);
	$stmt = $mysqli->prepare("update cdn_resource set cache=?, recordstatus=".RECORD_STATUS_PENDING_EXEC." where userid=? and id=? and recordstatus in (".RECORD_STATUS_ACTIVE.",".RECORD_STATUS_PENDING_EXEC.")");
	$stmt->bind_param('sii', $expireTime, $userid, $hostid);
	return $stmt->execute();
}
function update_cdn_acl_http_origin($hostid, $userid, $http_origin) {
	global $mysqli;
	db_connect();
	$acl = json_encode($acl);
	$stmt = $mysqli->prepare("update cdn_resource set http_origin=?, recordstatus=".RECORD_STATUS_PENDING_EXEC." where userid=? and id=? and recordstatus in (".RECORD_STATUS_ACTIVE.",".RECORD_STATUS_PENDING_EXEC.")");
	$stmt->bind_param('sii', $http_origin, $userid, $hostid);
	return $stmt->execute();
}
function update_cdn_acl_geoip($hostid, $userid, $geoip) {
	global $mysqli;
	db_connect();
	$acl = json_encode($acl);
	$stmt = $mysqli->prepare("update cdn_resource set geoip_vn=?, recordstatus=".RECORD_STATUS_PENDING_EXEC." where userid=? and id=? and recordstatus in (".RECORD_STATUS_ACTIVE.",".RECORD_STATUS_PENDING_EXEC.")");
	$stmt->bind_param('iii', $geoip, $userid, $hostid);
	return $stmt->execute();
}
function update_cdn_cname_resource($data,$id) {
        global $mysqli;
        db_connect();
        $sql = "update cdn_cname SET `resource_id`=?,`hostname`=? WHERE `id`=?";
        $stmt = $mysqli->prepare($sql);
        $stmt->bind_param("isi",$data['resource_id'], $data['hostname'],$id);
        return $stmt->execute();
}
function delete_cdn_cname_resource($resource_id,$id){
	global $mysqli;
        db_connect();
        $sql = "DELETE FROM `cdn_cname` WHERE `resource_id`=? AND `id`=?";
        $stmt = $mysqli->prepare($sql);
        $stmt->bind_param("si",$resource_id , $id);
        return $stmt->execute();

}
/////////////////////// END cname

function check_data_input_withselecttime(&$ts,&$te){
			$ok=1;
			$pieces = explode("-", $ts);
                        if(count($pieces)<>3) $ok=0;
                        $ts =   $pieces[2].'-'.$pieces[1].'-'.$pieces[0]. " 00:00:00";
                        $pieces = explode("-", $te);
                        if(count($pieces)<>3) $ok=0;
                        $te =   $pieces[2].'-'.$pieces[1].'-'.$pieces[0]. " 23:59:59";
                        //$te = date("Y-m-d H:i:s", strtotime($te)+1);//+ 1s cho qua ngay hom qua; luc query lay' nho? hon
                                // max time is to te

                        if($ts<"2018-01-01 00:00:00") $ok=0;
                        if($te<"2018-01-01 23:59:59") $ok=0;
                        if($te<$ts) $ok=0;

			return $ok;
}
function get_chart_Time_template_withselecttime($ts,$te,&$endTime,&$startTime,&$duration,&$inteval,&$fchart){
											$endTime= strtotime($te)+1; //+ 1s cho qua ngay hom qua; luc query lay' nho? hon
											$startTime= strtotime($ts);
											$duration = $endTime-$startTime;
											$logest = 366*24*60*60;
											if($duration>$logest)  $startTime = $startTime - $logest - 1;
											if( $duration > (61*24*60*60)){ //tren 2 thang ; co' the? nam nhuan
																			$inteval=1*24*60*60;
																			$fchart  ="d-M";
											}
											elseif($duration > (24*60*60)){ // hon 1 ngay
															$inteval=24*60*60;
															$fchart  ="d-M";
											}
											else{
															$inteval=60*60;
															$fchart  ="H\h d-M";
											}

											return 1;
}	
function get_chart_Time_template($startTime){
	  $ccTime = strtotime( substr($startTime,0,-2)."00" );
	$ccTime = $ccTime - 5*60;
			// view 15"
		$arrTimeTemplate[1]['long']=15*60; //15"
		$arrTimeTemplate[1]['int']=1*60;      // 1"
		$arrTimeTemplate[1]['ftimekey']="H:i"; // format date display in chart
		$arrTimeTemplate[1]['EndTime'] = $ccTime;// numeric Endtime ; may be add inteval ex 5 10 15; 15 30, 45

		// view 30"
		$arrTimeTemplate[2]['long']=30*60; //30"
		$arrTimeTemplate[2]['int']=1*60;      // 1"
		$arrTimeTemplate[2]['ftimekey']="H:i"; // format date display in chart
		$arrTimeTemplate[2]['EndTime'] = $ccTime;// numeric Endtime ; may be add inteval ex 5 10 15; 15 30, 45

		// view 60"
		$minutes=5;
		$arrTimeTemplate[3]['long']=60*60; //60"
		$arrTimeTemplate[3]['int']=$minutes*60;      // 5"
		$arrTimeTemplate[3]['ftimekey']="H:i";
		$tmp=date("i",$ccTime);
		$addmore=0;
		$i=$tmp%$minutes;
		if($i) $addmore = ($minutes-$i)*60;
		$arrTimeTemplate[3]['EndTime']= $ccTime + $addmore ;

		// view 12h
		$minutes=30;
		$arrTimeTemplate[4]['long']=12*60*60; //12h
		$arrTimeTemplate[4]['int']=$minutes*60;      // 5"
		$arrTimeTemplate[4]['ftimekey']="H:i";
		$tmp=date("i",$ccTime);
		$addmore=0;
		$i=$tmp%$minutes;
		if($i) $addmore = ($minutes-$i)*60;
		$arrTimeTemplate[4]['EndTime']= $ccTime + $addmore ;

	 // view 24h
		$minutes=60;
		$arrTimeTemplate[5]['long']=24*60*60; //24h
		$arrTimeTemplate[5]['int']=$minutes*60;      // 60"
		$arrTimeTemplate[5]['ftimekey']="H:i";
		$tmp=date("i",$ccTime);
		$addmore=0;
		$i=$tmp%$minutes;
		if($i) $addmore = ($minutes-$i)*60;
		$arrTimeTemplate[5]['EndTime']= $ccTime + $addmore ;


		// view 1week
		$minutes= 24*60; //6h 1 diem
		$arrTimeTemplate[6]['long']=7*24*60*60; //7 days
		$arrTimeTemplate[6]['int']=$minutes*60;      // 1h
		$arrTimeTemplate[6]['ftimekey']="d-M";
	        $arrTimeTemplate[6]['EndTime']= strtotime(date("Y-m-d 00:00:00",$ccTime) ) ;




		// 30 day
		$minutes= 24*60; //24h 1 diem
		$arrTimeTemplate[7]['long']=30*24*60*60; //30 days
		$arrTimeTemplate[7]['int']=$minutes*60;      // 1ngay
		$arrTimeTemplate[7]['ftimekey']="d-M";
	        $arrTimeTemplate[7]['EndTime']= strtotime(date("Y-m-d 00:00:00",$ccTime) ) ;

	 // 60 day
		$minutes= 1*24*60; //2*24h 1 diem
		$arrTimeTemplate[8]['long']=60*24*60*60; //30 days
		$arrTimeTemplate[8]['int']=$minutes*60;      // 2ngay
		$arrTimeTemplate[8]['ftimekey']="d-M";
	 $arrTimeTemplate[8]['EndTime']= strtotime(date("Y-m-d 00:00:00",$ccTime) ) ;

	 // 1 year day
		$minutes= 1*24*60; //1 week 1 diem
		$arrTimeTemplate[9]['long']= 52*7*24*60*60; // 52 weeks
		$arrTimeTemplate[9]['int']=$minutes*60;      // 7 ngay
		$arrTimeTemplate[9]['ftimekey']="d-M";
		//echo date("Y-m-d 00:00:00",$ccTime);
		$tmptime = strtotime(date("Y-m-d 00:00:00",$ccTime) ) ;
		//echo 'vvvv'.date("w",$ccTime); echo "\n";
		//$i = (date("w",$ccTime));  // w -0 sunday  -> 6 sat
		$addmore=0;
		//if($i) $addmore= (7 - $i)*24*60*60 ; // 1 tuan 7 ngay
	  //echo  date("Y-m-d H:i:s",$tmptime);echo "-- $addmore --\n";
	  $arrTimeTemplate[9]['EndTime']= $tmptime + $addmore ;

		return $arrTimeTemplate;

}
function get_cdn_report_data($sql){
	 global $report_mysqli;
         db_connectReport();
	 $stmt = $report_mysqli->prepare($sql);
	 $stmt->execute();
         return $stmt->get_result();

}
function get_cdn_request($dstart,$dend,$hostid="",$page=1){
			global $report_mysqli;
			db_connectReport();
			$table='cdn_request_'.date("Y");
			$where=" (`logdate`>='$dstart' AND  `logdate`<'$dend') ";
			if($hostid)
				$where .=" AND `hostid`='$hostid'";
			$size = 1000;
			$f =  ($page-1)*$size;
			$limit="";// "Limit $f , $size";
			//$stmt = $report_mysqli->prepare("select logdate, sum(bandwidth) as bw FROM $table WHERE $where GROUP BY  `logdate` ORDER BY `logdate` $limit");
			//echo "select *  FROM $table WHERE $where  ORDER BY `logdate` $limit"; exit;
			$stmt = $report_mysqli->prepare("select *  FROM $table WHERE $where  ORDER BY `logdate` $limit");
			//echo "select logdate, bandwidth as  bw FROM $table WHERE $where  ORDER BY `logdate` $limit";
			//$stmt->bind_param('ii', $hostid, $userid);
			$stmt->execute();
			$result = $stmt->get_result();
			//print_r( $result); exit;
			$data=[];
			if ($result) {
				return $result;
				//$data = mysqli_DoFetchName($result);
			}
		
			$stmt->close();
			return false;

}
function delete_cdn_cname($hostid, $userid, $index) {
	global $mysqli;
	db_connect();
	$stmt = $mysqli->prepare("select user_cname from cdn_resource where id=? and userid=? and https=0 and recordstatus in (".RECORD_STATUS_ACTIVE.",".RECORD_STATUS_PENDING_EXEC.")");
	$stmt->bind_param('ii', $hostid, $userid);
	$stmt->execute();
	$result = $stmt->get_result();
	if ($result) {
		$data = $result->fetch_array(MYSQLI_ASSOC);
	}
	$stmt->close();

	if ($data['user_cname'] != '') {
		$data = json_decode($data['user_cname']);
		if (isset($data[$index])) {
			unset($data[$index]);

			// reset key
			$data2 = array();
			foreach($data as $value) {
				$data2[] = $value;
			}

			$data = json_encode($data2);
			$stmt = $mysqli->prepare("update cdn_resource set user_cname=?, recordstatus=".RECORD_STATUS_PENDING_EXEC." where userid=? and id=? and https=0 and recordstatus in (".RECORD_STATUS_ACTIVE.",".RECORD_STATUS_PENDING_EXEC.")");
			$stmt->bind_param('sii', $data, $userid, $hostid);
			return $stmt->execute();
		}
	}

	return false;
}

function update_cdn_cname($hostid, $userid, $cname) {
	global $mysqli;
	db_connect();
	$stmt = $mysqli->prepare("select user_cname from cdn_resource where id=? and userid=? and https=0 and recordstatus in (".RECORD_STATUS_ACTIVE.",".RECORD_STATUS_PENDING_EXEC.")");

	$stmt->bind_param('ii', $hostid, $userid);
	$stmt->execute();
	$result = $stmt->get_result();
	if ($result) {
		$data = $result->fetch_array(MYSQLI_ASSOC);
	}
	$stmt->close();

	if ($data['user_cname'] == '') {
		$data = array();
	} else {
		$data = json_decode($data['user_cname']);
		if (!is_array($data)) {
			return false;
		}
		if (in_array($cname, $data)) {
			return false;
		}
	}

	$data[] = $cname;
	$data = json_encode($data);
	$stmt = $mysqli->prepare("update cdn_resource set user_cname=?, recordstatus=".RECORD_STATUS_PENDING_EXEC." where userid=? and id=? and https=0 and recordstatus in (".RECORD_STATUS_ACTIVE.",".RECORD_STATUS_PENDING_EXEC.")");
	$stmt->bind_param('sii', $data, $userid, $hostid);
	return $stmt->execute();

	return false;
}
function delete_cdn_geoip($hostid, $userid, $index) {
	global $mysqli;
	db_connect();
	$stmt = $mysqli->prepare("select geoip from cdn_resource where id=? and userid=? and recordstatus in (".RECORD_STATUS_ACTIVE.",".RECORD_STATUS_PENDING_EXEC.")");
	$stmt->bind_param('ii', $hostid, $userid);
	$stmt->execute();
	$result = $stmt->get_result();
	if ($result) {
		$data = $result->fetch_array(MYSQLI_ASSOC);
	}
	$stmt->close();

	if ($data['geoip'] != '') {
		$data = json_decode($data['geoip']);
		if (isset($data[$index])) {
			unset($data[$index]);

			// reset key
			$data2 = array();
			foreach($data as $value) {
				$data2[] = $value;
			}

			$data = json_encode($data2);
			$stmt = $mysqli->prepare("update cdn_resource set geoip=?, recordstatus=".RECORD_STATUS_PENDING_EXEC." where userid=? and id=? and recordstatus in (".RECORD_STATUS_ACTIVE.",".RECORD_STATUS_PENDING_EXEC.")");
			$stmt->bind_param('sii', $data, $userid, $hostid);
			return $stmt->execute();
		}
	}

	return false;
}

function update_cdn_geoip($hostid, $userid, $referer) {
	global $mysqli;
	db_connect();
	$stmt = $mysqli->prepare("select geoip from cdn_resource where id=? and userid=? and recordstatus in (".RECORD_STATUS_ACTIVE.",".RECORD_STATUS_PENDING_EXEC.")");

	$stmt->bind_param('ii', $hostid, $userid);
	$stmt->execute();
	$result = $stmt->get_result();
	if ($result) {
		$data = $result->fetch_array(MYSQLI_ASSOC);
	}
	$stmt->close();

	if ($data['geoip'] == '') {
		$data = array();
	} else {
		$data = json_decode($data['geoip']);
		if (!is_array($data)) {
			return false;
		}
		if (in_array($referer, $data)) {
			return false;
		}
	}

	$data[] = $referer;
	$data = json_encode($data);
	$stmt = $mysqli->prepare("update cdn_resource set geoip=?, recordstatus=".RECORD_STATUS_PENDING_EXEC." where userid=? and id=? and recordstatus in (".RECORD_STATUS_ACTIVE.",".RECORD_STATUS_PENDING_EXEC.")");
	$stmt->bind_param('sii', $data, $userid, $hostid);
	return $stmt->execute();

	return false;
}

function delete_cdn_referer($hostid, $userid, $index) {
	global $mysqli;
	db_connect();
	$stmt = $mysqli->prepare("select user_referer from cdn_resource where id=? and userid=? and recordstatus in (".RECORD_STATUS_ACTIVE.",".RECORD_STATUS_PENDING_EXEC.")");
	$stmt->bind_param('ii', $hostid, $userid);
	$stmt->execute();
	$result = $stmt->get_result();
	if ($result) {
		$data = $result->fetch_array(MYSQLI_ASSOC);
	}
	$stmt->close();

	if ($data['user_referer'] != '') {
		$data = json_decode($data['user_referer']);
		if (isset($data[$index])) {
			unset($data[$index]);

			// reset key
			$data2 = array();
			foreach($data as $value) {
				$data2[] = $value;
			}

			$data = json_encode($data2);
			$stmt = $mysqli->prepare("update cdn_resource set user_referer=?, recordstatus=".RECORD_STATUS_PENDING_EXEC." where userid=? and id=? and recordstatus in (".RECORD_STATUS_ACTIVE.",".RECORD_STATUS_PENDING_EXEC.")");
			$stmt->bind_param('sii', $data, $userid, $hostid);
			return $stmt->execute();
		}
	}

	return false;
}

function update_cdn_referer($hostid, $userid, $referer) {
	global $mysqli;
	db_connect();
	$stmt = $mysqli->prepare("select user_referer from cdn_resource where id=? and userid=? and recordstatus in (".RECORD_STATUS_ACTIVE.",".RECORD_STATUS_PENDING_EXEC.")");

	$stmt->bind_param('ii', $hostid, $userid);
	$stmt->execute();
	$result = $stmt->get_result();
	if ($result) {
		$data = $result->fetch_array(MYSQLI_ASSOC);
	}
	$stmt->close();

	if ($data['user_referer'] == '') {
		$data = array();
	} else {
		$data = json_decode($data['user_referer']);
		if (!is_array($data)) {
			return false;
		}
		if (in_array($referer, $data)) {
			return false;
		}
	}

	$data[] = $referer;
	$data = json_encode($data);
	$stmt = $mysqli->prepare("update cdn_resource set user_referer=?, recordstatus=".RECORD_STATUS_PENDING_EXEC." where userid=? and id=? and recordstatus in (".RECORD_STATUS_ACTIVE.",".RECORD_STATUS_PENDING_EXEC.")");
	$stmt->bind_param('sii', $data, $userid, $hostid);
	return $stmt->execute();

	return false;
}


function generate_cdn_token($hostid, $userid) {
	global $mysqli;
	db_connect();
	$stmt = $mysqli->prepare("select hostname from cdn_resource where id=? and userid=? and recordstatus in (".RECORD_STATUS_ACTIVE.",".RECORD_STATUS_PENDING_EXEC.")");
	$stmt->bind_param('ii', $hostid, $userid);
	$stmt->execute();
	$result = $stmt->get_result();
	if ($result) {
		$data = $result->fetch_array(MYSQLI_ASSOC);
	}
	$stmt->close();

	if (empty($data['hostname'])) {
		return false;
	}
	return md5($data['hostname'].'20180907');
}

function cdn_get_hostid($hostname) {
	exec('/usr/bin/python3 '.BASE_PATH.'/api/cdn_get_hostid.py '.$hostname, $output, $code);
	if ($code == 0) {
		return trim($output[0]);
	}
	return false;
}

function get_cdn_resource($userid, $id) {
	global $mysqli;
	$data = array();
	
	db_connect();
	//debug("select * from cdn_resource where userid={$userid} and id={$id} and recordstatus in (".RECORD_STATUS_ACTIVE.",".RECORD_STATUS_PENDING_EXEC.")");
	$stmt = $mysqli->prepare("select * from cdn_resource where `deleted`=0 AND userid=? and id=? and recordstatus in (".RECORD_STATUS_ACTIVE.",".RECORD_STATUS_PENDING_EXEC.",".RECORD_STATUS_PENDING_DNS.")");
	$stmt->bind_param('ii', $userid, $id);
	
	$stmt->execute();
	$result = $stmt->get_result();
	if ($result) {
		$data = $result->fetch_array(MYSQLI_ASSOC);
	}
	$stmt->close();
	return $data;
}

function insert_cdn_resource($data, $storage='') {
	global $mysqli;
	$record_status = RECORD_STATUS_PENDING_EXEC;
	$my_chacheValue = [1=>'14d',2=>'15m',3=>'0'];
	$tmp=$data['type'];
	db_connect();
                $sql = "insert into cdn_resource (`id`,`name`,`cdn_domain`,`type`,`cache`,`origin`,`storage`,`userid`,`createdate`,`recordstatus`) values (?,?,?,?,?,?,?,?,NOW(),?)";
	$stmt = $mysqli->prepare($sql);
	$stmt->bind_param('ississsii', $data['id'], $data['name'], $data['cdn_domain'], $data['type'], $my_chacheValue[$tmp], $data['origin'], $storage , $data['userid'], $record_status);
	//print_r( $stmt); exit;
	return $stmt->execute();
}

function update_cdn_acl($hostid, $userid, $acl) {
	global $mysqli;
	db_connect();
	$acl = json_encode($acl);
	$stmt = $mysqli->prepare("update cdn_resource set acl=?, recordstatus=".RECORD_STATUS_PENDING_EXEC." where userid=? and id=? and recordstatus in (".RECORD_STATUS_ACTIVE.",".RECORD_STATUS_PENDING_EXEC.")");
	$stmt->bind_param('sii', $acl, $userid, $hostid);
	return $stmt->execute();
}

function select_cdn_resources($userid) {
	global $mysqli;
	$data = array();
	db_connect();
	if ($userid == 0) {
		$stmt = $mysqli->prepare("select id, name as hostname from cdn_resource where recordstatus in (".RECORD_STATUS_ACTIVE.",".RECORD_STATUS_PENDING_EXEC.")");
	} else {
		$stmt = $mysqli->prepare("select id, name as hostname from cdn_resource where userid=? and recordstatus in (".RECORD_STATUS_ACTIVE.",".RECORD_STATUS_PENDING_EXEC.")");
		$stmt->bind_param('i', $userid);
	}
	
	$stmt->execute();
	$result = $stmt->get_result();
	if ($result) {
		while ($row = $result->fetch_array(MYSQLI_ASSOC)) {
			$data[$row['id']] = $row['hostname'];
		}	
	}
	$stmt->close();
	return $data;
}

function select_cdn_resources2($userid) {
	global $mysqli;
	$data = array();

	db_connect();
	$stmt = $mysqli->prepare("select id, name, origin,  cdn_domain as hostname, type,recordstatus,deleted from cdn_resource where userid=? and recordstatus in (".RECORD_STATUS_ACTIVE.",".RECORD_STATUS_PENDING_EXEC.",".RECORD_STATUS_PENDING_DNS.")");
	$stmt->bind_param('i', $userid);
	
	$stmt->execute();
	$result = $stmt->get_result();
	if ($result) {
		while ($row = $result->fetch_array(MYSQLI_ASSOC)) {
			$data[] = $row;
		}	
	}
	$stmt->close();
	return $data;
}

function update_requests_last_minute($data, $new) {
	$obj = new StdClass();
	$obj->id = $new['id'];
	$obj->hostname =  $new['hostname'];
	$obj->time = $new['time'];
	$obj->requests = intval($new['requests']);

	$count = count($data);
	if ($count == 0) {
		$data[] = $obj;
		return json_encode($data);
	} else if ($count >= 15) {
		array_shift($data);
	}

	$data[] = $obj;
	return json_encode($data);
}

function get_requests_last_minute() {
	global $CONFIG;

	if (date('i') == '00') {
		$query = "select * from cdn.view_last_minute_59";
	} else {
		$query = "select * from cdn.view_last_minute";
	}
	
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $CONFIG['datalog_url']."/?user={$CONFIG['datalog']['user']}&password={$CONFIG['datalog']['password']}&query=".urlencode($query));
	curl_setopt($ch, CURLOPT_HEADER, false);
	curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
	curl_setopt($ch, CURLOPT_TIMEOUT, 20);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	$result = curl_exec($ch);

	$result = explode("\n", $result);
	$data = array();
	foreach ($result as $row) {
		if ($row == "") continue;
		$tmp = explode("\t", $row);
		$data['requests_last_minute_'.$tmp[0]] = $tmp[1];
	}
	return $data;
}

function redis_requests_last_minute($data) {
	$result = array();
	foreach ($data as $value) {
		$value = json_decode($value);
		$result['requests_last_minute_'.$value[0]->id] = $value;
	}
	return $result;
}

function calculate_bw_by_hour($past_hour) {
	global $CONFIG;

	$hour = $past_hour * 3600;

	if (date('H') == '00') {
		$query = "insert into cdn.cdn_bw_hourly select now(), hostid,year,month,day,hour,zoneid,intDiv(sum(size),1000000) from cdn.cdn_access_log_all where year=toYear(now()) and month=toMonth(now()) and day=toDayOfMonth(now()-86400) and hour=23 group by hostid,year,month,day,hour,zoneid";
	} else {
		$query = "insert into cdn.cdn_bw_hourly select now(), hostid,year,month,day,hour,zoneid,intDiv(sum(size),1000000) from cdn.cdn_access_log_all where year=toYear(now()) and month=toMonth(now()) and day=toDayOfMonth(now()) and hour=toHour(now()-{$hour}) group by hostid,year,month,day,hour,zoneid";
	}

	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $CONFIG['datalog_url']."/?user={$CONFIG['datalog']['user']}&password={$CONFIG['datalog']['password']}&query=".urlencode($query));
	//curl_setopt($ch, CURLOPT_ENCODING, "");
	curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-type: text/plain', 'Content-length: 0'));
	curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
	curl_setopt($ch, CURLOPT_TIMEOUT, 20);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, false);
	curl_setopt($ch, CURLOPT_POST, true);
	curl_exec($ch);
	$code1 = curl_getinfo($ch, CURLINFO_HTTP_CODE);

	// replicate
	curl_setopt($ch, CURLOPT_URL, $CONFIG['datalog_url2']."/?user={$CONFIG['datalog']['user']}&password={$CONFIG['datalog']['password']}&query=".urlencode($query));
	curl_exec($ch);
	$code2 = curl_getinfo($ch, CURLINFO_HTTP_CODE);

	return array($code1, $code2);
}

function get_bandwidth_last_hour() {
	global $CONFIG;

	if (date('H') == '00') {
		$query = "select * from cdn.view_last_hour_0";
	} else {
		$query = "select * from cdn.view_last_hour";
	}

	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $CONFIG['datalog_url']."/?user={$CONFIG['datalog']['user']}&password={$CONFIG['datalog']['password']}&query=".urlencode($query));
	curl_setopt($ch, CURLOPT_HEADER, false);
	curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
	curl_setopt($ch, CURLOPT_TIMEOUT, 20);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	$result = curl_exec($ch);
	$httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

	// retry slave
	if ($httpcode != 200) {
		curl_setopt($ch, CURLOPT_URL, $CONFIG['datalog_url2']."/?user={$CONFIG['datalog']['user']}&password={$CONFIG['datalog']['password']}&query=".urlencode($query));
		$result = curl_exec($ch);
	}

	$result = explode("\n", $result);
	$data = array();
	foreach ($result as $row) {
		if ($row == "") continue;
		$tmp = explode("\t", $row);
		$data['bandwidth_last_hour_'.$tmp[0]] = $tmp[1];
	}
	return $data;
}

function redis_bandwidth_last_hour($data) {
	$result = array();
	foreach ($data as $value) {
		$value = json_decode($value);
		$result['bandwidth_last_hour_'.$value[0]->id] = $value;
	}
	return $result;
}

function update_bandwidth_last_hour($data, $new) {
	$obj = new StdClass();
	$obj->id = $new['id'];
	$obj->hostname =  $new['hostname'];
	$obj->time = $new['time'];
	$obj->bandwidth = intval($new['bandwidth']);

	$count = count($data);
	if ($count == 0) {
		$data[] = $obj;
		return json_encode($data);
	} else if ($count >= 24) {
		array_shift($data);
	}

	$data[] = $obj;
	return json_encode($data);
}

function get_bandwidth_last_day() {
	global $CONFIG;

	$query = "select * from cdn.view_last_day";
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $CONFIG['datalog_url']."/?user={$CONFIG['datalog']['user']}&password={$CONFIG['datalog']['password']}&query=".urlencode($query));
	curl_setopt($ch, CURLOPT_HEADER, false);
	curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
	curl_setopt($ch, CURLOPT_TIMEOUT, 20);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	$result = curl_exec($ch);
	$httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

	// retry slave
	if ($httpcode != 200) {
		curl_setopt($ch, CURLOPT_URL, $CONFIG['datalog_url2']."/?user={$CONFIG['datalog']['user']}&password={$CONFIG['datalog']['password']}&query=".urlencode($query));
		$result = curl_exec($ch);
	}

	$result = explode("\n", $result);
	$data = array();
	foreach ($result as $row) {
		if ($row == "") continue;
		$tmp = explode("\t", $row);
		$data['bandwidth_last_day_'.$tmp[0]] = $tmp[1];
	}
	return $data;
}

function redis_bandwidth_last_day($data) {
	$result = array();
	foreach ($data as $value) {
		$value = json_decode($value);
		$result['bandwidth_last_day_'.$value[0]->id] = $value;
	}
	return $result;
}

function update_bandwidth_last_day($data, $new) {
	$obj = new StdClass();
	$obj->id = $new['id'];
	$obj->hostname =  $new['hostname'];
	$obj->time = $new['time'];
	$obj->bandwidth = intval($new['bandwidth']);

	$count = count($data);
	if ($count == 0) {
		$data[] = $obj;
		return json_encode($data);
	} else if ($count >= 14) {
		array_shift($data);
	}

	$data[] = $obj;
	return json_encode($data);
}

function json_costs($data, $new) {
	$obj = new StdClass();
	$obj->id = $new['id'];
	$obj->hostname =  $new['hostname'];
	$obj->time = $new['time'];
	$obj->costs = intval($new['costs']);

	$count = count($data);
	if ($count == 0) {
		$data[] = $obj;
		return json_encode($data);
	} else if ($count >= 21) {
		array_shift($data);
	}

	$data[] = $obj;
	return json_encode($data);
}

function get_bandwidth_last_month() {
	global $CONFIG;

	$query = "select * from cdn.view_last_month";
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $CONFIG['datalog_url']."/?user={$CONFIG['datalog']['user']}&password={$CONFIG['datalog']['password']}&query=".urlencode($query));
	curl_setopt($ch, CURLOPT_HEADER, false);
	curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
	curl_setopt($ch, CURLOPT_TIMEOUT, 20);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	$result = curl_exec($ch);
	$httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

	// retry slave
	if ($httpcode != 200) {
		curl_setopt($ch, CURLOPT_URL, $CONFIG['datalog_url2']."/?user={$CONFIG['datalog']['user']}&password={$CONFIG['datalog']['password']}&query=".urlencode($query));
		$result = curl_exec($ch);
	}

	$result = explode("\n", $result);
	$data = array();
	foreach ($result as $row) {
		if ($row == "") continue;
		$tmp = explode("\t", $row);
		$data['bandwidth_last_month_'.$tmp[0]] = $tmp[1];
	}
	return $data;
}

function redis_bandwidth_last_month($data) {
	$result = array();
	foreach ($data as $value) {
		$value = json_decode($value);
		$result['bandwidth_last_month_'.$value[0]->id] = $value;
	}
	return $result;
}

function update_bandwidth_last_month($data, $new) {
	$obj = new StdClass();
	$obj->id = $new['id'];
	$obj->hostname =  $new['hostname'];
	$obj->time = $new['time'];
	$obj->bandwidth = intval($new['bandwidth']);

	$count = count($data);
	if ($count == 0) {
		$data[] = $obj;
		return json_encode($data);
	} else if ($count >= 12) {
		array_shift($data);
	}

	$data[] = $obj;
	return json_encode($data);
}

function calculate_hits_by_hour($past_hour) {
	global $CONFIG;

	$hour = $past_hour * 3600;

	if (date('H') == '00') {
		$query = "insert into cdn.cdn_hits_hourly select now(),hostid,year,month,day,hour,hits,misses from (select hostid,year,month,day,hour,count(cache) as hits from cdn.cdn_access_log_all where cache='hit' and year=toYear(now()) and month=toMonth(now()) and day=toDayOfMonth(now()-86400) and hour=23 group by hostid,year,month,day,hour) any left join (select hostid,year,month,day,hour,count(cache) as misses from cdn.cdn_access_log_all where cache='miss' and year=toYear(now()) and month=toMonth(now()) and day=toDayOfMonth(now()-86400) and hour=23 group by hostid,year,month,day,hour) using hostid";
	} else {
		$query = "insert into cdn.cdn_hits_hourly select now(),hostid,year,month,day,hour,hits,misses from (select hostid,year,month,day,hour,count(cache) as hits from cdn.cdn_access_log_all where cache='hit' and year=toYear(now()) and month=toMonth(now()) and day=toDayOfMonth(now()) and hour=toHour(now()-{$hour}) group by hostid,year,month,day,hour) any left join (select hostid,year,month,day,hour,count(cache) as misses from cdn.cdn_access_log_all where cache='miss' and year=toYear(now()) and month=toMonth(now()) and day=toDayOfMonth(now()) and hour=toHour(now()-{$hour}) group by hostid,year,month,day,hour) using hostid";
	}

	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $CONFIG['datalog_url']."/?user={$CONFIG['datalog']['user']}&password={$CONFIG['datalog']['password']}&query=".urlencode($query));
	//curl_setopt($ch, CURLOPT_ENCODING, "");
	curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-type: text/plain', 'Content-length: 0'));
	curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
	curl_setopt($ch, CURLOPT_TIMEOUT, 20);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, false);
	curl_setopt($ch, CURLOPT_POST, true);
	curl_exec($ch);
	$code1 = curl_getinfo($ch, CURLINFO_HTTP_CODE);

	// replicate
	curl_setopt($ch, CURLOPT_URL, $CONFIG['datalog_url2']."/?user={$CONFIG['datalog']['user']}&password={$CONFIG['datalog']['password']}&query=".urlencode($query));
	curl_exec($ch);
	$code2 = curl_getinfo($ch, CURLINFO_HTTP_CODE);

	return array($code1, $code2);
}

function get_hits_last_hour() {
	global $CONFIG;

	if (date('H') == '00') {
		$query = "select * from cdn.view_hits_last_hour_0";
	} else {
		$query = "select * from cdn.view_hits_last_hour";
	}

	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $CONFIG['datalog_url']."/?user={$CONFIG['datalog']['user']}&password={$CONFIG['datalog']['password']}&query=".urlencode($query));
	curl_setopt($ch, CURLOPT_HEADER, false);
	curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
	curl_setopt($ch, CURLOPT_TIMEOUT, 20);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	$result = curl_exec($ch);
	$httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

	// retry slave
	if ($httpcode != 200) {
		curl_setopt($ch, CURLOPT_URL, $CONFIG['datalog_url2']."/?user={$CONFIG['datalog']['user']}&password={$CONFIG['datalog']['password']}&query=".urlencode($query));
		$result = curl_exec($ch);
	}

	$result = explode("\n", $result);
	$data = array();
	foreach ($result as $row) {
		if ($row == "") continue;
		$tmp = explode("\t", $row);
		$data['hits_last_hour_'.$tmp[0]] = $tmp[1].','.$tmp[2];
	}
	return $data;
}

function redis_hits_last_hour($data) {
	$result = array();
	foreach ($data as $value) {
		$value = json_decode($value);
		$result['hits_last_hour_'.$value[0]->id] = $value;
	}
	return $result;
}

function update_hits_last_hour($data, $new) {
	$obj = new StdClass();
	$obj->id = $new['id'];
	$obj->hostname =  $new['hostname'];
	$obj->time = $new['time'];
	$obj->hits = intval($new['hits']);
	$obj->misses = intval($new['misses']);

	$count = count($data);
	if ($count == 0) {
		$data[] = $obj;
		return json_encode($data);
	} else if ($count >= 12) {
		array_shift($data);
	}

	$data[] = $obj;
	return json_encode($data);
}

function redis_hits_last_day($data) {
	$result = array();
	foreach ($data as $value) {
		$value = json_decode($value);
		$result['hits_last_day_'.$value[0]->id] = $value;
	}
	return $result;
}

function update_hits_last_day($data, $new) {
	$obj = new StdClass();
	$obj->id = $new['id'];
	$obj->hostname =  $new['hostname'];
	$obj->time = $new['time'];
	$obj->hits = intval($new['hits']);
	$obj->misses = intval($new['misses']);

	$count = count($data);
	if ($count == 0) {
		$data[] = $obj;
		return json_encode($data);
	} else if ($count >= 12) {
		array_shift($data);
	}

	$data[] = $obj;
	return json_encode($data);
}

function redis_hits_last_month($data) {
	$result = array();
	foreach ($data as $value) {
		$value = json_decode($value);
		$result['hits_last_month_'.$value[0]->id] = $value;
	}
	return $result;
}

function update_hits_last_month($data, $new) {
	$obj = new StdClass();
	$obj->id = $new['id'];
	$obj->hostname =  $new['hostname'];
	$obj->time = $new['time'];
	$obj->hits = intval($new['hits']);
	$obj->misses = intval($new['misses']);

	$count = count($data);
	if ($count == 0) {
		$data[] = $obj;
		return json_encode($data);
	} else if ($count >= 12) {
		array_shift($data);
	}

	$data[] = $obj;
	return json_encode($data);
}

function calculate_code_by_hour($past_hour) {
	global $CONFIG;

	$hour = $past_hour * 3600;

	if (date('H') == '00') {
		$query = "insert into cdn.cdn_code_hourly select now(),hostid,year,month,day,hour,code_200,code_4xx,code_5xx from (select hostid,year,month,day,hour,code_200,code_4xx from (select hostid,year,month,day,hour,count(code) as code_200 from cdn.cdn_access_log_all where code=200 and year=toYear(now()) and month=toMonth(now()) and day=toDayOfMonth(now()-86400) and hour=23 group by hostid,year,month,day,hour) any left join (select hostid,year,month,day,hour,count(code) as code_4xx from cdn.cdn_access_log_all where code>=400 and code<=499 and year=toYear(now()) and month=toMonth(now()) and day=toDayOfMonth(now()-86400) and hour=23 group by hostid,year,month,day,hour) using hostid) any left join (select hostid,year,month,day,hour,count(code) as code_5xx from cdn.cdn_access_log_all where code>=500 and code<=599 and year=toYear(now()) and month=toMonth(now()) and day=toDayOfMonth(now()-86400) and hour=23 group by hostid,year,month,day,hour) using hostid";
	} else {
		$query = "insert into cdn.cdn_code_hourly select now(),hostid,year,month,day,hour,code_200,code_4xx,code_5xx from (select hostid,year,month,day,hour,code_200,code_4xx from (select hostid,year,month,day,hour,count(code) as code_200 from cdn.cdn_access_log_all where code=200 and year=toYear(now()) and month=toMonth(now()) and day=toDayOfMonth(now()) and hour=toHour(now()-{$hour}) group by hostid,year,month,day,hour) any left join (select hostid,year,month,day,hour,count(code) as code_4xx from cdn.cdn_access_log_all where code>=400 and code<=499 and year=toYear(now()) and month=toMonth(now()) and day=toDayOfMonth(now()) and hour=toHour(now()-{$hour}) group by hostid,year,month,day,hour) using hostid) any left join (select hostid,year,month,day,hour,count(code) as code_5xx from cdn.cdn_access_log_all where code>=500 and code<=599 and year=toYear(now()) and month=toMonth(now()) and day=toDayOfMonth(now()) and hour=toHour(now()-{$hour}) group by hostid,year,month,day,hour) using hostid";
	}

	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $CONFIG['datalog_url']."/?user={$CONFIG['datalog']['user']}&password={$CONFIG['datalog']['password']}&query=".urlencode($query));
	//curl_setopt($ch, CURLOPT_ENCODING, "");
	curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-type: text/plain', 'Content-length: 0'));
	curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
	curl_setopt($ch, CURLOPT_TIMEOUT, 20);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, false);
	curl_setopt($ch, CURLOPT_POST, true);
	curl_exec($ch);
	$code1 = curl_getinfo($ch, CURLINFO_HTTP_CODE);

	// replicate
	curl_setopt($ch, CURLOPT_URL, $CONFIG['datalog_url2']."/?user={$CONFIG['datalog']['user']}&password={$CONFIG['datalog']['password']}&query=".urlencode($query));
	curl_exec($ch);
	$code2 = curl_getinfo($ch, CURLINFO_HTTP_CODE);

	return array($code1, $code2);
}

function get_code_last_hour() {
	global $CONFIG;

	if (date('H') == '00') {
		$query = "select * from cdn.view_code_last_hour_0";
	} else {
		$query = "select * from cdn.view_code_last_hour";
	}

	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $CONFIG['datalog_url']."/?user={$CONFIG['datalog']['user']}&password={$CONFIG['datalog']['password']}&query=".urlencode($query));
	curl_setopt($ch, CURLOPT_HEADER, false);
	curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
	curl_setopt($ch, CURLOPT_TIMEOUT, 20);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	$result = curl_exec($ch);
	$httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

	// retry slave
	if ($httpcode != 200) {
		curl_setopt($ch, CURLOPT_URL, $CONFIG['datalog_url2']."/?user={$CONFIG['datalog']['user']}&password={$CONFIG['datalog']['password']}&query=".urlencode($query));
		$result = curl_exec($ch);
	}

	$result = explode("\n", $result);
	$data = array();
	foreach ($result as $row) {
		if ($row == "") continue;
		$tmp = explode("\t", $row);
		$data['code_last_hour_'.$tmp[0]] = $tmp[1].','.$tmp[2].','.$tmp[3];
	}
	return $data;
}

function redis_code_last_hour($data) {
	$result = array();
	foreach ($data as $value) {
		$value = json_decode($value);
		$result['code_last_hour_'.$value[0]->id] = $value;
	}
	return $result;
}

function update_code_last_hour($data, $new) {
	$obj = new StdClass();
	$obj->id = $new['id'];
	$obj->hostname =  $new['hostname'];
	$obj->time = $new['time'];
	$obj->code_200 = intval($new['code_200']);
	$obj->code_4xx = intval($new['code_4xx']);
	$obj->code_5xx = intval($new['code_5xx']);

	$count = count($data);
	if ($count == 0) {
		$data[] = $obj;
		return json_encode($data);
	} else if ($count >= 24) {
		array_shift($data);
	}

	$data[] = $obj;
	return json_encode($data);
}


function redis_code_last_day($data) {
	$result = array();
	foreach ($data as $value) {
		$value = json_decode($value);
		$result['code_last_day_'.$value[0]->id] = $value;
	}
	return $result;
}

function update_code_last_day($data, $new) {
	$obj = new StdClass();
	$obj->id = $new['id'];
	$obj->hostname =  $new['hostname'];
	$obj->time = $new['time'];
	$obj->code_200 = intval($new['code_200']);
	$obj->code_4xx = intval($new['code_4xx']);
	$obj->code_5xx = intval($new['code_5xx']);

	$count = count($data);
	if ($count == 0) {
		$data[] = $obj;
		return json_encode($data);
	} else if ($count >= 14) {
		array_shift($data);
	}

	$data[] = $obj;
	return json_encode($data);
}

function redis_code_last_month($data) {
	$result = array();
	foreach ($data as $value) {
		$value = json_decode($value);
		$result['code_last_month_'.$value[0]->id] = $value;
	}
	return $result;
}

function update_code_last_month($data, $new) {
	$obj = new StdClass();
	$obj->id = $new['id'];
	$obj->hostname =  $new['hostname'];
	$obj->time = $new['time'];
	$obj->code_200 = intval($new['code_200']);
	$obj->code_4xx = intval($new['code_4xx']);
	$obj->code_5xx = intval($new['code_5xx']);

	$count = count($data);
	if ($count == 0) {
		$data[] = $obj;
		return json_encode($data);
	} else if ($count >= 12) {
		array_shift($data);
	}

	$data[] = $obj;
	return json_encode($data);
}

function execute_datalog($query) {
	global $CONFIG;

	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $CONFIG['datalog_url']."/?user={$CONFIG['datalog']['user']}&password={$CONFIG['datalog']['password']}&query=".urlencode($query));
	curl_setopt($ch, CURLOPT_HEADER, false);
	curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
	curl_setopt($ch, CURLOPT_TIMEOUT, 20);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	$result = curl_exec($ch);

	$result = explode("\n", $result);
	$data = array();
	foreach ($result as $row) {
		if ($row == "") continue;
		$tmp = explode("\t", $row);
		$data[] = $tmp;
	}
	return $data;
}
