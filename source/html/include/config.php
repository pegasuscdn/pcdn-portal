<?php

error_reporting(E_ALL & ~E_NOTICE);

define('BASE_PATH', '/var/www/html');
define('TEMPLATE_PATH', BASE_PATH.'/templates/light');

// Development
if ($_SERVER['HTTP_HOST'] === 'portal-dev.pegasustech.io') {
  define('SITE_URL', 'http://portal-dev.pegasustech.io');
  define('CDN_URL', 'http://portal-dev.pegasustech.io');
  define('SITE_NAME', 'CDN Portal Dev');
  define('CDN_DOMAIN', 'pegasus-cdn.com');

  $CONFIG['db'] = array(
        'host' => 'hostdb',
        'user' => 'cdn_portal',
        'pass' => 'JHLL8fcfwPkZDEQ8',
        'dbname' => 'cdn_portal'
  );
  $CONFIG['db_report'] = array(
        'host' => '172.18.0.7',
        'user' => 'cdn_report_dev',
        'pass' => 'FfUeaLyH4DFb58Mn',
        'dbname' => 'cdn_report'
  );
  $CONFIG['redis'] = array(
        'host' => 'hostredis',
        'port' => '6379',
        'db' => 11
  );

// Production
} 
elseif ($_SERVER['HTTP_HOST'] === 'cdn.viettelidc.com.vn') {
  define('SITE_URL', 'http://cdn.viettelidc.com.vn');
  define('CDN_URL', 'http://cdn.viettelidc.com.vn');
  define('SITE_NAME', 'CDN Portal - viettelidc.com.vn');
  define('CDN_DOMAIN', 'pegasus-cdn.com');

  $CONFIG['db'] = array(
        'host' => 'hostdb',
        'user' => 'cdn_portal',
        'pass' => 'JHLL8fcfwPkZDEQ8',
        'dbname' => 'cdn_portal'
  );
  $CONFIG['db_report'] = array(
        'host' => 'hostdb',
        'user' => 'cdn_report',
        'pass' => 'pMjMTtHDZVrTV2Vw',
        'dbname' => 'cdn_report'
  );
  $CONFIG['redis'] = array(
        'host' => 'hostredis',
        'port' => '6379',
        'db' => 11
  );
}
elseif ( ($_SERVER['HTTP_HOST'] === 'portal.cdnservice.vn') ||  ($_SERVER['HTTP_HOST'] === 'cdnservice.pegasustech.io')) {
	$http='http';
	if( $_SERVER['HTTP_X_FORWARDED_PROTO'] ==='https')  $http='https'; 
  define('SITE_URL', $http.'://'.$_SERVER['HTTP_HOST']);
  define('CDN_URL',  $http.'://'.$_SERVER['HTTP_HOST']);
  define('SITE_NAME', 'CDN Portal - cdnservice.vn');
  define('CDN_DOMAIN', 'pegasus-cdn.com'); // CNAME

  $CONFIG['db'] = array(
        'host' => 'hostdb',
        'user' => 'cdn_portal',
        'pass' => 'JHLL8fcfwPkZDEQ8',
        'dbname' => 'cdn_portal'
  );
  $CONFIG['db_report'] = array(
        'host' => 'hostdb',
        'user' => 'cdn_report',
        'pass' => 'pMjMTtHDZVrTV2Vw',
        'dbname' => 'cdn_report'
  );
  $CONFIG['redis'] = array(
        'host' => 'hostredis',
        'port' => '6379',
        'db' => 11
  );
}
else {
  define('SITE_URL', 'https://portal.pegasustech.io');
  define('CDN_URL', 'https://portal.pegasustech.io');
  define('SITE_NAME', 'CDN Portal - PEGASUSTECH.io');
  define('CDN_DOMAIN', 'pegasus-cdn.com');

  $CONFIG['db'] = array(
        'host' => 'hostdb',
        'user' => 'cdn_portal',
        'pass' => 'JHLL8fcfwPkZDEQ8',
        'dbname' => 'cdn_portal'
  );
  $CONFIG['db_report'] = array(
        'host' => 'hostdb',
        'user' => 'cdn_report',
        'pass' => 'pMjMTtHDZVrTV2Vw',
        'dbname' => 'cdn_report'
  );
  $CONFIG['redis'] = array(
        'host' => 'hostredis',
        'port' => '6379',
        'db' => 11
  );
}

define('RECORD_STATUS_DISABLE', 0);
define('RECORD_STATUS_ACTIVE', 11);
define('RECORD_STATUS_INACTIVE', 1);
define('RECORD_STATUS_PENDING_EXEC', 2);
define('RECORD_STATUS_PENDING_DNS', 3);
define('RECORD_STATUS_TRIAL', 4);
define('RECORD_STATUS_TRIAL_EXPIRED', 5);
define('RECORD_LIMIT', 15);

define('MAILGUN_APIKEY', '79e23882b310303900c4ac6f73e6bbf9-816b23ef-aa99f276');
define('KEY_SECRET', "Y7eMrn4naJbe8rZ72a4NREsHBvJJnCuf");//key create token reset pass
define('EMAIL_SENT', "cdnportal11@gmail.com");//email sent info register for user
define('PASSWORD_EMAIL', "PHPMailerAutoload");//password email sent

define('API_SUCCESS', 11);
define('API_ERROR', 40);
define('API_ERROR_UNSUPPORTED', 41);
define('API_ERROR_AUTH', 42);
define('API_ERROR_PARAMETER', 43);
define('API_ERROR_RESOURCE', 44);

$CONFIG['debug'] = true;

$CONFIG['datalog_url'] = 'http://hostdatalog:8123';
$CONFIG['datalog_url2'] = 'http://hostdatalog2:8123';
$CONFIG['datalog'] = array(
	'user' => 'cdn',
	'password' => 'jVh7T026'
);

$CONFIG['cost_by_day'] = 500; // vnd

$CONFIG['start_time'] = 1538326801; // 2018-10-01 00:00:01

// turn on output buffer
ob_start();
session_name('PCDNSESS');
session_start();
