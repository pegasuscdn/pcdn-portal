<?php

require_once 'include/config.php';
require_once 'include/global.php';
require_once 'include/invoice.php';
require_once 'include/Page.php';

check_session();

$active_menu = 'billing';
include TEMPLATE_PATH.'/main_header.php';

$arrStatus=array(1=>"unPaid",11=>"Paid",0=>"Cancel");

$intLimit = 15;
$intPage =  isset($_GET['page']) ? (int)htmlspecialchars($_GET['page']) : 1;
if($intPage==0)
	$intPage=1;
$intStatus =  isset($_GET['status']) ? (int)htmlspecialchars($_GET['status']) : 10;
$arrData = getinvoice($_SESSION['portal']['userid'],$intStatus,$intPage,$intLimit);
$intTotal = gettotalinvoice($_SESSION['portal']['userid'],$intStatus);

$objPaging = new Page();
$objPaging -> TotalResults 		= $intTotal ? $intTotal : 0;
$objPaging -> CurrentPage 		= $intPage;
$objPaging -> ResultsPerPage 	= $intLimit;
$objPaging -> LinksPerPage 		= 5;
$arrPaging = $objPaging -> InfoArray2();

include TEMPLATE_PATH.'/invoice.php';

include TEMPLATE_PATH.'/main_footer.php';
