#!/bin/sh

container='pcdn-portal-redis'
docker run --name $container --hostname $container --network=netcdn --ip=172.18.0.8 --restart always -d registry.pegasus-cdn.com/pcdn-portal-redis:latest
