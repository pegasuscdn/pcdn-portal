#!/bin/sh

container="pcdn-portal"
docker rm -f $container
docker pull registry.pegasus-cdn.com/pcdn-portal:latest
docker run --name $container --hostname $container --network netcdn --ip=172.18.0.6 --add-host="hostdb:172.18.0.7" --add-host="hostredis:172.18.0.8" --add-host="hostdatalog:172.16.10.76" --add-host="hostdatalog2:172.16.10.75" --restart always -d registry.pegasus-cdn.com/pcdn-portal:latest
docker logs -f $container
