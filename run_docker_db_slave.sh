#!/bin/sh

container='pcdn-database-slave'
docker pull registry.pegasus-cdn.com/pcdn-database-slave:latest
docker run --name $container --hostname $container --network=netcdn --ip="172.18.0.7" -p "3306:3306" -e MYSQL_RANDOM_ROOT_PASSWORD=yes -v /cdn/cdn-database/mysql:/var/lib/mysql:rw -v /cdn/cdn-database/log:/var/log/mysql:rw -d registry.pegasus-cdn.com/pcdn-database-slave:latest
