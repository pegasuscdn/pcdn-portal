#!/bin/sh

container='pcdn-database-dev'
docker pull registry.pegasus-cdn.com/pcdn-database:latest
docker run --name $container --hostname $container --network=netcdn --ip="172.18.0.17" -e MYSQL_RANDOM_ROOT_PASSWORD=yes -v /cdn/cdn-database-dev/mysql:/var/lib/mysql:rw -d registry.pegasus-cdn.com/pcdn-database:latest
