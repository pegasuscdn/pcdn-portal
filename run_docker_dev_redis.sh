#!/bin/sh

container='pcdn-portal-redis-dev'
docker run --name $container --hostname $container --network=netcdn --ip=172.18.0.18 --restart always -d registry.pegasus-cdn.com/pcdn-portal-redis:latest
