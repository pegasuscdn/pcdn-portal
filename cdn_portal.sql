-- MySQL dump 10.15  Distrib 10.0.36-MariaDB, for debian-linux-gnu (x86_64)
--
-- Host: 172.18.0.7    Database: cdn_portal
-- ------------------------------------------------------
-- Server version	5.5.61-MariaDB-1~trusty

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `cdn_resource`
--

DROP TABLE IF EXISTS `cdn_resource`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cdn_resource` (
  `id` bigint(20) unsigned NOT NULL,
  `name` varchar(255) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `hostname` varchar(100) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `cdn_cname` varchar(100) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `https` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `type` tinyint(3) unsigned NOT NULL DEFAULT '1',
  `token` varchar(32) DEFAULT NULL,
  `origin` varchar(255) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `userid` int(10) unsigned NOT NULL,
  `user_referer` text CHARACTER SET latin1 COLLATE latin1_general_ci,
  `user_cname` text CHARACTER SET latin1 COLLATE latin1_general_ci,
  `acl` text CHARACTER SET latin1 COLLATE latin1_general_ci,
  `ssl` text CHARACTER SET latin1 COLLATE latin1_general_ci,
  `createdate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `recordstatus` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `hostname` (`hostname`),
  UNIQUE KEY `cdn_cname` (`cdn_cname`),
  UNIQUE KEY `name` (`name`),
  KEY `userid` (`userid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cdn_resource`
--

LOCK TABLES `cdn_resource` WRITE;
/*!40000 ALTER TABLE `cdn_resource` DISABLE KEYS */;
INSERT INTO `cdn_resource` VALUES (15115415510062774316,'ss-media','ss-media.catscdn.vn','1000822946.pegasus-cdn.com',1,1,NULL,'http://video.saostar.vn',1000,'[]',NULL,NULL,'{\"cert\":\"catscdn.crt\",\"key\":\"catscdn.key\"}','2018-10-13 04:46:56',11),(14587280981109154995,'ss-statics-m','ss-statics-m.catscdn.vn','1000533221.pegasus-cdn.com',1,1,NULL,'http://static-m.saostar.vn',1000,NULL,NULL,NULL,'{\"cert\":\"catscdn.crt\",\"key\":\"catscdn.key\"}','2018-10-13 04:47:43',11),(10677500984581616391,'ss-statics','ss-statics.catscdn.vn','1000128313.pegasus-cdn.com',1,1,NULL,'http://static-pc.saostar.vn',1000,NULL,NULL,NULL,'{\"cert\":\"catscdn.crt\",\"key\":\"catscdn.key\"}','2018-10-13 04:50:35',11),(13241967759880956384,'ss-images','ss-images.catscdn.vn','1000799464.pegasus-cdn.com',1,1,NULL,'http://img.saostar.vn',1000,NULL,NULL,NULL,'{\"cert\":\"catscdn.crt\",\"key\":\"catscdn.key\"}','2018-10-13 04:51:16',11),(17549724177856672592,'imgnuoa','imgnuoa.catscdn.vn','1000247141.pegasus-cdn.com',1,1,NULL,'https://imgnuoa.saostar.vn',1000,'[]',NULL,'{\"token\":\"\",\"origin\":\"\"}','{\"cert\":\"catscdn.crt\",\"key\":\"catscdn.key\"}','2018-11-07 11:55:02',11),(14015911089918200838,'Yeah1 Live','yeah1-live-1.uizadev.io','1010305945.pegasus-cdn.com',1,2,NULL,'http://43.239.220.99:2046',1010,NULL,NULL,NULL,'{\"cert\":\"uizadev.crt\",\"key\":\"uizadev.key\"}','2018-11-15 09:34:49',11),(13617988738278596949,'Yeah1 VOD','yeah1-vod.uizadev.io','1010783935.pegasus-cdn.com',1,1,NULL,'http://210.245.20.101',1010,NULL,NULL,NULL,'{\"cert\":\"uizadev.crt\",\"key\":\"uizadev.key\"}','2018-11-15 09:35:36',11),(8657157217956392546,'Live VNDIG','1009277246.pegasus-cdn.com','1009277246.pegasus-cdn.com',1,2,NULL,'http://live.vndig.com',1009,NULL,NULL,'{\"token\":\"b104568a8d163950a8308e511a3a2a8b\",\"origin\":\"\"}','{\"cert\":\"pegasuscdncom.crt\",\"key\":\"pegasuscdncom.key\"}','2018-11-15 09:26:17',11);
/*!40000 ALTER TABLE `cdn_resource` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `edge`
--

DROP TABLE IF EXISTS `edge`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `edge` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `hostname` varchar(100) NOT NULL,
  `ip` varchar(20) NOT NULL,
  `bandwidth` tinyint(3) unsigned NOT NULL,
  `site_id` int(10) unsigned NOT NULL,
  `createdate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `hostname` (`hostname`),
  UNIQUE KEY `ip` (`ip`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `edge`
--

LOCK TABLES `edge` WRITE;
/*!40000 ALTER TABLE `edge` DISABLE KEYS */;
INSERT INTO `edge` VALUES (1,'hni-fpt-edge1','45.118.146.13',10,12,'2018-11-15 16:54:35',1),(2,'hni-fpt-edge2','45.118.146.14',10,12,'2018-11-15 16:54:28',1),(3,'hcm-fpt-edge1','45.119.213.132',10,11,'2018-11-15 16:54:23',1),(4,'hcm-fpt-edge2','45.119.213.133',10,11,'2018-11-16 04:35:05',1);
/*!40000 ALTER TABLE `edge` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `invoice`
--

DROP TABLE IF EXISTS `invoice`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `invoice` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `userid` int(10) unsigned NOT NULL,
  `created_date` datetime NOT NULL,
  `expired_date` datetime NOT NULL,
  `total` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=1002 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `invoice`
--

LOCK TABLES `invoice` WRITE;
/*!40000 ALTER TABLE `invoice` DISABLE KEYS */;
INSERT INTO `invoice` VALUES (1000,2,'2018-10-29 11:28:07','2018-10-29 11:28:07',100000,1),(1001,2,'2018-10-29 11:53:35','2018-10-29 11:53:35',20,1);
/*!40000 ALTER TABLE `invoice` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `invoice_detail`
--

DROP TABLE IF EXISTS `invoice_detail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `invoice_detail` (
  `invoice_id` int(10) unsigned NOT NULL,
  `service_id` int(10) unsigned NOT NULL,
  `amount` int(10) unsigned NOT NULL,
  `description` text NOT NULL,
  PRIMARY KEY (`invoice_id`,`service_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `invoice_detail`
--

LOCK TABLES `invoice_detail` WRITE;
/*!40000 ALTER TABLE `invoice_detail` DISABLE KEYS */;
INSERT INTO `invoice_detail` VALUES (1000,1,500000,'Mua CDN');
/*!40000 ALTER TABLE `invoice_detail` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `portal_admin`
--

DROP TABLE IF EXISTS `portal_admin`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `portal_admin` (
  `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL,
  `password` varchar(32) NOT NULL,
  `hash` varchar(32) NOT NULL,
  `ggtoken` varchar(255) DEFAULT NULL,
  `createdate` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `lastupdate` timestamp NULL DEFAULT NULL,
  `recordstatus` tinyint(3) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `portal_admin`
--

LOCK TABLES `portal_admin` WRITE;
/*!40000 ALTER TABLE `portal_admin` DISABLE KEYS */;
INSERT INTO `portal_admin` VALUES (1,'cnit@gmail.com','158cad3c0c6d868aada6ab7072b7258a','098e6519c961375140f1ea1cbc987e38','DA6XL554Y6R755I3','2018-11-08 04:22:56','2018-11-08 04:22:56',11),(2,'aaa@gmail.com','158cad3c0c6d868aada6ab7072b7258a','098e6519c961375140f1ea1cbc987e38',NULL,'2018-11-15 03:23:18','2018-11-06 03:33:13',11),(3,'cnitluan','a8770934b1f512b0b3d083182e82d7d4','9c2dd68518e584707558a390c8e2ec77',NULL,'2018-11-07 04:49:34','2018-11-07 04:49:34',11),(4,'dungweb','897a21c47d7ea3b39fbc96184f407ae1','etqe8dhhtu3afyj6bsa2atfew56wqhk9','E5ZJHG6QD5VUWW5R','2018-11-09 15:53:50',NULL,11);
/*!40000 ALTER TABLE `portal_admin` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `portal_user`
--

DROP TABLE IF EXISTS `portal_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `portal_user` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(255) NOT NULL,
  `fullname` varchar(255) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `password` varchar(32) NOT NULL,
  `hash` varchar(32) NOT NULL,
  `registerdate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `ggtoken` varchar(255) DEFAULT NULL,
  `ggdata` varchar(255) DEFAULT NULL,
  `lastupdate` datetime DEFAULT NULL,
  `recordstatus` tinyint(3) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`),
  KEY `recordstatus` (`recordstatus`)
) ENGINE=MyISAM AUTO_INCREMENT=1011 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `portal_user`
--

LOCK TABLES `portal_user` WRITE;
/*!40000 ALTER TABLE `portal_user` DISABLE KEYS */;
INSERT INTO `portal_user` VALUES (1,'dungweb@gmail.com','Dung Vo',NULL,NULL,'897a21c47d7ea3b39fbc96184f407ae1','etqe8dhhtu3afyj6bsa2atfew56wqhk9','2018-10-09 08:58:16',NULL,NULL,NULL,11),(2,'cnit@gmail.com','Cnit09','0923123123','123 trÆ°Æ¡ng Ä‘á»‹nh1@``','158cad3c0c6d868aada6ab7072b7258a','098e6519c961375140f1ea1cbc987e38','2018-10-11 04:37:00','','','2018-10-30 11:50:31',11),(1000,'saostar@gmail.com','Cat Tien Sa',NULL,NULL,'897a21c47d7ea3b39fbc96184f407ae1','etqe8dhhtu3afyj6bsa2atfew56wqhk9','2018-10-13 04:58:51','','','2018-10-16 19:42:48',11),(1003,'test@gmail.com','test',NULL,NULL,'ba9ff1824a675cf05b6ff35058a7f244','9254369c5cbb7a7ca0650da8443178e4','2018-10-23 05:51:15',NULL,NULL,NULL,1),(1004,'test1@gmail.com','test1@gmail.com',NULL,NULL,'e31f648ebfd9319442a03247a471bd2d','166a96086fb51c09e1f2f5f433ae259f','2018-10-23 05:53:02',NULL,NULL,NULL,1),(1005,'luan@gmail.com','LuÃ¢n09','098776767609','456 trÆ°Æ¡ng Ä‘á»‹nh09','10e47a250ab1f838dbbc5ff4d5f4e428','c5d5a9bd3de8dedd8febf004302ecad3','2018-11-06 07:59:21',NULL,NULL,'2018-11-06 16:05:13',11),(1006,'trungbqx@gmail.com','trungbq',NULL,NULL,'4cf70cbe3f70a544e23e7acf804aff03','01519e19968226d371f97e7cd832ccf4','2018-11-08 06:47:40',NULL,NULL,NULL,11),(1007,'hoangdm@pegasustech.io','Hoang Nguyen',NULL,NULL,'897a21c47d7ea3b39fbc96184f407ae1','etqe8dhhtu3afyj6bsa2atfew56wqhk9','2018-11-09 15:57:45',NULL,NULL,'2018-11-09 22:57:45',11),(1008,'hungnguyen@viettelidc.com.vn','Hung Nguyen',NULL,NULL,'00e8f5a446fb84af25bd97da963f644e','0b923288d17de669209169c6c92949dd','2018-11-09 15:59:03',NULL,NULL,NULL,11),(1009,'vndig@gmail.com','VN Dig',NULL,NULL,'6e9efe45413f4ef7a57956e5645c2af8','61ac67eaf3a7b59650f73bcdb75fa825','2018-11-15 09:21:42',NULL,NULL,NULL,11),(1010,'uiza@gmail.com','UIZA',NULL,NULL,'ff197b2083f21c6b6f229a8533107328','55f319697e1d884d68831712acf9797e','2018-11-15 09:29:16',NULL,NULL,NULL,11);
/*!40000 ALTER TABLE `portal_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `provider`
--

DROP TABLE IF EXISTS `provider`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `provider` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `description` text,
  `createdate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` tinyint(3) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `provider`
--

LOCK TABLES `provider` WRITE;
/*!40000 ALTER TABLE `provider` DISABLE KEYS */;
INSERT INTO `provider` VALUES (1,'Long Van','CÔNG TY CỔ PHẦN GIẢI PHÁP HỆ THỐNG LONG VÂN\r\n\r\nVP Hồ Chí Minh: Tòa nhà Long Vân, 37/2/6 Đường 12, P. Bình An, Q. 2, TP. Hồ Chí Minh\r\nĐiện thoại: (028) 7303 9168\r\nFax: (028) 3740 4100\r\n\r\nVP Hà Nội: Tòa nhà HTL, số 23, ngách 37/2 phố Dịch Vọng, P. Dịch Vọng, Q. Cầu Giấy, Hà Nội\r\nĐiện thoại: (04) 6282 0238\r\nFax: (04) 6282 0239','2018-11-15 12:10:00',1),(2,'VNDATA','123A Lê Trung Nghĩa, Phường 12, Quận Tân Bình, Tp. Hồ Chí Minh\r\n\r\n0961074444\r\ninfo@vndata.vn','2018-11-16 05:52:32',1);
/*!40000 ALTER TABLE `provider` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `service`
--

DROP TABLE IF EXISTS `service`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `service` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `service`
--

LOCK TABLES `service` WRITE;
/*!40000 ALTER TABLE `service` DISABLE KEYS */;
INSERT INTO `service` VALUES (1,'cdn','cdn service',11),(2,'cdn storage','storage for cdn',11);
/*!40000 ALTER TABLE `service` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `site`
--

DROP TABLE IF EXISTS `site`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `site` (
  `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `bandwidth` tinyint(3) unsigned NOT NULL,
  `network` varchar(100) DEFAULT NULL,
  `provider_id` int(10) unsigned NOT NULL,
  `createdate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` tinyint(4) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `site`
--

LOCK TABLES `site` WRITE;
/*!40000 ALTER TABLE `site` DISABLE KEYS */;
INSERT INTO `site` VALUES (11,'VN_HCM_FPT',30,'45.119.213.128/26',1,'2018-11-16 05:43:16',1),(12,'VN_HNI_FPT',30,'45.118.146.0/26',1,'2018-11-16 05:50:54',1),(13,'VN_HCM_VT',10,'103.104.120.64/26',2,'2018-11-16 05:54:56',1);
/*!40000 ALTER TABLE `site` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `zone`
--

DROP TABLE IF EXISTS `zone`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `zone` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `description` text,
  `createdate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` tinyint(3) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `zone`
--

LOCK TABLES `zone` WRITE;
/*!40000 ALTER TABLE `zone` DISABLE KEYS */;
INSERT INTO `zone` VALUES (1,'Default','Default zone for all customers','2018-11-15 12:04:04',1);
/*!40000 ALTER TABLE `zone` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `zone_edge`
--

DROP TABLE IF EXISTS `zone_edge`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `zone_edge` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `zone_id` int(10) unsigned NOT NULL,
  `edge_id` int(10) unsigned NOT NULL,
  `createdate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `zone_edge`
--

LOCK TABLES `zone_edge` WRITE;
/*!40000 ALTER TABLE `zone_edge` DISABLE KEYS */;
INSERT INTO `zone_edge` VALUES (1,1,1,'2018-11-15 12:22:01'),(2,1,2,'2018-11-15 12:22:05');
/*!40000 ALTER TABLE `zone_edge` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-11-18 15:16:11
